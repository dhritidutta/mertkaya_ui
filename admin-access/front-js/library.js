/*
function library file for the project
put all essential javascript function in this file
author:joyesh@codomotive.com
contributors:
file created:02-11-2016(dd-mm-yyyy)
dependencies:jQuery
*/

//error alert and console.log function substitute
function e_alert(inp)
{
	if(DEBUG==1)
	{
		var text=inp;
	}
	else
	{
		var text="Please reload the page. If the problem persists, try again later.";
	}

	//sweet alert
	swal({
  title: "Oops! An error occurred.",
  text: text,
  type: "error",
  showCancelButton: false,
  allowOutsideClick:false,
  allowEscapeKey:false,
  confirmButtonText: "Reload Page!",
  closeOnConfirm: false,
}).then(function(){
	location.reload();
});

}
function e_console(inp)
{
	if(DEBUG==1)
	{
		console.log(inp);
	}
}

//set work in progress
function wip(id)
{
	if(id===undefined || id=="")
	{
		e_alert("FATAL:wip needs a id.");
	}
	//send a identity string to wip to pinpoint which function is calling wip
	e_console(id);
	window.WIP=1;
	//show a loader screen
	$("#WIP_loader").show();
}

//unset wok in progress
function drop_wip(id)
{
	e_console(id);
	window.WIP=0;
	//hide a loader screen
	$("#WIP_loader").hide();
}

//a fancy alert box shown to user
function ui_alert(message,type)
{
	if(type=="warning")
	{
		var title="Attention!";
	}
	else if(type=="success")
	{
		var title="Success!";
	}
	else if(type=="error")
	{
		var title="Error!";
	}
	else
	{
	var title="";
	var type="";
	}

	return swal({
		title: title,
		text: message,
		type: type,
		allowOutsideClick:false,
		allowEscapeKey:false});

}

function call(url,input_json,callback,oauth,httpMethod,pageAccessToken)
{	/*
	if(window.WIP==1)
	{
		ui_alert("Please wait until the previous request to server gets a reply.",'warning');
	}
	else
  {*/
    if(pageAccessToken==undefined){
      var accessToken=localStorage.getItem('access_token_us');
    }else{
      var accessToken=pageAccessToken;
    }

    if(httpMethod==undefined){
     var methodType="POST";
    }else{
      if(httpMethod==true){
       var methodType="GET"
      }else{
        var methodType="POST"
      }
    }
		if(url===undefined || input_json===undefined || url=="" || input_json=="")
		e_alert("FATAL:call required 2 arguments, url and input_json and 1 optional argument callback");
		else
		{
			$.ajax({
type: methodType,
url: url,
contentType: "application/json",
data: input_json,
dataType:"text",

beforeSend:function(xhr){
          wip("calling ~"+url+"~ from function 'call'.");
          if(oauth==undefined){
						xhr.setRequestHeader( "Authorization", "BEARER " + accessToken );
					}else{
						if(oauth==true){
							xhr.setRequestHeader( "Authorization", "BEARER " + accessToken );
						}else{

						}
					}
				},
success: function(result)
				{

					setTimeout(function(){
						drop_wip("Result arrived for ~"+url+"~ from function 'call->success'.");
					if(!is_valid_JSON(result))
					{
						e_console("WARNING:~"+url+"~ sending non JSON output.");
          }
						callback(result);
					},300);

				},
error: function(xhr){
            if (xhr.readyState == 4) {
              // HTTP error (can be checked by XMLHttpRequest.status and XMLHttpRequest.statusText)
              if(xhr.status == "401")
              {
                localStorage.clear();
                window.location = "login.html?reason=Session expired, please login again.";
              }else{
                ui_alert('Internal Server Error','error').then(function(){
                  drop_wip();
                });
              }
          }
          else if (xhr.readyState == 0) {
             // Network error (i.e. connection refused, access denied due to CORS, etc.)

            if(xhr.status == "401")
              {
                localStorage.clear();
                window.location = "login.html?reason=Session expired, please login again.";
              }else{
                ui_alert('Network Error','error').then(function(){
                drop_wip();
              });
            }
          }
          else {

          drop_wip("error occoured while calling ~"+url+"~ from function 'call->error'");
          if(xhr.status == "401")
          {
            localStorage.clear();
            window.location = "login.html?reason=Session expired, please login again.";
          }
          else
          e_alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
        }
				}
			});
		}
	/*}*/
}

//check if string is a valid json or not
function is_valid_JSON (jsonString){
	try {
		var o = JSON.parse(jsonString);
		if (o && typeof o === "object") {
			return o;
		}
	}
	catch (e) { }

	return false;
};

//check if valid email address
function is_mail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

//function for getting URL data
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

//mysql timestamp to readable timestamp
function readable_timestamp(t)
{
// Split timestamp into [ Y, M, D, h, m, s ]
var t = t.split(/[- :]/);

// Apply each element to the Date function
var d = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));

//handle parsing GMT as UTC
d.setMinutes(d.getMinutes() - 330);

var t=d.toTimeString();
t=t.split(" ");
return d.toDateString()+" "+t[0];
}

//FUNCTION WRITTEN BY RUPAM

//convert time to 12 hours format
function timeTo12HrFormat(time)
{   // Take a time in 24 hour format and format it in 12 hour format
    var time_part_array = time.split(":");
    var ampm = 'AM';

    if (time_part_array[0] >= 12) {
        ampm = 'PM';
    }

    if (time_part_array[0] > 12) {
        time_part_array[0] = time_part_array[0] - 12;
    }

    formatted_time = time_part_array[0] + ':' + time_part_array[1] + ':' + time_part_array[2] + ' ' + ampm;

    return formatted_time;
}

$.fn.serializeObject = function(){
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name] !== undefined) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

//validation for form
    function validation(formId){
        var returnFlag=true;
        $(formId).find('.validate').each(function(){
          // alert($(this).data('validate-type'));
if($(this).data('required')=='required'){

        if($(this).val()==''){

		   $(this).css("borderColor","#E34234");
          var validateType=$(this).data('validate-type');

          console.log(validateType);

		   returnFlag=false;
          // alert("Not SET");
           return returnFlag;
        }else{
             var validateType=$(this).data('validate-type');

			 var value=$(this).val();
          console.log(validateType);
             if(validateType=='phone'){
              console.log('@@@'+value.length);
              if(value.length < 8){
                $(this).css("borderColor","#E34234");
                returnFlag=false;
              }

          }
          if(validateType=='zipcode'){
            console.log('@@'+value);
            if(isNaN(value)){
              $(this).css("borderColor","#E34234");
                returnFlag=false;
            }
          }
            //returnFlag=true;

          // alert("set");
        }

}

/*if($(this).data('validate-type')=='phone'){
		var phoneVal=$(this).val();
		if(isNaN(phoneVal)){
			$(this).css("borderColor","#E34234");
				returnFlag=false;
		}
		if(phoneVal.length < 8){
			$(this).css("borderColor","#E34234");
			returnFlag=false;
		}

}

if($(this).data('validate-type')=='zipcode'){
		var zipVal=$(this).val();
		if(isNaN(zipVal)){
			$(this).css("borderColor","#E34234");
				returnFlag=false;
		}
}*/

       });
       //alert('outside loop');
         return returnFlag;
    }

function addContact(id){
if(id==undefined){

	//var formHtml=' <div class="col-md-6">';
      var  formHtml= '<form class="form-horizontal client_contact_person_1" id="client_contact_person_1" method="post">';
        formHtml+=      '<div class="col-md-6 contact_persons_1">';
         formHtml+=       '<div class="box box-default">';
                      formHtml+='<div class="box-header with-border">';
                        formHtml+='<h3 class="box-title"><b> Contact Person</b></h3>';

                        formHtml+='<div class="box-tools pull-right">';
                          formHtml+='<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>';
                          formHtml+='</button>';
                        formHtml+='</div>';
                        formHtml+='<!-- /.box-tools -->';
                      formHtml+='</div>';
                      formHtml+='<!-- /.box-header -->';
                      formHtml+='<div class="box-body">';
                        formHtml+='<div class="col-sm-12">';
                        formHtml+='<div class="form-group">';
                          formHtml+='<label class="col-sm-4 control-label">Contact Name:</label>';
                          formHtml+='<div class="col-sm-8">';
                            formHtml+='<input type="text" class="form-control validate" id="client_contact_name_1" name="cont_name" data-validate-type="name" data-required="required" placeholder="Person Name">';
                          formHtml+='</div>';
                        formHtml+='</div>';

                        formHtml+='<div class="form-group">';
                          formHtml+='<label class="col-sm-4 control-label">Email ID:</label>';
                          formHtml+='<div class="col-sm-8">';
                            formHtml+='<input type="email" class="form-control validate" id="client_contact_email_1" name="cont_email" data-validate-type="email" placeholder="testmail@mail.com">';
                          formHtml+='</div>';
                        formHtml+='</div>';

                        formHtml+='<div class="form-group">';
                          formHtml+='<label class="col-sm-4 control-label">Phone No:</label>';
                          formHtml+='<div class="col-sm-8">';
                            formHtml+='<input type="tel" class="form-control validate" id="mobile-number2" name="cont_phone" data-validate-type="phone">';
                          formHtml+='</div>';
                        formHtml+='</div>';

                       formHtml+= '<div class="form-group">';
                        formHtml+= ' <label class="col-sm-4 control-label">Skype Name:</label>';
                        formHtml+= ' <div class="col-sm-8">';
                        formHtml+= '   <input type="text" class="form-control validate" id="client_contact_skype_1" name="cont_skype_id" data-validate-type="name" placeholder="Skype Name">';
                       formHtml+= '  </div>';
                       formHtml+= '</div>';

                        formHtml+='<div class="form-group">';
                          formHtml+='<label class="col-sm-4 control-label">Whatsapp no:</label>';
                          formHtml+='<div class="col-sm-8">';
                            formHtml+='<input type="text" class="form-control validate" id="client_contact_whatsapp_1" name="cont_whatsapp_no" data-validate-type="phone" placeholder="9876543210">';
                          formHtml+='</div>';
                        formHtml+='</div>';

                        formHtml+='<div class="form-group">';
                          formHtml+='<label class="col-sm-4 control-label">Available From:</label>';
                          formHtml+='<div class="col-sm-8">';
                            formHtml+='<input type="time" class="form-control validate" id="client_contact_time_1" name="cont_available_from" data-validate-type="time" placeholder="12:00 - 12:00">';
                          formHtml+='</div>';
                        formHtml+='</div>';

                        formHtml+='<div class="form-group">';
                          formHtml+='<label class="col-sm-4 control-label">Available To:</label>';
                          formHtml+='<div class="col-sm-8">';
                            formHtml+='<input type="time" class="form-control validate" id="client_contact_time_1" name="cont_available_to" data-validate-type="time" placeholder="12:00 - 12:00">';
                          formHtml+='</div>';
                        formHtml+='</div>';

                        formHtml+='<div class="row">';
                          formHtml+='<div class="col-sm-12">';
                              formHtml+='<div class="box-footer">';
                                formHtml+='<button type="submit" class="btn btn-default">CANCEL</button>';
                                formHtml+='<button type="submit" class="btn btn-info pull-right">SAVE</button>';
                              formHtml+='</div>';
                            formHtml+='<!-- /.box-footer -->';
                            formHtml+='</div>';
                        formHtml+='</div>';

                        formHtml+='</div>';
                      formHtml+='</div>';
                      formHtml+='<!-- /.box-body -->';
                    formHtml+='</div>';
              formHtml+='</div>';
              formHtml+='</form>';
           // formHtml+='</div>';
				 }else{
					 var  formHtml= '<form class="form-horizontal client_contact_person_update" id="client_contact_person_'+id+'" method="post">';

						 formHtml+=      '<div class="col-md-6 contact_persons_1">';
							formHtml+=       '<div class="box box-default">';
													 formHtml+='<div class="box-header with-border">';
														 formHtml+='<h3 class="box-title"><b> Contact Person</b></h3>';

														 formHtml+='<div class="box-tools pull-right">';
															 formHtml+='<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>';
															 formHtml+='</button>';
														 formHtml+='</div>';
														 formHtml+='<!-- /.box-tools -->';
													 formHtml+='</div>';
													 formHtml+='<!-- /.box-header -->';
													 formHtml+='<div class="box-body">';
														 formHtml+='<div class="col-sm-12">';
														 formHtml+='<div class="form-group">';
															 formHtml+='<label class="col-sm-4 control-label">Contact Name:</label>';
															 formHtml+='<div class="col-sm-8">';
																 formHtml+='<input type="text" class="form-control validate" id="client_contact_name_'+id+'" name="cont_name" data-validate-type="name" data-required="required" placeholder="Person Name">';
															 formHtml+='</div>';
														 formHtml+='</div>';

														 formHtml+='<div class="form-group">';
															 formHtml+='<label class="col-sm-4 control-label">Email ID:</label>';
															 formHtml+='<div class="col-sm-8">';
																 formHtml+='<input type="email" class="form-control validate" id="client_contact_email_'+id+'" name="cont_email" data-validate-type="email" placeholder="testmail@mail.com">';
															 formHtml+='</div>';
														 formHtml+='</div>';

														  formHtml+='<input type="hidden" id="contact_id_'+id+'" name="cont_id" value="">'

														 formHtml+='<div class="form-group">';
															 formHtml+='<label class="col-sm-4 control-label">Phone No:</label>';
															 formHtml+='<div class="col-sm-8">';
																 formHtml+='<input type="tel" class="form-control validate contact_number_'+id+'" id="mobile-number2" name="cont_phone" data-validate-type="phone">';
															 formHtml+='</div>';
														 formHtml+='</div>';

														formHtml+= '<div class="form-group">';
														 formHtml+= ' <label class="col-sm-4 control-label">Skype Name:</label>';
														 formHtml+= ' <div class="col-sm-8">';
														 formHtml+= '   <input type="text" class="form-control validate" id="client_contact_skype_'+id+'" name="cont_skype_id" data-validate-type="name" placeholder="Skype Name">';
														formHtml+= '  </div>';
														formHtml+= '</div>';

														 formHtml+='<div class="form-group">';
															 formHtml+='<label class="col-sm-4 control-label">Whatsapp no:</label>';
															 formHtml+='<div class="col-sm-8">';
																 formHtml+='<input type="text" class="form-control validate" id="client_contact_whatsapp_'+id+'" name="cont_whatsapp_no" data-validate-type="phone" placeholder="9876543210">';
															 formHtml+='</div>';
														 formHtml+='</div>';

														 formHtml+='<div class="form-group">';
															 formHtml+='<label class="col-sm-4 control-label">Available From:</label>';
															 formHtml+='<div class="col-sm-8">';
																 formHtml+='<input type="time" class="form-control validate" id="client_contact_from_'+id+'" name="cont_available_from" data-validate-type="time" placeholder="12:00 - 12:00">';
															 formHtml+='</div>';
														 formHtml+='</div>';

														 formHtml+='<div class="form-group">';
															 formHtml+='<label class="col-sm-4 control-label">Available To:</label>';
															 formHtml+='<div class="col-sm-8">';
																 formHtml+='<input type="time" class="form-control validate" id="client_contact_to_'+id+'" name="cont_available_to" data-validate-type="time" placeholder="12:00 - 12:00">';
															 formHtml+='</div>';
														 formHtml+='</div>';

														 formHtml+='<div class="row">';
															 formHtml+='<div class="col-sm-12">';
																	 formHtml+='<div class="box-footer">';
																		 formHtml+='<button type="submit" class="btn btn-default">CANCEL</button>';
																		 formHtml+='<button type="submit" class="btn btn-info pull-right">SAVE</button>';
																	 formHtml+='</div>';
																 formHtml+='<!-- /.box-footer -->';
																 formHtml+='</div>';
														 formHtml+='</div>';

														 formHtml+='</div>';
													 formHtml+='</div>';
													 formHtml+='<!-- /.box-body -->';
												 formHtml+='</div>';
									 formHtml+='</div>';
									 formHtml+='</form>';
								// formHtml+='</div>';
				 }
return formHtml;
}

function projectLists(project,i){
/*for codo_projects.html*/
var prjName = project.prj_name;
var tasks = parseInt(project.totalTaskCount);
var completedTasks = parseInt(project.completedTaskCount);
var prjId = project.prj_id;
var moduleList = project.moduleList;
var employeeList = project.employeeList;
var clientId=project.prj_client_id;
var project_status="";
var projectStatusBackground='';
var progressBarColor='';
var statusClass='';
var elapsedTime=project.totalTaskElapsedTime;
var estimateTime=project.totalModuleEstimatedTime;
if(project.prj_status==19){
project_status="Created";
projectStatusBackground='bg-grey';
statusClass='box-default';}
if(project.prj_status==20){
project_status="Started";
projectStatusBackground='bg-aqua';
statusClass='box-info';}
if(project.prj_status==21){
project_status="Finished";
projectStatusBackground='bg-green'
statusClass='box-success';}

        var	prjHtml='<div class="box '+statusClass+' '+clientId+' '+project_status+' '+btoa(prjName).replace(/=/g,'_')+'">';
            prjHtml+='<a data-toggle="tab" href="#project1">';
            	prjHtml+='<div class="box-header with-border" data-id="'+prjId+'">';
                	//prjHtml+='<button class="btn btn-primary btn-sm btn-project-list m-r-5"><i class="fa fa-bars"></i></button>';
              		prjHtml+='<h3 class="box-title text-blue project_name">  '+prjName+'  </h3>';
									prjHtml+='<br>';
									prjHtml+='<button class="btn btn-sm btn-primary edit_project pull-right m-l-5" data-id="'+prjId+'" ><i class="fa fa-edit"></i> Edit</button>';
                  prjHtml+='<input type="hidden" id="project_id_'+i+'" value="">';
                    prjHtml+='<button class="btn btn-primary btn-sm pull-right view_project" data-project="'+prjId+'" ><i class="fa fa-eye"></i> Modules</button>';
            	prjHtml+='</div>';
                prjHtml+='<div class="box-body " id="desc_'+i+'">';

	                  prjHtml+='<p class="m-t-0 text-uppercase"> <span class="h4 '+projectStatusBackground+' p-l-10 p-r-10">'+project_status+': '+ parseFloat((completedTasks/tasks)*100).toFixed(2) +'%</span></p>';

                    for(var m =0; m<moduleList.length;m++)
                    {
                      var module= moduleList[m];
                      var Mtasks = parseInt(module.totalTaskCount);
                      var McompletedTasks = parseInt(module.completedTaskCount);
                      var percentage = parseFloat((McompletedTasks/Mtasks)*100).toFixed(2);

                      var status='';
                      if(module["mdl_status"]==14)
                        status="Created";
                      if(module["mdl_status"]==15)
                        status="Started";
                      if(module["mdl_status"]==16)
                        status="Finished";
                      if(module["mdl_status"]==17)
                        status="Error";

                        if(percentage<50){
                          progressBarColor='progress-bar-aqua';
                        }
                        if(percentage>=50){
                          progressBarColor='progress-bar-yellow';
                        }
                         if(percentage==100){
                          progressBarColor='progress-bar-green';
                        }

                      prjHtml+='<div class="clearfix"><div>';
                      prjHtml+='<div class="progress progress-sm codo-progress-bar pull-left" data-module-id="'+module['mdl_id']+'" data-project-id="'+prjId+'" style="width:19%;" data-toggle="popover" title="<b>'+ module["mdl_name"] +'</b>" data-content="<div><b>'+status+'</b> - '+percentage+'%<br>';
                      //prjHtml+='<p>Status: '+status+'</p>';
                      prjHtml+='<p><small>(Completed Task:'+McompletedTasks+' &nbsp;&nbsp;&nbsp; Total Tasks:'+Mtasks+')</small></p></div>">';
                      prjHtml+='<div class="progress-bar '+progressBarColor+' codo-progress" style="width: '+ percentage +'%;" ></div>';
                       prjHtml+='<div class="hidden-xs"><div class="clearfix"></div><small class="module_name_small" data-module-id="'+module['mdl_id']+'">'+module["mdl_name"]+'</small></div>';
                      prjHtml+='</div>';
                    }

                  prjHtml+='<div class="clearfix"></div>';
                  prjHtml+='<div><strong class="text-black">Elapsed Time:</strong>'+elapsedTime+'</div>';
                  prjHtml+='<div><strong class="text-black">Estimate Time:</strong>'+estimateTime+'</div>';
                  prjHtml+='<strong class="m-t-15 m-r-10 pull-left text-black text-uppercase">Work on this project:</strong>';
                  prjHtml+='<ul class="list-inline m-b-0 pull-left icon">';

                  for(var k=0; k<employeeList.length;k++)
                  {
                    var employee = employeeList[k];
                    prjHtml+='<li><img class="img-circle m-t-10 m-b-10" width="30" src="'+WEBSERVICE+'/user/file/'+employee['emp_image']+'" alt="developer" data-toggle="tooltip" data-placement="top" title="'+employee['emp_name']+'"></li>';
                  }
                    prjHtml+='</ul>';
                prjHtml+='</div>';
            prjHtml+='</a>';
            prjHtml+='</div>';

            /*prjHtml+='<div class="box">';
            prjHtml+='<a data-toggle="tab" href="#project2">';
            	prjHtml+='<div class="box-header with-border">';
                	prjHtml+='<button class="btn btn-primary btn-sm " onclick="w3_open()"><i class="fa fa-bars"></i></button>';
              		prjHtml+='<h3 class="box-title">Project 2</h3>';
            	prjHtml+='</div>';
                prjHtml+='<div class="box-body">';
                  prjHtml+='Start creating your amazing application!';
                prjHtml+='</div>';
            prjHtml+='</a>';
            prjHtml+='</div>';

            prjHtml+='<div class="box">';
            prjHtml+='<a data-toggle="tab" href="#project3">';
            	prjHtml+='<div class="box-header with-border">';
                	prjHtml+='<button class="btn btn-primary btn-sm " onclick="w3_open()"><i class="fa fa-bars"></i></button>';
              		prjHtml+='<h3 class="box-title">Project 3</h3>';
            	prjHtml+='</div>';
                prjHtml+='<div class="box-body">';
                  prjHtml+='Start creating your amazing application!';
                prjHtml+='</div>';
            prjHtml+='</a>';
            prjHtml+='</div>';*/
            prjHtml+='</div>';

            	prjHtml+='<!--<div class="box">';
                	prjHtml+='<div id="wrapper" class="active">';

      prjHtml+='<div id="sidebar-wrapper">';
      prjHtml+='<ul id="sidebar_menu" class="sidebar-nav">';
           prjHtml+='<li class="sidebar-brand"><a id="menu-toggle" href="#">Menu<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>';

      prjHtml+='</ul>';
      prjHtml+='</div>';
      prjHtml+='<div id="page-content-wrapper">';
        prjHtml+='<div class="page-content inset">';
          prjHtml+='<div class="row">';
              prjHtml+='<div class="col-md-12">';
              prjHtml+='<p class="well lead">An Experiment using the sidebar template from startbootstrap.com which I integrated in my website (<a href="http://animeshmanglik.name">animeshmanglik.name</a>)</p>';
              prjHtml+='<p class="well lead">Click on the Menu to Toggle Sidebar . Hope you enjoy it!</p>';
              prjHtml+='<p class="well lead">Click on the Menu to Toggle Sidebar . Hope you enjoy it!</p>';
            prjHtml+='</div>';
          prjHtml+='</div>';
        prjHtml+='</div>';
      prjHtml+='</div>';

    prjHtml+='</div>';
                prjHtml+='</div>-->';

      prjHtml+='	<!-- /.box -->';
		//prjHtml+='</div>';
     // prjHtml+='</div>';
   // prjHtml+='</section>';

    return prjHtml;
}

function projectList(prjName,i,prjId,status){

    if(status==14){

      var statusClass='box-default';
    }
    if(status==15){
       var statusClass='box-info';
    }
    if(status==16){
       var statusClass='box-success';
    }

        var	prjHtml='<div class="box '+statusClass+' '+btoa(prjName).replace(/=/g,'_')+'">';
            prjHtml+='<a data-toggle="tab" href="#project1">';
            	prjHtml+='<div class="box-header with-border" data-id="'+prjId+'">';
                	//prjHtml+='<button class="btn btn-primary btn-sm btn-project-list m-r-5"><i class="fa fa-bars"></i></button>';
              		prjHtml+='<h3 class="box-title text-blue">  '+prjName+'  </h3>';
									prjHtml+='<br>';
									prjHtml+='<button class="btn btn-sm btn-primary edit_project pull-right m-l-5" data-id="'+prjId+'" ><i class="fa fa-edit"></i> Edit</button>';
                  prjHtml+='<input type="hidden" id="project_id_'+i+'" value="">';
                    prjHtml+='<button class="btn btn-primary btn-sm pull-right view_project" data-project="'+prjId+'" ><i class="fa fa-eye"></i> Modules</button>';
            	prjHtml+='</div>';
                prjHtml+='<div class="box-body " id="desc_'+i+'">';

	                  prjHtml+='<p class="m-t-0 text-uppercase"><strong>Project Completion:</strong> <span class="h4 bg-yellow p-l-10 p-r-10">Running: 75%</span></p>';

								prjHtml+='<div class="clearfix"></div>';
                    prjHtml+='<div class="progress progress-sm codo-progress-bar pull-left" style="width:19%;" data-toggle="popover" title="<b>Module A</b> - Work 1" data-content="<div><b>Complete Rate</b> - 45%<br>';
                    prjHtml+='<p>Lorem ipsum multiple text..</p>';
                    prjHtml+='<p><small>(DT:2/TT:4)<br>(ELT:45/ST:100)</small></p></div>">';
                    prjHtml+='<div class="progress-bar progress-bar-yellow codo-progress" style="width: 45%;" ></div>';
                  prjHtml+='</div>';

                 prjHtml+='<div class="progress progress-sm codo-progress-bar pull-left" style="width:19%;" data-toggle="popover"';
                      prjHtml+='title="<b>Module B</b> - Work 2"';
                     prjHtml+=' data-content="<div><b>Complete Rate</b> - 100%<br>';
                      prjHtml+='<p>Lorem ipsum multiple text..</p>';
                      prjHtml+='<p><small>(DT:0/TT:4)<br>(ELT:100/ST:100)</small></p></div>">';
                      prjHtml+='<div class="progress-bar progress-bar-green codo-progress" style="width: 100%;" ></div>';
                  prjHtml+='</div>';

                  prjHtml+='<div class="progress progress-sm codo-progress-bar pull-left" style="width:19%;" data-toggle="popover"';
                    prjHtml+='title="<b>Module C</b> - Work 3"';
   					prjHtml+='data-content="<div><b>Complete Rate</b> - 70%<br>';
                    prjHtml+='<p>Lorem ipsum multiple text..</p>';
                    prjHtml+='<p><small>(DT:1/TT:4)<br>(ELT:70/ST:100)</small></p></div>">';
                    prjHtml+='<div class="progress-bar progress-bar-red codo-progress" style="width: 70%;"></div>';
                  prjHtml+='</div>';

                  prjHtml+='<div class="progress progress-sm codo-progress-bar pull-left" style="width:19%;" data-toggle="popover"';
                    prjHtml+='title="<b>Module D</b> - Work 4"';
   					prjHtml+='data-content="<div><b>Complete Rate</b> - 0%<br>';
                    prjHtml+='<p>Not Started</p>';
                    prjHtml+='<p><small>(DT:4/TT:4)<br>(ELT:0/ST:100)</small></p></div>">';
                    prjHtml+='<div class="progress-bar progress-bar-yellow codo-progress" style="width: 0%;" ></div>';
                  prjHtml+='</div>';

                  prjHtml+='<div class="progress progress-sm codo-progress-bar pull-left" style="width:19%;" data-toggle="popover"';
                    prjHtml+='title="<b>Module E</b> - Work 5"';
   					prjHtml+='data-content="<div><b>Complete Rate</b> - 85%<br>';
                    prjHtml+='<p>Lorem ipsum multiple text..</p>';
                    prjHtml+='<p><small>(DT:1/TT:4)<br>(ELT:85/ST:100)</small></p></div>">';
                    prjHtml+='<div class="progress-bar progress-bar-orange codo-progress" style="width: 85%;" ></div>';
                  prjHtml+='</div>';

                  prjHtml+='<div class="clearfix"></div>';

                  prjHtml+='<strong class="m-t-15 m-r-10 pull-left text-uppercase">Work on this project:</strong>';
                  prjHtml+='<ul class="list-inline m-b-0 pull-left icon">';
                    	prjHtml+='<li><img class="img-circle m-t-10 m-b-10" width="36" src="dist/img/user2-160x160.jpg" alt="developer" data-toggle="tooltip" data-placement="top" title="Abhisek Admin"></li>';
                        prjHtml+='<li><img class="img-circle m-t-10 m-b-10" width="36" src="dist/img/avatar5.png" alt="developer" data-toggle="tooltip" data-placement="top" title="Joyeshk"></li>';
                        prjHtml+='<li><img class="img-circle m-t-10 m-b-10" width="36" src="dist/img/user8-128x128.jpg" alt="developer" data-toggle="tooltip" data-placement="top" title="Arijit@arj"></li>';
                        prjHtml+='<li><img class="img-circle m-t-10 m-b-10" width="36" src="dist/img/avatar04.png" alt="developer" data-toggle="tooltip" data-placement="top" title="Sourabh@codo"></li>';
                        prjHtml+='<li><img class="img-circle m-t-10 m-b-10" width="36" src="dist/img/user1-128x128.jpg" alt="developer" data-toggle="tooltip" data-placement="top" title="Rupam_Jana"></li>';
                        prjHtml+='<li><img class="img-circle m-t-10 m-b-10" width="36" src="dist/img/avatar.png" alt="developer" data-toggle="tooltip" data-placement="top" title="Surya@Mohanty"></li>';
                    prjHtml+='</ul>';
                prjHtml+='</div>';
            prjHtml+='</a>';
            prjHtml+='</div>';

            /*prjHtml+='<div class="box">';
            prjHtml+='<a data-toggle="tab" href="#project2">';
            	prjHtml+='<div class="box-header with-border">';
                	prjHtml+='<button class="btn btn-primary btn-sm " onclick="w3_open()"><i class="fa fa-bars"></i></button>';
              		prjHtml+='<h3 class="box-title">Project 2</h3>';
            	prjHtml+='</div>';
                prjHtml+='<div class="box-body">';
                  prjHtml+='Start creating your amazing application!';
                prjHtml+='</div>';
            prjHtml+='</a>';
            prjHtml+='</div>';

            prjHtml+='<div class="box">';
            prjHtml+='<a data-toggle="tab" href="#project3">';
            	prjHtml+='<div class="box-header with-border">';
                	prjHtml+='<button class="btn btn-primary btn-sm " onclick="w3_open()"><i class="fa fa-bars"></i></button>';
              		prjHtml+='<h3 class="box-title">Project 3</h3>';
            	prjHtml+='</div>';
                prjHtml+='<div class="box-body">';
                  prjHtml+='Start creating your amazing application!';
                prjHtml+='</div>';
            prjHtml+='</a>';
            prjHtml+='</div>';*/
            prjHtml+='</div>';

            	prjHtml+='<!--<div class="box">';
                	prjHtml+='<div id="wrapper" class="active">';

      prjHtml+='<div id="sidebar-wrapper">';
      prjHtml+='<ul id="sidebar_menu" class="sidebar-nav">';
           prjHtml+='<li class="sidebar-brand"><a id="menu-toggle" href="#">Menu<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>';

      prjHtml+='</ul>';
      prjHtml+='</div>';
      prjHtml+='<div id="page-content-wrapper">';
        prjHtml+='<div class="page-content inset">';
          prjHtml+='<div class="row">';
              prjHtml+='<div class="col-md-12">';
              prjHtml+='<p class="well lead">An Experiment using the sidebar template from startbootstrap.com which I integrated in my website (<a href="http://animeshmanglik.name">animeshmanglik.name</a>)</p>';
              prjHtml+='<p class="well lead">Click on the Menu to Toggle Sidebar . Hope you enjoy it!</p>';
              prjHtml+='<p class="well lead">Click on the Menu to Toggle Sidebar . Hope you enjoy it!</p>';
            prjHtml+='</div>';
          prjHtml+='</div>';
        prjHtml+='</div>';
      prjHtml+='</div>';

    prjHtml+='</div>';
                prjHtml+='</div>-->';

      prjHtml+='	<!-- /.box -->';
		//prjHtml+='</div>';
     // prjHtml+='</div>';
   // prjHtml+='</section>';

    return prjHtml;
}
function moduleList(id,name,desc,created,enddate,time,status,estimatedTime,employeeList,projectId,projectName){

  if(status=='14'){
    var moduleStatus='<span>Created</span>';
  }
  if(status=='15'){
    var moduleStatus='<span class="text-aqua">Started</span>';
  }
  if(status=='16'){
    var moduleStatus='<span class="text-green">Finished</span>';
  }
  if(estimatedTime==null){
    var mdlEstimatedTime=0;
  }else{
    var mdlEstimatedTime=estimatedTime;
  }

    var prjHtml='';
    for(var k=0; k<employeeList.length;k++)
    {
      var employee = employeeList[k];
      prjHtml+='<li><img class="img-circle m-t-10 m-b-10" width="30" src="'+WEBSERVICE+'/user/file/'+employee['emp_image']+'" alt="developer" data-toggle="tooltip" data-placement="top" title="'+employee['emp_name']+'"></li>';
    }

  var moduleHtml='<li class="well '+btoa(name).replace(/=/g,'_')+'">';
        moduleHtml+='<h4><i class="fa fa-cubes text-black"></i> <a href="javascript:void(0);"><b class="module_name" data-id="'+id+'" data-project-id="'+projectId+'" data-project-name="'+projectName+'">'+name+'</b></a></h4>';
            moduleHtml+='<p><b class="text-black">Description:</b> '+desc+'</p>';
            moduleHtml+='<p><b class="text-black">Estimated Time:</b> '+mdlEstimatedTime+' Hrs</p>';
            moduleHtml+='<p><b class="text-black">Elapsed Time:</b> '+time+' Hrs</p>';
            moduleHtml+='<p><b class="text-black">End Date:</b> '+enddate+'</p>';
            moduleHtml+='<p><b class="text-black">Status :</b> '+moduleStatus+'</p>';
            moduleHtml+='<ul class="list-inline m-b-0 icon"><li><b class="text-black">Working on the module :</b></li>'+prjHtml+'</ul>';
          moduleHtml+='</li>';

      return moduleHtml;
}
function taskList(id,name,desc,created,duedate,time,assignee,status,elapsedTime){

  if(status==8){
      var taskIcon='fa-file-code-o';
      var taskStatusVal='<span>Assigned</span>';
      var taskIconClass='text-black';
  }
  if(status==9){
      var taskIcon='fa-file-code-o';
      var taskStatusVal='<span>Created</span>';
      var taskIconClass='text-black';
  }
  if(status==10){
      var taskIcon='fa-arrow-circle-o-right';
      var taskStatusVal='<span class="text-blue">Started</span>';
      var taskIconClass='text-blue';
  }
  if(status==11){
      var taskIcon='fa-check-square-o';
      var taskStatusVal='<span class="text-green">Finished</span>';
      var taskIconClass='text-green';
  }
  if(status==12){
      var taskIcon='fa-exclamation-triangle';
      var taskStatusVal='<span class="text-red">Error</span>';
       var taskIconClass='text-red';
  }
  if(status==13){
      var taskIcon='fa-file-code-o';
      var taskStatusVal='<span class="text-orange">Modified</span>';
       var taskIconClass='text-orange';
  }

  if(time==null){
    time=0;
  }else{
    time=time;
  }
if(elapsedTime==null){
    elapsedTime=0;
  }else{
    elapsedTime=elapsedTime;
  }
  var moduleHtml='<li class="well '+btoa(name).replace(/=/g,'_')+'">';
        moduleHtml+='<h4><i class="fa '+taskIcon+' '+taskIconClass+'"></i> <a href="javascript:void(0);" class="task_name" data-id="'+id+'"><b>'+name+'</b></a></h4> <button class="btn btn-sm btn-warning re_assign pull-right" id="re_assign_'+id+'" data-id="'+id+'" style="margin-top: -31px;"><i class="fa fa-retweet"></i> Re-assign</button>';
            moduleHtml+='<p><b class="text-black">Description:</b> '+desc+'</p>';
            moduleHtml+='<p><b class="text-black">Estimate Time:</b> '+time+' Hrs</p>';
            moduleHtml+='<p><b class="text-black">Elapsed Time:</b> '+elapsedTime+' Hrs</p>';
            moduleHtml+='<p><b class="text-black">Created :</b> '+readable_timestamp(created)+'</p>';
            moduleHtml+='<p><b class="text-black">Due Date:</b> '+duedate+'</p>';
            moduleHtml+='<p><b class="text-black">Assignee:</b> '+assignee+'</p>';
            moduleHtml+='<p><b class="text-black">Status:</b> '+taskStatusVal+'</p>';
          moduleHtml+='</li>';

      return moduleHtml;
}

function employeeList(id,emp_id,name,desg,email,phone,img,projectList){
  var employeeListHtml='<div class="box box-primary collapsed-box">';
                employeeListHtml+='<div class="box-header with-border">';
                  employeeListHtml+='<div class="row">';
                    employeeListHtml+='<div class="col-md-8 col-sm-7 col-xs-12">';
                      employeeListHtml+='<div class="user-block"> <img class="img-circle" src="'+WEBSERVICE+'/user/file/'+img+'" alt="User Image">';
                      	employeeListHtml+='<span class="username" style="color:rgba(33, 137, 243, 0.8);"> '+name+' <span class="small text-aqua m-l-15">';
                        		employeeListHtml+='<span class="flagstrap-icon flagstrap-in" alt="INDIA" /></span> INDIA</span></span>';
                        employeeListHtml+='<span class="description">';
                        	employeeListHtml+='<ul class="list-inline">';
                            	employeeListHtml+='<li>Employee ID: '+id+'</li>';
                              employeeListHtml+='<li>User ID: '+emp_id+'</li>';
                                employeeListHtml+='<li>|</li>';
                            	employeeListHtml+='<li>'+desg+'</li>';
                                employeeListHtml+='<li>|</li>';
                                employeeListHtml+='<li><a href="#">'+email+'</a></li>';
                                employeeListHtml+='<li>|</li>';
                                employeeListHtml+='<li><i class="fa fa-phone"></i> +91 '+phone+'</li>';
                            employeeListHtml+='</ul>';
                        employeeListHtml+='</span>';
                      employeeListHtml+='</div>';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<div class="col-md-4 col-sm-5 col-xs-12">';
                      employeeListHtml+='<ul class="list-inline pull-right">';
                        //employeeListHtml+='<li><input id="input-1" type="checkbox" data-group-cls="btn-group-sm" checked disabled></li>';
                        employeeListHtml+='<li><a href="codo_employee_edit.html" class="btn btn-info btn-sm edit_emp" data-emp-id="'+id+'"><i class="fa fa-edit"></i></a></li>';
                        //employeeListHtml+='<li><button type="button" class="btn bg-red btn-sm"><i class="fa fa-trash"></i></button></li>';
                        employeeListHtml+='<li><input type="hidden" class="employee_id" value="'+id+'"</li>';
                        employeeListHtml+='<li><button type="button" class="btn btn-box-tool expand" data-widget="collapse"><i class="fa fa-plus"></i></button></li>';
                      employeeListHtml+='</ul>';
                      employeeListHtml+='<div class="box-tools pull-right"> </div>';
                    employeeListHtml+='</div>';
                  employeeListHtml+='</div>';

                  employeeListHtml+='<div class="row">';
                  	employeeListHtml+='<div class="col-sm-12"><hr class="m-t-10 m-b-10">';
                    	employeeListHtml+='<h4 class="m-t-0">Working Projects:</h4>';

                      if(projectList.length>0)
                      {
                        for(var p=0;p<projectList.length;p++)
                        {
                          var project=projectList[p];
                          employeeListHtml+='<a class="btn btn-flat btn-sm bg-aqua m-r-5" href="codo_projects.html?project_id='+project["prj_id"]+'">'+project['prj_name']+'</a>';
                        }
                      }
                      else
                      {
                        employeeListHtml+="<span>None</span>";
                      }

                    employeeListHtml+='</div>';
                  employeeListHtml+='</div>';
                  employeeListHtml+='<!-- /.box-tools -->';
                employeeListHtml+='</div>';
                employeeListHtml+='<!-- /.box-header -->';
                employeeListHtml+='<div class="box-body" style="display: none;">';

                  employeeListHtml+='<div class="col-md-6">';
                   employeeListHtml+='<div class="box box-default collapsed-box">';
                    employeeListHtml+='<div class="box-header with-border">';
                      employeeListHtml+='<h3 class="box-title text-uppercase"><i class="fa fa-user text-blue"></i> Employee Info</h3>';

                      employeeListHtml+='<div class="box-tools pull-right">';
                      employeeListHtml+='<input type="hidden" class="employee_id" value="'+id+'">';
                        employeeListHtml+='<button type="button" class="btn btn-box-tool info_expand" data-widget="collapse"><i class="fa fa-plus"></i>';
                        employeeListHtml+='</button>';
                      employeeListHtml+='</div>';
                      employeeListHtml+='<!-- /.box-tools -->';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<!-- /.box-header -->';
                    employeeListHtml+='<div class="box-body form-horizontal">';
                      	 employeeListHtml+='<div class="row emp_name_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Employee Name:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 emp_name_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 emp_name"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row emp_type_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Employee Type:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 emp_type_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 emp_type"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row emp_id_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Employee ID:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 emp_id_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 emp_id"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row emp_email_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Email:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 emp_email_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 emp_email"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row emp_phone_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Phone No.:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 emp_phone_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 emp_phone">+91 </p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row emp_desg_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Designation:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 emp_desg_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 emp_desg"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row emp_dept_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Department:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 emp_dept_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 emp_dept"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row emp_skills_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Skills:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 emp_skills_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 emp_skills"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<!-- /.box-body -->';
                  employeeListHtml+='</div>';
                 employeeListHtml+='</div>';

                 employeeListHtml+='<div class="col-md-6">';
                   employeeListHtml+='<div class="box box-default collapsed-box">';
                    employeeListHtml+='<div class="box-header with-border">';
                      employeeListHtml+='<h3 class="box-title text-uppercase"><i class="fa fa-file-zip-o text-blue"></i> Documents from company</h3>';

                      employeeListHtml+='<div class="box-tools pull-right">';
                          employeeListHtml+='<input type="hidden" class="employee_id" value="'+id+'">';
                        employeeListHtml+='<button type="button" class="btn btn-box-tool doc_expand" data-widget="collapse"><i class="fa fa-plus"></i>';
                        employeeListHtml+='</button>';
                      employeeListHtml+='</div>';
                      employeeListHtml+='<!-- /.box-tools -->';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<!-- /.box-header -->';
                    employeeListHtml+='<div class="box-body form-horizontal">';
                      	employeeListHtml+='<div class="row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Offer Letter:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7">';
                                employeeListHtml+='<p class="m-t-7">';
                                		employeeListHtml+='<a href="javascript:void" class="btn btn-default btn-sm m-t--5" download=""> <!--Jonathan-Burke-Jr_offer_letter.pdf-->';
                                        employeeListHtml+='<i class="fa fa-download"></i> </a>';
                                employeeListHtml+='</p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Joining Letter:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7">';
                                employeeListHtml+='<p class="m-t-7">';
                                		employeeListHtml+='<a href="javascript:void" class="btn btn-default btn-sm m-t--5" download=""> <!--Jonathan-Burke-Jr_joining_letter.pdf-->';
                                        employeeListHtml+='<i class="fa fa-download"></i> </a>';
                                employeeListHtml+='</p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Payslip:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7">';
                                employeeListHtml+='<p class="m-t-7">';
                                		employeeListHtml+='<a href="javascript:void" class="btn btn-default btn-sm m-t--5" download=""> <!--Jonathan-Burke-Jr_payslip.pdf-->';
                                        employeeListHtml+='<i class="fa fa-download"></i> </a>';
                                employeeListHtml+='</p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Insurance:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7">';
                                employeeListHtml+='<p class="m-t-7 m-b-0"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<!-- /.box-body -->';
                  employeeListHtml+='</div>';
                 employeeListHtml+='</div>';
                 employeeListHtml+='<div class="clearfix"></div>';
                 employeeListHtml+='<div class="col-md-6">';
                   employeeListHtml+='<div class="box box-default collapsed-box">';
                    employeeListHtml+='<div class="box-header with-border">';
                      employeeListHtml+='<h3 class="box-title text-uppercase"><i class="fa fa-file-text text-blue"></i> Personal details</h3>';

                      employeeListHtml+='<div class="box-tools pull-right">';
                      employeeListHtml+='<input type="hidden" class="employee_id" value="'+id+'">';
                        employeeListHtml+='<button type="button" class="btn btn-box-tool per_expand" data-widget="collapse"><i class="fa fa-plus"></i>';
                        employeeListHtml+='</button>';
                      employeeListHtml+='</div>';
                      employeeListHtml+='<!-- /.box-tools -->';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<!-- /.box-header -->';
                    employeeListHtml+='<div class="box-body form-horizontal">';
                         employeeListHtml+='<div class="row address_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Present Address:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 address_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 address"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row email_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Personal Email:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 email_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 email"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row alt_phone_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Alternate Ph no:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 alt_phone_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 alt_phone"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row hq_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Highest Qualification:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 hq_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 hq"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row co_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">C/O Person:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 co_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 co"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row co_add_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">C/O Address:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 co_add_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 co_add"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row co_relation_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Relation with C/O:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 co_relation_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 co_relation"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row co_phone_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">C/O Person Phone no:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 co_phone_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 co_phone"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<!-- /.box-body -->';
                  employeeListHtml+='</div>';
                 employeeListHtml+='</div>';

                 employeeListHtml+='<div class="col-md-6">';
                   employeeListHtml+='<div class="box box-default collapsed-box">';
                    employeeListHtml+='<div class="box-header with-border">';
                      employeeListHtml+='<h3 class="box-title text-uppercase"><i class="fa fa-credit-card text-blue"></i> Employee bank deatils</h3>';

                      employeeListHtml+='<div class="box-tools pull-right">';
                      employeeListHtml+='<input type="hidden" class="employee_id" value="'+id+'">';
                        employeeListHtml+='<button type="button" class="btn btn-box-tool bank_expand" data-widget="collapse"><i class="fa fa-plus"></i>';
                        employeeListHtml+='</button>';
                      employeeListHtml+='</div>';
                      employeeListHtml+='<!-- /.box-tools -->';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<!-- /.box-header -->';
                    employeeListHtml+='<div class="box-body form-horizontal">';
                      	 employeeListHtml+='<div class="row bank_name_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Bank Name:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 bank_name_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 bank_name"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row acc_name_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Account Name:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 acc_name_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 acc_name"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row acc_no_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">Account Number:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 acc_no_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 acc_no"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row ifsc_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">IFSC Code:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 ifsc_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 ifsc"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                         employeeListHtml+='<div class="row pan_row">';
                              employeeListHtml+='<label class="col-sm-4 col-xs-5 control-label">PAN No:</label>';
                              employeeListHtml+='<div class="col-sm-8 col-xs-7 pan_div">';
                                employeeListHtml+='<p class="m-t-7 m-b-0 pan"></p>';
                              employeeListHtml+='</div>';
                         employeeListHtml+='</div>';
                    employeeListHtml+='</div>';
                    employeeListHtml+='<!-- /.box-body -->';
                  employeeListHtml+='</div>';
                 employeeListHtml+='</div>';

                   employeeListHtml+='</div><!--end box body-->';
                employeeListHtml+='</div><!---end client one box-->';

          employeeListHtml+='</div>';

  return employeeListHtml;
}

function clientList(id,name,country,phone,freelancer_id,address,no_of_contact,payment_method){
  var flag=country.toLowerCase();
  var clientListHtml='';
  clientListHtml+='<div class="box box-primary collapsed-box">';
                clientListHtml+='<div class="box-header with-border">';
                  clientListHtml+='<div class="row">';
                    clientListHtml+='<div class="col-md-8 col-sm-7 col-xs-12">';
                      clientListHtml+='<div class="user-block"> <img class="img-circle" src="dist/img/user1-128x128.jpg" alt="User Image">';
                      	clientListHtml+='<span class="username" style="color:rgba(33, 137, 243, 0.8);"> '+name+' <span class="small text-aqua m-l-15">';
                        		clientListHtml+='<span class="flagstrap-icon flagstrap-'+flag+'" alt="ISRAEL" /></span> '+country+'</span></span>';
                        clientListHtml+='<span class="description">';
                        	clientListHtml+='<ul class="list-inline">';
                            	clientListHtml+='<li> Freelancer Id: '+freelancer_id+'</li>';

                                clientListHtml+='<li>|</li>';
                                clientListHtml+='<li><i class="fa fa-phone"></i> '+phone+'</li>';
                            clientListHtml+='</ul>';
                        clientListHtml+='</span>';
                      clientListHtml+='</div>';
                    clientListHtml+='</div>';
                    clientListHtml+='<div class="col-md-4 col-sm-5 col-xs-12">';
                      clientListHtml+='<ul class="list-inline pull-right">';
                        /*clientListHtml+='<li>';
                            clientListHtml+='<a href="#" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Projects 11">';
                            	clientListHtml+='<span class="badge bg-blue"><i class="fa fa-code"></i> 11</span>';
                            clientListHtml+='</a>';
                        clientListHtml+='</li>';
                        clientListHtml+='<li>';
                        	clientListHtml+='<a href="#" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Completed 8">';
                        		clientListHtml+='<span class="badge bg-green"><i class="fa fa-check"></i> 8</span>';
                        	clientListHtml+='</a>';
                        clientListHtml+='</li>';
                        clientListHtml+='<li>';
                        	clientListHtml+='<a href="#"><i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star-half-o"></i>';
                            clientListHtml+='</a>';
                        clientListHtml+='</li>';*/
                        clientListHtml+='<li>';
                         clientListHtml+='<a href="codo_clients_edit.html" class="btn btn-info btn-sm edit_client" data-client-id="'+id+'"><i class="fa fa-edit"></i></a>';
                          clientListHtml+='</li>';

                        clientListHtml+='<li>';
                        clientListHtml+='<input type="hidden" class="client_id" value="'+id+'">';
                        clientListHtml+='<input type="hidden" class="no_of_contact" value="'+no_of_contact+'">';
                          clientListHtml+='<button type="button" class="btn btn-box-tool client_expand" data-widget="collapse"><i class="fa fa-plus"></i></button>';
                        clientListHtml+='</li>';
                      clientListHtml+='</ul>';
                      clientListHtml+='<div class="box-tools pull-right"> </div>';
                    clientListHtml+='</div>';
                  clientListHtml+='</div>';

                  clientListHtml+='<!-- /.box-tools -->';
                clientListHtml+='</div>';
                clientListHtml+='<!-- /.box-header -->';
                clientListHtml+='<div class="box-body p-t-0" style="display: none;">';
                   clientListHtml+='<div class="box-header with-border">';
                       clientListHtml+='<h4 class="m-t-0">';
                        clientListHtml+='<div class="pull-left">'+name+'</div>';
                       clientListHtml+='</h4>';
                   clientListHtml+='</div>';

                   clientListHtml+='<div class="col-xs-12 form-horizontal">';
                   	 clientListHtml+='<div class="row">';
                     	clientListHtml+='<div class="col-sm-12">';
                        	clientListHtml+='<h4>Running Projects:</h4>';
                        	clientListHtml+='<a class="btn btn-flat btn-sm bg-orange" href="codo_single_project.html">Blockchain</a>';
                            clientListHtml+='<a class="btn btn-flat btn-sm bg-aqua" href="codo_single_project.html">Unidefend</a>';
                            clientListHtml+='<a class="btn btn-flat btn-sm bg-green" href="codo_single_project.html">Restaurant Management</a>';
                        clientListHtml+='</div>';
                     clientListHtml+='</div><!--end row-->';

                     	 clientListHtml+='<div class="row">';
                     	clientListHtml+='<hr>';
                       clientListHtml+='<div class="col-md-6">';
                       	 clientListHtml+='<p class="lead m-b-0"> <span class="label bg-blue">Client Info :</span></p>';
                         clientListHtml+='<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label" id="label_name_'+id+'">Company Name:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0">'+name+'</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>';
                         clientListHtml+='<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Client Type:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0" id="type_'+id+'">Company</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>';
                         clientListHtml+='<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Country:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0"><span class="flag flag-il" alt="ISRAEL" /></span> '+country+'</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>';
                         clientListHtml+='<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Address:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0">'+address+'</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>';
                         //clientListHtml+='<div class="row">';
                           //   clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Email:</label>';
                             // clientListHtml+='<div class="col-sm-8 col-xs-7">';
                               // clientListHtml+='<p class="m-t-7 m-b-0"><a href="#">mailtest@mail.com</a></p>';
                              //clientListHtml+='</div>';
                         //clientListHtml+='</div>';
                         clientListHtml+='<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Phone:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0">'+phone+'</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>';
                         clientListHtml+='<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Freelancer ID:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0">'+freelancer_id+'</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>';
                         clientListHtml+='<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">No. of Contact Person:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0">'+no_of_contact+'</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>';
                         clientListHtml+='<div class="clearfix"></div>';
                         clientListHtml+='<p class="lead m-b-0 m-t-10"> <span class="label bg-blue">Payment Info :</span></p>';
                         clientListHtml+='<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Payment Method:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0">'+payment_method+'</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>';
                         clientListHtml+='<!--<div class="row">';
                              clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Payment Type:</label>';
                              clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                clientListHtml+='<p class="m-t-7 m-b-0">Paypal</p>';
                              clientListHtml+='</div>';
                         clientListHtml+='</div>-->';

                       clientListHtml+='</div>';

                       clientListHtml+='<div class="col-md-6">';
                       		clientListHtml+='<p class="lead m-b-0"> <span class="label bg-blue">Client Rating :</span></p>';
                       	 	clientListHtml+='<div class="row">';
                            	  clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Client Rating:</label>';
                                  clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                    clientListHtml+='<p class="m-t-7 m-b-0">';
                                    	clientListHtml+='<a href="#"><i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star-half-o"></i>';
                            			clientListHtml+='</a>';
                                    clientListHtml+='</p>';
                                  clientListHtml+='</div>';
                            clientListHtml+='</div>';
                            clientListHtml+='<div class="row">';
                            	  clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Payment:</label>';
                                  clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                    clientListHtml+='<p class="m-t-7 m-b-0">';
                                    	clientListHtml+='<a href="#"><i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i>';
                            			clientListHtml+='</a>';
                                    clientListHtml+='</p>';
                                  clientListHtml+='</div>';
                            clientListHtml+='</div>';
                            clientListHtml+='<div class="row">';
                            	  clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Flexibility:</label>';
                                  clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                    clientListHtml+='<p class="m-t-7 m-b-0">';
                                    	clientListHtml+='<a href="#"><i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star-half-o"></i>';
                            			clientListHtml+='</a>';
                                    clientListHtml+='</p>';
                                  clientListHtml+='</div>';
                            clientListHtml+='</div>';
                            clientListHtml+='<div class="row">';
                            	  clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Easy To work:</label>';
                                  clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                    clientListHtml+='<p class="m-t-7 m-b-0">';
                                    	clientListHtml+='<a href="#"><i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star-o"></i>';
                            			clientListHtml+='</a>';
                                    clientListHtml+='</p>';
                                  clientListHtml+='</div>';
                            clientListHtml+='</div>';
                            clientListHtml+='<div class="row">';
                            	  clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Clarity Of requirement:</label>';
                                  clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                    clientListHtml+='<p class="m-t-7 m-b-0">';
                                    	clientListHtml+='<a href="#"><i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star-half-o"></i> <i class="fa text-yellow fa-star-o"></i>';
                            			clientListHtml+='</a>';
                                    clientListHtml+='</p>';
                                  clientListHtml+='</div>';
                            clientListHtml+='</div>';
                            clientListHtml+='<div class="row">';
                            	  clientListHtml+='<label class="col-sm-4 col-xs-5 control-label">Availablity:</label>';
                                  clientListHtml+='<div class="col-sm-8 col-xs-7">';
                                    clientListHtml+='<p class="m-t-7 m-b-0">';
                                    	clientListHtml+='<a href="#"><i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star"></i> <i class="fa text-yellow fa-star-o"></i>';
                            			clientListHtml+='</a>';
                                    clientListHtml+='</p>';
                                  clientListHtml+='</div>';
                            clientListHtml+='</div>';
                       clientListHtml+='</div>';
                       clientListHtml+='<div class="clearfix"></div>';
                       clientListHtml+='<hr>';

                       clientListHtml+='<div id="client_contact_'+id+'" class="client_contact"></div>'

                   clientListHtml+='</div><!--end box body-->';
                clientListHtml+='</div><!---end client one box-->';

            return clientListHtml;
}

function contactUi(contId,contName,contPhone,contSkypeId,contEmail,contWhatsapp,contAvailTo,contAvailFrom){
  var availTo=timeTo12HrFormat(contAvailTo);
  var availFrom=timeTo12HrFormat(contAvailFrom);
  var contactHtml='';
  contactHtml+='<div class="col-md-6">';
                           contactHtml+='<div class="box box-default collapsed-box">';
                           		contactHtml+='<div class="box-header with-border">';
                                  contactHtml+='<h3 class="box-title">'+contName+'</h3> <a href="#">'+contEmail+'</a>';
                                  contactHtml+='<div class="box-tools pull-right">';
                contactHtml+='<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>';
                contactHtml+='</button>';
              contactHtml+='</div>';
                                contactHtml+='</div>';
                                contactHtml+='<div class="box-body">';
                                	contactHtml+='<ul class="list-unstyled">';
                                    	contactHtml+='<li><p><b>E-mail:</b> <a href="#">'+contEmail+'</a></p></li>';
                                        //contactHtml+='<li><p><b>Country:</b> <span class="flag flag-vn" alt="Viet Nam "></span> Viet Nam </p></li>';
                                        contactHtml+='<li><p><b>Phone No:</b> '+contPhone+'</p></li>';
                                        contactHtml+='<li><p><b>Skype Name:</b> '+contSkypeId+'</p></li>';
                                        contactHtml+='<li><p><b>Whatsapp No:</b> '+contWhatsapp+'</p></li>';
                                        contactHtml+='<li><p><b>Available Time:</b> '+availFrom+' - '+availTo+'</p></li>';
                                    contactHtml+='</ul>';
                                contactHtml+='</div>';
                           contactHtml+='</div>';
                       contactHtml+='</div>';

                       return contactHtml;
}

function assignmentList(assgnId,projectName,moduleName,taskName,startDate,endDate,status,elapsedTime,taskDesc,projectId,moduleId){
  if(status==4){ //start task
    var taskStatus='Task Started';
    var resumeClass='btn  btn-md pull-right m-r-5 assgn_play bg-gray';
    var pauseClass='btn  btn-md pull-right m-r-5 assgn_pause bg-orange';
    var stopClass='btn  btn-md pull-right m-r-5 assgn_stop bg-red';
  }else if(status==5){ //pause task
     var taskStatus='Task Paused';
    var resumeClass='btn  btn-md pull-right m-r-5 assgn_play bg-green';
    var pauseClass='btn  btn-md pull-right m-r-5 assgn_pause bg-gray';
    var stopClass='btn  btn-md pull-right m-r-5 assgn_stop bg-red';
  }else if(status==6){ //stop task
    var taskStatus='Task Stopped';
    var resumeClass='btn  btn-md pull-right m-r-5 assgn_play';
    var pauseClass='btn  btn-md pull-right m-r-5 assgn_pause';
    var stopClass='btn  btn-md pull-right m-r-5 assgn_stop bg-gray';
  }else if(status==3){ //assigned task
    var taskStatus='Task Assigned';
    var resumeClass='btn  btn-md pull-right m-r-5 assgn_play bg-green';
    var pauseClass='btn  btn-md pull-right m-r-5 assgn_pause bg-gray';
    var stopClass='btn  btn-md pull-right m-r-5 assgn_stop bg-red';
  }

  var assignHtml='';
  assignHtml+=' <div class="box m-b-15 item '+btoa(projectName).replace(/=/g,'_')+' '+btoa(moduleName).replace(/=/g,'_')+'" id="item_'+assgnId+'" style="padding: 10px;">';
                assignHtml+='<div class="row">';
                  	assignHtml+='<div class="col-sm-12">';
                    	assignHtml+='<h4 class="m-t-0 text-blue">'+taskName+'</h4>';

                    assignHtml+='</div>';

										assignHtml+='<div class="col-sm-12">';
										if(status==4)
										assignHtml+='<div class="btn bg-yellow"><i class="fa fa-clock-o pull-left p-r-5" style="padding-top:3px;"></i><div class="current_task_elaspsed_time pull-left" style="cursor:default;"></div></div>';
										else
										assignHtml+='<div  class="btn bg-yellow" style="cursor:default;">Time:'+elapsedTime+'</div>';
											assignHtml+='<input type="hidden" class="elapsed_time" value="'+elapsedTime+'"> '
											assignHtml+='<input type="hidden" class="assgn_id" value="'+assgnId+'">';
											assignHtml+='<button class="'+stopClass+'" onclick=""><i class="fa fa-stop"></i> STOP</button>';
											assignHtml+='<button class="'+pauseClass+'" onclick=""><i class="fa fa-pause"></i> PAUSE</button>';
											assignHtml+='<button class="'+resumeClass+'" onclick=""><i class="fa fa-play"></i> START</button>';
										assignHtml+='</div>';

                  	assignHtml+='<div class="col-sm-12 m-t-15">';
							assignHtml+='<ul class="list-inline m-b-0">';
                            	assignHtml+='<li class="elapsed_time_li"><b class="text-black">Task Status:</b> <span class="label bg-gray font-13" id="assgn_status_'+assgnId+'">'+taskStatus+'</span></li>';
                                assignHtml+='<li>|</li>';
                            	//assignHtml+='<li><b>Start Date:</b> <span>'+startDate+'</span></li>';
                                //assignHtml+='<li>|</li>';
                                assignHtml+='<li class="class_due_date"><b class="text-black">Due Date:</b> <span class="span_due_date">'+endDate+'</span></li>';
                                assignHtml+='<li class="class_calender_btn"><a href="#" class="add_to_calender" data-task_description="'+btoa(taskDesc)+'" data-task_name="'+btoa(taskName)+'" data-project_name="'+btoa(projectName)+'" data-module_name="'+btoa(moduleName)+'" data-module_id="'+moduleId+'" data-project_id="'+projectId+'" data-task_id="'+assgnId+'">Add to Calender</a></li>';
                            assignHtml+='</ul>';
                    assignHtml+='</div>';

											assignHtml+='<div class="col-sm-12 m-t-15">';
														assignHtml+='<div class="task_prj_row">';
															assignHtml+='<p><b class="text-black"> Project Name: </b><span class="task_prj font-14"><a href="codo_projects.html?project_id='+projectId+'" target="_self">'+projectName+'</a></span></p>';
														assignHtml+='</div>';
														assignHtml+='<div class="task_mdl_row">';
															assignHtml+='<p><b class="text-black"> Module Name: </b><span class="task_mdl font-14"><a href="codo_project_module.html?module_id='+moduleId+'&project_id='+projectId+'&project_name='+projectName+'" target="_self">'+moduleName+'</a></span></p>';
														assignHtml+='</div>';
														assignHtml+='<div class="task_desc_row">';
																assignHtml+='<p><b class="text-black"> Details: </b><span class="task_desc">'+taskDesc+'</span></p>';
                            assignHtml+='</div>';
                            assignHtml+='<div class="task_end_date_row">';
																assignHtml+='<p><b class="text-black"> End Date: </b><span class="task_end_date">'+endDate+'</span></p>';
														assignHtml+='</div>';
											assignHtml+='</div>';

                  assignHtml+='</div>';
              assignHtml+='</div>';
             assignHtml+=' <!-- /.item -->';

            //  assignHtml+='<hr style="margin:5px 0;" class="item '+projectName.replace(/ /g,'_')+' '+moduleName.replace(/ /g,'_')+'">';

              return assignHtml;
}

function countDown(){

  if(window.second==59){
    window.minute=window.minute+01;
    window.second=00;
    if(window.minute==59){
      window.hour=window.hour+01;
      window.minute=00;
    }
  }else{
    window.second=window.second+01
  }

  $('.current_task_elaspsed_time').html(window.hour+' Hrs: '+window.minute+' Min: '+window.second+' Sec');
  //console.log(window.hour+' Hrs: '+window.minute+' Min: '+window.second+' Sec');

}

function todoList(id,descShort,desc,status){
	if(status==22){
		var spanClass='text';
		var spanCss='';
		var checked=''
	}else{
		var spanClass='text text-success';
		var spanCss='text-decoration:line-through';
		var checked='checked';
	}

	var todoListHtml='';
	todoListHtml+='<li id="todo_li_'+id+'">';
				todoListHtml+='<span class="handle">';
					todoListHtml+='<i class="fa fa-ellipsis-v"></i>';
					todoListHtml+='<i class="fa fa-ellipsis-v"></i>';
				todoListHtml+='</span>';
		todoListHtml+='<input type="checkbox" class="todo_checkbox" id="todo_checkbox_'+id+'" data-id="'+id+'" value="" '+checked+'>';
		todoListHtml+='<span class="'+spanClass+'" style="'+spanCss+'" id="desc_span_'+id+'">'+descShort+'</span>';
		todoListHtml+='<div class="tools" style="display:block;">';
			todoListHtml+='<i class="fa fa-edit edit_todo" id="edit_todo_'+id+'" data-id="'+id+'"></i>';
			todoListHtml+='<i class="fa fa-trash-o delete_todo" id="delete_todo_'+id+'" data-id="'+id+'"></i>';
		todoListHtml+='</div>';
		todoListHtml+='<textarea class="form-control" id="todo_desc_'+id+'" style="display:none;">'+desc+'</textarea>';
		todoListHtml+='<button class="btn btn-sm btn-success add_new_todo  m-t-7" id="save_todo_btn_'+id+'" data-id="'+id+'" style="display:none;">Save</button>';
	todoListHtml+='</li>';

	return todoListHtml;
}

/*function todoListEdit(id){
	var todoEditHtml='';
	todoEditHtml+='<li id="todo_edit_'+id+'">';
				todoEditHtml+='<span class="handle">';
					todoEditHtml+='<i class="fa fa-ellipsis-v"></i>';
					todoEditHtml+='<i class="fa fa-ellipsis-v"></i>';
				todoEditHtml+='</span>';
		todoEditHtml+='<input type="checkbox" id="todo_chekbox_'+id+'" value="">';
		todoEditHtml+='<div class="tools" style="display:block;">';
			todoEditHtml+='<i class="glyphicon glyphicon-ok save_todo" data-id="'+id+'"></i>';
			todoEditHtml+='<i class="fa fa-trash-o delete_todo" data-id="'+id+'"></i>';
		todoEditHtml+='</div>';

	todoEditHtml+='</li>';

	return todoEditHtml;
}*/
//login heartbeat function
function loginHeartBeat(){
    var heartBeatUserId={};
    heartBeatUserId['user_id']=localStorage.getItem('user_id_us');
    var jsonString=JSON.stringify(heartBeatUserId);
      console.log('Login heart beat calling');
      console.log(jsonString);
      call(WEBSERVICE+'/user/SetUserHeartBeat',jsonString,function(data){

    });
}

function callList(id,name,email,phone,country,status,desc){
  var statusVal='';
  var statusClass='';
  if(status==28){
      statusVal='ANSWERING';
      statusClass='text-white pull-right label-info p-5';
  }
  if(status==29){
      statusVal='BUSY';
      statusClass='text-white pull-right label-warning p-5';
  }
  if(status==30){
      statusVal='CALL BACK';
      statusClass='text-white pull-right label-primary p-5';
  }
  if(status==31){
      statusVal='DISCONNECTED NUMBER';
      statusClass='text-white pull-right label-danger p-5';
  }
  if(status==32){
      statusVal='DECLINED CALL';
      statusClass='text-white pull-right label-danger p-5';
  }
  if(status==33){
      statusVal='DO NOT CALL';
      statusClass='text-white pull-right label-danger p-5';
  }
  if(status==34){
      statusVal='NO ANSWER';
      statusClass='text-white pull-right label-primary p-5';
  }
  if(status==35){
      statusVal='NOT INTERESTED';
      statusClass='text-white pull-right label-default p-5';
  }
  if(status==36){
      statusVal='HUNG UP';
      statusClass='text-white pull-right label-info p-5';
  }
  if(status==37){
      statusVal='SALE MADE';
      statusClass='text-white pull-right label-success p-5';
  }
  if(status==38){
      statusVal='CALL TRANSFERRED';
      statusClass='text-white pull-right label-info p-5';
  }
  var callListHtml="";
  callListHtml+=' <div class="box box-primary collapsed-box calling_list" data-id="'+id+'" data-name="'+name+'" data-email="'+email+'" data-phone="'+phone+'" data-country="'+country+'" data-status="'+statusVal+'" data-desc="'+desc+'">';
            callListHtml+='<div class="box-header with-border">';
                callListHtml+='<div class="row">';
                    callListHtml+='<div class="col-md-8 col-sm-7 col-xs-12">';
                        callListHtml+='<div class="user-block">';
                            callListHtml+='<ul class="list-inline">';
                              callListHtml+='<li class="text-aqua">'+name+'</li>';
                              callListHtml+='<li class="small text-aqua m-l-15">'+email+'</li>';
                              callListHtml+='<li class="description"></li>';
                            callListHtml+='</ul>';
                                callListHtml+='<ul class="list-inline">';
                                    callListHtml+='<li>'+phone+'</li>';
                                    callListHtml+='<li>|</li>';
                                    callListHtml+='<li>'+country+'</li>';
                                callListHtml+='</ul>';
                            callListHtml+='</span>';
                        callListHtml+='</div>';
                    callListHtml+='</div>';
                    callListHtml+='<div class="col-md-4 col-sm-7 col-xs-12">';
                          callListHtml+='<span class="'+statusClass+'">'+statusVal+'</span>';
                    callListHtml+='</div>';
                callListHtml+='</div>';
           callListHtml+=' </div>';
        callListHtml+='</div>';

        return callListHtml;
}

function getAllCallingLeads(userId,timestamp){
  var callLeadObj={};
  callLeadObj['call_user_id']=userId;
  callLeadObj['page_no']=1;
  callLeadObj['no_of_items_per_page']=10;
  if(timestamp!=undefined){
    callLeadObj['call_start_time']=timestamp;
  }
   var jsonString=JSON.stringify(callLeadObj);
    console.log(jsonString);

    call(WEBSERVICE+'/lead/GetAllCallingLeads',jsonString,function(data){
        var response=JSON.parse(data);

        if(response.error.error_data=='1'){
              ui_alert(response.error.error_msg,'error');
        }else{
            console.log(response);
            var leadResultLength=response.leadResult.length;
            if(leadResultLength!=null){
                for(var i=0; i<response.leadResult.length;i++){
                  var id= response.leadResult[i].leadCalls.call_id;
                  var name= response.leadResult[i].registrant_name;
                  var email= response.leadResult[i].registrant_email;
                  var phone= response.leadResult[i].registrant_phone;
                  var country= response.leadResult[i].registrant_country;
                  var status= response.leadResult[i].leadCalls.call_status;
                  var desc= response.leadResult[i].leadCalls.call_remarks;
                  //$('#calling_list').html('');
                  $('#calling_list').append(callList(id,name,email,phone,country,status,desc));

                }
            }
        }
    });
}
function notificationList(id,message,status,contentType){

    if(contentType==1){
      var textClass='text-aqua';
      var icon='fa-globe';
    }
    if(contentType==2){
      var textClass='text-yellow';
      var icon='fa-users';
    }

    if(contentType==4){
      var textClass='text-blue';
      var icon='fa-code';
    }
    if(contentType==5){
      var textClass='text-green';
      var icon='fa-file-text';
    }
    if(contentType==6){
      var textClass='text-aqua';
      var icon='fa-money';
    }
    if(contentType==7){
      var textClass='text-black';
      var icon='fa-desktop';
    }
    if(contentType==8){
      var textClass='text-red';
      var icon='fa-square';
    }
    if(contentType==9){
      var textClass='text-yellow';
      var icon='fa-thumb-tack';
    }
    if(contentType==10){
      var textClass='text-aqua';
      var icon='fa-clock-o';
    }
    if(contentType==11){
      var textClass='text-blue';
      var icon='fa-phone';
    }

    if(status!=25){
      var boldTextClass='text-blue text-bold font-13';
      var markRead='<i title="Mark As Read" class="fa fa-check-circle-o m-r-20"></i>';
    }else{
      var boldTextClass='font-13';
      var markRead=''
    }

    var notificatonHtml='';
    notificatonHtml+='<li class="notification_li m-l-10 m-t-5  '+boldTextClass+'" data-id="'+id+'" id="notification_'+id+'"><i class="fa '+icon+' '+textClass+'"></i> '+message+' </a> <span class="mark_read text-orange pull-right" style="cursor:pointer;" data-id="'+id+'">'+markRead+'</span> </li>';
    return notificatonHtml;
}

function listOfAllUser(response){
  var allUserResult=response.usersListResult;
  var listOfAllUserHtml='';
  for(var i=0;i<allUserResult.length;i++){

    if(allUserResult[i].user_docs_status==''){
      var userDocStatus='<span>No document</span>';
    }
    if(allUserResult[i].user_docs_status=='0'){
      var userDocStatus='<span style="color:#2196f3;"><i class="fa fa-file-text"></i> Document Submitted</span>';
    }
    if(allUserResult[i].user_docs_status=='1'){
      var userDocStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Approved</span>';
    }
    if(allUserResult[i].user_docs_status=='2'){
      var userDocStatus='<span style="color:#f44336"><i class="fa fa-times"></i> Disapproved</span>';
    }

    if(allUserResult[i].bank_details_status==''){
      var userBankDocStatus='<span>Not Submitted</span>';
    }
    if(allUserResult[i].bank_details_status=='0'){
      var userBankDocStatus='<span style="color:#2196f3;"><i class="fa fa-file-text"></i> Details Submitted</span>';
    }
    if(allUserResult[i].bank_details_status=='1'){
      var userBankDocStatus='<span style="color:#2196f3;"><i class="fa fa-money"></i> Micro Amount Sent</span>';
    }
    if(allUserResult[i].bank_details_status=='2'){
      var userBankDocStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Verified</span>';
    }
    if(allUserResult[i].bank_details_status=='3'){
      var userBankDocStatus='<span style="color:#f44336"><i class="fa fa-times"></i> Declined</span>';
    }
    if(allUserResult[i].buy_limit=='0'){
      var buyStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Buy Not Restricted</span>';
    }else{
      var buyStatus='<span style="color:#F39F1F;"><i class="fa fa-exclamation-triangle"></i> Buy Restricted</span>';
    }
    if(allUserResult[i].sell_limit=='0'){
      var sellStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Sell Not Restricted</span>';
    }else{
      var sellStatus='<span style="color:#F39F1F;"><i class="fa fa-exclamation-triangle"></i> Sell Restricted</span>';
    }
    listOfAllUserHtml+='<tr>';
        //listOfAllUserHtml+='<td data-title="">'+i+'</td>';
        listOfAllUserHtml+='<td data-title="User Id">'+allUserResult[i].user_id+'</td>';
        listOfAllUserHtml+='<td data-title="Name">'+allUserResult[i].full_name+'</td>';
       // listOfAllUserHtml+='<td data-title="Email">'+allUserResult[i].email+'</td>';
        //listOfAllUserHtml+='<td data-title="Phone">'+allUserResult[i].phone+'</td>';
        listOfAllUserHtml+='<td data-title="Status">'+readable_timestamp(allUserResult[i].created)+'</td>';
        listOfAllUserHtml+='<td data-title="DocDetails">'+userDocStatus+'</td>';
        listOfAllUserHtml+='<td data-title="BankDetails">'+userBankDocStatus+'</td>';
        listOfAllUserHtml+='<td data-title="BuyStatus">'+buyStatus+'</td>';
        listOfAllUserHtml+='<td data-title="SellStatus">'+sellStatus+'</td>';
        listOfAllUserHtml+='<td data-title="" class="text-center" width="5px">';
            listOfAllUserHtml+='<a class="btn btn-sm btn-primary view_details" href="user_details.html?user_id='+allUserResult[i].user_id+'&name='+allUserResult[i].full_name+'&status='+allUserResult[i].status+'&email='+allUserResult[i].email+'&phone='+allUserResult[i].phone+'&address='+allUserResult[i].address+'&ssn='+allUserResult[i].ssn+'&city='+allUserResult[i].city+'&state='+allUserResult[i].state+'&zip='+allUserResult[i].zip+'&country='+allUserResult[i].country+'"><i class="fa fa-eye"></i> VIEW DETAILS</a>';
        listOfAllUserHtml+='</td>';
    listOfAllUserHtml+='</tr>';

  }
  return listOfAllUserHtml;
}

function batchOperationList(response){
  var Batchlist=response.transactionValidationList;
  var listOfBatchOperationhtml='';
  for(var i=0;i<Batchlist.length;i++){
   var BatchId = Batchlist[i].id;
   var BatchName = Batchlist[i].customerName;
   var BatchTimeStamp = Batchlist[i].createdTimestamp;
   var BatchCustomerId = Batchlist[i].customerId;
   var BatchToAdd =  Batchlist[i].toAdd;
   var BatchCurrency = Batchlist[i].currency;
   var BatchAmount = Batchlist[i].amount;
   var BatchDescription = Batchlist[i].description;
   var Action = Batchlist[i].action;
   var BatchCurrencyId= Batchlist[i].currencyId;

     listOfBatchOperationhtml+='<tr>';
       //listOfAllUserHtml+='<td data-title="">'+i+'</td>';
       listOfBatchOperationhtml+='<td data-title="Transaction Id">'+Batchlist[i].transactionId+'</td>';
       listOfBatchOperationhtml+='<td data-title="Name">'+Batchlist[i].customerName+'</td>';
      //  listOfBatchOperationhtml+='<td data-title="Customer Id">'+Batchlist[i].customerId+'</td>';
       listOfBatchOperationhtml+='<td data-title="To add">'+Batchlist[i].toAdd+'</td>';
       listOfBatchOperationhtml+='<td data-title="Currency">'+Batchlist[i].currency+'</td>';
       listOfBatchOperationhtml+='<td data-title="Amount">'+Batchlist[i].amount+'</td>';
       listOfBatchOperationhtml+='<td data-title="TimeStamp">'+Batchlist[i].createdTimestamp+'</td>';
       listOfBatchOperationhtml+='<td data-title=""><input type="button" class="btn cus-btn btn-success btndsble'+i+'" onclick="batchOperationAccept(\''+BatchId+','+BatchName+','+BatchTimeStamp+','+BatchCustomerId+','+BatchDescription+','+Action+','+BatchAmount+','+BatchCurrencyId+','+BatchCurrency+','+BatchToAdd+','+i+'\');" value="APPROVE"><input type="button" class="btn cus-btn btn-danger btndecline'+i+'" onclick="batchOperationDecline(\''+BatchId+','+BatchName+','+BatchTimeStamp+','+BatchCustomerId+','+BatchDescription+','+Action+','+BatchAmount+','+BatchCurrencyId+','+BatchCurrency+','+BatchToAdd+','+i+'\');" value="DECLINE"></td>';
 }
 return listOfBatchOperationhtml;
 }

 function batchOperationAccept(params){
   var fields = params.split(',');
   var batchOperaionData = {};
   batchOperaionData['id'] = fields[0];
   batchOperaionData['customerName']=fields[1];
   batchOperaionData['transactionTimestamp']=fields[2];
   batchOperaionData['customerId'] = fields[3];
   batchOperaionData['description'] = fields[4];
   batchOperaionData['action'] = 'approve';
   batchOperaionData['amount'] = fields[6];
   batchOperaionData['currencyId']=fields[7]
   batchOperaionData['currency'] = fields[8];
   batchOperaionData['toAdd'] = fields[9];

   var jsonString=JSON.stringify(batchOperaionData);
    call(WEBSERVICE+'/transactions/ReviewSendRequest',jsonString,function(data){
      var result=JSON.parse(data);
     if (result.error.error_data == '0'){
       var classv='.btndsble'+fields[10];
       var classd='.btndecline'+fields[10];
       $(classv).addClass("btn-disbl").css({"pointer-events": "none", "opacity": "0.5"});
       $(classd).addClass("btn-disbl").css({"pointer-events": "none", "opacity": "0.5"});
       ui_alert('Customer Approved','success');
     }
     else{
       ui_alert(result.error.error_msg,'error');
       //this.data.alert(result.error.error_msg, 'danger');
     }

 }
   )};

   function batchOperationDecline(params){
     var fields = params.split(',');
     var batchOperaionData = {};
   batchOperaionData['id'] = fields[0];
   batchOperaionData['customerName']=fields[1];
   batchOperaionData['transactionTimestamp']=fields[2];
   batchOperaionData['customerId'] = fields[3];
   batchOperaionData['description'] = fields[4];
   batchOperaionData['action'] = 'decline';
   batchOperaionData['amount'] = fields[6];
   batchOperaionData['currencyId']=fields[7];
   batchOperaionData['currency'] = fields[8];
   batchOperaionData['toAdd'] = fields[9];

     var jsonString=JSON.stringify(batchOperaionData);
      call(WEBSERVICE+'/transactions/ReviewSendRequest',jsonString,function(data){
       var result=JSON.parse(data);
       if (result.error.error_data == '0'){
         var classD='.btndecline'+fields[10];
         var classA='.btndsble'+fields[10];
         $(classD).addClass("btn-disbl").css({"pointer-events": "none", "opacity": "0.5"});
         $(classA).addClass("btn-disbl").css({"pointer-events": "none", "opacity": "0.5"});
         ui_alert('send transaction declined','success');
       }
       else{
         ui_alert(result.error.error_msg, 'danger');
       }
   }
     )};

function listOfAllConfirmedUser(response,pageAuthToken){
  var allUserResult=response.usersListResult;
  var listOfAllUserHtml='';
  for(var i=0;i<allUserResult.length;i++){

    if(allUserResult[i].user_docs_status==''){
      var userDocStatus='<span>No document</span>';
    }
    if(allUserResult[i].user_docs_status=='0'){
      var userDocStatus='<span style="color:#2196f3;"><i class="fa fa-file-text"></i> Document Submitted</span>';
    }
    if(allUserResult[i].user_docs_status=='1'){
      var userDocStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Approved</span>';
    }
    if(allUserResult[i].user_docs_status=='2'){
      var userDocStatus='<span style="color:#f44336"><i class="fa fa-times"></i> Disapproved</span>';
    }

    if(allUserResult[i].bank_details_status==''){
      var userBankDocStatus='<span>Not Submitted</span>';
    }
    if(allUserResult[i].bank_details_status=='0'){
      var userBankDocStatus='<span style="color:#2196f3;"><i class="fa fa-file-text"></i> Details Submitted</span>';
    }
    if(allUserResult[i].bank_details_status=='1'){
      var userBankDocStatus='<span style="color:#2196f3;"><i class="fa fa-money"></i> Micro Amount Sent</span>';
    }
    if(allUserResult[i].bank_details_status=='2'){
      var userBankDocStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Verified</span>';
    }
    if(allUserResult[i].bank_details_status=='3'){
      var userBankDocStatus='<span style="color:#f44336"><i class="fa fa-times"></i> Declined</span>';
    }
    if(allUserResult[i].buy_limit=='0'){
      var buyStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Buy Not Restricted</span>';
    }else{
      var buyStatus='<span style="color:#F39F1F;"><i class="fa fa-exclamation-triangle"></i> Buy Restricted</span>';
    }
    if(allUserResult[i].sell_limit=='0'){
      var sellStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Sell Not Restricted</span>';
    }else{
      var sellStatus='<span style="color:#F39F1F;"><i class="fa fa-exclamation-triangle"></i> Sell Restricted</span>';
    }
    listOfAllUserHtml+='<tr>';
        //listOfAllUserHtml+='<td data-title="">'+i+'</td>';
        listOfAllUserHtml+='<td data-title="User Id">'+allUserResult[i].user_id+'</td>';
        listOfAllUserHtml+='<td data-title="Name">'+allUserResult[i].full_name+'</td>';
       // listOfAllUserHtml+='<td data-title="Email">'+allUserResult[i].email+'</td>';
        //listOfAllUserHtml+='<td data-title="Phone">'+allUserResult[i].phone+'</td>';
        listOfAllUserHtml+='<td data-title="Status">'+readable_timestamp(allUserResult[i].created)+'</td>';
        listOfAllUserHtml+='<td data-title="DocDetails">'+userDocStatus+'</td>';
        listOfAllUserHtml+='<td data-title="BankDetails">'+userBankDocStatus+'</td>';
        listOfAllUserHtml+='<td data-title="BuyStatus">'+buyStatus+'</td>';
        listOfAllUserHtml+='<td data-title="SellStatus">'+sellStatus+'</td>';
        listOfAllUserHtml+='<td data-title="" class="text-center" width="5px">';
          if(isMethodExist('confirm_view_user')==true){
            listOfAllUserHtml+='<a class="btn btn-sm btn-primary view_details" href="user_details.html?user_id='+allUserResult[i].user_id+'&name='+allUserResult[i].full_name+'&status='+allUserResult[i].status+'&email='+allUserResult[i].email+'&phone='+allUserResult[i].phone+'&address='+allUserResult[i].address+'&ssn='+allUserResult[i].ssn+'&city='+allUserResult[i].city+'&state='+allUserResult[i].state+'&zip='+allUserResult[i].zip+'&country='+allUserResult[i].country+'&skq='+pageAuthToken+'&for=confirm"><i class="fa fa-eye"></i> VIEW DETAILS</a>';
          }else{
            listOfAllUserHtml+='<i class="fa fa-eye"></i> VIEW DETAILS';
          }
        listOfAllUserHtml+='</td>';
    listOfAllUserHtml+='</tr>';

  }
  return listOfAllUserHtml;
}

function listOfAllUnconfirmedUser(response,pageAuthToken){
  var allUserResult=response.usersListResult;
  var listOfAllUserHtml='';
  for(var i=0;i<allUserResult.length;i++){

    if(allUserResult[i].user_docs_status==''){
      var userDocStatus='<span>No document</span>';
    }
    if(allUserResult[i].user_docs_status=='0'){
      var userDocStatus='<span style="color:#2196f3;"><i class="fa fa-file-text"></i> Document Submitted</span>';
    }
    if(allUserResult[i].user_docs_status=='1'){
      var userDocStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Approved</span>';
    }
    if(allUserResult[i].user_docs_status=='2'){
      var userDocStatus='<span style="color:#f44336"><i class="fa fa-times"></i> Disapproved</span>';
    }

    if(allUserResult[i].bank_details_status==''){
      var userBankDocStatus='<span>Not Submitted</span>';
    }
    if(allUserResult[i].bank_details_status=='0'){
      var userBankDocStatus='<span style="color:#2196f3;"><i class="fa fa-file-text"></i> Details Submitted</span>';
    }
    if(allUserResult[i].bank_details_status=='1'){
      var userBankDocStatus='<span style="color:#2196f3;"><i class="fa fa-money"></i> Micro Amount Sent</span>';
    }
    if(allUserResult[i].bank_details_status=='2'){
      var userBankDocStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Verified</span>';
    }
    if(allUserResult[i].bank_details_status=='3'){
      var userBankDocStatus='<span style="color:#f44336"><i class="fa fa-times"></i> Declined</span>';
    }
    if(allUserResult[i].buy_limit=='0'){
      var buyStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Buy Not Restricted</span>';
    }else{
      var buyStatus='<span style="color:#F39F1F;"><i class="fa fa-exclamation-triangle"></i> Buy Restricted</span>';
    }
    if(allUserResult[i].sell_limit=='0'){
      var sellStatus='<span style="color:#4caf50;"><i class="fa fa-check"></i> Sell Not Restricted</span>';
    }else{
      var sellStatus='<span style="color:#F39F1F;"><i class="fa fa-exclamation-triangle"></i> Sell Restricted</span>';
    }
    listOfAllUserHtml+='<tr>';
        //listOfAllUserHtml+='<td data-title="">'+i+'</td>';
        listOfAllUserHtml+='<td data-title="User Id">'+allUserResult[i].user_id+'</td>';
        listOfAllUserHtml+='<td data-title="Name">'+allUserResult[i].full_name+'</td>';
       // listOfAllUserHtml+='<td data-title="Email">'+allUserResult[i].email+'</td>';
        //listOfAllUserHtml+='<td data-title="Phone">'+allUserResult[i].phone+'</td>';
        listOfAllUserHtml+='<td data-title="Status">'+readable_timestamp(allUserResult[i].created)+'</td>';
        listOfAllUserHtml+='<td data-title="DocDetails">'+userDocStatus+'</td>';
        listOfAllUserHtml+='<td data-title="BankDetails">'+userBankDocStatus+'</td>';
        listOfAllUserHtml+='<td data-title="BuyStatus">'+buyStatus+'</td>';
        listOfAllUserHtml+='<td data-title="SellStatus">'+sellStatus+'</td>';
        listOfAllUserHtml+='<td data-title="" class="text-center" width="5px">';
        if(isMethodExist('unconfirm_view_user')==true){
          listOfAllUserHtml+='<a class="btn btn-sm btn-primary view_details" href="user_details.html?user_id='+allUserResult[i].user_id+'&name='+allUserResult[i].full_name+'&status='+allUserResult[i].status+'&email='+allUserResult[i].email+'&phone='+allUserResult[i].phone+'&address='+allUserResult[i].address+'&ssn='+allUserResult[i].ssn+'&city='+allUserResult[i].city+'&state='+allUserResult[i].state+'&zip='+allUserResult[i].zip+'&country='+allUserResult[i].country+'&skq='+pageAuthToken+'&for=unconfirm"><i class="fa fa-eye"></i> VIEW DETAILS</a>';
        }else{
          listOfAllUserHtml+='<i class="fa fa-eye"></i> VIEW DETAILS';
        }
        listOfAllUserHtml+='</td>';
    listOfAllUserHtml+='</tr>';

  }
  return listOfAllUserHtml;
}

function loginDuration(){

	var tokenEndTime =localStorage.getItem('expires_in_us');
	var curentTimestamp=$.now();

	if(curentTimestamp>tokenEndTime){
    window.location='login.html?reason=Session expired, please login again.';
    localStorage.clear();
	}
	if(curentTimestamp==tokenEndTime){

			var username=localStorage.getItem('email_us');

			//$('#loginModal').modal('show');

			//$('#email').val(username);

      clearInterval(window.checkOauth);

      localStorage.clear();
      window.location="login.html?reason=Session expired, please login again.";

	}
	if(curentTimestamp<tokenEndTime){
    var modalOpenTime=tokenEndTime-curentTimestamp;
    //alert('Token End Time: '+tokenEndTime);
    //alert('Current Time: '+curentTimestamp);
    //alert('Modal open time: '+modalOpenTime);
		/*window.checkOauth=setInterval(function(){
			var username=localStorage.getItem('admin_email_us');

			$('#loginModal').modal('show');
			$('#email').val(username);

			clearInterval(window.checkOauth);
		}, modalOpenTime);*/
		modalOpenTime=modalOpenTime+5000;
		setInterval(function(){
			var username=localStorage.getItem('email_us');
      console.log('modalOpenTime: '+modalOpenTime);
      var presentRefreshToken=localStorage.getItem('refresh_token_us');
      var fd = new FormData();
      fd.append('refresh_token',presentRefreshToken);
      fd.append('grant_type','refresh_token');
      $.ajax({
          url: WEBSERVICE+'/oauth/token',
          method:'POST',
          processData: false,
          contentType: false,
          data: fd,

          beforeSend: function(xhr) {
            xhr.setRequestHeader("authorization", "Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0");
          },

          success: function(data) {
            console.log(data);
            localStorage.setItem('access_token_us',data.access_token);
            localStorage.setItem('refresh_token_us',data.refresh_token);
            var expiresTime=data.expires_in;
            expiresTime=expiresTime*1000;
            var start_time=$.now();
            var expiresIn=start_time+expiresTime;
            localStorage.setItem('expires_in_us',expiresIn);

          },
          error:function(res)
            {
              if (xhr.readyState == 4) {
                    ui_alert('Internal Server Error','error').then(function(){
                      drop_wip();
                    });
                }
                else if (xhr.readyState == 0) {
                    ui_alert('Network Error','error').then(function(){
                      drop_wip();
                    });
                }
                else {

                drop_wip("error occoured while calling ~"+url+"~ from function 'call->error'");
                if(xhr.status == "401")
                {
                  localStorage.clear();
                  window.location = "login.html?reason=Session expired, please login again.";
                }
                else
                e_alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
              }

            }
        });

    //localStorage.clear();
    //window.location="login.html?reason=Session expired, please login again.";
		}, modalOpenTime);
	}
}

function loginModal(username){
	//alert('SSSS');
	 loginModalHtml='<div class="modal hide" id="loginModal">';
           loginModalHtml+='<div class="modal-header">';
             loginModalHtml+='<button type="button" class="close" data-dismiss="modal">x</button>';
             loginModalHtml+='<h3>Login</h3>';
           loginModalHtml+='</div>';
           loginModalHtml+='<div class="modal-body">';
             loginModalHtml+='<form method="post" name="login_form">';
               loginModalHtml+='<p><input type="text" class="span3" name="eid" id="username" placeholder="Username" value="'+username+'"></p>';
               loginModalHtml+='<p><input type="password" class="span3" name="passwd" placeholder="Password"></p>';
               loginModalHtml+='<p><button type="submit" class="btn btn-primary">Sign in</button>';
                 loginModalHtml+='<a href="#">Forgot Password?</a>';
               loginModalHtml+='</p>';
             loginModalHtml+='</form>';
           loginModalHtml+='</div>';

         loginModalHtml+='</div>';
	 loginModalHtml+='</div>';
 loginModalHtml+='</div>';
 return loginModalHtml;
}

function stopOauthCheck(data){
	clearInterval(data);
}

function askTableData(response){
  var askTableHtml='';
  var askResult=response.askResult;
  if(askResult!=null){
  for(var i=0; i<askResult.length;i++){
  askTableHtml+='<tr>';
                askTableHtml+='<td data-title="" class="text-center">'+askResult[i].askId+'</td>';
                askTableHtml+='<td data-title="Amount" id="ask_amount_'+askResult[i].askId+'" class="text-center">'+CURRENCY+' '+askResult[i].askprice+'</td>';
                askTableHtml+='<td data-title="Quantity" id="ask_qty_'+askResult[i].askId+'" class="text-center">'+askResult[i].currentQty+'</td>';
                askTableHtml+='<td class="text-center">';
                if(isMethodExist('edit bid')==true){
                  askTableHtml+='<a class="btn btn-sm btn-info edit_ask_row" data-id="'+askResult[i].askId+'" data-amount="'+askResult[i].askprice+'" data-qty="'+askResult[i].currentQty+'"  data-toggle="modal" data-target="#add_edit_modal" ><i class="fa fa-edit"></i> EDIT</a>';
                }else{
                  bidTableHtml+='<i class="fa fa-edit"></i> EDIT';
                }

                askTableHtml+='</td>';
                askTableHtml+='<td class="text-center">';
                if(isMethodExist('edit bid')==true){
                  askTableHtml+='<a class="btn btn-sm btn-danger delete_ask_row" data-id="'+askResult[i].askId+'" data-amount="'+askResult[i].askprice+'" data-qty="'+askResult[i].currentQty+'" ><i class="fa fa-trash-o"></i> DELETE</a>';
                }else{
                  bidTableHtml+='<i class="fa fa-trash-o"></i> DELETE';
                }

            askTableHtml+='</td>';
            askTableHtml+='</tr>';
  }
}else{
  askTableHtml+='<tr>';
  askTableHtml+='<td colspan="5" class="text-center">No Data Exist</td>';
  askTableHtml+='</tr>';
}
  return askTableHtml;

}

function bidTableData(response){
  var bidTableHtml='';
  var bidResult=response.bidResult;
  if(bidResult!=null){
  for(var i=0; i<bidResult.length;i++){
  bidTableHtml+='<tr>';
                bidTableHtml+='<td data-title="" class="text-center">'+bidResult[i].bidId+'</td>';
                bidTableHtml+='<td data-title="Amount" id="bid_amount_'+bidResult[i].bidId+'" class="text-center">'+CURRENCY+' '+bidResult[i].bidprice+'</td>';
                bidTableHtml+='<td data-title="Quantity" id="bid_qty_'+bidResult[i].bidId+'" class="text-center">'+bidResult[i].currentQty+'</td>';
                bidTableHtml+='<td class="text-center">';
                if(isMethodExist('edit bid')==true){
                  bidTableHtml+='<a class="btn btn-sm btn-info edit_bid_row" data-id="'+bidResult[i].bidId+'" data-amount="'+bidResult[i].bidprice+'" data-qty="'+bidResult[i].currentQty+'" data-toggle="modal" data-target="#add_edit_modal" ><i class="fa fa-edit"></i> EDIT</a>';
                }else{
                  bidTableHtml+='<i class="fa fa-edit"></i> EDIT';
                }

                bidTableHtml+='</td>';
                bidTableHtml+='<td class="text-center">';
                if(isMethodExist('edit bid')==true){
                  bidTableHtml+='<a class="btn btn-sm btn-danger delete_bid_row" data-id="'+bidResult[i].bidId+'" data-amount="'+bidResult[i].bidprice+'" data-qty="'+bidResult[i].currentQty+'" ><i class="fa fa-trash-o"></i> DELETE</a>';
                }else{
                  bidTableHtml+='<i class="fa fa-trash-o"></i> DELETE';
                }

                bidTableHtml+='</td>';
            bidTableHtml+='</tr>';
  }
}else{
  bidTableHtml+='<tr>';
  bidTableHtml+='<td colspan="5" class="text-center">No Data Exist</td>';
  bidTableHtml+='</tr>';
}
  return bidTableHtml;

}
function paymentOrderData(response){
  var paymentOrderHtml='';
  var paymentOrderResult=response.paymentOrdersListResult;
  for(var i=0;i<paymentOrderResult.length;i++){
    var status=paymentOrderResult[i].status;
    if(status==15){
      var statusText='<span style="color:#795548;">Order Recieved</span>';
      var disabledClass='';
      var buttonClass='btn-info';
    }
    if(status==16){
      var statusText='<span style="color:#03a9f4;">Reference No Updated</span>';
      var disabledClass='';
      var buttonClass='btn-info';
    }
    if(status==17){
      var statusText='<span style="color:#4CAF50;">Order Confirmed</span>';
      var disabledClass='disabled';
      var buttonClass='btn-basic';
    }
    if(status==20){
      var statusText='<span style="color:#03a9f4;">Razor Pay Order Created</span>';
      var disabledClass='';
      var buttonClass='btn-info';
    }
    if(status==21){
      var statusText='<span style="color:#4CAF50;">Razor Pay Order Success</span>';
      var disabledClass='disabled';
      var buttonClass='btn-basic';
    }
    if(status==22){
      var statusText='<span style="color:#ae0404de;">Razor Pay Suspicious Order</span>';
      var disabledClass='';
      var buttonClass='btn-info';
    }
    paymentOrderHtml+='<tr>';
         paymentOrderHtml+='<td>'+paymentOrderResult[i].orderId+'</td>';
	paymentOrderHtml+='<td>'+paymentOrderResult[i].orderNo+'</td>';
         paymentOrderHtml+='<td>'+paymentOrderResult[i].users.full_name+'</td>';
        //  paymentOrderHtml+='<td>'+paymentOrderResult[i].userId+'</td>';
         //paymentOrderHtml+='<td>'+paymentOrderResult[i].users.phone+'</td>';
         paymentOrderHtml+='<td>'+CURRENCY+paymentOrderResult[i].amount+'</td>';
         paymentOrderHtml+='<td>'+readable_timestamp(paymentOrderResult[i].created)+'</td>';
         paymentOrderHtml+='<td>'+paymentOrderResult[i].bankDetails.bank_name+'</td>';
         paymentOrderHtml+='<td>'+paymentOrderResult[i].bankDetails.account_no+'</td>';
         paymentOrderHtml+='<td>'+paymentOrderResult[i].bankDetails.benificiary_name+'</td>';
         paymentOrderHtml+='<td>'+paymentOrderResult[i].bankDetails.routing_no+'</td>';
         paymentOrderHtml+='<td class="confirm_order_status">'+statusText+'</td>';
         paymentOrderHtml+='<td>'+paymentOrderResult[i].referenceNo+'</td>';
         if(isMethodExist('confirm order')==true){
          paymentOrderHtml+='<td width="5px"><a class="btn btn-sm '+buttonClass+' confirm_order_btn '+disabledClass+'" data-order-id="'+paymentOrderResult[i].orderId+'" data-user-id="'+paymentOrderResult[i].userId+'" data-reference-id="'+paymentOrderResult[i].referenceNo+'"><i class="fa fa-check-square-o"></i> Confirm Order</a></td>';
      paymentOrderHtml+='</tr>';
         }else{
          paymentOrderHtml+='<td><i class="fa fa-check-square-o"></i> Confirm Order</td>';
         }

  }
  return paymentOrderHtml;
}

function userTransaction(response){
  var userTransactionTableHtml='';
  var userTransactionResult=response.userTransactionsListResult;
  for(var i=0;i<userTransactionResult.length;i++){

      if(userTransactionResult[i].creditAmount!=0.00 && userTransactionResult[i].creditAmount!=null){
        var bitcoinAmountData='<h4 class="m-b-0">' + (userTransactionResult[i].currency) + ' '+ userTransactionResult[i].creditAmount;
        var actionStatus='</h4> <span class="small text-muted">'+ userTransactionResult[i].action +'</span>';
      }
      else if(userTransactionResult[i].debitAmount!=0.00 && userTransactionResult[i].debitAmount!=null){
        var bitcoinAmountData='<h4 class="m-b-0">' + (userTransactionResult[i].currency) + ' '+ userTransactionResult[i].debitAmount;
        var actionStatus=' </h4> <span class="small text-muted">'+ userTransactionResult[i].action +'</span>';
      }

      if((userTransactionResult[i].action)=='Load'){
        if(userTransactionResult[i].creditAmount!=null){
          var bitcoinAmountData='<h4 class="m-b-0">'+ (userTransactionResult[i].baseCurrency) + ' ' + userTransactionResult[i].creditAmount;
          var actionStatus=' </h4> <span class="small text-muted"> Loaded </span>';
      }
    }
    if((userTransactionResult[i].action)=='Withdraw'){
      if(userTransactionResult[i].debitAmount!=null){
        var bitcoinAmountData='<h4 class="m-b-0">'+ (userTransactionResult[i].baseCurrency) + ' ' + userTransactionResult[i].debitAmount;
        var actionStatus='</h4> <span class="small text-muted"> Withdraw </span>';
      }
    }

    // if (userTransactionResult[i].status == '0') {
    //   status = 'Pending';
    // } else if (userTransactionResult[i].status == '1') {
    //   status = 'Confirmed';
    // } else{
    //   status = userTransactionResult[i].action;
    // }
      userTransactionTableHtml+='<tr>';
      userTransactionTableHtml+='<td data-title="Timestamp"><span>'+readable_timestamp(userTransactionResult[i].transactionTimestamp)+'</span></td>'
      userTransactionTableHtml+='<td data-title="ID">'+userTransactionResult[i].transactionId+'</td>';
     /*  userTransactionTableHtml+='<td data-title="Description">'+descriptionText+'</td>'; */
      userTransactionTableHtml+='<td data-title="Bitcoin Amount">'+bitcoinAmountData+'</td>';
      userTransactionTableHtml+='<td data-title="Action">'+actionStatus+'</td>';
      // userTransactionTableHtml+='<td class="">' + status + '</td>';
      userTransactionTableHtml+='</tr>';
  }
   return userTransactionTableHtml;
}

function purchaseDetails(response){
  var purchaseDetailsHtml='';
  var purchaseDetailsResult=response.userTransactionsListResult;
  for(var i=0;i<purchaseDetailsResult.length;i++){

    if((purchaseDetailsResult[i].action)=='Buy'){

      if(purchaseDetailsResult[i].creditAmount!=0.00 && purchaseDetailsResult[i].creditAmount!=null){
        var bitcoinAmountData='<h4 class="m-b-0">' + (purchaseDetailsResult[i].currency) + ' '+ purchaseDetailsResult[i].creditAmount+'  </h4> <span class="small text-muted"> bought </span>';
      }
    }

    if((purchaseDetailsResult[i].action)=='Sell'){

      if(purchaseDetailsResult[i].creditAmount!=0.00 && purchaseDetailsResult[i].creditAmount!=null){
        var bitcoinAmountData='<h4 class="m-b-0">'+ (purchaseDetailsResult[i].currency) + ' ' + purchaseDetailsResult[i].creditAmount+'  </h4> <span class="small text-muted"> sold </span>';
      }
    }

    if((purchaseDetailsResult[i].action)=='Selloffer' || (purchaseDetailsResult[i].action)=='sell_offer'){

      if(purchaseDetailsResult[i].debitAmount!=0.00 && purchaseDetailsResult[i].debitAmount!=null){
        var bitcoinAmountData='<h4 class="m-b-0">' + (purchaseDetailsResult[i].currency) + purchaseDetailsResult[i].debitAmount+'  </h4> <span class="small text-muted"> sell offer </span>';
      }
    }

    if((purchaseDetailsResult[i].action)=='Buyoffer' || (purchaseDetailsResult[i].action)=='buy_offer'){

      if(purchaseDetailsResult[i].debitAmount!=0.00 && purchaseDetailsResult[i].debitAmount!=null){
        var bitcoinAmountData='<h4 class="m-b-0">' + (purchaseDetailsResult[i].currency) + purchaseDetailsResult[i].debitAmount+'  </h4> <span class="small text-muted"> buy offer </span>';
      }
    }

    purchaseDetailsHtml+='<tr>';
    purchaseDetailsHtml+='<td data-title="ID">'+purchaseDetailsResult[i].transactionId+'</td>';
    purchaseDetailsHtml+='<td>'+purchaseDetailsResult[i].fullName+'</td>';
    //purchaseDetailsHtml+='<td>'+purchaseDetailsResult[i].email+'</td>';
    purchaseDetailsHtml+='<td data-title="Timestamp"><span>'+readable_timestamp(purchaseDetailsResult[i].transactionTimestamp)+'</span></td>';
    //purchaseDetailsHtml+='<td data-title="Description">'+descriptionText+'</td>';
    purchaseDetailsHtml+='<td data-title="Bitcoin Amount">'+bitcoinAmountData+'</td>';
    purchaseDetailsHtml+='</tr>';
  }
  return purchaseDetailsHtml;
}

function withdrawTableData(response){
  var withdrawList=response.paymentWithdrawalListResult;
  var withdrawHtml='';
  for(var i=0;i<withdrawList.length;i++){

    if(withdrawList[i].action=='withdraw'){ //checking for withdraw action

      if(withdrawList[i].status=='pending'){
        var statusText='<span style="color:blue;"><i class="fa fa-spinner" ></i> Pending </span>';
        var payButtonOption=1;
         var paybuttonClass='btn-primary';
         var disableOption='';
      }

      if(withdrawList[i].status=='confirm'){
        var statusText='<span style="color:green;"><i class="fa fa-check" ></i> Paid</span>';
        var payButtonOption=0;
        var paybuttonClass='btn-default';
       var disableOption='disabled';
      }

      // if(withdrawList[i].status=='withdraw'){
      //   var statusText='<span style="color:green;">Withdraw</span>';
      //   var payButtonOption=1;
      //   var paybuttonClass='btn-primary';
      //  var disableOption='';
      // }

      if(withdrawList[i].accountNo!=null){
        var accountNo=withdrawList[i].accountNo;
      }else{
        var accountNo='<i class="fa fa-minus"></i>';
      }

      if(withdrawList[i].customerName!=null){
        var customerName=withdrawList[i].customerName;
      }else{
        var customerName='<i class="fa fa-minus"></i>';
      }

      if(withdrawList[i].benificiaryName!=null){
        var beneficiaryName=withdrawList[i].benificiaryName;
      }else{
        var beneficiaryName='<i class="fa fa-minus"></i>';
      }

      if(withdrawList[i].bankName!=null){
        var bankName=withdrawList[i].bankName;
      }else{
        var bankName='<i class="fa fa-minus"></i>';
      }

      if(withdrawList[i].withdrwalAmount!=null){
        var withdrwalAmount=CURRENCY+' '+withdrawList[i].creditAmount;
      }else{
        var withdrwalAmount='<i class="fa fa-minus"></i>';
      }

      withdrawHtml+='<tr>';
      withdrawHtml+='<td class="text-center">'+withdrawList[i].customerid+'</td>';
      withdrawHtml+='<td class="text-center">'+customerName+'</td>';
      withdrawHtml+='<td class="text-center">'+beneficiaryName+'</td>';
      withdrawHtml+='<td class="text-center">'+bankName+'</td>';
      withdrawHtml+='<td class="text-center">'+accountNo+'</td>';
      withdrawHtml+='<td class="text-center">'+withdrawList[i].ifscCode+'</td>';
      withdrawHtml+='<td class="text-center">'+withdrwalAmount+'</td>';
//       withdrawHtml+='<td class="text-center" id="withdraw_reference_'+withdrawList[i].withdrawalid
// +'">'+withdrawList[i].description+'</td>';
      withdrawHtml+='<td class="text-center" id="withdraw_status_'+withdrawList[i].withdrawalid
+'">'+statusText+'</td>';
      if(isMethodExist('pay')==true){
      withdrawHtml+='<td class="text-center"><button class="btn '+paybuttonClass+' make_paid" '+disableOption+' data-withdraw-id="'+withdrawList[i].withdrawalid
+'" data-customer-id="'+withdrawList[i].customerid+'" data-amount="'+withdrawList[i].creditAmount+'" data-description="'+withdrawList[i].description+'" id="pay_btn_'+withdrawList[i].withdrawalid
+'" data-cgst-charge="'+withdrawList[i].chargeCgst+'" data-sgst-charge="'+withdrawList[i].chargeSgst+'" data-txn-charge="'+withdrawList[i].txnCharge+'">Pay</button></td>';
      }else{
        withdrawHtml+='<td class="text-center">Pay</td>';
      }
    withdrawHtml+='</tr>';

    }//withdraw action checking closed

  }
return withdrawHtml;
}

function sendRecieveList(response){
  var sendRecieveHtml='';
  if(response.userTransactionsListResult==null){
    sendRecieveHtml+='<tr>';
    sendRecieveHtml+='<td colspan="7">No Data Exist</td>';
    sendRecieveHtml+='</tr>';
  }else{
    for(var i=0;i<response.userTransactionsListResult.length;i++){
      if(response.userTransactionsListResult[i].action=='Received'){
        if(response.userTransactionsListResult[i].creditAmount!=null && response.userTransactionsListResult[i].creditAmount!='0' && response.userTransactionsListResult[i].creditAmount!=''){
          var sendRecieveAmount=response.userTransactionsListResult[i].creditAmount;
        }
      }else{
        if(response.userTransactionsListResult[i].debitAmount!=null && response.userTransactionsListResult[i].debitAmount!='0' && response.userTransactionsListResult[i].debitAmount!=''){
          var sendRecieveAmount=response.userTransactionsListResult[i].debitAmount;
        }
      }
      if((response.userTransactionsListResult[i].debitAmount!='0' && response.userTransactionsListResult[i].debitAmount!=null && response.userTransactionsListResult[i].debitAmount!='' ) || (response.userTransactionsListResult[i].creditAmount!='0' && response.userTransactionsListResult[i].creditAmount!=null && response.userTransactionsListResult[i].creditAmount!='')){
        var currency=response.userTransactionsListResult[i].currency;
      }
      if(currency==response.userTransactionsListResult[i].currency){
        var transactionReferenceId=response.userTransactionsListResult[i].currencyTxnid;
      }
//       if(currencyTxnid=null){
// "-"
//       }
//       else{
//       if(currency==response.userTransactionsListResult[i].currency){
//           var transactionReferenceId=response.userTransactionsListResult[i].currencyTxnid;
//         }
//       }

      sendRecieveHtml+='<tr>';
      sendRecieveHtml+='<td>'+response.userTransactionsListResult[i].transactionId+'</td>';
      sendRecieveHtml+='<td>'+response.userTransactionsListResult[i].fullName+'</td>';
      //sendRecieveHtml+='<td>'+response.userTransactionsListResult[i].email+'</td>';
      sendRecieveHtml+='<td>'+readable_timestamp(response.userTransactionsListResult[i].transactionTimestamp)+'</td>';
      sendRecieveHtml+='<td>'+response.userTransactionsListResult[i].description+'</td>';
      sendRecieveHtml+='<td>'+transactionReferenceId+'</td>';
      sendRecieveHtml+='<td>'+response.userTransactionsListResult[i].action+'</td>';
      sendRecieveHtml+='<td> '+currency+' '+sendRecieveAmount+'</td>';
      sendRecieveHtml+='</tr>';
    }
  }
  return sendRecieveHtml;
}

function btcPendingTransaction(response){

  var pendingTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingTransactionHtml+='<tr>';
    pendingTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingTransactionHtml+='<td> BTC '+response.userTransactionsListResult[i].credit_btc_amount+'</td>';
    pendingTransactionHtml+='<tr>';
    $('#btc_csv_download').removeAttr('disabled');
    $('#btc_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingTransactionHtml+='<tr>';
  pendingTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingTransactionHtml+='<tr>';
  $('#btc_csv_download').attr('disabled','disabled');
  $('#btc_clear_pending_transaction').attr('disabled','disabled');
}
return pendingTransactionHtml;
}

function bchPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> BCH '+response.userTransactionsListResult[i].credit_bch_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#bch_csv_download').removeAttr('disabled');
  $('#bch_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#bch_csv_download').attr('disabled','disabled');
  $('#bch_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}

function hcxPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> HCX '+response.userTransactionsListResult[i].credit_bch_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#hcx_csv_download').removeAttr('disabled');
    $('#hcx_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#hcx_csv_download').attr('disabled','disabled');
  $('#hcx_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}

function iecPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> IEC '+response.userTransactionsListResult[i].credit_bch_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#iec_csv_download').removeAttr('disabled');
    $('#iec_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#iec_csv_download').attr('disabled','disabled');
  $('#iec_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}

function trigPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> TrigX '+response.userTransactionsListResult[i].credit_trigx_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#trig_csv_download').removeAttr('disabled');
    $('#trig_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#trig_csv_download').attr('disabled','disabled');
  $('#trig_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}
function gssPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> GSS '+response.userTransactionsListResult[i].credit_gss_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#gss_csv_download').removeAttr('disabled');
    $('#gss_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#gss_csv_download').attr('disabled','disabled');
  $('#gss_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}
function trsPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> TRS '+response.userTransactionsListResult[i].credit_trs_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#trs_csv_download').removeAttr('disabled');
    $('#trs_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#trs_csv_download').attr('disabled','disabled');
  $('#trs_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}
function dpPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> 3DP '+response.userTransactionsListResult[i].credit_dp_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#dp_csv_download').removeAttr('disabled');
    $('#dp_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#dp_csv_download').attr('disabled','disabled');
  $('#dp_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}
function iotPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> IOT '+response.userTransactionsListResult[i].credit_bot_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#iot_csv_download').removeAttr('disabled');
    $('#iot_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#iot_csv_download').attr('disabled','disabled');
  $('#iot_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}
function gnyPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> GNY '+response.userTransactionsListResult[i].credit_gny_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#gny_csv_download').removeAttr('disabled');
    $('#gny_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#gny_csv_download').attr('disabled','disabled');
  $('#gny_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}
function cvlPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> CVL '+response.userTransactionsListResult[i].credit_cvl_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#cvl_csv_download').removeAttr('disabled');
    $('#cvl_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#cvl_csv_download').attr('disabled','disabled');
  $('#cvl_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}
function ethPendingTransaction(response){

  var pendingBchTransactionHtml='';
  if(response.userTransactionsListResult!=null){
  for(var i=0;i<response.userTransactionsListResult.length;i++){
    pendingBchTransactionHtml+='<tr>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].transaction_id+'</td>';
    pendingBchTransactionHtml+='<td>'+response.userTransactionsListResult[i].customerID+'</td>';
    pendingBchTransactionHtml+='<td> ETH '+response.userTransactionsListResult[i].credit_eth_amount+'</td>';
    pendingBchTransactionHtml+='<tr>';
    $('#eth_csv_download').removeAttr('disabled');
    $('#eth_clear_pending_transaction').removeAttr('disabled');
  }
}else{
  pendingBchTransactionHtml+='<tr>';
  pendingBchTransactionHtml+='<td colspan="3"> No Data Exist</td>';
  pendingBchTransactionHtml+='<tr>';
  $('#eth_csv_download').attr('disabled','disabled');
  $('#eth_clear_pending_transaction').attr('disabled','disabled');
}
console.log(pendingBchTransactionHtml);
return pendingBchTransactionHtml;
}

 /*********Get all logged in user module details defination **********/
 function getLoggedInUserModule(){
  var moduleList=JSON.parse(localStorage.getItem('admin_module_list'));
  if(moduleList!=null || moduleList!=''){
    $('.nav_menu_list').hide();
    for(var i=0;i<moduleList.length;i++){
      /* if(moduleList[i].module_title=='ACTIVE USER'){
        $('#nav_user_management').show();
      }
      if(moduleList[i].module_title=='GROUP MASTER'){
        $('#nav_group_management').show();
      }
      if(moduleList[i].module_title=='METHOD MASTER'){
        $('#nav_report_management').show();
      } */
      if(moduleList[i].module_title=='View Confirmed Users'){
        $('#nav_view_confirmed_user').show();
      }
      if(moduleList[i].module_title=='View Unconfirmed Users'){
        $('#nav_view_unconfirmed_user').show();
      }
      if(moduleList[i].module_title=='View Buy-Sell'){
        $('#nav_view_buy_sell').show();
      }
      if(moduleList[i].module_title=='View Send-Receive'){
        $('#nav_view_send_recieve').show();
      }
      if(moduleList[i].module_title=='Orderbook'){
        $('#nav_orderbook').show();
      }
      if(moduleList[i].module_title=='Payment Order'){
        $('#nav_payment_order').show();
      }
      if(moduleList[i].module_title=='Monitor Balance'){
        $('#nav_monitor_balance').show();
      }
      if(moduleList[i].module_title=='Withdrawal Request'){
        $('#nav_withdraw').show();
      }
      if(moduleList[i].module_title=='Bot Control'){
        $('#nav_bot_control').show();
      }
      if(moduleList[i].module_title=='Green Tech User'){
        $('#nav_paybito_user').show();
      }
      if(moduleList[i].module_title=='Pending Transaction'){
        $('#nav_pending_transaction').show();
      }
      if(moduleList[i].module_title=='BO User Management'){
        $('#nav_user_management').show();
      }
      if(moduleList[i].module_title=='BO Group Management'){
        $('#nav_group_management').show();
      }
      if(moduleList[i].module_title=='Report Management'){
        $('#nav_report_management').show();
      }
      if(moduleList[i].module_title=='Liquidity Management'){
        $('#nav_liquidity').show();
      }
      if(moduleList[i].module_title=='Exchange Report'){
        $('#nav_exchangereport').show();
      }

    }
  }else{
    $('.nav_menu_list').hide();
  }
}

/************get an existing module status function defination *******/
function isMethodExist(methodName){
  var methodList=JSON.parse(localStorage.getItem('admin_method_list'));
  if(methodList!=null || methodList!=''){
    //console.log(methodList.length);
    for(var i=0;i<methodList.length;i++){
      var state=0;
      //console.log(i);
      //console.log(methodList[i].method_name);
     if(methodList[i].method_name==methodName){
      //console.log(methodList[i].method_id+' '+methodList[i].method_name+' '+methodName);
       state=1;
       break;
     }else if(methodList[i].method_title==methodName){
       state=1;
       break
     }else{
      state=0;
     }
    }
    if(state==1){
      return true;
    }else{
      return false;
    }
  }else{
    return false
  }

}