/*
config file for the project
initialize all global variables in this file
author:joyesh@codomotive.com
contributors:
file created:02-11-2016(dd-mm-yyyy)
*/

//siteurl
//var ADDR="http://ec2-54-67-100-17.us-west-1.compute.amazonaws.com/admin/Super-Admin";
   var ADDR="http://localhost/admin-access";
  //  var ADDR="https://trade.tomya.com/admin-access";

//webservice
// var WEBSERVICE="http://52.8.166.47:8080/AdminService";
 var WEBSERVICE="https://api.tomya.com/AdminApi";
//var WEBSERVICE="https://api.bitrump.com/AdminApi";

//set as debug mode
var DEBUG=1;

//page load identifier
var PL=0;

//work in progress identifier
var WIP=0;

//orderbook lowest quantity for adding ask and bid
var ORDERBOOKLOWESTQTY='00';

// currency icon for showining in the tables
var CURRENCY='<i class="fa fa-usd"></i>';