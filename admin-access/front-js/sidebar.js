/*
sidebar dom manipulation
after loading sidebar.html
author:joyesh@codomotive.com
contributors:
file created:14-11-2016(dd-mm-yyyy)
*/
setTimeout(function(){

var url=window.location.href ;
var filename = url.split('/').pop();

//SET NAME OF USER FROM LOCALSTORAGE TO SIDE BAR AND HEADER
/*$('#profile_name_sidebar').html(localStorage.getItem('google_user_name'));
$('#header_bar').html(localStorage.getItem('google_user_name'));
$('#profile_name_header').html(localStorage.getItem('google_user_name')+' '+localStorage.getItem('email'));
$("#user_image").attr("src",localStorage.getItem('google_user_img'));
$("#header_user_image").attr("src",localStorage.getItem('google_user_img'));
$("#header_user_image_sml").attr("src",localStorage.getItem('google_user_img'));
*/

if(
filename=='index.html'

)
{
	$("#nav_dashboard").addClass("active");
}

if(

filename=='view_user.html'
)
{
	$("#nav_view_user").addClass("active");
}
if(

filename=='view_confirmed_user.html'
)
{
	$("#nav_7").addClass("active");
}
if(

filename=='view_unconfirmed_user.html'
)
{
	$("#nav_8").addClass("active");
}

if(
filename=='view_buy_sell.html'
)
{
	$("#nav_9").addClass("active");
}

if(
filename=='orderbook.html'
)
{
	$("#nav_orderbook").addClass("active");
}
if(
	filename=='liquidity.html'
	)
	{
		$("#nav_liquidity").addClass("active");
	}
if(
filename=='payment_order.html'
)
{
	$("#nav_payment_order").addClass("active");
}

if(
filename=='monitor_balance.html'
)
{
	//alert(filename);
	$("#nav_monitor_balance").addClass("active");
}
if(
filename=='withdraw.html'
)
{
	//alert(filename);
	$("#nav_withdraw").addClass("active");
}
if(
filename=='paybito_user.html'
)
{
	//alert(filename);
	$("#nav_16").addClass("active");
}
if(
filename=='pending_transaction.html'
)
{
	//alert(filename);
	$("#nav_17").addClass("active");
}
if(
filename=='view_send_recieved.html'
)
{
	//alert(filename);
	$("#nav_10").addClass("active");
}
if(
filename=='bot-control.html'
)
{
	//alert(filename);
	$("#nav_bot_control").addClass("active");
}

if(
	filename=='user_management.html'
)
{
	$("#nav_18").addClass("active");
}

if(
	filename=='group_management.html'
)
{
		$("#nav_19").addClass("active");
}
if(
	filename=='report_management.html'
)
{
	$("#nav_report_management").addClass("active");
}
if(
	filename=='exchangereport.html'
)
{
	$("#nav_exchangereport").addClass("active");
}

// if(
// 	filename=='Batch_operations.html'
// )
// {
// 	$("#nav_Batch_operations").addClass("active");
// }

//calling get logged in user module
//getLoggedInUserModule()

$('.btn').addClass('waves-effect');
//SHOW USENAME IN HEADER AND SIDEBAR
var userFirstName=localStorage.getItem('first_name_us');
var userLastName=localStorage.getItem('last_name_us');
$('.logged_user_name').html('<b>'+userFirstName+'</b>');

//LOGOUT FUNCTIONALITY
$(document).on('click','.logout',function(){
	var inputObj={};
	inputObj['user_name']=localStorage.getItem('user_name_us');
	var jsonString=JSON.stringify(inputObj);
	call(WEBSERVICE+'/adminAccess/LogoutAdmin',jsonString,function(data){
		localStorage.clear();
		window.location="login.html";
	});

});

/* loginDuration();

$(document).on('click','#sign_in_btn',function(e){
	//alert(AAA);
e.preventDefault();
var username=$('#username').val();
	var password=$('#password').val();
	var emailCheck=username.indexOf('@');
	if(emailCheck<0){
			//error
			$('.email_error').css('display','block');
			 $(this).val('');
	}else{
			//success
	}
if(username !='' && password !=''){
var fd = new FormData();
		fd.append('username',username);
		fd.append('password',password);
		fd.append('grant_type','password');
		$.ajax({
			url: WEBSERVICE+'/oauth/token',
			method:'POST',
			processData: false,
			contentType: false,
			data: fd,

			beforeSend: function(xhr) {
				xhr.setRequestHeader("authorization", "Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0");
			},

			success: function(data) {
				console.log(data);
				localStorage.setItem('access_token_us',data.access_token);
				localStorage.setItem('refresh_token_us',data.refresh_token);

				var expiresTime=data.expires_in;
				expiresTime=expiresTime*1000;
				var start_time=$.now();
				var expiresIn=start_time+expiresTime;
  localStorage.setItem('expires_in_us',expiresIn);
  //Getting User details
  var adminObj={};
  adminObj['admin_id']=1;
  jsonString=JSON.stringify(adminObj);
  call(WEBSERVICE+"/admin/GetAdminDetails",jsonString,function(data){
	var response=JSON.parse(data);
	if(response.error.error_data!='0'){
	  ui_alert(response.error.error_msg,'error');
	}else{
	  console.log(response);
	  localStorage.setItem('admin_id_us',response.adminUsersResult.admin_id);
	  localStorage.setItem('email_us',response.adminUsersResult.email);
	  localStorage.setItem('first_name_us',response.adminUsersResult.first_name);
	  localStorage.setItem('last_name_us',response.adminUsersResult.last_name);
	  localStorage.setItem('role_us',response.adminUsersResult.role);
	  //window.location="dashboard.html";
	  location.reload();
	}
  });
			},
			error:function(res)
				{
					console.log(res);

				}
		});

}else{
ui_alert('username and passwords are mandatory','error');
}

}); */

//logged user's initials
var userFirstName=localStorage.getItem('first_name_us');
var firstChar=userFirstName.charAt(0);
firstChar=firstChar.toUpperCase();
$('.logged_user_initial').html(firstChar);

$(document).on('click','#menu_toggle',function(){
	var bodyClassFlag=$('#body_id').hasClass('nav-md');
	//alert(bodyClassFlag);
	if(bodyClassFlag==true){
		$('#body_id').removeClass('nav-md');
		$('#body_id').addClass('nav-sm');
	}else{
		$('#body_id').addClass('nav-md');
		$('#body_id').removeClass('nav-sm');
	}
});

},1);
