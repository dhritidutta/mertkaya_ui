var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from './core-data.service';
// import {
//   TradesComponent
// } from "./trades/trades.component";
import * as $ from 'jquery';
import { Router } from '@angular/router';
//import { OrderBookComponent } from './order-book/order-book.component';
var BodyService = /** @class */ (function () {
    function BodyService(http, data, route) {
        this.http = http;
        this.data = data;
        this.route = route;
        this.recievingAddress = 0;
        this.pgn = [];
        this.triggerslink = 'https://xchain.io/tx/';
        this.noOfItemPerPage = '20';
        this.timeSpan = 'all';
        //this.orderbk.tradePageSetup();
    }
    BodyService.prototype.getUserTransaction = function () {
        var _this = this;
        var userTransObj = {};
        userTransObj['customerId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(userTransObj);
        this.http.post(this.data.WEBSERVICE + '/transaction/getUserBalance', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token')
            }
        })
            .subscribe(function (response) {
            var result = response;
            // console.log('++++++++++++','result');
            if (result.error.error_data != '0') {
                _this.data.alert('Cannot fetch user balance', 'danger');
            }
            else {
                _this.balencelist = result.userBalanceList;
                if (_this.balencelist != null) {
                    for (var i = 0; i < _this.balencelist.length; i++) {
                        if (_this.balencelist[i].currencyCode == "USD") {
                            localStorage.setItem('usdbalance', _this.balencelist[i].closingBalance);
                        }
                    }
                }
            }
            _this.selectedCryptoCurrency = localStorage.getItem('selected_currency');
            // console.log('+++++++++++++++++', this.balencelist);
            for (var i = 0; i <= _this.balencelist.length - 1; i++) {
            }
            //   console.log('********',this.usdBalance, this.btcBalance,this.ethBAlance,this.bccBalance,this.diamBalance,this.ltcBalance,this.hcxBalance,this.etcBalance,this.bsvBalance,this.xrpBalance);
        }, function (reason) {
            //   wip(0);
            _this.data.logout();
            if (reason.error.error == 'invalid_token') {
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    BodyService.prototype.getDashBoardInfo = function () {
        var _this = this;
        var infoObj = {};
        infoObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(infoObj);
        this.http.post(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            var result = response;
            //  localStorage.setItem("currencyId",result.settingsList[i].currencyId);
            //   console.log('+++++++++++appsettings',response);
            if (result.error.error_data != '0') {
                //    alert(result.error.error_msg);
            }
            else {
                var storeDashboardInfo = JSON.stringify(result);
                var environmentSettingsListObj = {};
                localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
                for (var i = 0; i < result.settingsList.length; i++) {
                    // alert(result.settingsList[i].currencyId);
                    environmentSettingsListObj['' + result.settingsList[i].name + result.settingsList[i].currencyId + ''] = result.settingsList[i];
                }
                environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
                localStorage.setItem('environment_settings_list', environmentSettingsListObj);
                ////showing disclaimer for sell
                _this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
                if (result.userAppSettingsResult.user_docs_status == '') {
                    _this.indentificationStatus = 'Identity verification documents not submitted';
                    _this.data.alert('PLEASE ENSURE THAT ALL DOCUMENTS ARE IN JPG OR JPEG FORMAT. PNG, GIF, and other formats are not permitted.', 'info');
                }
                if (result.userAppSettingsResult.user_docs_status == '1') {
                    _this.indentificationStatus = 'Identity verification documents verified';
                }
                if (result.userAppSettingsResult.user_docs_status == '0') {
                    _this.indentificationStatus = ' Identity verification documents submitted for Verification';
                }
                if (result.userAppSettingsResult.user_docs_status == '2') {
                    _this.indentificationStatus = ' Identity verification documents declined, please submit again';
                }
                if (result.userAppSettingsResult.bank_details_status == '') {
                    _this.bankDetailStatus = 'Bank details not submitted';
                }
                if (result.userAppSettingsResult.bank_details_status == '0') {
                    _this.bankDetailStatus = 'Bank details  submitted for Verification';
                }
                if (result.userAppSettingsResult.bank_details_status == '2') {
                    _this.bankDetailStatus = ' Bank details verified';
                }
                if (result.userAppSettingsResult.bank_details_status == '3') {
                    _this.bankDetailStatus = ' Bank documents declined, please submit again';
                }
                if (localStorage.getItem('check_id_verification_status') &&
                    result.userAppSettingsResult.user_docs_status == '') {
                    // this.nav.alert('Please submit Identity verification documents to access all Paybito features','danger');
                }
                localStorage.setItem('check_id_verification_status', 'false');
            }
        }, function (reason) {
            //   wip(0);
            _this.data.logout();
            if (reason.error.error == 'invalid_token') {
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    BodyService.prototype.userDocVerificationStatus = function () {
        var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
        var userDocStatus = userAppSettingsObj.user_docs_status;
        if (userDocStatus == '') {
            this.verificationTitle = 'Submit ID Verification';
            this.verificationText = 'Please submit Identity verification documents to access all Tomya features';
            this.data.alert(this.verificationText, 'danger');
            this.route.navigateByUrl('/identity-verification');
            return false;
        }
        else if (userDocStatus == '2') {
            //todo: show Modal;
            this.verificationTitle = 'Submit ID Verification';
            this.verificationText = 'Your Identity verification documents has been declined in the verification process. Please submit again.';
            this.data.alert(this.verificationText, 'warning');
            this.route.navigateByUrl('/identity-verification');
            return false;
        }
        else if (userDocStatus == '0') {
            //todo: show Modal;
            this.verificationTitle = 'Document Verification Pending';
            this.verificationText = 'Your Id proof documents have not yet been verified by us. You will have restricted access to the features of the app until they are approved.';
            this.data.alert(this.verificationText, 'info');
            return false;
        }
        else {
            return true;
        }
    };
    BodyService.prototype.userBankVerificationStatus = function () {
        var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
        var userBankStatus = userAppSettingsObj.bank_details_status;
        if (userBankStatus == '') {
            this.verificationTitle = 'Submit Payment Method';
            this.verificationText = 'Your Payment details are not yet submitted. Please submit your Payment Details to proceed';
            return false;
        }
        else if (userBankStatus == '0') {
            //todo: show Modal;
            this.verificationTitle = 'Payment Being Verified';
            this.verificationText = 'Your Payment details are being verified. This step will be available after verification.';
            return false;
        }
        else if (userBankStatus == '3') {
            this.verificationTitle = 'Submit Payment Method';
            this.verificationText = 'Your Payment details have been disapproved. Kindly submit the details again to proceed.';
            return false;
        }
        else {
            return true;
        }
    };
    BodyService.prototype.pagination = function (totalCount, noOfItemPerPage, functionName) {
        var paginationButtonCount = parseInt(totalCount) / parseInt(noOfItemPerPage);
        this.paginationBtn = '';
        // $('.pagination_div').html('');
        if (Math.ceil(paginationButtonCount) >= 1) {
            for (var i = 1; i <= paginationButtonCount + 1; i++) {
                this.paginationBtn += '<button type="button" class="btn btn-dark font-xs filter-button" onclick="angular.element(this).scope().' + functionName + '(' + i + ')" >' + i + '</button>';
            }
        }
        else {
            this.paginationBtn += '';
        }
        // $('.pagination_div').html(this.paginationBtn);
    };
    BodyService.prototype.transactionHistory = function (pageNo) {
        var _this = this;
        this.historyTableTr = "<tr>\n    <td colspan=\"5\" class=\"text-center py-3\">\n    <img src=\"./assets/svg-loaders/puff.svg\" width=\"50\" alt=\"\">\n    </td>\n  </tr>";
        $('.historyTableBody').html(this.historyTableTr);
        var historyObj = {};
        historyObj['pageNo'] = pageNo;
        historyObj['noOfItemsPerPage'] = 20;
        historyObj['userId'] = localStorage.getItem('user_id');
        historyObj['timeSpan'] = this.timeSpan;
        historyObj['transactionType'] = 'all';
        var jsonString = JSON.stringify(historyObj);
        //   wip(1);
        this.http.post(this.data.WEBSERVICE + '/transaction/getUserAllTransaction', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            //   wip(0);
            _this.historyTableTr = '';
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.historyDetails = result.userTransactionsResult;
                _this.totalCount = result.totalCount;
                if (_this.historyDetails != null) {
                    for (var i = 0; i < _this.historyDetails.length; i++) {
                        var timestamp = _this.historyDetails[i].transactionTimestamp;
                        var dt = new Date(_this.historyDetails[i].transactionTimestamp);
                        var timestampArr = timestamp.split('.');
                        timestamp = _this.data.readable_timestamp(timestampArr[0]);
                        var action = _this.historyDetails[i].action;
                        _this.selectedCurrency = localStorage.getItem('selected_currency').toUpperCase();
                        _this.selectedCurrencyText = _this.selectedCurrency;
                        if (_this.historyDetails[i].baseCurrency != 'usd') {
                            var baseCurrency = _this.historyDetails[i].baseCurrency;
                        }
                        else {
                            var baseCurrency = 'fiat';
                        }
                        if (_this.historyDetails[i].currency != 'usd') {
                            var counterCurrency = _this.historyDetails[i].currency;
                        }
                        else {
                            var counterCurrency = 'fiat';
                        }
                        if (action == 'Buy') {
                            if (_this.historyDetails[i]['debitAmount'] != 0
                                && _this.historyDetails[i]['creditAmount'] != 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr.</span>';
                            }
                            else {
                                if (_this.historyDetails[i]['debitAmount'] != 0) {
                                    var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                                }
                                if (_this.historyDetails[i]['creditAmount'] != 0) {
                                    var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                                }
                            }
                        }
                        if (action == 'Buyoffer') {
                            var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                        }
                        if (action == 'Sell') {
                            if (_this.historyDetails[i]['creditAmount'] != 0 && _this.historyDetails[i]['debitAmount'] != 0) {
                                // var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + this.historyDetails[i]['creditAmount'] + ' Dr. </span>';
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr. </span>';
                            }
                            else {
                                if (_this.historyDetails[i]['creditAmount'] != 0) {
                                    var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                                }
                                if (_this.historyDetails[i]['debitAmount'] != 0) {
                                    var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' ' + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                                }
                            }
                        }
                        //Buydel
                        if (action == 'Buydel') {
                            if (_this.historyDetails[i].baseCurrency == 'usd' && _this.historyDetails[i]['creditAmount'] == 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                            }
                            else if (_this.historyDetails[i].baseCurrency == 'usd' && _this.historyDetails[i]['debitAmount'] == 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                            }
                            else if (_this.historyDetails[i].baseCurrency != 'usd' && _this.historyDetails[i]['debitAmount'] == 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                            }
                            else {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                            }
                        }
                        if (action == 'Selldel') {
                            if (_this.historyDetails[i]['creditAmount'] == 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                            }
                            else {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                            }
                        }
                        //end Buydel
                        //RollBack Code
                        if (action == 'Rollback' || action == 'rollback' || action == 'RollBack') {
                            if (_this.historyDetails[i].baseCurrency != 'usd' && _this.historyDetails[i].currency != 'usd') {
                                if ((_this.historyDetails[i]['creditAmount']) != 0) {
                                    if (_this.historyDetails[i].baseCurrency == '' || _this.historyDetails[i].baseCurrency == '-') {
                                        var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' '
                                            + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                                    }
                                }
                            }
                        }
                        //Rollback Code Ends
                        //BuyOffer Modify
                        if (action == 'Buymodify') {
                            if (_this.historyDetails[i].baseCurrency == 'usd' && _this.historyDetails[i]['creditAmount'] == 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                            }
                            else if (_this.historyDetails[i].baseCurrency == 'usd' && _this.historyDetails[i]['debitAmount'] == 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                            }
                            else if (_this.historyDetails[i].baseCurrency != 'usd' && _this.historyDetails[i]['creditAmount'] == 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                            }
                            else {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                            }
                        }
                        //BuyOffer Modify End
                        //SellOffer Modify
                        if (action == 'Sellmodify') {
                            if (_this.historyDetails[i].currency != 'usd' || _this.historyDetails[i]['creditAmount'] == 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].currency) + ' '
                                    + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                            }
                            else {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' '
                                    + _this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                            }
                        }
                        //SellOffer Modify End
                        if (action == 'Selloffer') {
                            var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' ' + _this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                        }
                        var hrefForTxn;
                        if (action == 'Send' || action == 'Sent') {
                            if (_this.historyDetails[i]['debitAmount'] != 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].currency) + _this.historyDetails[i]['debitAmount'] + ' Dr.';
                                if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                    if ((_this.historyDetails[i].currencyTxnid).length >= 10) {
                                        hrefForTxn = '<a target="_blank" href="https://blockexplorer.com/tx/' + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                    }
                                }
                            }
                        }
                        if (action == 'Received' || action == 'Receive') {
                            if (_this.historyDetails[i]['creditAmount'] != 0) {
                                var amount = '<span class="text-white">' + (_this.historyDetails[i].currency) + _this.historyDetails[i]['creditAmount'] + ' Cr.';
                                if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                    if ((_this.historyDetails[i].currencyTxnid).length >= 10) {
                                        hrefForTxn = '<a target="_blank" href="https://blockexplorer.com/tx/' + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                    }
                                }
                            }
                        }
                        if (action == 'Load') {
                            var amount = ' <span class="text-white">' + _this.data.CURRENCYICON + ' ' + _this.historyDetails[i].creditAmount + ' Cr.</span>';
                        }
                        if (action == 'Withdraw') {
                            var amount = ' <span class="text-white">' + _this.data.CURRENCYICON + ' ' + _this.historyDetails[i].debitAmount + ' Dr.</span>';
                        }
                        if (action == 'Decline') {
                            var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr. </span>';
                        }
                        if (action == 'Send Decline') {
                            var amount = '<span class="text-white">' + (_this.historyDetails[i].currency).toUpperCase() + ' ' + _this.historyDetails[i]['debitAmount'] + ' Dr. </span>';
                        }
                        if (_this.historyDetails[i].status == '0') {
                            status = 'Pending';
                            var statusClass = 'text-orange';
                        }
                        else if (_this.historyDetails[i].status == '1') {
                            status = 'Confirmed';
                            var statusClass = 'text-green';
                        }
                        else {
                            status = _this.historyDetails[i].action;
                            var statusClass = 'text-red';
                        }
                        // if (this.historyDetails[i].status == 'cancel') {
                        //   var status = 'Cancelled';
                        //   var statusClass = 'text-red';
                        // }
                        if (_this.historyDetails[i].orderid != null) {
                            var transactionId = _this.historyDetails[i].orderid;
                        }
                        else {
                            var transactionId = _this.historyDetails[i].transactionId;
                        }
                        if (action == 'Buyoffer') {
                            var action = 'Buy Offer';
                        }
                        if (action == 'Selloffer') {
                            var action = 'Sell Offer';
                        }
                        _this.historyTableTr += '<tr>';
                        _this.historyTableTr += '<td>' + timestamp + '</td>';
                        _this.historyTableTr += '<td>' + transactionId + '</td>';
                        _this.historyTableTr += '<td class="text-white">' + action + '</td>';
                        _this.historyTableTr += '<td>' + amount + '</td>';
                        _this.historyTableTr += '<td class="' + statusClass + '">' + status + '</td>';
                        _this.historyTableTr += '</tr>';
                    }
                    //   this.pagination(this.totalCount,this.noOfItemPerPage,'transactionHistory');
                }
                else {
                    _this.historyTableTr += '<tr><td colspan="5" class="text-center">No Data Exist</td></tr>';
                }
                $('.historyTableBody').html(_this.historyTableTr);
                //   console.log(this.historyTableTr);
                //all
                _this.pgn = [];
                for (i = 1; i <= Math.ceil(_this.totalCount / 20); i++) {
                    _this.pgn.push(i);
                }
                // console.log(this.pgn);
            }
        }, function (reason) {
            //   wip(0);
            _this.data.logout();
            if (reason.error.error == 'invalid_token') {
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    BodyService.prototype.getUserDetails = function () {
        var _this = this;
        this.loader = true;
        var userObj = {};
        userObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(userObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/GetUserDetails', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            //   wip(0);
            var result = response;
            // console.log('result+++++++',result);
            if (result.error.error_data != '0') {
            }
            else {
                _this.loader = false;
                _this.fullName = (result.userListResult[0].fullName);
                _this.editName = result.userListResult[0].fullName;
                _this.userName = result.userListResult[0].fullName;
                _this.address = result.userListResult[0].address;
                _this.country = result.userListResult[0].country;
                _this.email = result.userListResult[0].email;
                _this.editEmail = result.userListResult[0].email;
                _this.phone = result.userListResult[0].phone;
                if (localStorage.getItem('profile_pic') != '') {
                    _this.profilePic = _this.data.WEBSERVICE + '/user/' + localStorage.getItem('user_id') + '/file/' + result.userListResult[0].profilePic + '?access_token=' + localStorage.getItem('access_token');
                }
                else {
                    _this.profilePic = './assets/img/default.png';
                }
                _this.joinDate = _this.data.readable_timestamp(result.userListResult[0].created);
                _this.CurrencyBalance = result.userBalanceList;
                // if(result.userBalanceList !=null){
                //   for(var i=0;i<result.userBalanceList.length;i++){
                //     this.currencyId=result.userBalanceList.currencyId;
                //   //  this. =currencyCode;
                //   //    =closingBalance;
                //   //    =sendAccess;
                //   //    =receiveAccess;
                //   //    =totalBuy;
                //   //   = totalSell;
                //    }
                // }
            }
        }, function (reason) {
            //   wip(0);
            _this.data.logout();
            if (reason.error.error == 'invalid_token') {
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    BodyService.prototype.getCurrencyForSend = function (elem) {
        console.log('elem:' + elem);
        this.cryptoCurrency = elem;
        this.selectedCurrency = elem;
        if (this.userDocVerificationStatus() == true) {
            var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
            this.lockOutgoingTransactionStatus = userAppSettingsObj.lock_outgoing_transactions;
            if (this.lockOutgoingTransactionStatus == 1) {
                $('.sendOtpSection').show();
                // $('.get_Otp_btn').show();
                $('.send_btn').show();
            }
            else {
                $('.sendOtpSection').hide();
                // $('.get_Otp_btn').hide();
                $('.send_btn').show();
            }
            this.paybito_phone = '';
            this.paybito_amount = '';
            this.paybito_otp = '';
            this.other_address = '';
            this.other_amount = '';
            this.other_otp = '';
            $('#sendModal').modal('show');
            this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
            this.sendDisclaimer = this.environmentSettingListObj['send_other_min_value'][this.cryptoCurrency + '_description'];
            this.sendMiningDisclaimer = this.environmentSettingListObj['send_other_m_charges'][this.cryptoCurrency + '_description'];
        }
    };
    BodyService.prototype.getCurrencyForRecieve = function (currency) {
        // this.modalService.open(elem, { centered: true });
        // console.log('elem:' + currency);
        this.cryptoCurrency = currency;
        $('.receive_address_label, .receive_address, .recieve_qr_code').hide();
        $('.generate_address_btn').hide();
        $('#qr_code').html('');
        if (this.userDocVerificationStatus() == true) {
            this.generateAddress();
        }
    };
    BodyService.prototype.generateAddress = function () {
        var _this = this;
        var rcvObj = {};
        rcvObj['customerID'] = localStorage.getItem('user_id');
        rcvObj['crypto_currency'] = this.cryptoCurrency;
        var jsonString = JSON.stringify(rcvObj);
        // wip(1);
        if (this.cryptoCurrency != 'trigger') {
            this.http.post(this.data.WEBSERVICE + '/userTransaction/ReceiveBTC', jsonString, {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                }
            })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.rcvCode = result.customerkeysResult.fromadd;
                }
            });
        }
        else {
            this.http.post(this.data.WEBSERVICE + '/userTransaction/GetCounterPartyNewAddress', jsonString, {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                }
            })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.rcvCode = result.customerkeysResult.fromadd;
                }
            });
        }
    };
    BodyService.prototype.sessionExpiredLogout = function () {
        localStorage.clear();
        // window.location="login.html?reason=Session expired, please login again.";.
        this.route.navigateByUrl('/login');
    };
    BodyService.prototype.setInterval = function () {
        var _this = this;
        var userTransObj = {};
        userTransObj['customerId'] = localStorage.getItem('user_id');
        // userTransObj['crypto_currency'] = localStorage.getItem('selected_currency');
        var jsonString = JSON.stringify(userTransObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/userTransaction/GetUserBalance', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
                if (result.error.error_msg == '' || result.error.error_msg == null) {
                }
                else {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                if (result.error.error_msg == 'invalid_token') {
                    _this.sessionExpiredLogout();
                }
            }
            else {
                _this.totalFiatBalance = _this.data.CURRENCYICON + (result.userBalanceResult.fiat_balance).toFixed(2);
                _this.fiatBalanceLabel = 'Total ' + _this.data.CURRENCYNAME + ' Balance';
                if (result.userBalanceResult.btc_balance == 'null' || result.userBalanceResult.btc_balance == null) {
                    _this.btcBalance = '0';
                }
                else {
                    _this.btcBalance = result.userBalanceResult.btc_balance;
                }
                if (result.userBalanceResult.bch_balance == 'null' || result.userBalanceResult.bch_balance == null) {
                    _this.bchBalance = '0';
                }
                else {
                    _this.bchBalance = result.userBalanceResult.bch_balance;
                }
                if (result.userBalanceResult.hcx_balance == 'null' || result.userBalanceResult.hcx_balance == null) {
                    _this.hcxBalance = '0';
                }
                else {
                    _this.hcxBalance = result.userBalanceResult.hcx_balance;
                }
                if (result.userBalanceResult.iec_balance == 'null' || result.userBalanceResult.iec_balance == null) {
                    _this.iecBalance = '0';
                }
                else {
                    _this.iecBalance = result.userBalanceResult.iec_balance;
                }
                // etc
                if (result.userBalanceResult.etc_balance == 'null' || result.userBalanceResult.etc_balance == null) {
                    _this.etcBalance = '0';
                }
                else {
                    _this.etcBalance = result.userBalanceResult.etc_balance;
                }
                //bsv
                if (result.userBalanceResult.bsv_balance == 'null' || result.userBalanceResult.bsv_balance == null) {
                    _this.bsvBalance = '0';
                }
                else {
                    _this.bsvBalance = result.userBalanceResult.bsv_balance;
                }
                //
                if (result.userBalanceResult.diam_balance == 'null' || result.userBalanceResult.diam_balance == null) {
                    _this.diamBalance = '0';
                }
                else {
                    _this.diamBalance = result.userBalanceResult.diam_balance;
                }
                if (result.userBalanceResult.triggers_balance == 'null' || result.userBalanceResult.triggers_balance == null) {
                    _this.triggersBalance = '0';
                }
                else {
                    _this.triggersBalance = result.userBalanceResult.triggers_balance;
                }
                _this.buyPrice = result.userBalanceResult.crypto_buy_price;
                _this.btcBalanceInUsd = (parseFloat(_this.btcBalance) * parseFloat(_this.buyPrice)).toFixed(8);
                _this.bchBalanceInUsd = (parseFloat(_this.bchBalance) * parseFloat(_this.buyPrice)).toFixed(3);
                _this.hcxBalanceInUsd = (parseFloat(_this.hcxBalance) * parseFloat(_this.buyPrice)).toFixed(3);
                _this.iecBalanceInUsd = (parseFloat(_this.iecBalance) * parseFloat(_this.buyPrice)).toFixed(3);
                //new etc
                _this.etcBalanceInUsd = (parseFloat(_this.etcBalance) * parseFloat(_this.buyPrice)).toFixed(3);
                //bsv
                _this.bsvBalanceInUsd = (parseFloat(_this.bsvBalance) * parseFloat(_this.buyPrice)).toFixed(3);
                //diam
                _this.diamBalanceInUsd = (parseFloat(_this.diamBalance) * parseFloat(_this.buyPrice)).toFixed(3);
                // this.buyPrice=(this.buyPrice).toFixed(2);
                _this.sellPrice = result.userBalanceResult.crypto_sell_price;
                //this.sellPrice=(this.sellPrice).toFixed(2);
                _this.buyPriceText = _this.data.CURRENCYICON + ' ' + _this.buyPrice;
                _this.sellPriceText = _this.data.CURRENCYICON + ' ' + _this.sellPrice;
                _this.fiatBalance = result.userBalanceResult.fiat_balance;
                _this.fiatBalanceText = _this.data.CURRENCYICON + ' ' + result.userBalanceResult.fiat_balance;
                _this.selectedCryptoCurrency = localStorage.getItem('selected_currency');
                if (result['userBalanceResult'][_this.selectedCryptoCurrency + '_balance'] == null || result['userBalanceResult'][_this.selectedCryptoCurrency + '_balance'] == 'null') {
                    _this.selectedCryptoCurrencyBalance = '0';
                }
                else {
                    _this.selectedCryptoCurrencyBalance = result['userBalanceResult'][_this.selectedCryptoCurrency + '_balance'];
                }
                _this.btcBought = result.userBalanceResult.bitcoins_bought;
                _this.btcSold = result.userBalanceResult.bitcoins_sold;
                _this.bchBought = result.userBalanceResult.bitcoinCash_bought;
                _this.bchSold = result.userBalanceResult.bitcoinCash_sold;
                _this.hcxBought = result.userBalanceResult.hcx_bought;
                _this.hcxSold = result.userBalanceResult.hcx_sold;
                _this.iecBought = result.userBalanceResult.iec_bought;
                _this.iecSold = result.userBalanceResult.iec_sold;
                _this.ethSold = result.userBalanceResult.ethereum_sold;
                _this.ethBought = result.userBalanceResult.ethereum_bought;
                //new etcBought
                _this.etcSold = result.userBalanceResult.etc_sold;
                _this.etcBought = result.userBalanceResult.etc_Bought;
                //new bsv
                _this.bsvSold = result.userBalanceResult.bsv_sold;
                _this.bsvBought = result.userBalanceResult.bsv_Bought;
                _this.ltcBought = result.userBalanceResult.ltc_bought;
                _this.ltcSold = result.userBalanceResult.ltc_sold;
                //diam
                _this.diamBought = result.userBalanceResult.diam_bought;
                _this.diamSold = result.userBalanceResult.diam_sold;
            }
        }, function (reason) {
            //   wip(0);
            if (reason.error.error == 'invalid_token') {
                _this.data.logout();
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    BodyService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient, CoreDataService, Router])
    ], BodyService);
    return BodyService;
}());
export { BodyService };
//# sourceMappingURL=body.service.js.map