var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(modalService, route, data, http) {
        var _this = this;
        this.modalService = modalService;
        this.route = route;
        this.data = data;
        this.http = http;
        this.token = localStorage.getItem('access_token');
        this.time = 10;
        this.isNavbarCollapsed = true;
        this.sideMenu = [
            { name: 'Dashboard', alias: 'dashboard' },
            { name: 'Profile Details', alias: 'profile-details' },
            { name: 'My Wallet', alias: 'my-wallet' },
            { name: 'History', alias: 'history' },
            { name: 'Report', alias: 'report' },
            { name: 'Identity Verification', alias: 'identity-verification' },
            // { name:'Bank Details', alias:'bank-details'},
            { name: 'Promotions', alias: 'promotion' },
            { name: 'Settings', alias: 'settings' },
            { name: 'Support', alias: 'support' },
        ];
        this.upperMenu = [
            // {name:'My Wallet',alias:'my-wallet'},
            { name: 'Deposit Funds', alias: 'deposit-funds' },
            { name: 'Withdraw Funds', alias: 'withdraw-funds' },
        ];
        this.route.events.subscribe(function (val) {
            _this.currentRoute = val;
        });
    }
    NavbarComponent.prototype.ngOnInit = function () {
        if (this.route.url != '/dashboard') {
            // this.orderBook.source12.close();
            // this.orderBook.source2.close();
        }
        if (this.token == null) {
            this.route.navigateByUrl('/login');
        }
        this.userName = localStorage.getItem('user_name');
        this.userid = localStorage.getItem('user_id');
        this.act = localStorage.getItem('access_token');
        this.profilePic = localStorage.getItem('profile_pic');
        this.imageLink = (this.profilePic) ? this.data.WEBSERVICE + '/user/' + this.userid + '/file/' + this.profilePic + '?access_token=' + this.act : './assets/img/default.png';
        this.data.idleLogout();
    };
    NavbarComponent.prototype.open = function (content) {
        this.modalService.open(content);
    };
    NavbarComponent.prototype.slidebar = function () {
        // document.getElementById("exampleAccordion").style.width = "55px";
        var e = document.getElementById("exampleAccordion");
        if (e.style.width == '55px') {
            e.style.width = '230px';
        }
        else {
            e.style.width = '55px';
        }
    };
    NavbarComponent = __decorate([
        Component({
            selector: 'app-navbar',
            templateUrl: './navbar.component.html',
            styleUrls: ['./navbar.component.css']
        }),
        __metadata("design:paramtypes", [NgbModal, Router, CoreDataService, HttpClient])
    ], NavbarComponent);
    return NavbarComponent;
}());
export { NavbarComponent };
//# sourceMappingURL=navbar.component.js.map