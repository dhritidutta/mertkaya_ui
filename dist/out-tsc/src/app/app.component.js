var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { CoreDataService } from './core-data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { TradesComponent } from './trades/trades.component';
var AppComponent = /** @class */ (function () {
    function AppComponent(modalService, data, http, route, trade) {
        var _this = this;
        this.modalService = modalService;
        this.data = data;
        this.http = http;
        this.route = route;
        this.trade = trade;
        this.route.events.subscribe(function () {
            if (_this.route.url != '/dashboard') {
                // console.log('stop');
                clearInterval(_this.trade.tradeInterval);
            }
        });
        this.data.idleLogout();
    }
    AppComponent.prototype.warn = function (warn) {
        this.modalService.open(warn);
    };
    AppComponent = __decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        }),
        __metadata("design:paramtypes", [NgbModal, CoreDataService, HttpClient, Router, TradesComponent])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map