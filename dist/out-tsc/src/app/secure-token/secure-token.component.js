var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';
var SecureTokenComponent = /** @class */ (function () {
    function SecureTokenComponent(http, data, route) {
        this.http = http;
        this.data = data;
        this.route = route;
    }
    SecureTokenComponent.prototype.ngOnInit = function () {
    };
    //signup function
    SecureTokenComponent.prototype.secureToken = function (isValid) {
        var _this = this;
        if (isValid) {
            var tokenObj = {};
            tokenObj['email'] = this.email;
            var jsonString = JSON.stringify(tokenObj);
            // wip(1);
            //login webservice
            this.http.post(this.data.WEBSERVICE + '/user/ForgotPassword', jsonString, { headers: {
                    'Content-Type': 'application/json'
                } })
                .subscribe(function (response) {
                // wip(0);
                console.log(response);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.data.alert('Secure Token sent to registered email', 'success');
                    _this.route.navigateByUrl('/forget-password');
                }
            }, function (reason) {
                // wip(0);
                this.data.alert('Internal Server Error', 'danger');
            });
        }
        else {
            this.data.alert('Please provide valid email', 'warning');
        }
    };
    SecureTokenComponent = __decorate([
        Component({
            selector: 'app-secure-token',
            templateUrl: './secure-token.component.html',
            styleUrls: ['./secure-token.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient, CoreDataService, Router])
    ], SecureTokenComponent);
    return SecureTokenComponent;
}());
export { SecureTokenComponent };
//# sourceMappingURL=secure-token.component.js.map