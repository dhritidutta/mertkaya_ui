var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { BodyService } from '../body.service';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { CoreDataService } from '../core-data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
var ProfileDetailsComponent = /** @class */ (function () {
    function ProfileDetailsComponent(main, http, data, modalService) {
        this.main = main;
        this.http = http;
        this.data = data;
        this.modalService = modalService;
    }
    ProfileDetailsComponent.prototype.ngOnInit = function () {
        this.main.getDashBoardInfo();
        this.main.getUserDetails();
        this.main.getUserTransaction();
    };
    ProfileDetailsComponent.prototype.changeProfilePic = function () {
        var fd = new FormData();
        fd.append('userId', localStorage.getItem('user_id'));
        fd.append('profile_pic', $('.change_profile_pic')[0].files[0]);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/UpdateUserDocs', fd, { headers: {
                'Content-Type': undefined,
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
        }, function (reason) {
            // wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.alert('Could Not Connect To Server', 'danger');
            }
        });
    };
    ProfileDetailsComponent.prototype.resendOtpForOutgoing = function (content) {
        var _this = this;
        this.modalService.open(content, { centered: true });
        var otpObj = {};
        otpObj['email'] = localStorage.getItem('email');
        var jsonString = JSON.stringify(otpObj);
        this.http.post(this.data.WEBSERVICE + '/user/ResendOTP', jsonString, { headers: {
                'Content-Type': 'application/json'
            } })
            .subscribe(function (response) {
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                $('.send_btn').show();
                $('.get_Otp_btn').hide();
                _this.editName = _this.main.fullName;
                _this.editEmail = _this.main.email;
            }
        }, function (reason) {
            // wip(0);
            this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    ProfileDetailsComponent.prototype.updateUserProfileInfo = function () {
        var _this = this;
        // console.log('in update function');
        var userInfoObj = {};
        if (this.editName != undefined) {
            userInfoObj['full_name'] = this.editName;
        }
        else {
            userInfoObj['full_name'] = '';
        }
        if (this.editEmail != undefined) {
            userInfoObj['email'] = this.editEmail;
        }
        else {
            userInfoObj['email'] = '';
        }
        userInfoObj['otp'] = this.editOtp;
        userInfoObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(userInfoObj);
        //  wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/UpdateUserDetails', jsonString, { headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                $('#editProfileModal').modal('hide');
                _this.data.alert('Profile Update Successfully', 'success');
                localStorage.setItem('user_name', _this.editName);
                localStorage.setItem('email', _this.editEmail);
                _this.main.getUserDetails();
            }
        }, function (reason) {
            //  wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.alert('Could Not Connect To Server', 'danger');
            }
        });
    };
    ProfileDetailsComponent = __decorate([
        Component({
            selector: 'app-profile-details',
            templateUrl: './profile-details.component.html',
            styleUrls: ['./profile-details.component.css']
        }),
        __metadata("design:paramtypes", [BodyService, HttpClient, CoreDataService, NgbModal])
    ], ProfileDetailsComponent);
    return ProfileDetailsComponent;
}());
export { ProfileDetailsComponent };
//# sourceMappingURL=profile-details.component.js.map