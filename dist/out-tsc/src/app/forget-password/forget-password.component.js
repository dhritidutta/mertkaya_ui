var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';
var ForgetPasswordComponent = /** @class */ (function () {
    function ForgetPasswordComponent(http, data, route) {
        this.http = http;
        this.data = data;
        this.route = route;
        this.forgetpwdObj = {};
    }
    ForgetPasswordComponent.prototype.ngOnInit = function () {
    };
    ForgetPasswordComponent.prototype.confirmPassword = function (e) {
        if ((this.forgetpwdObj.password != '' && this.forgetpwdObj.password != undefined) &&
            (this.forgetpwdObj.repassword1 != '' && this.forgetpwdObj.repassword1 != undefined)) {
            if (this.forgetpwdObj.password == this.forgetpwdObj.repassword1) {
                $('.confirm_password_text').html('Password Matched');
                $('.confirm_password_text').css('color', 'lightgreen');
                $('#submit_btn').removeAttr('disabled');
            }
            else {
                $('.confirm_password_text').html('Password  Mismatched');
                $('.confirm_password_text').css('color', 'red');
                $('#submit_btn').attr('disabled', 'disabled');
            }
        }
        else {
        }
    };
    ForgetPasswordComponent.prototype.checkPassword = function () {
        var password = this.forgetpwdObj.password;
        if (password != '' && password != undefined && password.length >= 8) {
            var passwordStatus = this.checkAlphaNumeric(password);
            if (passwordStatus == false) {
                this.data.alert('Password should have atleast one upper case letter, one lowercase letter , one special case and one number', 'warning');
            }
            else {
            }
        }
        else {
            this.data.alert('Password should be minimum 8 Charecter', 'warning');
        }
    };
    ForgetPasswordComponent.prototype.checkAlphaNumeric = function (string) {
        if (string.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)) {
            return true;
        }
        else {
            return false;
        }
    };
    ForgetPasswordComponent.prototype.forgotPassword = function (isValid) {
        var _this = this;
        if (isValid) {
            // $('.submit_btn').removeAttr('disabled');
            //  var forgotPasswordObj={};
            //  forgotPasswordObj['email']=this.email;
            //  forgotPasswordObj['password']=this.password;
            //  forgotPasswordObj['secure_token']=this.secure_token;
            //  var jsonString=JSON.stringify(forgotPasswordObj);
            //  wip(1);
            //login webservice
            var jsonString = JSON.stringify(this.forgetpwdObj);
            this.http.post(this.data.WEBSERVICE + '/user/ResetPassword', jsonString, { headers: {
                    'Content-Type': 'application/json'
                } })
                .subscribe(function (response) {
                //  wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    /* $.confirm({
                       title: 'Success',
                       content: 'Password successfully reset',
                       type: 'green',
                       buttons: {
                           ok: function () {
                              window.location='login.html';
                       }
                     }
                   });*/
                    _this.data.alert('Password successfully reset', 'success');
                    _this.route.navigateByUrl('/login');
                }
            }, function (reason) {
                //  wip(0);
                this.data.alert('Internal Server Error', 'danger');
            });
        }
        else {
            //$('.submit_btn').attr('disabled','disabled');
            this.data.alert('Please provide valid email', 'warning');
        }
    };
    ForgetPasswordComponent.prototype.showHide = function () {
        $(".showHide-password").each(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            }
            else {
                input.attr("type", "password");
            }
        });
    };
    ForgetPasswordComponent = __decorate([
        Component({
            selector: 'app-forget-password',
            templateUrl: './forget-password.component.html',
            styleUrls: ['./forget-password.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient, CoreDataService, Router])
    ], ForgetPasswordComponent);
    return ForgetPasswordComponent;
}());
export { ForgetPasswordComponent };
//# sourceMappingURL=forget-password.component.js.map