var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { TradesComponent } from "../trades/trades.component";
import * as $ from "jquery";
import { CoreDataService } from "../core-data.service";
import { Router } from "@angular/router";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';
var OrderBookComponent = /** @class */ (function () {
    // session:any = localStorage.getItem('access_token');
    function OrderBookComponent(data, route, dash, http, trade) {
        this.data = data;
        this.route = route;
        this.dash = dash;
        this.http = http;
        this.trade = trade;
        this.any = 0;
        this.chartlist = 0;
        this.ctpdata = 0;
        this._todos = new BehaviorSubject([]);
        this.dataStore = { todos: [] };
        this.todos = this._todos.asObservable();
        switch (this.data.environment) {
            case "live":
                this.orderBookUrl = "https://mainnet.tomya.com/"; //live
                break;
            case "dev":
                this.orderBookUrl =
                    ""; //dev
                break;
            case "uat":
                this.orderBookUrl = "";
                //"http://ec2-52-8-41-112.us-west-1.compute.amazonaws.com:8000/"; //uat
                break;
            case "demo":
                this.orderBookUrl = "https://mainnet.tomya.com/"; //demo
                break;
        }
        // $("#orderbookBidBody").html(this.bidBody);
    }
    // marketTradeBodyHtml: any;
    // issuerLink: any =
    //   "./assets/appdata/assetIssuer." + this.data.environment + ".json";
    OrderBookComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName;
        this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName;
        this.currency_code = 'BTC';
        this.base_currency = 'TRY';
        this.http
            .get(this.data.CHARTSERVISE +
            "trendsTradeGraphFor24Hours/" +
            this.currency_code +
            "/" +
            this.base_currency)
            .subscribe(function (value) {
            if (value != "") {
                _this.chartlist = value[0];
                var randomNoForFlashArr = [];
                var arraylength = 2;
                _this.ctpdata = _this.chartlist.CTP;
                _this.lowprice = _this.chartlist.LOW_PRICE;
                _this.highprice = _this.chartlist.HIGH_PRICE;
                var randomNo = _this.randomNoForOrderBook(0, arraylength);
                randomNoForFlashArr.push(randomNo);
                // console.log("DDDDDDDDDDDDD"+randomNo);
                for (var i = 0; i < arraylength; i++) {
                    if (!$.inArray(i, randomNoForFlashArr)) {
                        if (i == 0) {
                            document.getElementById("pricered").style.display =
                                "inline-block";
                            document.getElementById("pricegreen").style.display = "none";
                        }
                        else {
                            document.getElementById("pricered").style.display = "none";
                            document.getElementById("pricegreen").style.display =
                                "inline-block";
                        }
                    }
                }
            }
        });
        this.serverSentEventForOrderbookAsk();
        //this.serverSentEventForOrderbookBid();
        this.getMarketTradeBuy();
    };
    OrderBookComponent.prototype.changemode = function () {
        // console.log('++++++++++++++',this.data.changescreencolor);
        if (this.data.changescreencolor == true) {
            $(".bg_new_class")
                .removeClass("bg-dark")
                .css("background-color", "#fefefe");
            $(".sp-highlow").css("background-color", "#d3dddd");
            $(".sp-highlow").css("color", "Black");
            $(".border-col").css("border-color", "#d3dddd");
            $("th").css({ "background-color": "#d3dddd", color: "#273338" });
            $(".text-left").css("color", "black");
            $(".text-right").css("color", "black");
        }
        else {
            $(".bg_new_class")
                .removeClass("bg-dark")
                .css("background-color", "#16181a");
            $(".sp-highlow").css("background-color", "#273338");
            $(".sp-highlow").css("color", "yellow");
            $(".border-col").css("border-color", "#273338");
            $("th").css({ "background-color": "#273338", color: "#d3dddd" });
            $(".text-left")
                .css("color", "")
                .css("color", "rgb(153, 152, 152)");
            $(".text-right").css("color", "rgb(153, 152, 152)");
        }
    };
    OrderBookComponent.prototype.ngDoCheck = function () {
        this.changemode();
        var randomNoForFlashArr = [];
        var arraylength = 2;
        var valcurrency = this.data.ctpdata;
        if (valcurrency == undefined) {
            this.ctpdata = this.chartlist.CTP;
            this.lowprice = this.chartlist.LOW_PRICE;
            this.highprice = this.chartlist.HIGH_PRICE;
            // console.log("data++++++++1", this.ctpdata, this.lowprice, this.highprice);
        }
        else {
            this.ctpdata = this.data.ctpdata;
            this.lowprice = this.data.lowprice;
            this.highprice = this.data.highprice;
            //console.log('data2++++++++',    this.ctpdata,this.lowprice, this.highprice)
        }
        var randomNo = this.randomNoForOrderBook(0, arraylength);
        randomNoForFlashArr.push(randomNo);
        for (var i = 0; i < arraylength; i++) {
            if (!$.inArray(i, randomNoForFlashArr)) {
                if (i == 0) {
                    document.getElementById("pricered").style.display = "inline-block";
                    document.getElementById("pricegreen").style.display = "none";
                }
                else {
                    document.getElementById("pricered").style.display = "none";
                    document.getElementById("pricegreen").style.display = "inline-block";
                }
            }
        }
    };
    OrderBookComponent.prototype.getNewCurrency = function () {
        var _this = this;
        this.http.get(this.data.WEBSERVICE + '/userTrade/GetCurrencyDetails')
            .subscribe(function (responseCurrency) {
            _this.filterCurrency = responseCurrency;
            var mainArr = [];
            _this.header = _this.filterCurrency.Header;
            _this.assets = _this.filterCurrency.Values;
        });
    };
    OrderBookComponent.prototype.buySellCode = function (item) {
        var _this = this;
        //   (item.currency=="BCC")?this.currency_code ="BCH":this.currency_code = item.currency; //changed by sanu
        this.currency_code = item.currencyCode;
        this.base_currency = item.baseCurrency;
        this.data.selectedSellingAssetText = this.base_currency;
        this.data.selectedBuyingAssetText = this.currency_code;
        this.data.selectedBuyingCryptoCurrencyName = this.base_currency + this.currency_code;
        this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
        this.selectedBuyingAssetText = item.currencyCode;
        this.selectedSellingAssetText = item.baseCurrency;
        // this.data.cur =this.data.selectedBuyingAssetText === "TRY"
        //           ? "$"
        //           : this.data.selectedSellingAssetText + " ";
        localStorage.setItem("buying_crypto_asset", this.currency_code.toLocaleLowerCase());
        localStorage.setItem("selling_crypto_asset", this.base_currency.toLocaleLowerCase());
        this.data.cur = this.selectedSellingAssetText;
        //localStorage.setItem("CurrencyID", this.currencyId);//change bu sanu
        this.assetpairsell = item.currencyCode + item.baseCurrency;
        this.assetpairbuy = item.baseCurrency + item.currencyCode;
        this.data.selectedSellingCryptoCurrencyissuer = "";
        this.data.selectedBuyingCryptoCurrencyissuer = "";
        this.http.get(this.data.WEBSERVICE + '/userTrade/GetAssetIssuer')
            .subscribe(function (responseBuySell) {
            _this.responseBuySell = responseBuySell;
            var x = _this.responseBuySell.length;
            for (var i = 0; i < x; i++) {
                if (_this.assetpairsell == _this.responseBuySell[i].assetPair) {
                    _this.data.selectedSellingCryptoCurrencyissuer = _this.responseBuySell[i].issuer;
                }
                else if (_this.assetpairbuy == _this.responseBuySell[i].assetPair) {
                    _this.data.selectedBuyingCryptoCurrencyissuer = _this.responseBuySell[i].issuer;
                }
                else if (_this.data.selectedSellingCryptoCurrencyissuer != "" && _this.data.selectedBuyingCryptoCurrencyissuer != "") {
                    break;
                }
            }
            _this.serverSentEventForOrderbookAsk();
            _this.getMarketTradeBuy();
            _this.trade.reload();
            _this.trade.myTradeDisplay(0);
            //  this.trade.GetTradeDetail();
            //  this.trade.getOfferLists();
            //  this.trade.getStopLossOfferForBuy();
            //  this.trade.getStopLossOfferForSell();
            $('#dropHolder').css('overflow', 'scroll');
            $(window).resize(function () {
                var wd = $('#chartHolder').width();
                $('#dropHolder').width(wd);
                $('#dropHolder').css('overflow', 'scroll');
            });
            _this.http.get(_this.data.CHARTSERVISE + '/trendsTradeGraphFor24Hours/' + _this.currency_code + '/' + _this.base_currency)
                .subscribe(function (value) {
                if (value != '') {
                    _this.chartlist = value[0];
                    _this.ctpdata = _this.data.ctpdata = _this.chartlist.CTP;
                    _this.lowprice = _this.data.lowprice = _this.chartlist.LOW_PRICE;
                    _this.highprice = _this.data.highprice = _this.chartlist.HIGH_PRICE;
                    _this.act = _this.data.ACTION = _this.chartlist.ACTION;
                    // console.log('ctpdata_______________', this.act, this.ctpdata);
                    _this.data.rocdata = _this.chartlist.ROC.toFixed(2);
                    if (_this.data.rocdata > 0) {
                        //  this.negetive =
                        _this.rocreact = true;
                        //alert ("Hi");//this.rocreact = true}
                    }
                    else {
                        _this.data.rocreact = false;
                    }
                    if (_this.data.rocdata < 0) {
                        _this.data.negetive = true;
                    }
                    else {
                        _this.data.negetive = false;
                    }
                    //this.droc = this.rocdata.
                    _this.data.volumndata = _this.chartlist.VOLUME.toFixed(2);
                    //  if (this.rocdata >= 0){this.rocreact = true}
                    //this.droc = this.rocdata.
                    if (_this.data.rocdata >= 0) {
                        _this.rocreact = true;
                    }
                    if (_this.data.act == 'sell') {
                        _this.data.react = true;
                    }
                    else {
                        _this.data.react = false;
                    }
                }
                else {
                    _this.ctpdata = 0;
                    _this.lowprice = 0;
                    _this.highprice = 0;
                    _this.rocdata = 0;
                    _this.volumndata = 0;
                    // alert(123);
                    // console.log(123);
                }
            });
        });
    };
    //random function for flash class in server sent orderbook
    OrderBookComponent.prototype.randomNoForOrderBook = function (minVal, maxVal) {
        var minVal1 = parseInt(minVal);
        var maxVal1 = parseInt(maxVal);
        return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
    };
    OrderBookComponent.prototype.getMarketTradeBuy = function () {
        var _this = this;
        $('#marketTradeBody').html('<tr><td colspan="3" class="text-center"><img src="./assets/svg-loaders/three-dots.svg" alt="" width="50"></td></tr>');
        var result;
        this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
        this.selectedSellingAssetText = this.data.selectedSellingAssetText;
        this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
        this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer; //'GBDWSH2SAZM3U5FR6LPO4E2OQZNVZXUAZVH5TWEJD64MGYJJ7NWF4PBX';
        var url = 'https://api.tomya.com/StreamApi/rest/TradeHistory?baseAssetCode=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase() + '&baseAssetIssuer=' + this.sellingAssetIssuer + '&counterAssetCode=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&counterAssetIssuer=' + this.buyingAssetIssuer + '&order=desc' + '&limit=' + 50;
        if (this.source != undefined) {
            this.source.close();
        }
        this.source = new EventSource(url);
        this.source.addEventListener('message', function (event) {
            result = JSON.parse(event.data);
            var response = JSON.parse(result.response);
            _this.marketTradeRecords = response._embedded.records;
            _this.itemcount = _this.marketTradeRecords.length;
            var arraylength = _this.marketTradeRecords.length;
            if (arraylength != null) {
                _this.marketTradeBodyHtml = '';
                var randomNoForFlashArr = [];
                var randomNo = _this.randomNoForOrderBook(0, 10);
                randomNoForFlashArr.push(randomNo);
                for (var i = 0; i < _this.marketTradeRecords.length; i++) {
                    var className = "text-green";
                    if (!$.inArray(i, randomNoForFlashArr)) {
                        if (randomNo % 2 == 0) {
                            var className = 'text-red';
                        }
                        else if (randomNo % 2 != 0) {
                            var className = 'text-red';
                        }
                    }
                    var timeStampString = _this.marketTradeRecords[i].ledger_close_time;
                    var timeStampStringArr = timeStampString.split('T');
                    var date = timeStampStringArr[0];
                    var time = (timeStampStringArr[1]).slice(0, -1);
                    var amount = parseFloat(_this.marketTradeRecords[i].base_amount);
                    var price = parseFloat(_this.marketTradeRecords[i].counter_amount) / parseFloat(_this.marketTradeRecords[i].base_amount);
                    //this.marketTradeBodyHtml+='<tr class="' + className + '">';
                    _this.marketTradeBodyHtml += '<tr class="text-ash">';
                    _this.marketTradeBodyHtml += '<td class="text-left">' + time + '</td>';
                    // this.marketTradeBodyHtml+='<td>'+ this.marketTradeRecords[i].base_asset_code+'</td>';
                    if (_this.selectedSellingAssetText == 'eur') {
                        _this.marketTradeBodyHtml += '<td class="text-right">' + parseFloat(amount) + '</td>';
                        _this.marketTradeBodyHtml += '<td class="' + className + ' right">' + (parseFloat(price)).toFixed(4) + '</td>';
                    }
                    else {
                        _this.marketTradeBodyHtml += '<td class="text-right">' + (parseFloat(amount)).toFixed(8) + '</td>';
                        _this.marketTradeBodyHtml += '<td class="' + className + ' right">' + (parseFloat(price)).toFixed(8) + '</td>';
                    }
                    _this.marketTradeBodyHtml += '</tr>';
                    //  console.log(this.marketTradeBodyHtml);
                    //}
                }
                // this.getMarketTradeBuy();
                //
            }
            $('#marketTradeBody').html(_this.marketTradeBodyHtml);
            if (_this.marketTradeRecords.length == 0)
                $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
        }, function (error) {
            $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
        });
    };
    OrderBookComponent.prototype.serverSentEventForOrderbookAsk = function () {
        var _this = this;
        this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
        this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
        this.token = localStorage.getItem('access_token');
        // alert(this.data.selectedSellingCryptoCurrencyName+"_____"+this.data.selectedBuyingCryptoCurrencyName)
        var url = "https://api.tomya.com/StreamApi/rest/OrderBook?sellingAssetCode=" + this.data.selectedSellingCryptoCurrencyName + "&sellingAssetIssuer=" + this.sellingAssetIssuer + "&buyingAssetCode=" + this.data.selectedBuyingCryptoCurrencyName + "&buyingAssetIssuer=" + this.buyingAssetIssuer + "&limit=50";
        if (this.data.source2 != undefined) {
            this.data.source2.close();
        }
        if (this.token.length >= "null" || this.token.length >= 0) {
            this.data.source2 = new EventSource(url);
            this.urlAsk = url;
            var result = new Object();
            this.data.source2.onmessage = function (event) {
                result = event.data;
                result = JSON.parse(event.data);
                var response = JSON.parse(result.response);
                var askdata = [];
                askdata = response.asks;
                var biddata = [];
                biddata = response.bids;
                var bidHtml = "";
                var askHtml = "";
                if (askdata.length != 0) {
                    if (askdata.length > 5) {
                        var askLength = askdata.length;
                    }
                    else {
                        var askLength = askdata.length;
                    }
                    var randomNoForFlashArr = [];
                    var randomNo = _this.randomNoForOrderBook(0, askdata.length);
                    randomNoForFlashArr.push(randomNo);
                    for (var i = askLength - 1; i >= 0; i--) {
                        // console.log(result.asks[i].amount);
                        var className = "";
                        if (!$.inArray(i, randomNoForFlashArr)) {
                            var className = "bg-flash-red";
                            //console.log(i+' in array');
                        }
                        if (parseFloat(askdata[i].amount).toFixed(4) != "0.0000") {
                            askHtml += '<tr class="' + className + '">';
                            askHtml +=
                                '<td class="text-left">' +
                                    parseFloat(askdata[i].amount).toFixed(4) +
                                    "</td>";
                            if (_this.data.selectedSellingCryptoCurrencyName == "usd") {
                                askHtml +=
                                    '<td class="text-red text-right">' +
                                        parseFloat(askdata[i].price).toFixed(4) +
                                        "</td>";
                            }
                            else {
                                askHtml +=
                                    '<td class="text-red text-right">' +
                                        parseFloat(askdata[i].price).toFixed(8) +
                                        "</td>";
                            }
                            askHtml +=
                                '<td class="text-right">' +
                                    (askdata[i].amount * askdata[i].price).toFixed(4) +
                                    "</td>";
                            askHtml += "</tr>";
                        }
                        else {
                        }
                    }
                }
                else {
                    askHtml += "<tr>";
                    askHtml += '<td class="text-white" colspan="2">No Data</td>';
                    askHtml += "</tr>";
                }
                _this.askBody = askHtml;
                $("#orderbookAskBody").html(_this.askBody);
                // Bid Order Book Code Start
                if (biddata.length != 0) {
                    if (biddata.length > 5) {
                        var bidLength = biddata.length;
                    }
                    else {
                        var bidLength = biddata.length;
                    }
                    var randomNoForFlashArr = [];
                    var randomNo = _this.randomNoForOrderBook(0, biddata.length);
                    randomNoForFlashArr.push(randomNo);
                    for (var i = 0; i < bidLength; i++) {
                        // console.log(result.asks[i].amount);
                        var className = "";
                        if (!$.inArray(i, randomNoForFlashArr)) {
                            var className = "bg-flash-red";
                            //console.log(i+' in array');
                        }
                        if (parseFloat(biddata[i].amount).toFixed(4) != "0.0000") {
                            bidHtml += '<tr class="' + className + '">';
                            bidHtml +=
                                '<td class="text-left">' +
                                    parseFloat(biddata[i].amount).toFixed(4) +
                                    "</td>";
                            if (_this.data.selectedSellingCryptoCurrencyName == "usd") {
                                bidHtml +=
                                    '<td class="text-green text-right">' +
                                        parseFloat(biddata[i].price).toFixed(4) +
                                        "</td>";
                            }
                            else {
                                bidHtml +=
                                    '<td class="text-green text-right">' +
                                        parseFloat(biddata[i].price).toFixed(8) +
                                        "</td>";
                            }
                            bidHtml +=
                                '<td class="text-right">' +
                                    (biddata[i].amount * biddata[i].price).toFixed(4) +
                                    "</td>";
                            bidHtml += "</tr>";
                        }
                        else {
                        }
                    }
                }
                else {
                    bidHtml += "<tr>";
                    bidHtml += '<td class="text-white" colspan="2">No Data</td>';
                    bidHtml += "</tr>";
                }
                _this.bidBody = bidHtml;
                $("#orderbookBidBody").html(_this.bidBody);
            };
        }
        else {
            this.data.source2.close();
        }
    };
    OrderBookComponent.prototype.ngOnDestroy = function () {
        if (this.data.source2 != undefined && this.source != undefined) {
            this.data.source2.close();
            this.source.close();
        }
    };
    OrderBookComponent = __decorate([
        Component({
            selector: "app-order-book",
            templateUrl: "./order-book.component.html",
            styleUrls: ["./order-book.component.css"]
        }),
        __metadata("design:paramtypes", [CoreDataService,
            Router,
            DashboardComponent,
            HttpClient,
            TradesComponent])
    ], OrderBookComponent);
    return OrderBookComponent;
}());
export { OrderBookComponent };
//# sourceMappingURL=order-book.component.js.map