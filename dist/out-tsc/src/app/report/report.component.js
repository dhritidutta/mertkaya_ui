var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import * as $ from 'jquery';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';
var ReportComponent = /** @class */ (function () {
    function ReportComponent(http, data) {
        this.http = http;
        this.data = data;
        this.reportarray = "selectreport";
        this.selectyear = "fyear";
        this.selectpdf = "selectoption";
        this.downloadUrl = 'javascript:void(0)';
        this.buttonName = 'Show';
        /*****date picker max and min date*******/
        this.currentDate = new Date().toISOString().slice(0, 10);
        this.currentYear = new Date().getFullYear();
        this.currentMonth = (new Date().getMonth()) + 1;
        this.currentDay = new Date().getDate();
        /*********for showing export csv file button*******/
        this.showExportButton = 0;
        /********pagination related variables**********/
        this.totalCount = 0;
        this.pageNo = 1;
        this.noOfItemsPerPage = 10;
        this.showingRowValue = 10;
        this.showPrevButton = 0;
        this.showNextButton = 0;
        this.pagiArr = [];
    }
    ReportComponent.prototype.ngOnInit = function () {
        this.getreportdata();
        this.financialyear();
    };
    ReportComponent.prototype.reset = function () {
        this.fromdate = this.fromdate = this.fromdate = '';
        this.reportarray = this.selectyear = '';
        this.todate = this.todate = this.todate = '';
        this.model = this.model = this.model = '';
        $(function () {
            $('input.form-control').val('');
        });
    };
    ReportComponent.prototype.fundBuy = function (content) {
        if (this.reportarray && this.fromdate && this.todate) {
            open(content);
        }
        else {
            alert("Please Fillup all the given field");
        }
    };
    ReportComponent.prototype.getreportdata = function () {
        var _this = this;
        this.http.get(this.data.REPORTSERVISE + 'viewAllMaster')
            .subscribe(function (response) {
            _this.reportkeys = Object.keys(response);
            //this.reportlist = Object.values(response);
            _this.reportlist = (response);
            //  console.log("funding home view response: "+this.reportlist);
        });
    };
    ReportComponent.prototype.finddata = function (datastore, reportarray) {
        //if statement date formal
        var _this = this;
        var usr = localStorage.getItem('user_id');
        //alert(this.reportarray.values);
        this.repu = reportarray;
        var ds = this.fromdate;
        var dp = this.todate;
        if (ds == undefined && dp == undefined) {
            this.sumfdt = '1' + '1' + this.selectyear;
            this.sumtdt = '31' + '12' + this.selectyear + 1;
        }
        else {
            this.fromDate = ds.day + '-' + ds.month + '-' + ds.year;
            this.toDate = dp.day + '-' + dp.month + '-' + dp.year;
            this.sumfdt = ds.month + ds.year;
            this.sumtdt = dp.month + dp.year;
        }
        if (this.repu == 6) {
            this.sumfdt = '0';
            this.sumtdt = '1';
        }
        // alert(this.sumtdt+'  '+this.sumfdt)
        if ((this.sumtdt > this.sumfdt) || (this.sumfdt == this.sumtdt && dp.day > ds.day) || (dp.year > ds.year)) {
            if (this.repu != 6) {
                this.http.get(this.data.REPORTSERVISE + 'viewRepotDetails/' + usr + '/' + reportarray + '/' + this.fromDate + '/' + this.toDate + '/' + 2018 + '/' + this.pageNo)
                    .subscribe(function (response) {
                    //Table Header
                    _this.headerdata = Object.keys(response);
                    _this.keyval = Object.values(response[0]);
                    _this.datastore = _this.keyval[0];
                    _this.fundingheader = _this.keyval[1];
                    //this.results = Object.keys(this.fundingheader[1]);
                    _this.results = Object.keys(_this.fundingheader);
                    _this.showExportButton = 1;
                    _this.totalCount = response[0]['Page count'][0];
                    _this.pagiArr = [];
                    var limit = _this.totalCount;
                    for (var i = 1; i <= limit; i++) {
                        if (i <= 7) {
                            _this.pagiArr.push(i);
                        }
                    }
                    //console.log(this.pagiArr);
                    _this.pagiLength = (_this.pagiArr).length;
                });
            }
            else {
                this.fromDate = '0';
                this.toDate = '0';
                this.http.get(this.data.REPORTSERVISE + 'viewRepotDetails/' + usr + '/' + reportarray + '/' + this.fromDate + '/' + this.toDate + '/' + this.selectyear + '/' + this.pageNo)
                    .subscribe(function (response) {
                    //Table Header
                    _this.headerdata = Object.keys(response);
                    //alert(this.headerdata);
                    _this.keyval = Object.values(response[0]);
                    _this.datastore = _this.keyval[0];
                    //console.log(datastore);
                    _this.fundingheader = _this.keyval[1];
                    _this.results = Object.keys(_this.fundingheader);
                    _this.showExportButton = 1;
                    _this.totalCount = response[0]['Page count'][0];
                    _this.pagiArr = [];
                    var limit = _this.totalCount;
                    for (var i = 1; i <= limit; i++) {
                        if (i <= 7) {
                            _this.pagiArr.push(i);
                        }
                    }
                    //console.log(this.pagiArr);
                    _this.pagiLength = (_this.pagiArr).length;
                });
            }
            if (this.repu != 6) {
                this.downloadUrl = this.data.REPORTSERVISE + 'viewRepotDetails/' + localStorage.getItem('user_id') + '/' + this.repu + '/' + this.fromDate + '/' + this.toDate + '/2018';
            }
            else {
                this.downloadUrl = this.data.REPORTSERVISE + 'viewRepotDetails/' + localStorage.getItem('user_id') + '/' + this.repu + '/0/0/' + this.selectyear;
            }
        }
        else {
            alert("From Date is greater than To date");
            this.sumtdt = '';
            this.sumfdt = '';
        }
    };
    ReportComponent.prototype.OnChange = function (val) {
        // CHANGE THE NAME OF THE BUTTON.
        // console.log(val);
        if (val == 6)
            this.show = 1;
        else {
            this.show = 0;
        }
    };
    /********function defination for explode csv*******/
    ReportComponent.prototype.exportTableToCSV = function (table, filename) {
        var $headers = $(table).find('tr:has(th)'), $rows = $(table).find('tr:has(td)')
        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        , tmpColDelim = String.fromCharCode(11) // vertical tab character
        , tmpRowDelim = String.fromCharCode(0) // null character
        // actual delimiter characters for CSV format
        , colDelim = '","', rowDelim = '"\r\n"';
        // Grab text from table into CSV formatted string
        var csv = '"';
        csv += formatRows($headers.map(grabRow));
        csv += rowDelim;
        csv += formatRows($rows.map(grabRow)) + '"';
        // Data URI
        var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
        $('#downloadCSVBtn')
            .attr({
            'download': filename,
            'href': csvData
            //,'target' : '_blank' //if you want it to open in a new window
        });
        //------------------------------------------------------------
        // Helper Functions
        //------------------------------------------------------------
        // Format the output so it has the appropriate delimiters
        function formatRows(rows) {
            return rows.get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim);
        }
        // Grab and format a row from the table
        function grabRow(i, row) {
            var $row = $(row);
            //for some reason $cols = $row.find('td') || $row.find('th') won't work...
            var $cols = $row.find('td');
            if (!$cols.length)
                $cols = $row.find('th');
            return $cols.map(grabCol)
                .get().join(tmpColDelim);
        }
        // Grab and format a column from the table
        function grabCol(j, col) {
            var $col = $(col), $text = $col.text();
            return $text.replace('"', '""'); // escape double quotes
        }
    };
    /***********previous function defination for pagination ********/
    ReportComponent.prototype.showPreviousList = function () {
        this.pageNo = this.pageNo - 1;
        this.finddata(this.datastore, this.reportarray);
    };
    /***********next function defination for pagination ********/
    ReportComponent.prototype.showNextList = function () {
        this.pageNo = this.pageNo + 1;
        this.finddata(this.datastore, this.reportarray);
    };
    ReportComponent.prototype.financialyear = function () {
        var _this = this;
        this.http.get(this.data.REPORTSERVISE + 'financialYearList')
            .subscribe(function (response) {
            _this.fiancialyear = response;
            // alert(this.fiancialyear);
        });
    };
    /****************get pagination number*******/
    ReportComponent.prototype.pagi = function (num) {
        this.pageNo = parseInt(num);
        this.finddata(this.datastore, this.reportarray);
    };
    /**************down load csv from backend***********/
    ReportComponent.prototype.downloadCsvFromBackend = function () {
        //alert(this.repu);
        if (this.repu != 6) {
            var downloadUrl = this.data.REPORTSERVISE + 'viewRepotDetails/' + localStorage.getItem('user_id') + '/' + this.repu + '/' + this.fromDate + '/' + this.toDate + '/2018';
        }
        else {
            var downloadUrl = this.data.REPORTSERVISE + 'viewRepotDetails/' + localStorage.getItem('user_id') + '/' + this.repu + '/0/0/' + this.selectyear;
        }
        this.http.get(downloadUrl)
            .subscribe(function (response) {
        });
    };
    ReportComponent = __decorate([
        Component({
            selector: 'app-report',
            templateUrl: './report.component.html',
            styleUrls: ['./report.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient,
            CoreDataService])
    ], ReportComponent);
    return ReportComponent;
}());
export { ReportComponent };
//# sourceMappingURL=report.component.js.map