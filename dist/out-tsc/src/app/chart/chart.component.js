var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { OrderBookComponent } from "../order-book/order-book.component";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { TradesComponent } from "../trades/trades.component";
import * as $ from "jquery";
import { DashboardComponent } from "../dashboard/dashboard.component";
var ChartComponent = /** @class */ (function () {
    function ChartComponent(http, data, orderBook, stoploss, trade, dash) {
        this.http = http;
        this.data = data;
        this.orderBook = orderBook;
        this.stoploss = stoploss;
        this.trade = trade;
        this.dash = dash;
        this.url = null;
        this.indicatorStatus = 0;
        this.indicatorName = "";
        this.currencyListEur = 0;
        this.currencyListBtc = 0;
        this.currencyListEth = 0;
        this.currencyListUsd = 0;
        this.chartD = "d";
        this.dateD = "1m";
        this.ctpdata = 0;
        this.lowprice = 0;
        this.highprice = 0;
        this.rocdata = 0;
        this.volumndata = 0;
        this.testval = [];
    }
    ChartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currency_code = 'BTC';
        this.base_currency = 'TRY';
        localStorage.setItem("buying_crypto_asset", 'BTC');
        localStorage.setItem("selling_crypto_asset", 'TRY');
        this.data.selectedSellingAssetText = this.base_currency;
        this.data.selectedBuyingAssetText = this.currency_code;
        this.data.cur = this.data.selectedBuyingAssetText === "TRY"
            ? "$"
            : this.data.selectedSellingAssetText + " ";
        this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
        this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName = this.base_currency + this.currency_code;
        this.selectedBuyingAssetText = this.data.selectedBuyingAssetText = this.currency_code;
        this.selectedSellingAssetText = this.data.selectedSellingAssetText = this.base_currency;
        this.selectedCryptoCurrencySymbol = this.data.selectedCryptoCurrencySymbol =
            "./assets/img/currency/" +
                this.data.selectedSellingAssetText.toUpperCase() +
                ".svg";
        this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer = "GCJK5PBMMAGO3SIH3BVDKNSDNTJHETXFQEOSYUILXFWSDNCA3LTCSI4U";
        this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer = "GBXI2ZBD44SZQ7OVCJXEDOTPEMIBWNJ5VQFCFROW5G3BSMWPLLWSSXNS";
        this.http.get(this.data.WEBSERVICE + '/userTrade/GetAssetIssuer')
            .subscribe(function (responseBuySell) {
            $('#' + _this.dateD).addClass('active');
            $('.reset').click();
            _this.tools = true;
            $('#dropHolder').css('overflow', 'scroll');
            $(window).resize(function () {
                var wd = $('#chartHolder').width();
                $('#dropHolder').width(wd);
                $('#dropHolder').css('overflow', 'scroll');
            });
            _this.http.get(_this.data.CHARTSERVISE + '/trendsTradeGraphFor24Hours/' + _this.currency_code + '/' + _this.base_currency)
                .subscribe(function (value) {
                if (value != '') {
                    _this.chartlist = value[0];
                    _this.ctpdata = _this.data.ctpdata = _this.chartlist.CTP;
                    _this.lowprice = _this.data.lowprice = _this.chartlist.LOW_PRICE;
                    _this.highprice = _this.data.highprice = _this.chartlist.HIGH_PRICE;
                    _this.act = _this.chartlist.ACTION;
                    _this.rocdata = _this.chartlist.ROC.toFixed(2);
                    if (_this.rocdata > 0) {
                        _this.rocreact = true;
                    }
                    else {
                        _this.rocreact = false;
                    }
                    if (_this.rocdata < 0) {
                        _this.negetive = true;
                    }
                    else {
                        _this.negetive = false;
                    }
                    _this.volumndata = _this.chartlist.VOLUME.toFixed(2);
                    if (_this.rocdata >= 0) {
                        _this.rocreact = true;
                    }
                    if (_this.act == 'sell') {
                        _this.react = true;
                    }
                    else {
                        _this.react = false;
                    }
                }
                else {
                    _this.ctpdata = 0;
                    _this.lowprice = 0;
                    _this.highprice = 0;
                    _this.rocdata = 0;
                    _this.volumndata = 0;
                }
            });
        });
    };
    ChartComponent.prototype.changemode = function () {
        if (this.data.changescreencolor == true) {
            $(".bg_new_class")
                .removeClass("bg-dark")
                .css("background-color", "#fefefe");
            $(".btn")
                .removeClass("bg-black")
                .css("background-color", "#dedede");
            $(".btn").css("border-color", "#b3b4b4");
            $(".btn").css("color", "#000");
            $(".charts-tab.active").css("background-color", "#fefefe");
        }
        else {
            $(".bg_new_class")
                .removeClass("bg-dark")
                .css("background-color", "#16181a");
            $(".btn")
                .removeClass("bg-black")
                .css("background-color", "rgb(44, 65, 66)");
            $(".btn").css("border-color", "transparent");
            $(".btn").css("color", "#fff");
            $(".charts-tab.active").css("background-color", "#242e3e");
        }
    };
    ChartComponent.prototype.ngDoCheck = function () {
        this.changemode();
    };
    ChartComponent.prototype.getNewCurrency = function () {
        var _this = this;
        this.http.get(this.data.WEBSERVICE + '/userTrade/GetCurrencyDetails')
            .subscribe(function (responseCurrency) {
            _this.filterCurrency = responseCurrency;
            var mainArr = [];
            _this.header = _this.filterCurrency.Header;
            _this.assets = _this.filterCurrency.Values;
        });
    };
    //clear indicators
    ChartComponent.prototype.destroy = function () {
        this.selected = null;
    };
    ChartComponent = __decorate([
        Component({
            selector: "app-chart",
            templateUrl: "./chart.component.html",
            styleUrls: ["./chart.component.css"]
        }),
        __metadata("design:paramtypes", [HttpClient,
            CoreDataService,
            OrderBookComponent,
            StopLossComponent,
            TradesComponent,
            DashboardComponent])
    ], ChartComponent);
    return ChartComponent;
}());
export { ChartComponent };
//# sourceMappingURL=chart.component.js.map