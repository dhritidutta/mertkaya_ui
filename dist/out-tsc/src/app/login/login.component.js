var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
//import { OrderBookComponent } from '../order-book/order-book.component';
import { TradesComponent } from '../trades/trades.component';
import { TranslateService } from '@ngx-translate/core';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(http, data, cookie, route, nav, trade, translate) {
        this.http = http;
        this.data = data;
        this.cookie = cookie;
        this.route = route;
        this.nav = nav;
        this.trade = trade;
        this.translate = translate;
        this.otpBlock = false;
        translate.addLangs(['Turkish', 'English']);
        translate.setDefaultLang('Turkish');
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.error = false;
        this.loader = false;
        this.logoutReason = this.nav.reason;
        this.route.events.subscribe(function (val) {
            _this.currentRoute = val;
            if (_this.currentRoute.url != '/dashboard') {
                // this.orderBook.source12.close();
                // this.orderBook.source2.close();
                //this.trade.getMarketTradeBuy.close();
            }
        });
    };
    LoginComponent.prototype.basic = function () {
        //location.href = 'https://www.paybito.com/dashboard/login';
    };
    LoginComponent.prototype.loginData = function (isValid) {
        var _this = this;
        if (isValid && this.email != "" && this.password != "") {
            this.loader = true;
            this.logoutReason = '';
            var loginObj = {};
            loginObj['email'] = this.email;
            loginObj['password'] = this.password;
            var jsonString = JSON.stringify(loginObj);
            //login webservice
            this.http.post(this.data.WEBSERVICE + '/user/LoginWithUsernamePassword', jsonString, { headers: { 'content-Type': 'application/json' } })
                .subscribe(function (data) {
                // error
                if (data.error.error_data == '1') {
                    _this.error = true;
                    _this.loader = false;
                }
                // valid
                if (data.error.error_data == '0') {
                    _this.error = false;
                    if (data.userResult.twoFactorAuth == 0) {
                        _this.setLoginData(data);
                    }
                    else {
                        _this.loader = false;
                        _this.otpBlock = true;
                        $('.otp_segment').show();
                        $('.otp_btn').show();
                        $('.login_btn').hide();
                        $('#loginInputOTP').focus();
                    }
                }
            }, function (error) {
                _this.loader = true;
            });
        }
        else {
            this.error = true;
        }
    };
    LoginComponent.prototype.loginThroughOtp = function () {
        var _this = this;
        var otpObj = {};
        otpObj['email'] = this.email;
        otpObj['otp'] = this.otp;
        var jsonString = JSON.stringify(otpObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/CheckTwoFactor', jsonString, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .subscribe(function (response) {
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.setLoginData(result);
            }
        }, function (reason) {
            _this.data.alert(reason, 'danger');
        });
    };
    LoginComponent.prototype.setLoginData = function (data) {
        var _this = this;
        this.userId = data.userResult.userId;
        var body = new URLSearchParams();
        body.set('username', this.userId);
        body.set('password', this.password);
        body.set('grant_type', 'password');
        var options = {
            headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('authorization', 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg==')
        };
        this.http.post(this.data.WEBSERVICE + '/oauth/token', body.toString(), options)
            .subscribe(function (dataAuth) {
            _this.accessToken = dataAuth.access_token;
            _this.refreshToken = dataAuth.refresh_token;
            _this.expiresIn = dataAuth.expires_in;
            localStorage.setItem('access_token', _this.accessToken);
            localStorage.setItem('refresh_token', _this.refreshToken);
            // console.log(this.accessToken,'++++++++',this.refreshToken);
            var expiresTime = _this.expiresIn;
            expiresTime = expiresTime * 300000;
            var start_time = $.now();
            var expiresIn = start_time + expiresTime;
            localStorage.setItem('expires_in', expiresIn);
            var userObj = {};
            userObj['userId'] = _this.userId;
            var userJsonString = JSON.stringify(userObj);
            _this.http.post(_this.data.WEBSERVICE + '/user/GetUserDetails', userJsonString, { headers: { 'Content-Type': 'application/json', 'authorization': 'BEARER ' + _this.accessToken } })
                .subscribe(function (dataRecheck) {
                //console.log('dataRecheck+++++',dataRecheck.userListResult[0].fullName);
                if (dataRecheck.error.error_data == '0') {
                    localStorage.setItem('user_name', dataRecheck.userListResult[0].fullName);
                    localStorage.setItem('user_id', dataRecheck.userListResult[0].userId);
                    localStorage.setItem('phone', dataRecheck.userListResult[0].phone);
                    localStorage.setItem('email', dataRecheck.userListResult[0].email);
                    localStorage.setItem('address', dataRecheck.userListResult[0].address);
                    localStorage.setItem('profile_pic', dataRecheck.userListResult[0].profilePic);
                    localStorage.setItem('check_id_verification_status', 'true');
                    localStorage.setItem('selected_currency', 'btc');
                    localStorage.setItem('buying_crypto_asset', 'btc');
                    localStorage.setItem('selling_crypto_asset', 'usd');
                    _this.cookie.set('access_token', localStorage.getItem('access_token'), 60);
                    _this.route.navigateByUrl('/dashboard');
                    _this.data.alert('Login Successful!', 'success');
                }
            }, function (reason) {
                // wip(0);
                _this.data.alert(reason, 'danger');
            });
        }, function (reason) {
            // wip(0);
            _this.data.alert(reason, 'danger');
        });
    };
    LoginComponent.prototype.showHide = function () {
        $(".showHide-password").each(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            }
            else {
                input.attr("type", "password");
            }
        });
    };
    LoginComponent = __decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient,
            CoreDataService,
            CookieService,
            Router,
            NavbarComponent,
            TradesComponent, TranslateService])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map