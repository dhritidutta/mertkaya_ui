var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import * as $ from 'jquery';
import { BodyService } from '../body.service';
var HistoryComponent = /** @class */ (function () {
    function HistoryComponent(main) {
        this.main = main;
        this.page = 1;
    }
    HistoryComponent.prototype.ngOnInit = function () {
        this.main.getDashBoardInfo();
        this.main.transactionHistory(1);
        this.collection = this.main.noOfItemPerPage;
        $('.historyTableBody').html(this.main.historyTableTr);
    };
    HistoryComponent.prototype.pager = function (pg) {
        // this.page = pg;
        this.main.transactionHistory(pg);
    };
    HistoryComponent.prototype.pagerNext = function (pg) {
        pg++;
        this.page = pg;
        this.main.transactionHistory(pg);
    };
    HistoryComponent.prototype.pagerPre = function (pg) {
        pg--;
        this.page = pg;
        this.main.transactionHistory(pg);
    };
    //function for get duration
    HistoryComponent.prototype.getDuration = function (duration) {
        this.main.timeSpan = duration;
        this.main.transactionHistory('1');
        if (duration == 'all') {
            $('.filter-button').removeClass('btn_active');
            $('.all_btn').addClass('btn_active');
        }
        if (duration == 'last week') {
            $('.filter-button').removeClass('btn_active');
            $('.last_week_btn').addClass('btn_active');
        }
        if (duration == 'last month') {
            $('.filter-button').removeClass('btn_active');
            $('.last_month_btn').addClass('btn_active');
        }
    };
    HistoryComponent = __decorate([
        Component({
            selector: 'app-history',
            templateUrl: './history.component.html',
            styleUrls: ['./history.component.css']
        }),
        __metadata("design:paramtypes", [BodyService])
    ], HistoryComponent);
    return HistoryComponent;
}());
export { HistoryComponent };
//# sourceMappingURL=history.component.js.map