var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { BodyService } from '../body.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { Router } from '@angular/router';
var MyWalletComponent = /** @class */ (function () {
    function MyWalletComponent(http, data, main, modalService, route) {
        this.http = http;
        this.data = data;
        this.main = main;
        this.modalService = modalService;
        this.route = route;
        this.transactionType = 'send';
        this.mining_fees = 0;
        this.balance = 0;
        this.limit = 0;
        this.btcLink = 'https://live.blockcypher.com/btc/tx/';
        this.ltcLink = 'https://live.blockcypher.com/ltc/tx/';
        this.bchLink = 'https://explorer.bitcoin.com/bch/search/';
        this.ethLink = 'https://etherscan.io/tx/';
        this.etclink = 'http://gastracker.io/tx/';
        this.bsvlink = 'https://blockchair.com/search?q=';
        this.xrplink = 'https://test.bithomp.com/explorer/';
        this.triggerslink = 'https://xchain.io/tx/';
        this.disclaim = false;
    }
    MyWalletComponent.prototype.ngOnInit = function () {
        this.main.getUserTransaction();
        this.walletHistoryList('1');
        this.main.getDashBoardInfo();
        this.sendMax();
        this.currencyBalance = this.main.balencelist;
        // console.log('this.currencyBalance',this.currencyBalance);
        if (this.currencyBalance != null) {
            for (var i = 0; i < this.currencyBalance.length; i++) {
                if (this.currencyBalance[i].currencyCode == "TRY") {
                    this.usdbalance = this.currencyBalance[i].closingBalance;
                    this.currencyId = this.currencyBalance[i].currencyId;
                }
            }
        }
    };
    MyWalletComponent.prototype.walletHistoryList = function (pageNo) {
        var _this = this;
        $('.walletHistoryTableBody').html("<tr>\n    <td colspan=\"5\" class=\"text-center py-3\">\n    <img src=\"./assets/svg-loaders/puff.svg\" width=\"50\" alt=\"\">\n    </td>\n  </tr>");
        var historyObj = {};
        historyObj['userId'] = localStorage.getItem('user_id');
        historyObj['pageNo'] = 1;
        historyObj['noOfItemsPerPage'] = 20;
        historyObj['timeSpan'] = this.main.timeSpan;
        historyObj['transactionType'] = this.transactionType;
        var jsonString = JSON.stringify(historyObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/transaction/getUserAllTransaction', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                if (result.error.error_msg == 'no result') {
                    $('.walletHistoryTableBody').html("<tr>\n            <td colspan=\"5\" class=\"text-center py-3\">\n              No Data Available\n            </td>\n          </tr>");
                }
                else {
                    _this.historyDetails = result.userTransactionsResult;
                    _this.totalCount = result.totalCount;
                    _this.historyTableTr = '';
                    if (_this.historyDetails != null) {
                        for (var i = 0; i < _this.historyDetails.length; i++) {
                            //if(this.historyDetails[i].action=='Send' || this.historyDetails[i].action=='Recieved'){
                            var timestamp = _this.historyDetails[i].transactionTimestamp;
                            var timestampArr = timestamp.split('.');
                            timestamp = _this.data.readable_timestamp(timestampArr[0]);
                            var action = _this.historyDetails[i].action;
                            // this.selectedCurrency = localStorage.getItem('selected_currency');
                            // this.selectedCurrencyText = this.selectedCurrency.toUpperCase();
                            var hrefForTxn = '';
                            var toolTipDesc = '';
                            var amount = '';
                            if (action == 'Send' || _this.transactionType == 'send') {
                                if (_this.historyDetails[i]['debitAmount'] != '0' && _this.historyDetails[i]['currency'] == 'BTC') {
                                    var amount = '<span class="text-white">' + _this.historyDetails[i]['currency'] + _this.historyDetails[i]['debitAmount'] + ' Dr.';
                                    if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                        if ((_this.historyDetails[i].currencyTxnid).length > 0) {
                                            hrefForTxn = '<br><a target="_blank" href="' + _this.btcLink + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                        }
                                    }
                                }
                                if (_this.historyDetails[i]['debitAmount'] != '0' && _this.historyDetails[i]['currency'] == 'ETH') {
                                    var amount = '<span class="text-white">' + _this.historyDetails[i]['currency'] + _this.historyDetails[i]['debitAmount'] + ' Dr.';
                                    if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                        if ((_this.historyDetails[i].currencyTxnid).length > 0) {
                                            hrefForTxn = '<br><a target="_blank" href="' + _this.ethLink + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                        }
                                    }
                                }
                                if (_this.historyDetails[i]['debitAmount'] != '0' && _this.historyDetails[i]['currency'] == 'XRP') {
                                    var amount = '<span class="text-white">' + _this.historyDetails[i]['currency'] + ' ' + _this.historyDetails[i]['debitAmount'] + ' Dr.';
                                    if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                        if ((_this.historyDetails[i].currencyTxnid).length > 0) {
                                            hrefForTxn = '<br><a target="_blank" href="' + _this.xrplink + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                        }
                                    }
                                }
                                if (_this.historyDetails[i]['debitAmount'] != '0' && _this.historyDetails[i]['currency'] == 'USDT') {
                                    var amount = '<span class="text-white">' + _this.historyDetails[i]['currency'] + ' ' + _this.historyDetails[i]['debitAmount'] + ' Dr.';
                                    if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                        if ((_this.historyDetails[i].currencyTxnid).length > 0) {
                                            hrefForTxn = '<br><a target="_blank" href="' + _this.ethLink + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                        }
                                    }
                                }
                            }
                            if (action == 'Received' || _this.transactionType == 'received') {
                                if (_this.historyDetails[i]['creditAmount'] != '0' && _this.historyDetails[i]['currency'] == 'BTC') {
                                    var amount = '<span class="text-white">' + _this.historyDetails[i]['currency'] + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr.';
                                    if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                        if ((_this.historyDetails[i].currencyTxnid).length > 0) {
                                            hrefForTxn = '<br><a target="_blank" href="' + _this.btcLink + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                        }
                                    }
                                }
                                if (_this.historyDetails[i]['creditAmount'] != '0' && _this.historyDetails[i]['currency'] == 'ETH') {
                                    var amount = '<span class="text-white">' + _this.historyDetails[i]['currency'] + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr.';
                                    if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                        if ((_this.historyDetails[i].currencyTxnid).length > 0) {
                                            hrefForTxn = '<br><a target="_blank" href="' + _this.ethLink + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                        }
                                    }
                                }
                                if (_this.historyDetails[i]['creditAmount'] != '0' && _this.historyDetails[i]['currency'] == 'XRP') {
                                    var amount = '<span class="text-white">' + _this.historyDetails[i]['currency'] + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr.';
                                    if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                        if ((_this.historyDetails[i].currencyTxnid).length > 0) {
                                            hrefForTxn = '<br><a target="_blank" href="' + _this.xrplink + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                        }
                                    }
                                }
                                if (_this.historyDetails[i]['creditAmount'] != '0' && _this.historyDetails[i]['currency'] == 'USDT') {
                                    var amount = '<span class="text-white">' + _this.historyDetails[i]['currency'] + ' ' + _this.historyDetails[i]['creditAmount'] + ' Cr.';
                                    if (_this.historyDetails[i].currencyTxnid != null && _this.historyDetails[i].currencyTxnid != 'null') {
                                        if ((_this.historyDetails[i].currencyTxnid).length > 0) {
                                            hrefForTxn = '<br><a target="_blank" href="' + _this.ethLink + _this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                                        }
                                    }
                                }
                            }
                            if (_this.historyDetails[i].status == '1' || _this.historyDetails[i].status == '1') {
                                //  console.log(i+' '+this.historyDetails[i].status+' confirmend');
                                var status = 'Confirmed';
                                var statusClass = 'text-green';
                            }
                            if (_this.historyDetails[i].status == '0' || _this.historyDetails[i].status == '0') {
                                //  console.log(i+' '+this.historyDetails[i].status+' pending');
                                var status = 'Pending';
                                var statusClass = 'text-orange';
                            }
                            if (_this.historyDetails[i].status == 'cancel' || _this.historyDetails[i].status == 'Cancel') {
                                // console.log(i+' '+this.historyDetails[i].status+' cancelled');
                                var status = 'Cancelled';
                                var statusClass = 'text-red';
                            }
                            toolTipDesc = '<div class="position-absolute tool_tip_div tool_tip_' + _this.historyDetails[i].transactionId + ' mt-4 bg-pbgreen text-white p-1 rounded " style="display:none;">' + _this.historyDetails[i].description + '  <span data-txn-id="' + _this.historyDetails[i].transactionId + '" onClick="angular.element(this).scope().hideToolTipDesc()"><i class="fa fa-times"></i></span></div>';
                            var description = (_this.historyDetails[i].description != null) ? _this.historyDetails[i].description : 'Received Amount';
                            if (description.length < 25) {
                                var descriptionTd = '<td width="30%" data-txn-id="' + _this.historyDetails[i].transactionId + '" > ' + description + ' ' + hrefForTxn + '</td>';
                                ;
                            }
                            else {
                                var descriptionTd = '<td width="30%" data-txn-id="' + _this.historyDetails[i].transactionId + '" title="' + description + '">' + toolTipDesc + ' ' + (description).substr(0, 25) + '...  ' + hrefForTxn + '</td>';
                            }
                            _this.historyTableTr += '<tr>';
                            _this.historyTableTr += '<td width="30%">' + timestamp + '</td>';
                            _this.historyTableTr += descriptionTd;
                            _this.historyTableTr += '<td class="text-white" width="10%">' + action + '</td>';
                            _this.historyTableTr += '<td width="20%">' + amount + '</td>';
                            _this.historyTableTr += '<td class="' + statusClass + '" width="10%">' + status + ' </td>';
                            _this.historyTableTr += '</tr>';
                            //}
                        }
                        _this.main.pagination(_this.totalCount, _this.main.noOfItemPerPage, 'walletHistoryList');
                    }
                    else {
                        _this.historyTableTr += '<tr colspan="5" class="text-center">No Data Exist</tr>';
                    }
                    $('.walletHistoryTableBody').html(_this.historyTableTr);
                    _this.main.getUserTransaction();
                }
            }
        }, function (reason) {
            // wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'warning');
            }
        });
    };
    MyWalletComponent.prototype.filterType = function (type) {
        if (type == 'send') {
            this.transactionType = 'send';
            this.walletHistoryList('1');
            $('.mywallet_filter_btn').removeClass('btn_active');
            $('.send_filter_btn_wallet').addClass('btn_active');
        }
        else {
            this.transactionType = 'received';
            this.walletHistoryList('1');
            $('.mywallet_filter_btn').removeClass('btn_active');
            $('.recieved_filter_btn_wallet').addClass('btn_active');
        }
    };
    MyWalletComponent.prototype.showToolTipDesc = function (elem) {
        var txnId = elem.getAttribute('data-txn-id');
        $('.tool_tip_div').hide();
        $('.tool_tip_' + txnId).show();
    };
    MyWalletComponent.prototype.hideToolTipDesc = function () {
        setTimeout(function () {
            console.log('hide function');
            $('.tool_tip_div').hide();
        }, 100);
    };
    MyWalletComponent.prototype.copy = function (inputElement) {
        inputElement.select();
        document.execCommand('copy');
        inputElement.setSelectionRange(0, 0);
    };
    MyWalletComponent.prototype.getCurrencyForRecieve = function (elem, currency, CID) {
        this.cryptoCurrency = currency;
        this.currencyID = CID;
        $('.receive_address_label, .receive_address, .recieve_qr_code').hide();
        $('.generate_address_btn').hide();
        $('#qr_code').html('');
        if (this.main.userDocVerificationStatus() == true) {
            this.generateAddress(this.currencyID);
            this.modalService.open(elem, {
                centered: true
            });
        }
    };
    MyWalletComponent.prototype.generateAddress = function (currencyId) {
        var _this = this;
        this.rcv = null;
        this.xrptag = null;
        var rcvObj = {};
        rcvObj['userId'] = localStorage.getItem('user_id');
        rcvObj['currencyId'] = currencyId;
        var jsonString = JSON.stringify(rcvObj);
        // wip(1);
        if (this.cryptoCurrency != 'triggers') {
            this.http.post(this.data.WEBSERVICE + '/transaction/getCryptoAddress', jsonString, {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                }
            })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.rcv = result.customerLedgerResult.publicKey; //change by sanu
                    _this.xrptag = result.customerLedgerResult.xrpTag;
                    // console.log(this.rcv);
                }
            });
        }
        else {
            this.http.post(this.data.WEBSERVICE + '/userTransaction/GetCounterPartyNewAddress', jsonString, {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                }
            })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.rcv = result.customerLedgerResult.publicKey; //change by sanu
                }
            });
        }
    };
    MyWalletComponent.prototype.getCurrencyForSend = function (md, elem, bal, cId) {
        this.cryptoCurrency = elem;
        this.selectedCurrency = elem;
        this.balance = bal;
        this.mining_fees = 0;
        this.currencyId = cId;
        localStorage.setItem("currencyId", this.currencyId);
        if (this.main.userDocVerificationStatus() == true) {
            this.modalService.open(md, {
                centered: true
            });
            this.paybito_phone = this.paybito_amount = this.other_address = this.other_amount = null;
            var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
            this.lockOutgoingTransactionStatus = userAppSettingsObj.lock_outgoing_transactions;
            if (this.lockOutgoingTransactionStatus == 1) {
                $('.sendOtpSection').show();
                // $('.get_Otp_btn').show();
                $('.send_btn').show();
            }
            else {
                $('.sendOtpSection').hide();
                // $('.get_Otp_btn').hide();
                $('.send_btn').show();
            }
            var settingsList = JSON.parse(localStorage.getItem('environment_settings_list'));
            this.sendDisclaimer = settingsList["send_other_min_value" + this.currencyId].description;
            this.sendDisclaimer2 = settingsList["send_other_m_charges" + this.currencyId].description;
        }
        if (this.cryptoCurrency == 'triggers') {
            this.flag = true;
        }
        else {
            ////alert('2');
            this.flag = false;
        }
    };
    MyWalletComponent.prototype.rate = function (content) {
        var _this = this;
        this.modalService.open(content, {
            centered: true
        });
        this.rateList = null;
        var feeObj = {};
        feeObj['currency'] = this.cryptoCurrency;
        var jsonString = JSON.stringify(feeObj);
        this.http.post(this.data.WEBSERVICE + '/transaction/getFees', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token')
            }
        })
            .subscribe(function (data) {
            _this.rateList = data.feesListResult;
        });
    };
    MyWalletComponent.prototype.Disclaimerdtl = function (trigger) {
        this.modalService.open(trigger, {
            centered: true
        });
    };
    MyWalletComponent.prototype.transactionSendWithinPaybito = function () {
        var _this = this;
        //send webservice
        var sendToPaybitoObj = {};
        //{"customerID":"38","toadd":"61","btcAmount":"0.75"}
        sendToPaybitoObj['userId'] = localStorage.getItem('user_id');
        sendToPaybitoObj['toAdd'] = this.paybito_phone;
        sendToPaybitoObj['sendAmount'] = this.paybito_amount;
        sendToPaybitoObj['currencyId'] = localStorage.getItem("currencyId");
        // sendToPaybitoObj['crypto_currency'] = this.cryptoCurrency;
        if (this.lockOutgoingTransactionStatus == 1) {
            sendToPaybitoObj['otp'] = this.paybito_otp;
        }
        var jsonString = JSON.stringify(sendToPaybitoObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/transaction/sendToWallet', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.data.alert('Sent succesfully', 'success');
                //$('#sendModal').modal('hide');
                _this.paybito_phone = '';
                _this.paybito_amount = '';
                _this.other_address = '';
                _this.other_amount = '';
                _this.main.getUserTransaction();
                _this.route.navigateByUrl('/my-wallet');
            }
        }, function (reason) {
            // wip(0);
            _this.data.logout();
            // $('#sendModal').modal('hide');
            _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    MyWalletComponent.prototype.transactionSendForOthers = function (openmodal) {
        var _this = this;
        //alert($scope.cryptoCurrency);
        var sendToPaybitoObj = {};
        //{"customerID":"38","toadd":"61","btcAmount":"0.75"}
        sendToPaybitoObj['userId'] = localStorage.getItem('user_id');
        sendToPaybitoObj['currencyId'] = localStorage.getItem("currencyId");
        sendToPaybitoObj['toAdd'] = this.other_address;
        sendToPaybitoObj['sendAmount'] = this.other_amount;
        // sendToPaybitoObj['crypto_currency'] = this.cryptoCurrency;
        if (this.lockOutgoingTransactionStatus == 1) {
            sendToPaybitoObj['otp'] = this.other_otp;
        }
        var jsonString = JSON.stringify(sendToPaybitoObj);
        if (this.cryptoCurrency != 'triggers') {
            this.http.post(this.data.WEBSERVICE + '/transaction/sendToOther', jsonString, {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                }
            })
                .subscribe(function (response) {
                //  wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.modalService.open(openmodal, {
                        centered: true
                    });
                    //$('#batchmsg').modal('show');
                    _this.data.alert('Balance transfered successfully', 'success');
                    _this.main.getUserTransaction();
                    //  $('#sendModal').modal('hide');
                    _this.paybito_phone = '';
                    _this.paybito_amount = '';
                    _this.other_address = '';
                    _this.other_amount = '';
                }
            }, function (reason) {
                //  wip(0);
                //  $('#sendModal').modal('hide');
                _this.data.alert(reason, 'danger');
            });
        }
        else {
            this.http.post(this.data.WEBSERVICE + '/userTransaction/SendTriggers', jsonString, {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                }
            })
                .subscribe(function (response) {
                //  wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.modalService.open(openmodal, {
                        centered: true
                    });
                    _this.data.alert('Balance transfered successfully', 'success');
                    //  $('#sendModal').modal('hide');
                    _this.paybito_phone = '';
                    _this.paybito_amount = '';
                    _this.other_address = '';
                    _this.other_amount = '';
                }
            }, function (reason) {
                //  wip(0);
                //  $('#sendModal').modal('hide');
                _this.data.alert(reason, 'danger');
            });
        }
    };
    MyWalletComponent.prototype.testmodal = function (test) {
    };
    MyWalletComponent.prototype.myFunction = function () {
        var checked = document.forms["uc-disclaimer-form"]["disclaim"].checked;
        if (checked == true) {
            // this.disclaim = true;
            // this.cryptoCurrency='submit';
            document.getElementById('sclaimer').style.display = 'block';
        }
        else {
            document.getElementById('sclaimer').style.display = 'none';
            //this.disclaim = false;
        }
    };
    MyWalletComponent.prototype.swapModal = function (content, bal) {
        this.modalService.open(content, {
            centered: true,
            size: 'lg'
        });
        this.trigx = bal;
    };
    MyWalletComponent.prototype.getRate = function (event, cur) {
        var _this = this;
        this.mining_fees = 0;
        if (typeof event == 'object') {
            var am = event.target.value;
        }
        else {
            var am = event;
        }
        this.http.get('./assets/appdata/rateRange.json')
            .subscribe(function (data) {
            var datax = data;
            function curx(arr) {
                return arr.currency == cur;
            }
            var solid = datax.filter(curx);
            function ratex(arr) {
                return arr.fromFee <= parseInt(am) && arr.toFee >= parseInt(am);
            }
            var solid2 = solid.find(ratex);
            // console.log(solid2, am, cur, solid);
            if (solid2) {
                _this.mining_fees = solid2.feeRate;
                _this.limit = _this.balance - _this.mining_fees;
                // console.log(this.mining_fees, this.limit);
            }
        });
    };
    MyWalletComponent.prototype.sendMax = function () {
        var _this = this;
        this.other_amount = 0;
        this.getRate(this.balance, this.cryptoCurrency);
        this.loading = true;
        setTimeout(function () {
            if (_this.balance > 0 && _this.balance > _this.limit || _this.balance < _this.mining_fees) {
                _this.other_amount = _this.balance - _this.mining_fees;
            }
            else {
                _this.other_amount = 0;
            }
            _this.loading = false;
        }, 500);
    };
    MyWalletComponent = __decorate([
        Component({
            selector: 'app-my-wallet',
            templateUrl: './my-wallet.component.html',
            styleUrls: ['./my-wallet.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient, CoreDataService, BodyService, NgbModal, Router])
    ], MyWalletComponent);
    return MyWalletComponent;
}());
export { MyWalletComponent };
//# sourceMappingURL=my-wallet.component.js.map