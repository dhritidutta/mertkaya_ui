var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { MyWalletComponent } from '../my-wallet/my-wallet.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
var ModalsComponent = /** @class */ (function () {
    function ModalsComponent(activeModal, modalService, mywallet, modalservice) {
        this.activeModal = activeModal;
        this.modalService = modalService;
        this.mywallet = mywallet;
        this.modalservice = modalservice;
    }
    ModalsComponent_1 = ModalsComponent;
    ModalsComponent.prototype.ngOnInit = function () {
    };
    ModalsComponent.prototype.open = function () {
        var modalRef = this.modalService.open(ModalsComponent_1);
        modalRef.componentInstance.name = 'World';
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], ModalsComponent.prototype, "name", void 0);
    ModalsComponent = ModalsComponent_1 = __decorate([
        Component({
            selector: 'app-modals',
            templateUrl: './modals.component.html',
            styleUrls: ['./modals.component.css']
        }),
        __metadata("design:paramtypes", [NgbActiveModal, NgbModal, MyWalletComponent, NgbModal])
    ], ModalsComponent);
    return ModalsComponent;
    var ModalsComponent_1;
}());
export { ModalsComponent };
//# sourceMappingURL=modals.component.js.map