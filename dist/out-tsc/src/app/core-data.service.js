var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';
import { HttpClient } from '@angular/common/http';
var CoreDataService = /** @class */ (function () {
    function CoreDataService(route, userIdle, http) {
        this.route = route;
        this.userIdle = userIdle;
        this.http = http;
        /* Change here */
        this.CURRENCYICON = '$ ';
        this.CURRENCYNAME = 'TRY';
        this.exchange = 'Tomya Pro';
        this.logo = './assets/img/bitrump-trimmed-logo_new.png';
        /* environment: any = 'dev';
         ADDR = "http://52.53.237.92:8080/main.html#!/";*/
        this.environment = 'live';
        this.ADDR = "http://52.53.237.92:8080/main.html#!/";
        /*Change here*/
        this.loader = false;
        this.triggersBalance = 0;
        this.changescreencolor = false;
        this.indicatorGroup1 = ['ATR', 'BBAND', 'MACD', 'EMA', 'ROC', 'KDJ', 'MFI', 'CMF'];
        this.indicatorGroup2 = ['ARN', 'CHO', 'HA', 'KCH', 'SSMA', 'SOSC', 'Willams %r', 'TRIX'];
        this.url = false;
        this.resize = false;
        var alertPl = "<div class=\"alertPlace\"></div>";
        $('html').append(alertPl).fadeIn();
        switch (this.environment) {
            case 'live':
                // this.WEBSERVICE = "https://api.bitrump.com/api";
                // this.TRADESERVICE = "https://api.bitrump.com/TradeAmount/rest/MyAmount";
                // this.CHARTSERVISE = "https://api.bitrump.com/TrendSpriceVolume/paybito/";//live
                // this.REPORTSERVISE = "https://api.bitrump.com/PaybitoReportModule/report/";
                this.WEBSERVICE = "https://api.tomya.com/api";
                this.TRADESERVICE = "https://api.tomya.com/TradeAmount/rest/MyAmount";
                this.CHARTSERVISE = "https://api.tomya.com/TrendSpriceVolume/paybito/";
                this.REPORTSERVISE = "https://api.tomya.com/PaybitoReportModule/report/";
                break;
        }
        this.getSettings();
    }
    CoreDataService.prototype.getSettings = function () {
        var _this = this;
        this.http.get('./assets/settings.json')
            .subscribe(function (data) {
            _this.settings = data;
        });
    };
    // Login Session Management
    CoreDataService.prototype.idleLogout = function () {
        var _this = this;
        var us = localStorage.getItem('access_token');
        if (us != null) {
            //this.userIdle.onTimerStart();
            this.userIdle.startWatching();
            this.userIdle.resetTimer();
            // this.userIdle.onTimerStart().subscribe(count =>
            //   console.log(count));
            this.userIdle.onTimerStart().subscribe(function (count) {
                //  console.log(this.time);
                // console.log('count', count);
                if (count == 1) {
                    $('#logoutWarn').click();
                }
                else if (count == 20) {
                    $('#logoutWarncl').click();
                    _this.logout();
                }
                _this.count = count;
            });
            this.userIdle.ping$.subscribe(function (ping) {
            });
            this.userIdle.onTimeout().subscribe(function () {
                //  console.log('Time is up!');
                $('#logoutWarncl').click();
                _this.logout();
            });
        }
    };
    CoreDataService.prototype.startWatching = function () {
        this.userIdle.startWatching();
    };
    CoreDataService.prototype.stopWatching = function () {
        this.userIdle.stopWatching();
    };
    CoreDataService.prototype.Stop = function () {
        var _this = this;
        // alert('bmnb');
        $('#logoutWarncl').click();
        $('.d-block').removeClass('show');
        $('.d-block').siblings().removeClass("modal-backdrop");
        this.userIdle.stopTimer();
        this.userIdle.stopWatching();
        this.userIdle.resetTimer();
        // this.restart();
        var presentRefreshToken = localStorage.getItem('refresh_token');
        //console.log('localstorage',presentRefreshToken)
        var fd = new FormData();
        fd.append('refresh_token', presentRefreshToken);
        fd.append('grant_type', 'refresh_token');
        //this.data.Stop();
        this.http.post(this.WEBSERVICE + '/oauth/token?grant_type=refresh_token&refresh_token=' + presentRefreshToken, '', {
            headers: {
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'authorization': 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg=='
            }
        })
            .subscribe(function (response) {
            var result = response;
            localStorage.setItem('access_token', result.access_token);
            localStorage.setItem('refresh_token', result.refresh_token);
            var userObj = {};
            userObj['user_id'] = localStorage.getItem('user_id');
            var userJsonString = JSON.stringify(userObj);
            // wip(1);
            var accessToken = localStorage.getItem('access_token');
            // this.http.post<any>(this.WEBSERVICE + '/user/GetUserDetails', userJsonString, {
            //   headers: {
            //     'Content-Type': 'application/json',
            //     'authorization': 'BEARER ' + accessToken,
            //   }
            // })
            //   .subscribe(response => {
            //     // wip(0);
            //     var result = response;
            //     if (result.error.error_data != '0') {
            //       this.alert(result.error.error_msg, 'danger');
            //     } else {
            //       localStorage.setItem('user_name', result.userResult.full_name);
            //       localStorage.setItem('user_id', result.userResult.user_id);
            //       localStorage.setItem('phone', result.userResult.phone);
            //       localStorage.setItem('email', result.userResult.email);
            //       localStorage.setItem('address', result.userResult.address);
            //       localStorage.setItem('profile_pic', result.userResult.profile_pic);
            //       //  clearInterval(this.data.interval);
            //       //console.log('this.data.interval++++++++++++',this.interval);
            //       //  this.data.time = 10;
            //       this.idleLogout();
            //       //this.data.restart();
            //       //  location.reload();
            //       // window.location='main.html';
            //       //    $('#sessionExpireModal').modal('hide');
            //       //    clearInterval($scope.expireTimerInterval);
            //     }
            //   });
            _this.idleLogout();
        }, function (reason) {
            //   wip(0);
            //this.alert(reason.error.message,'danger');
            //this.data.idleLogout();
            location.reload();
            _this.logout();
        });
    };
    CoreDataService.prototype.restart = function () {
        this.userIdle.resetTimer();
    };
    CoreDataService.prototype.onTimeout = function () {
        this.userIdle.onTimeout();
    };
    CoreDataService.prototype.readable_timestamp = function (t) {
        // Split timestamp into [ Y, M, D, h, m, s ]
        var t = t.split(/[- :]/);
        // Apply each element to the Date function
        var d = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));
        //handle parsing GMT as UTC
        d.setMinutes(d.getMinutes() - 330);
        t = d.toTimeString();
        t = t.split(" ");
        return d.toDateString() + " " + t[0];
    };
    CoreDataService.prototype.logout = function () {
        localStorage.clear();
        //caches.delete;
        //location.reload();
        this.route.navigateByUrl('/login');
    };
    CoreDataService.prototype.alert = function (msg, type, time) {
        var _this = this;
        if (time === void 0) { time = 3000; }
        //console.log(msg, type);
        this.reason = msg;
        this.icon = 'puff';
        if (msg == 'Loading...') {
            this.loader = true;
            setTimeout(function () {
                _this.loader = false;
            }, 10000);
        }
        else {
            $('.alert:first').fadeOut();
            var htx = "<div class=\"alert alert-" + type + " my-2\" role=\"alert\">" + msg + "</div>";
            $('.alertPlace').append(htx).fadeIn();
            setTimeout(function () {
                $('.alert:last').remove().fadeOut();
            }, time);
        }
    };
    CoreDataService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [Router, UserIdleService, HttpClient])
    ], CoreDataService);
    return CoreDataService;
}());
export { CoreDataService };
//# sourceMappingURL=core-data.service.js.map