var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { BodyService } from '../body.service';
var SupportComponent = /** @class */ (function () {
    function SupportComponent(http, data, main) {
        this.http = http;
        this.data = data;
        this.main = main;
    }
    SupportComponent.prototype.ngOnInit = function () {
        this.main.getDashBoardInfo();
    };
    SupportComponent.prototype.supportMail = function () {
        var _this = this;
        if (this.message != undefined && this.subject != undefined) {
            var supportObj = {};
            supportObj['user_id'] = localStorage.getItem('user_id');
            supportObj['email'] = localStorage.getItem('email');
            supportObj['subject'] = this.subject;
            supportObj['message'] = this.message;
            supportObj['name'] = localStorage.getItem('user_name');
            var jsonString = JSON.stringify(supportObj);
            // wip(1);
            this.http.post(this.data.WEBSERVICE + '/user/SendMailToUser', jsonString, { headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                } })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.data.alert('Mail Sent , We Will Contact You Shortly', 'success');
                    _this.subject = '';
                    _this.message = '';
                }
            }, function (reason) {
                // wip(0);
                if (reason.data.error == 'invalid_token') {
                    this.data.logout();
                }
                else {
                    this.data.alert('Could Not Connect To Server', 'danger');
                }
            });
        }
        else {
            this.data.alert('Please provide proper details', 'warning');
        }
    };
    SupportComponent = __decorate([
        Component({
            selector: 'app-support',
            templateUrl: './support.component.html',
            styleUrls: ['./support.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient, CoreDataService, BodyService])
    ], SupportComponent);
    return SupportComponent;
}());
export { SupportComponent };
//# sourceMappingURL=support.component.js.map