var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { BodyService } from '../body.service';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MyWalletComponent } from '../my-wallet/my-wallet.component';
//import{OrderBookComponent}from '../order-book/order-book.component'
var StopLossComponent = /** @class */ (function () {
    function StopLossComponent(data, http, main, dash, modalService, mywallet) {
        this.data = data;
        this.http = http;
        this.main = main;
        this.dash = dash;
        this.modalService = modalService;
        this.mywallet = mywallet;
        this.selelectedBuyingAssetBalance = '0';
        this.selelectedSellingAssetBalance = '0';
        this.limitAmount = 0;
        this.limitPrice = 0;
        this.limitValue = 0;
        $(function () {
            $('.form-control').click(function () {
                $(this).select();
            });
        });
        this.valLimit = 0;
    }
    StopLossComponent.prototype.ngOnInit = function () {
    };
    StopLossComponent.prototype.ngDoCheck = function () {
        this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
        this.selectedSellingAssetText = this.data.selectedSellingAssetText;
        //this.asset = localStorage.getItem('selling_crypto_asset') //changed by sanu
        this.asset = this.selectedSellingAssetText;
        // console.log('+++++',this.selectedSellingAssetText);
        if (this.asset === "TRY") {
            this.valid = true;
        }
        else {
            this.valid = false;
        }
        // this.getUserTransaction();
        //  console.log('this.result+++++++++++++++',this.result);
        //   if (localStorage.getItem('buying_crypto_asset') != 'usd') {
        //   this.selelectedBuyingAssetBalance = parseFloat(this.result.userBalanceResult[localStorage.getItem('buying_crypto_asset') + '_balance']).toFixed(4);
        // } else if (
        //   ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'btc')) ||
        //   ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'eth'))
        // ) {
        //   this.selelectedBuyingAssetBalance = parseFloat(this.result.userBalanceResult[localStorage.getItem('buying_crypto_asset') + '_balance']).toFixed(8);
        // } else {
        //   this.selelectedBuyingAssetBalance = (this.result.userBalanceResult.fiat_balance).toFixed(2);
        // }
        // if (localStorage.getItem('selling_crypto_asset') != 'usd') {
        //   this.selelectedSellingAssetBalance = parseFloat(this.result.userBalanceResult[localStorage.getItem('selling_crypto_asset') + '_balance']).toFixed(4);
        // } else if (
        //   ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'btc')) ||
        //   ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'eth'))
        // ) {
        //   this.selelectedSellingAssetBalance = (this.result.userBalanceResult.fiat_balance).toFixed(8);
        // } else {
        //   this.selelectedSellingAssetBalance = (this.result.userBalanceResult.fiat_balance).toFixed(2);
        // }
    };
    StopLossComponent.prototype.reset = function () {
        this.limitPrice = "";
        this.limitValue = "";
        this.limitPrice = "";
        this.onlyBuyAmount = this.onlyBuyPrice = this.onlyBuyTotalPrice = '';
        this.onlySellAmount = this.onlySellPrice = this.onlySellTotalPrice = '';
        $(function () {
            $('input.form-control').val('');
        });
        this.getUserTransaction();
        // this._OrderBookComponent.serverSentEventForOrderbookAsk();
        //  this._OrderBookComponent.serverSentEventForOrderbookBid();
    };
    StopLossComponent.prototype.update = function () {
        this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
        this.selectedSellingAssetText = this.data.selectedSellingAssetText;
        this.market = true;
    };
    StopLossComponent.prototype.getBuyVal = function (event) {
        var _this = this;
        var val = event.target.value;
        if (val < 0) {
            // var onlyBuyAmount:any=val;
            this.data.alert('Price cannot be negative', 'warning');
            this.onlyBuyAmount = '';
        }
        else {
            var onlyBuyAmount = val;
        }
        this.http.get(this.data.TRADESERVICE + '/getAmountBuy/' + this.data.selectedSellingAssetText.toUpperCase() + this.data.selectedBuyingAssetText.toUpperCase() + '/' + this.data.selectedBuyingAssetText.toUpperCase() + this.data.selectedSellingAssetText.toUpperCase() + '/' + onlyBuyAmount)
            .subscribe(function (data) {
            var result = data;
            if (result.code == '0') {
                if (_this.data.selectedSellingAssetText == 'usd') {
                    _this.onlyBuyPrice = (parseFloat(result.price)).toFixed(4);
                    _this.onlyBuyTotalPrice = (parseFloat(result.price) * parseFloat(onlyBuyAmount)).toFixed(4);
                }
                else {
                    _this.onlyBuyPrice = (parseFloat(result.price)).toFixed(6);
                    _this.onlyBuyTotalPrice = (parseFloat(result.price) * parseFloat(onlyBuyAmount)).toFixed(6);
                }
                $('.onlyBuyError').hide();
            }
            else {
                _this.onlyBuyPrice = 0;
                _this.onlyBuyTotalPrice = 0;
                $('.onlyBuyError').show();
            }
        }, function (error) {
            console.log(error);
        });
    };
    StopLossComponent.prototype.marketBuy = function () {
        var _this = this;
        this.data.alert('Loading...', 'dark');
        var onlyBuyAmount = this.onlyBuyAmount;
        this.http.get(this.data.TRADESERVICE + '/getAmountBuy/' + this.data.selectedSellingAssetText.toUpperCase() + this.data.selectedBuyingAssetText.toUpperCase() + '/' + this.data.selectedBuyingAssetText.toUpperCase() + this.data.selectedSellingAssetText.toUpperCase() + '/' + onlyBuyAmount)
            .subscribe(function (data) {
            var result = data;
            if (result.code == '0') {
                if (_this.data.selectedSellingAssetText == 'usd') {
                    _this.onlyBuyPrice = (parseFloat(result.price)).toFixed(4);
                }
                else {
                    _this.onlyBuyPrice = (parseFloat(result.price)).toFixed(6);
                }
                $('.onlyBuyError').hide();
                var inputObj = {};
                inputObj['userId'] = localStorage.getItem('user_id');
                inputObj['selling_asset_code'] = _this.data.selectedSellingAssetText.toUpperCase();
                inputObj['buying_asset_code'] = _this.data.selectedBuyingAssetText.toUpperCase();
                inputObj['amount'] = parseFloat(_this.onlyBuyAmount);
                inputObj['price'] = parseFloat(result.price1);
                inputObj['txn_type'] = '1';
                var jsonString = JSON.stringify(inputObj);
                if ((result.price1 * _this.onlyBuyAmount) >= .001) {
                    _this.http.post(_this.data.WEBSERVICE + '/userTrade/TradeCreateOffer', jsonString, {
                        headers: {
                            'Content-Type': 'application/json',
                            'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                        }
                    })
                        .subscribe(function (data) {
                        _this.data.loader = false;
                        var result = data;
                        if (result.error.error_data != '0') {
                            if (result.error.error_data == 1)
                                _this.data.alert(result.error.error_msg, 'danger');
                            else
                                $('#warn').click();
                        }
                        else {
                            _this.reset();
                            _this.data.alert(result.error.error_msg, 'success');
                        }
                        _this.reset();
                    });
                }
                else {
                    _this.reset();
                    _this.data.loader = false;
                    _this.data.alert('Offer Value is lesser than permissible value', 'warning');
                }
            }
            else {
                _this.onlyBuyAmount = 0;
                $('.onlyBuyError').show();
            }
        });
    };
    StopLossComponent.prototype.getSellVal = function (event) {
        var _this = this;
        var val = event.target.value;
        if (val < 0) {
            // var onlyBuyAmount:any=val;
            this.data.alert('Price cannot be negative', 'warning');
            this.onlySellAmount = '';
        }
        else {
            var onlySellAmount = val;
        }
        this.http.get(this.data.TRADESERVICE + '/getAmountSell/' + this.data.selectedBuyingAssetText.toUpperCase() + this.data.selectedSellingAssetText.toUpperCase() + '/' +
            this.data.selectedSellingAssetText.toUpperCase() + this.data.selectedBuyingAssetText.toUpperCase() + '/' + onlySellAmount)
            .subscribe(function (data) {
            console.log(data);
            var result = data;
            if (result.code == '0') {
                if (_this.data.selectedSellingAssetText == 'usd') {
                    _this.onlySellPrice = (parseFloat(result.price)).toFixed(4);
                    _this.onlySellTotalPrice = (parseFloat(result.price) * parseFloat(onlySellAmount)).toFixed(4);
                }
                else {
                    _this.onlySellPrice = (parseFloat(result.price)).toFixed(6);
                    _this.onlySellTotalPrice = (parseFloat(result.price) * parseFloat(onlySellAmount)).toFixed(6);
                }
                $('.onlySellError').hide();
            }
            else {
                _this.onlySellPrice = 0;
                _this.onlySellTotalPrice = 0;
                $('.onlySellError').show();
            }
        });
    };
    StopLossComponent.prototype.marketSell = function () {
        var _this = this;
        this.data.alert('Loading...', 'dark');
        $('.load').fadeIn();
        $('#msell').attr('disabled', true);
        var onlyBuyAmount = this.onlySellAmount;
        this.http.get(this.data.TRADESERVICE + '/getAmountSell/' + this.data.selectedBuyingAssetText.toUpperCase() + this.data.selectedSellingAssetText.toUpperCase() + '/' + this.data.selectedSellingAssetText.toUpperCase() + this.data.selectedBuyingAssetText.toUpperCase() + '/' + onlyBuyAmount)
            .subscribe(function (data) {
            var result = data;
            if (result.code == '0') {
                if (_this.data.selectedSellingAssetText == 'usd') {
                    _this.onlySellPrice = (parseFloat(result.price)).toFixed(4);
                }
                else {
                    _this.onlySellPrice = (parseFloat(result.price)).toFixed(6);
                }
                $('.onlySellError').hide();
                var inputObj = {};
                inputObj['userId'] = localStorage.getItem('user_id');
                inputObj['selling_asset_code'] = (_this.data.selectedBuyingAssetText).toUpperCase();
                inputObj['buying_asset_code'] = (_this.data.selectedSellingAssetText).toUpperCase();
                inputObj['amount'] = parseFloat(_this.onlySellAmount);
                inputObj['price'] = parseFloat(result.price1);
                inputObj['txn_type'] = '2';
                var jsonString = JSON.stringify(inputObj);
                if ((result.price1 * _this.onlySellAmount) >= .001) {
                    _this.http.post(_this.data.WEBSERVICE + '/userTrade/TradeCreateOffer', jsonString, {
                        headers: {
                            'Content-Type': 'application/json',
                            'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                        }
                    })
                        .subscribe(function (data) {
                        _this.data.loader = false;
                        $('.load').fadeOut();
                        var result = data;
                        if (result.error.error_data != '0') {
                            if (result.error.error_data == 1)
                                _this.data.alert(result.error.error_msg, 'danger');
                            else
                                $('#warn').click();
                        }
                        else {
                            _this.reset();
                            _this.data.alert(result.error.error_msg, 'success');
                        }
                    });
                }
                else {
                    _this.reset();
                    _this.data.loader = false;
                    _this.data.alert('Offer Value is lesser than permissible value', 'warning');
                }
            }
            else {
                _this.onlySellPrice = 0;
                $('.onlySellError').show();
            }
        });
    };
    StopLossComponent.prototype.limitBuy = function () {
        var _this = this;
        $('.tradeBtn').attr('disabled', true);
        this.data.alert('Loading...', 'dark', 30000);
        var inputObj = {};
        inputObj['selling_asset_code'] = (this.data.selectedBuyingAssetText).toUpperCase(); // change by sanu
        inputObj['buying_asset_code'] = (this.data.selectedSellingAssetText).toUpperCase(); //
        inputObj['userId'] = localStorage.getItem('user_id');
        inputObj['price'] = this.limitPrice;
        inputObj['txn_type'] = '1';
        var jsonString = JSON.stringify(inputObj);
        if ((this.limitPrice * this.limitAmount) > this.valLimit) {
            this.http.post(this.data.WEBSERVICE + '/userTrade/OfferPriceCheck', jsonString, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .subscribe(function (response) {
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'warning');
                    $('.tradeBtn').attr('disabled', true);
                }
                else {
                    //START
                    if (_this.limitAmount != undefined && _this.limitPrice != undefined) {
                        var inputObj = {};
                        //inputObj['account_priv_key']=localStorage.getItem('trade_private_key');
                        //inputObj['account_pub_key']=localStorage.getItem('trade_public_key');
                        inputObj['userId'] = localStorage.getItem('user_id');
                        inputObj['selling_asset_code'] = _this.data.selectedSellingAssetText.toUpperCase();
                        inputObj['buying_asset_code'] = _this.data.selectedBuyingAssetText.toUpperCase();
                        inputObj['amount'] = _this.limitAmount;
                        inputObj['price'] = _this.limitPrice;
                        inputObj['txn_type'] = '1';
                        var jsonString = JSON.stringify(inputObj);
                        _this.http.post(_this.data.WEBSERVICE + '/userTrade/TradeCreateOffer', jsonString, {
                            headers: {
                                'Content-Type': 'application/json',
                                'authorization': 'BEARER ' + localStorage.getItem('access_token')
                            }
                        })
                            .subscribe(function (response) {
                            _this.data.loader = false;
                            var result = response;
                            if (result.error.error_data != '0') {
                                if (result.error.error_data == 1)
                                    _this.data.alert(result.error.error_msg, 'danger');
                                else
                                    $('#warn').click();
                                $('.tradeBtn').removeAttr('disabled');
                                $('.form-control').val('');
                                $('#totalValueTrade').val('');
                                $('.tradeBtn').attr('disabled', true);
                            }
                            else {
                                $('.form-control').val('');
                                _this.data.alert(result.error.error_msg, 'success');
                                _this.limitPrice = 0;
                                _this.limitAmount = 0;
                                _this.reset();
                                $('#trade').click();
                            }
                        });
                    }
                    else {
                        $('.tradeBtn').removeAttr('disabled');
                        $('.form-control').val('');
                        _this.data.alert('Please provide proper buying details', 'warning');
                    }
                    //End
                }
                _this.limitAmount = _this.limitPrice = _this.limitValue = null;
            });
        }
        else {
            this.limitAmount = this.limitPrice = this.limitValue = null;
            this.data.loader = false;
            this.data.alert('Your offer is too small', 'warning');
        }
    };
    StopLossComponent.prototype.limitSell = function () {
        var _this = this;
        $('.tradeBtn').attr('disabled', true);
        this.data.alert('Loading...', 'dark', 30000);
        if (this.limitPrice != undefined && this.limitAmount != undefined) {
            var inputObj = {};
            inputObj['selling_asset_code'] = (this.data.selectedBuyingAssetText).toUpperCase(); //
            inputObj['buying_asset_code'] = (this.data.selectedSellingAssetText).toUpperCase(); //change by sanu
            inputObj['userId'] = localStorage.getItem('user_id');
            inputObj['price'] = this.limitPrice;
            inputObj['txn_type'] = '2';
            var jsonString = JSON.stringify(inputObj);
            if ((this.limitPrice * this.limitAmount) > this.valLimit) {
                this.http.post(this.data.WEBSERVICE + '/userTrade/OfferPriceCheck', jsonString, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                    .subscribe(function (response) {
                    var result = response;
                    if (result.error.error_data != '0') {
                        if (result.error.error_data == 1)
                            _this.data.alert(result.error.error_msg, 'danger');
                        else
                            $('#warn').click();
                        $('.tradeBtn').attr('disabled', true);
                    }
                    else {
                        //Start
                        var inputObj = {};
                        inputObj['userId'] = localStorage.getItem('user_id');
                        inputObj['selling_asset_code'] = _this.data.selectedBuyingAssetText.toUpperCase();
                        inputObj['buying_asset_code'] = _this.data.selectedSellingAssetText.toUpperCase();
                        inputObj['amount'] = _this.limitAmount;
                        inputObj['price'] = _this.limitPrice;
                        inputObj['txn_type'] = '2';
                        var jsonString = JSON.stringify(inputObj);
                        _this.http.post(_this.data.WEBSERVICE + '/userTrade/TradeCreateOffer', jsonString, {
                            headers: {
                                'Content-Type': 'application/json',
                                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                            }
                        })
                            .subscribe(function (response) {
                            _this.data.loader = false;
                            var result = response;
                            if (result.error.error_data != '0') {
                                //wip(0);
                                if (result.error.error_data == 1)
                                    _this.data.alert(result.error.error_msg, 'danger');
                                else
                                    $('#warn').click();
                                $('.tradeBtn').removeAttr('disabled');
                                $('.form-control').val('');
                                $('#totalValueTrade').val('');
                                $('.tradeBtn').attr('disabled', true);
                            }
                            else {
                                // wip(0);
                                $('.form-control').val('');
                                _this.data.alert(result.error.error_msg, 'success');
                                _this.limitAmount = 0;
                                _this.limitPrice = 0;
                                _this.reset();
                                $('#trade').click();
                            }
                        });
                    }
                    _this.limitAmount = _this.limitPrice = _this.limitValue = null;
                });
            }
            else {
                this.limitAmount = this.limitPrice = this.limitValue = null;
                this.data.loader = false;
                this.data.alert('Your offer is too small', 'warning');
            }
        }
        else {
            $('.form-control').val('');
            $('.tradeBtn').removeAttr('disabled');
            this.data.alert('Please provide proper selling details', 'warning');
        }
    };
    StopLossComponent.prototype.getUserTransaction = function () {
        var _this = this;
        var userTransObj = {};
        userTransObj['customerId'] = localStorage.getItem('user_id');
        // userTransObj['crypto_currency'] = localStorage.getItem('buying_crypto_asset');
        // userTransObj['crypto_currency'] = this.data.selectedBuyingAssetText.toLocaleLowerCase();
        var jsonString = JSON.stringify(userTransObj);
        //wip(1);
        this.http.post(this.data.WEBSERVICE + '/transaction/getUserBalance', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            // wip(0);
            // console.log('+++++++++++++++++', response);
            var result = response;
            // console.log(this.result);
            _this.balencelist = result.userBalanceList;
            _this.currencyBalance = _this.balencelist;
            if (_this.currencyBalance != null) {
                for (var i = 0; i < _this.currencyBalance.length; i++) {
                    if (_this.currencyBalance[i].currencyCode == "TRY") {
                        localStorage.setItem('usdbalance', _this.currencyBalance[i].closingBalance);
                    }
                }
            }
            if (result.error.error_data != '0') {
                // if (result.error.error_msg != null) {
                //   this.data.alert(result.error.error_msg, 'danger');
                // }
                _this.data.alert('Cannot fetch user balance', 'danger');
            }
            else {
                _this.selectedCryptoCurrency = localStorage.getItem('selected_currency');
                localStorage.getItem("selling_crypto_asset");
                localStorage.getItem("buying_crypto_asset");
                //console.log('+++++++++++++++++', this.usdBalance, this.btcBalance);
                for (var i = 0; i <= _this.balencelist.length - 1; i++) {
                    if (_this.balencelist[i].currencyCode == localStorage.getItem("buying_crypto_asset").toUpperCase()) {
                        _this.selelectedBuyingAssetBalance = _this.balencelist[i].closingBalance.toFixed(4);
                    }
                    if (_this.balencelist[i].currencyCode == localStorage.getItem("selling_crypto_asset").toUpperCase()) {
                        _this.selelectedSellingAssetBalance = _this.balencelist[i].closingBalance.toFixed(4);
                    }
                    //  this.selelectedSellingAssetBalance
                    // else if (this.balencelist[i].currencyId == 2) {
                    //   this.btcBalance = this.balencelist[i].closingBalance;
                    // }
                    // else if (this.balencelist[i].currencyId == 3) {
                    //   this.ethBAlance = this.balencelist[i].closingBalance;
                    // }
                    // else if (this.balencelist[i].currencyId == 4) {
                    //   this.bccBalance = this.balencelist[i].closingBalance;
                    // }
                    // else if (this.balencelist[i].currencyId == 5) {
                    //   this.diamBalance = this.balencelist[i].closingBalance;
                    // }
                    // else if (this.balencelist[i].currencyId == 7) {
                    //   this.ltcBalance = this.balencelist[i].closingBalance;
                    // }
                    // else if (this.balencelist[i].currencyId == 8) {
                    //   this.hcxBalance = this.balencelist[i].closingBalance;
                    // }
                    //  else if (this.balencelist[i].currencyId == 11) {
                    //   this.etcBalance = this.balencelist[i].closingBalance;
                    // }
                    // else if (this.balencelist[i].currencyId == 12) {
                    //   this.bsvBalance = this.balencelist[i].closingBalance;
                    // }
                    // else if (this.balencelist[i].currencyId == 14) {
                    //   this.xrpBalance = this.balencelist[i].closingBalance;
                    // }
                }
                // alert(this.selelectedSellingAssetBalance);
                //   console.log('********',this.usdBalance, this.btcBalance,this.ethBAlance,this.bccBalance,this.diamBalance,this.ltcBalance,this.hcxBalance,this.etcBalance,this.bsvBalance,this.xrpBalance);
            }
            //   } else {
            //     this.buyPriceText = this.data.CURRENCYICON + ' 10000';
            //     this.sellPriceText = this.data.CURRENCYICON + ' 45200.00' + this.sellPrice;
            //     this.fiatBalance = 10000;
            //     this.fiatBalanceText = this.data.CURRENCYICON + ' 10000';
            //     this.selelectedSellingAssetBalance = '4569.87';
            //     this.selelectedSellingAssetBalance = '10000.00';
            //   }
            //   if (result.error.error_data == 1) {
            //     if (result.error.error_msg)
            //       this.data.alert(result.error.error_msg, 'danger');
            //   } else
            //     $('#warn').click();
            // } else {
            //   this.totalFiatBalance = this.data.CURRENCYICON + (result.userBalanceResult.fiat_balance).toFixed(2);
            //   this.fiatBalanceLabel = 'Total ' + this.data.CURRENCYNAME + ' Balance';
            //   if (result.userBalanceResult.btc_balance == 'null' || result.userBalanceResult.btc_balance == null) {
            //     this.btcBalance = '0'
            //   } else {
            //     this.btcBalance = result.userBalanceResult.btc_balance;
            //   }
            //   if (result.userBalanceResult.bch_balance == 'null' || result.userBalanceResult.bch_balance == null) {
            //     this.btcBalance = '0'
            //   } else {
            //     this.bchBalance = result.userBalanceResult.bch_balance;
            //   }
            //   if (result.userBalanceResult.hcx_balance == 'null' || result.userBalanceResult.hcx_balance == null) {
            //     this.hcxBalance = '0'
            //   } else {
            //     this.hcxBalance = result.userBalanceResult.hcx_balance;
            //   }
            //   if (result.userBalanceResult.iec_balance == 'null' || result.userBalanceResult.iec_balance == null) {
            //     this.iecBalance = '0'
            //   } else {
            //     this.iecBalance = result.userBalanceResult.iec_balance;
            //   }
            //   this.buyPrice = result.userBalanceResult.crypto_buy_price;
            //   this.btcBalanceInUsd = (parseFloat(this.btcBalance) * parseFloat(this.buyPrice)).toFixed(2);
            //   this.bchBalanceInUsd = (parseFloat(this.bchBalance) * parseFloat(this.buyPrice)).toFixed(2);
            //   this.hcxBalanceInUsd = (parseFloat(this.hcxBalance) * parseFloat(this.buyPrice)).toFixed(2);
            //   this.iecBalanceInUsd = (parseFloat(this.iecBalance) * parseFloat(this.buyPrice)).toFixed(2);
            //   // this.buyPrice=(this.buyPrice).toFixed(2);
            //   this.sellPrice = result.userBalanceResult.crypto_sell_price;
            //   //this.sellPrice=(this.sellPrice).toFixed(2);
            //   //  this.buyPriceText=CURRENCYICON+' '+ this.buyPrice;
            //   this.sellPriceText = this.data.CURRENCYICON + ' ' + this.sellPrice;
            //   this.fiatBalance = result.userBalanceResult.fiat_balance;
            //   this.fiatBalanceText = this.data.CURRENCYICON + ' ' + result.userBalanceResult.fiat_balance;
            //   this.selectedCryptoCurrency = localStorage.getItem('selected_currency');
            //   this.selectedCryptoCurrencyBuy = (localStorage.getItem('buying_crypto_asset')).toUpperCase();
            //   this.selectedCryptoCurrencySell = (localStorage.getItem('selling_crypto_asset')).toUpperCase();
            //   if (result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'] == null || result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'] == 'null') {
            //     this.selectedCryptoCurrencyBalance = '0';
            //   } else {
            //     this.selectedCryptoCurrencyBalance = result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'];
            //   }
            //   this.btcBought = result.userBalanceResult.bitcoins_bought;
            //   this.btcSold = result.userBalanceResult.bitcoins_sold;
            //   this.bchBought = result.userBalanceResult.bitcoinCash_bought;
            //   this.bchSold = result.userBalanceResult.bitcoinCash_sold;
            //   this.hcxBought = result.userBalanceResult.hcx_bought;
            //   this.hcxSold = result.userBalanceResult.hcx_sold;
            //   this.iecBought = result.userBalanceResult.iec_bought;
            //   this.iecSold = result.userBalanceResult.iec_sold;
            //   if (localStorage.getItem('buying_crypto_asset') != 'usd') {
            //     this.selelectedBuyingAssetBalance = parseFloat(result.userBalanceResult[localStorage.getItem('buying_crypto_asset') + '_balance']).toFixed(4);
            //   } else if (
            //     ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'btc')) ||
            //     ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'eth'))
            //   ) {
            //     this.selelectedBuyingAssetBalance = parseFloat(result.userBalanceResult[localStorage.getItem('buying_crypto_asset') + '_balance']).toFixed(8);
            //   } else {
            //     this.selelectedBuyingAssetBalance = (result.userBalanceResult.fiat_balance).toFixed(2);
            //   }
            //   if (localStorage.getItem('selling_crypto_asset') != 'usd') {
            //     this.selelectedSellingAssetBalance = parseFloat(result.userBalanceResult[localStorage.getItem('selling_crypto_asset') + '_balance']).toFixed(4);
            //   } else if (
            //     ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'btc')) ||
            //     ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'eth'))
            //   ) {
            //     this.selelectedSellingAssetBalance = (result.userBalanceResult.fiat_balance).toFixed(8);
            //   } else {
            //     this.selelectedSellingAssetBalance = (result.userBalanceResult.fiat_balance).toFixed(2);
            //   }
            //   // console.log('this.selelectedBuyingAssetBalance', this.selelectedSellingAssetBalance);
            // }
        }, function (reason) {
            // wip(0);
            if (reason.error.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                console.error(reason);
            }
        });
    };
    StopLossComponent.prototype.sellStoploss = function () {
        var _this = this;
        $('#placeOrderForStopLossBtn').attr('disabled', true);
        $('.stopLossError').hide();
        this.data.alert('Loading...', 'dark');
        if (this.stopLossPrice != undefined && this.stopLossTriggerPrice != undefined && this.stopLossQuantity != undefined) {
            // wip(1);
            this.http.get(this.data.TRADESERVICE + '/getAmountSell/' + localStorage.getItem('buying_crypto_asset').toUpperCase() + localStorage.getItem('selling_crypto_asset').toUpperCase() + '/' + localStorage.getItem('selling_crypto_asset').toUpperCase() + localStorage.getItem('buying_crypto_asset').toUpperCase() + '/' + this.stopLossQuantity)
                .subscribe(function (data) {
                $('#placeOrderForStopLossBtn').attr('disabled', false);
                // wip(0);
                var result = data;
                if (result.code == '0') {
                    _this.marketOrderPrice = parseFloat(result.price);
                    if (_this.marketOrderPrice > _this.stopLossTriggerPrice &&
                        _this.marketOrderPrice > _this.stopLossPrice &&
                        _this.stopLossTriggerPrice > _this.stopLossPrice) {
                        var inputObj = {};
                        inputObj['buying_asset_code'] = localStorage.getItem('selling_crypto_asset').toUpperCase();
                        inputObj['userId'] = localStorage.getItem('user_id');
                        inputObj['selling_asset_code'] = localStorage.getItem('buying_crypto_asset').toUpperCase();
                        inputObj['quantity'] = _this.stopLossQuantity;
                        inputObj['stop_loss_price'] = _this.stopLossPrice;
                        inputObj['trigger_price'] = _this.stopLossTriggerPrice;
                        inputObj['txn_type'] = '2';
                        var jsonString = JSON.stringify(inputObj);
                        // wip(1);
                        _this.http.post(_this.data.WEBSERVICE + '/userTrade/StopLossBuySellTrade', jsonString, {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                            .subscribe(function (data) {
                            // wip(0);
                            _this.data.loader = false;
                            var result = data;
                            if (result.error.error_data != '0') {
                                if (result.error.error_data == 1)
                                    _this.data.alert(result.error.error_msg, 'danger');
                                else
                                    $('#warn').click();
                            }
                            else {
                                _this.data.alert(result.error.error_msg, 'success');
                                $('#trade').click();
                                // wip(0);
                                //location.reload();
                            }
                        });
                    }
                    else {
                        _this.stopLossError = '*Market order price should be greater than trigger price & trigger price should be greater than stop loss price';
                        $('.stopLossError').html(_this.stopLossError);
                        $('.stopLossError').show();
                        _this.data.loader = false;
                    }
                }
                else {
                    _this.stopLossError = '*Orderbook depth reached, price not found';
                    $('.stopLossError').html(_this.stopLossError);
                    $('.stopLossError').show();
                    _this.data.loader = false;
                }
            });
        }
        else {
            this.data.alert('Please Provide Proper Details', 'error');
        }
    };
    StopLossComponent.prototype.buyStopLoss = function () {
        var _this = this;
        $('#buyForStopLossBtn').attr('disabled', true);
        $('.stopLossError').hide();
        this.data.alert('Loading...', 'dark');
        if (this.stopLossPrice != undefined && this.stopLossTriggerPrice != undefined && this.stopLossQuantity != undefined) {
            // wip(1);
            this.http.get(this.data.TRADESERVICE + '/getAmountBuy/' + localStorage.getItem('selling_crypto_asset').toUpperCase() + localStorage.getItem('buying_crypto_asset').toUpperCase() + '/' + localStorage.getItem('buying_crypto_asset').toUpperCase() + localStorage.getItem('selling_crypto_asset').toUpperCase() + '/' + this.stopLossQuantity)
                .subscribe(function (data) {
                $('#buyForStopLossBtn').attr('disabled', false);
                // wip(0);
                var result = data;
                if (result.code == '0') {
                    _this.marketOrderPrice = parseFloat(result.price);
                    console.log(_this.marketOrderPrice, _this.stopLossTriggerPrice, _this.stopLossPrice, _this.stopLossQuantity);
                    if (_this.marketOrderPrice < _this.stopLossTriggerPrice &&
                        _this.marketOrderPrice < _this.stopLossPrice &&
                        _this.stopLossTriggerPrice < _this.stopLossPrice) {
                        var inputObj = {};
                        inputObj['buying_asset_code'] = localStorage.getItem('buying_crypto_asset').toUpperCase();
                        inputObj['userId'] = localStorage.getItem('user_id');
                        inputObj['selling_asset_code'] = localStorage.getItem('selling_crypto_asset').toUpperCase();
                        inputObj['quantity'] = _this.stopLossQuantity;
                        inputObj['stop_loss_price'] = _this.stopLossPrice;
                        inputObj['trigger_price'] = _this.stopLossTriggerPrice;
                        inputObj['txn_type'] = '1';
                        var jsonString = JSON.stringify(inputObj);
                        // wip(1);
                        _this.http.post(_this.data.WEBSERVICE + '/userTrade/StopLossBuySellTrade', jsonString, {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                            .subscribe(function (data) {
                            _this.data.loader = false;
                            // wip(0);
                            var result = data;
                            if (result.error.error_data != '0') {
                                if (result.error.error_data == 1)
                                    _this.data.alert(result.error.error_msg, 'danger');
                                else
                                    $('#warn').click();
                            }
                            else {
                                _this.data.alert(result.error.error_msg, 'success');
                                $('#trade').click();
                                // wip(0);
                                //location.reload();
                            }
                            _this.stopLossPrice = _this.stopLossTriggerPrice = _this.stopLossQuantity = null;
                        });
                    }
                    else {
                        _this.stopLossError = '*Market order price should be less than trigger price & trigger price should be less than stop loss price';
                        $('.stopLossError').html(_this.stopLossError);
                        $('.stopLossError').show();
                        _this.data.loader = false;
                    }
                }
                else {
                    _this.stopLossError = '*Orderbook depth reached, price not found';
                    $('.stopLossError').html(_this.stopLossError);
                    $('.stopLossError').show();
                    _this.data.loader = false;
                }
            });
        }
        else {
            this.data.alert('Please Provide Proper Details', 'warning');
        }
    };
    StopLossComponent.prototype.warnKyc = function (content) {
        this.modalService.open(content, {
            centered: true
        });
    };
    StopLossComponent.prototype.nonNg = function (event) {
        var val = event.target.value;
        if (val < 0)
            this.data.alert('Price cannot be negative', 'warning');
    };
    StopLossComponent.prototype.send = function (content, val) {
        this.mywallet.getCurrencyForSend(content, val, '', '');
    };
    StopLossComponent.prototype.validateLimit = function () {
        var lv = 0.000001;
        return 0.0001 >= this.limitAmount || 0.00000001 >= this.limitPrice /* || lv >= this.limitValue*/;
    };
    StopLossComponent = __decorate([
        Component({
            selector: 'app-stop-loss',
            templateUrl: './stop-loss.component.html',
            styleUrls: ['./stop-loss.component.css']
        }),
        __metadata("design:paramtypes", [CoreDataService, HttpClient, BodyService, DashboardComponent, NgbModal, MyWalletComponent])
    ], StopLossComponent);
    return StopLossComponent;
}());
export { StopLossComponent };
//# sourceMappingURL=stop-loss.component.js.map