import { async, TestBed } from '@angular/core/testing';
import { IdentityVerificationComponent } from './identity-verification.component';
describe('IdentityVerificationComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [IdentityVerificationComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(IdentityVerificationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=identity-verification.component.spec.js.map