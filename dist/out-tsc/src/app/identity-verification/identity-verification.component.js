var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { BodyService } from "../body.service";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import * as $ from "jquery";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
var IdentityVerificationComponent = /** @class */ (function () {
    function IdentityVerificationComponent(main, http, data, modalService, route) {
        this.main = main;
        this.http = http;
        this.data = data;
        this.modalService = modalService;
        this.route = route;
    }
    // public contries: any;
    // currentDate: any = new Date().toISOString().slice(0, 10);
    // currentYear: any = new Date().getFullYear();
    // currentMonth: any = new Date().getMonth() + 1;
    // currentDay: any = new Date().getDate();
    IdentityVerificationComponent.prototype.ngOnInit = function () {
        this.main.getDashBoardInfo();
        this.getIdentityDetails();
        // this.getLoc();
    };
    IdentityVerificationComponent.prototype.getIdentityDetails = function () {
        var _this = this;
        this.data.alert("Loading...", "dark");
        var identityObj = {};
        identityObj["userId"] = localStorage.getItem("user_id");
        var jsonString = JSON.stringify(identityObj);
        // wip(1);
        this.http
            .post(this.data.WEBSERVICE + "/user/GetUserDetails", jsonString, {
            headers: {
                "Content-Type": "application/json",
                authorization: "BEARER " + localStorage.getItem("access_token")
            }
        })
            .subscribe(function (response) {
            // wip(0);
            var result = response;
            //   console.log(response);
            if (result.error.error_data) {
                _this.data.alert(result.error.error_msg, "danger");
            }
            else {
                //$('.pan_card').val(result.userResult.ssn);
                if (result.userListResult[0].profilePic != "") {
                    _this.main.profilePic =
                        _this.data.WEBSERVICE +
                            "/user/" +
                            localStorage.getItem("user_id") +
                            "/file/" +
                            result.userListResult[0].profilePic +
                            "?access_token=" +
                            localStorage.getItem("access_token");
                }
                else {
                    _this.main.profilePic = "./assets/img/default.png";
                }
                if (result.userListResult[0].idProofDoc != "") {
                    _this.panCardPic =
                        _this.data.WEBSERVICE +
                            "/user/" +
                            localStorage.getItem("user_id") +
                            "/file/" +
                            result.userListResult[0].idProofDoc +
                            "?access_token=" +
                            localStorage.getItem("access_token");
                }
                else {
                    _this.panCardPic = "./assets/img/file-empty-icon.png";
                }
                if (result.userListResult[0].addressProofDoc != "") {
                    _this.aadharCardFront =
                        _this.data.WEBSERVICE +
                            "/user/" +
                            localStorage.getItem("user_id") +
                            "/file/" +
                            result.userListResult[0].addressProofDoc +
                            "?access_token=" +
                            localStorage.getItem("access_token");
                }
                else {
                    _this.aadharCardFront = "./assets/img/file-empty-icon.png";
                }
                if (result.userListResult[0].addressProofDoc2 != "") {
                    _this.aadharCardBack =
                        _this.data.WEBSERVICE +
                            "/user/" +
                            localStorage.getItem("user_id") +
                            "/file/" +
                            result.userListResult[0].addressProofDoc2 +
                            "?access_token=" +
                            localStorage.getItem("access_token");
                }
                else {
                    _this.aadharCardBack = "./assets/img/file-empty-icon.png";
                }
            }
        }, function (reason) {
            // wip(0);
            if (reason.data.error == "invalid_token") {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert("Could Not Connect To Server", "danger");
            }
        }, function () {
            _this.data.loader = false;
        });
    };
    IdentityVerificationComponent.prototype.upload = function (content) {
        //console.log("form submit");
        if ($(".profile_pic")[0].files[0] != undefined ||
            $(".pan_card_pic")[0].files[0] != undefined ||
            $(".aadhar_card_front_side")[0].files[0] != undefined ||
            $(".aadhar_card_back_side")[0].files[0] != undefined) {
            this.modalService.open(content);
        }
        else {
            this.data.alert("Please submit a detail to update", "warning");
        }
    };
    // docCountry:'';
    // dateofbirth;
    // doctype;
    IdentityVerificationComponent.prototype.uploadDocs = function () {
        var _this = this;
        this.data.alert("Loading...", "dark");
        var fd = new FormData();
        fd.append("userId", localStorage.getItem("user_id"));
        fd.append("ssn", $(".pan_card").val());
        if ($(".profile_pic")[0].files[0] != undefined) {
            fd.append("profile_pic", $(".profile_pic")[0].files[0]);
        }
        else {
            fd.append("profile_pic", "");
        }
        if ($(".pan_card_pic")[0].files[0] != undefined) {
            fd.append("id_proof_doc", $(".pan_card_pic")[0].files[0]);
        }
        else {
            fd.append("id_proof_doc", "");
        }
        if ($(".aadhar_card_front_side")[0].files[0] != undefined) {
            fd.append("address_proof_doc", $(".aadhar_card_front_side")[0].files[0]);
        }
        else {
            fd.append("address_proof_doc", "");
        }
        if ($(".aadhar_card_back_side")[0].files[0] != undefined) {
            fd.append("address_proof_doc_2", $(".aadhar_card_back_side")[0].files[0]);
        }
        else {
            fd.append("address_proof_doc_2", "");
        }
        this.http
            .post(this.data.WEBSERVICE + "/user/UpdateUserDocs", fd, {
            headers: {
                Authorization: "BEARER " + localStorage.getItem("access_token")
            }
        })
            .subscribe(function (result) {
            _this.data.loader = false;
            if (result.error.error_data != "0") {
                _this.data.alert(result.error.error_msg, "dark");
            }
            else {
                _this.data.alert("Identity Verification Documents Submitted", "success");
                _this.getIdentityDetails();
            }
        }, function (error) {
            _this.data.alert(error.error.error_description, "danger");
        });
    };
    IdentityVerificationComponent.prototype.getSize = function (content) {
        var sz = $('#' + content)[0].files[0];
        // console.log(sz);
        if (sz.type == "image/jpeg") {
            if (sz.size > 2000000) {
                this.data.alert('File size should be less than 2MB', 'warning');
                $('#' + content).val('');
            }
        }
        else {
            this.data.alert('File should be in JPG or JPEG. ' + sz.type.split('/')[1].toUpperCase() + ' is not allowed', 'warning');
            $('#' + content).val('');
        }
    };
    IdentityVerificationComponent = __decorate([
        Component({
            selector: "app-identity-verification",
            templateUrl: "./identity-verification.component.html",
            styleUrls: ["./identity-verification.component.css"]
        }),
        __metadata("design:paramtypes", [BodyService,
            HttpClient,
            CoreDataService,
            NgbModal,
            Router])
    ], IdentityVerificationComponent);
    return IdentityVerificationComponent;
}());
export { IdentityVerificationComponent };
//# sourceMappingURL=identity-verification.component.js.map