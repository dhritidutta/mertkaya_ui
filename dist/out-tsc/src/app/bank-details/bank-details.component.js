var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { CoreDataService } from '../core-data.service';
import { BodyService } from '../body.service';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
var BankDetailsComponent = /** @class */ (function () {
    function BankDetailsComponent(data, main, http, modalService) {
        this.data = data;
        this.main = main;
        this.http = http;
        this.modalService = modalService;
    }
    BankDetailsComponent.prototype.ngOnInit = function () {
        this.main.getDashBoardInfo();
        this.getBankDetails();
    };
    //get bank details
    BankDetailsComponent.prototype.getBankDetails = function () {
        var _this = this;
        this.data.alert('Loading...', 'dark');
        var bankDetailsObj = {};
        bankDetailsObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(bankDetailsObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/GetUserBankDetails', jsonString, { headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
            _this.data.loader = false;
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
                alert(result.error.error_msg);
            }
            else {
                _this.account_no = result.bankDetails.account_no;
                _this.beneficiary_name = result.bankDetails.benificiary_name;
                _this.bank_name = result.bankDetails.bank_name;
                _this.routing_no = result.bankDetails.routing_no;
                _this.accountNo = result.bankDetails.account_no;
                _this.beneficiaryName = result.bankDetails.benificiary_name;
                _this.bankName = result.bankDetails.bank_name;
                _this.routingNo = result.bankDetails.routing_no;
                $('.update_bank_details_btn').hide();
            }
        }, function (error) {
            _this.data.logout();
            _this.data.alert(error.error.error_description, 'danger');
        });
    };
    BankDetailsComponent.prototype.checkBankInputfield = function (nameField) {
        if (nameField == 'beneficiary_name') {
            if (this.beneficiaryName != this.beneficiary_name) {
                $('.update_bank_details_btn').show();
            }
            else {
                $('.update_bank_details_btn').hide();
            }
        }
        if (nameField == 'bank_name') {
            if (this.bankName != this.bank_name) {
                $('.update_bank_details_btn').show();
            }
            else {
                $('.update_bank_details_btn').hide();
            }
        }
        if (nameField == 'account_no') {
            if (this.accountNo != this.account_no) {
                $('.update_bank_details_btn').show();
            }
            else {
                $('.update_bank_details_btn').hide();
            }
        }
        if (nameField == 'routing_no') {
            if (this.routingNo != this.IFSC_code) {
                $('.update_bank_details_btn').show();
            }
            else {
                $('.update_bank_details_btn').hide();
            }
        }
    };
    BankDetailsComponent.prototype.updateBankDetails = function (content) {
        if (this.beneficiary_name != '' && this.bank_name != '' && this.account_no != '' && this.routing_no != '') {
            this.modalService.open(content, { centered: true });
        }
        else {
            this.data.alert('All fields are mandatory. Please fill up and submit again', 'warning');
        }
    };
    BankDetailsComponent.prototype.yesConfirm = function () {
        var _this = this;
        if (this.beneficiary_name != '' && this.bank_name != '' && this.account_no != '' && this.routing_no != '') {
            var userId = parseInt(localStorage.getItem('user_id'));
            var updateBankObj = {};
            updateBankObj['userId'] = userId;
            updateBankObj['benificiary_name'] = this.beneficiary_name;
            updateBankObj['bank_name'] = this.bank_name;
            updateBankObj['account_no'] = this.account_no;
            updateBankObj['routing_no'] = this.routing_no;
            var jsonString = JSON.stringify(updateBankObj);
            // wip(1);
            this.http.post(this.data.WEBSERVICE + '/user/UpdateUserBankDetails', jsonString, { headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                } })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'dark');
                }
                else {
                    _this.data.alert('Bank Details Updated', 'success');
                    _this.getBankDetails();
                }
            }, function (error) {
                _this.data.logout();
                _this.data.alert(error.error.error_description, 'danger');
            });
        }
        else {
            this.data.alert('You have not made any changes', 'warning');
        }
    };
    BankDetailsComponent = __decorate([
        Component({
            selector: 'app-bank-details',
            templateUrl: './bank-details.component.html',
            styleUrls: ['./bank-details.component.css']
        }),
        __metadata("design:paramtypes", [CoreDataService, BodyService, HttpClient, NgbModal])
    ], BankDetailsComponent);
    return BankDetailsComponent;
}());
export { BankDetailsComponent };
//# sourceMappingURL=bank-details.component.js.map