import { TestBed, inject } from '@angular/core/testing';
import { CoreDataService } from './core-data.service';
describe('CoreDataService', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [CoreDataService]
        });
    });
    it('should be created', inject([CoreDataService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=core-data.service.spec.js.map