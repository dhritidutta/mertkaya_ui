var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { CoreDataService } from "../core-data.service";
import { Router } from "@angular/router";
import * as $ from "jquery";
//import { OrderBookComponent } from "../order-book/order-book.component";
import { HttpClient } from "@angular/common/http";
import { TvChartContainerComponent } from "../tv-chart-container/tv-chart-container.component";
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(data, route, http, tvChartContainerComponent) {
        this.data = data;
        this.route = route;
        this.http = http;
        this.tvChartContainerComponent = tvChartContainerComponent;
        this.chart = true;
        this.orderbook = true;
        this.trade = true;
        this.stoploss = true;
        this.drag = false;
        this.mode = false;
        this.any = 0;
        this.ctpdata = 0;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectedSellingCryptoCurrencyName = "try";
        this.selectedBuyingCryptoCurrencyName = "btc";
        this.data.changescreencolor = false;
        this.Themecolor = 'Dark';
        localStorage.setItem('themecolor', this.Themecolor);
        this.http
            .get(this.data.CHARTSERVISE +
            "trendsTradeGraphFor24Hours/" +
            this.selectedBuyingCryptoCurrencyName +
            "/" +
            this.selectedSellingCryptoCurrencyName)
            .subscribe(function (value) {
            if (value != "") {
                _this.chartlist = value[0];
                _this.ctpdata = _this.data.ctpdata = _this.chartlist.CTP;
                _this.lowprice = _this.data.lowprice = _this.chartlist.LOW_PRICE;
                _this.highprice = _this.data.highprice = _this.chartlist.HIGH_PRICE;
                // alert(123);
                // console.log(123);
                // this.ctpdata= 0;
                // this.lowprice= 0;
                // this.highprice= 0;
            }
        });
        $(document).ready(function () {
            $(this).scrollTop(0);
            var i = 1;
            $(".drg").click(function () {
                i++;
                $(this).css("z-index", i);
            });
            $(':input[type="number"]').keyup(function () {
                //  console.log('edde');
            });
        });
    };
    DashboardComponent.prototype.changebg = function (val) {
        //  $("#tv_chart_container").find('html').addClass("theme-darkhh");
        this.screencolor = val;
        this.data.changescreencolor = val;
        if (this.data.changescreencolor == true) {
            this.Themecolor = 'Light';
            localStorage.setItem('themecolor', this.Themecolor);
            $(".content-wrapper").css("background-color", "#ececec").addClass("intro");
            document.getElementById("night").style.display = "block";
            document.getElementById("light").style.display = "none";
            // document.getElementsByClassName('widget')[0].style.background = '#292931';
        }
        else {
            $(".content-wrapper").css("background-color", "#060707").removeClass("intro");
            //$("body").css("color", "white");
            document.getElementById("light").style.display = "block";
            document.getElementById("night").style.display = "none";
            document.getElementById("drag-btn").style.backgroundColor = "#292931";
            this.Themecolor = 'Dark';
            localStorage.setItem('themecolor', this.Themecolor);
        }
    };
    DashboardComponent.prototype.randomNoForOrderBook = function (minVal, maxVal) {
        var minVal1 = parseInt(minVal);
        var maxVal1 = parseInt(maxVal);
        return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
    };
    DashboardComponent = __decorate([
        Component({
            selector: "app-dashboard",
            templateUrl: "./dashboard.component.html",
            styleUrls: ["./dashboard.component.css"]
        }),
        __metadata("design:paramtypes", [CoreDataService,
            Router,
            HttpClient,
            TvChartContainerComponent])
    ], DashboardComponent);
    return DashboardComponent;
}());
export { DashboardComponent };
//# sourceMappingURL=dashboard.component.js.map