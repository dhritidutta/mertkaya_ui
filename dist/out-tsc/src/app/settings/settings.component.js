var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BodyService } from '../body.service';
import { CoreDataService } from '../core-data.service';
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(http, main, data, modalService) {
        this.http = http;
        this.main = main;
        this.data = data;
        this.modalService = modalService;
        this.match = false;
        this.out = false;
    }
    SettingsComponent.prototype.ngOnInit = function () {
        this.main.getDashBoardInfo();
        this.getUserAppSetting();
    };
    SettingsComponent.prototype.changePassword = function (isValid) {
        var _this = this;
        if (isValid) {
            var changePasswordObj = {};
            changePasswordObj['userId'] = localStorage.getItem('user_id');
            changePasswordObj['password'] = this.oldPassword;
            changePasswordObj['newPassword'] = this.newPassword;
            var jsonString = JSON.stringify(changePasswordObj);
            // wip(1);
            this.http.post(this.data.WEBSERVICE + '/user/ChangePassword', jsonString, { headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                } })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    _this.data.alert('Password Changed Successfully,You will be logged out', 'success');
                    _this.data.logout();
                }
            }, function (reason) {
                // wip(0);
                if (reason.data.error == 'invalid_token') {
                    this.data.logout();
                }
                else {
                    this.data.alert('Could Not Connect To Server', 'danger');
                }
            });
        }
        else {
            // ui_alert('Please Provide the proper details','');
        }
    };
    SettingsComponent.prototype.matchPassword = function () {
        if (this.retypePassword != undefined) {
            if (this.newPassword == this.retypePassword) {
                $('.match_error').css('color', 'green');
                $('.match_error').html('<i class="fa fa-check"></i> Password Matched');
                $('.generate_password_otp_btn').removeAttr('disabled');
                this.match = true;
            }
            else {
                $('.match_error').css('color', 'red');
                $('.match_error').html('<i class="fa fa-times"></i> Password Mismatched');
                $('.generate_password_otp_btn').attr('disabled', 'disabled');
                this.match = false;
            }
        }
        else {
            $('.match_error').html('');
            $('.generate_password_otp_btn').attr('disabled', 'disabled');
        }
    };
    SettingsComponent.prototype.getUserAppSetting = function () {
        var _this = this;
        this.data.alert('Loading...', 'dark');
        var settingObj = {};
        settingObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(settingObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, { headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
            _this.data.loader = false;
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.lockOutgoingTransactionStatus = result.userAppSettingsResult.lock_outgoing_transactions;
                _this.twoFactorStatus = result.userAppSettingsResult.two_factor_auth;
                _this.incomingTransactionAlert = result.userAppSettingsResult.incoming_transactions_alert;
                _this.pinLock = result.userAppSettingsResult.pin_lock;
                _this.soundAlert = result.userAppSettingsResult.sound_alert;
                _this.alertRate = result.userAppSettingsResult.alert_rate;
                _this.rateAlert = result.userAppSettingsResult.rate_alert;
                _this.vibrateAlert = result.userAppSettingsResult.vibrate_alert;
                if (result.userAppSettingsResult.user_docs_status == '') {
                    _this.indentificationStatus = 'Identity verification documents not submitted';
                }
                if (result.userAppSettingsResult.user_docs_status == '1') {
                    _this.indentificationStatus = 'Identity verification documents verified';
                }
                if (result.userAppSettingsResult.user_docs_status == '0') {
                    _this.indentificationStatus = ' Identity verification documents submitted for Verification';
                }
                if (result.userAppSettingsResult.user_docs_status == '2') {
                    _this.indentificationStatus = ' Identity verification documents declined, please submit again';
                }
                if (result.userAppSettingsResult.bank_details_status == '') {
                    _this.bankDetailStatus = 'Bank details not submitted';
                }
                if (result.userAppSettingsResult.bank_details_status == '0') {
                    _this.bankDetailStatus = 'Bank details  submitted for Verification';
                }
                if (result.userAppSettingsResult.bank_details_status == '2') {
                    _this.indentificationStatus = ' Bank details verified';
                }
                if (result.userAppSettingsResult.bank_details_status == '3') {
                    _this.bankDetailStatus = ' Identity verification documents declined, please submit again';
                }
                if (_this.twoFactorStatus == 0) {
                    $('.twoFactorTrueEffect').hide();
                    $('.twoFactorFalseEffect').show();
                }
                if (_this.twoFactorStatus == 1) {
                    $('.twoFactorFalseEffect').hide();
                    $('.twoFactorTrueEffect').show();
                }
            }
        }, function (reason) {
            // wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'danger');
            }
        });
    };
    SettingsComponent.prototype.updateUserAppSettings = function () {
        var _this = this;
        var updateSettingObj = {};
        updateSettingObj['userId'] = localStorage.getItem('user_id');
        updateSettingObj['lock_outgoing_transactions'] = this.lockOutgoingTransactionStatus;
        updateSettingObj['two_factor_auth'] = this.twoFactorStatus;
        updateSettingObj['rate_alert'] = this.rateAlert;
        updateSettingObj['vibrate_alert'] = this.vibrateAlert;
        updateSettingObj['incoming_transactions_alert'] = this.incomingTransactionAlert;
        updateSettingObj['pin_lock'] = this.pinLock;
        updateSettingObj['sound_alert'] = this.soundAlert;
        updateSettingObj['alert_rate'] = this.alertRate;
        var jsonString = JSON.stringify(updateSettingObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/SaveUserAppSettings', jsonString, { headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.getUserAppSetting();
            }
        }, function (reason) {
            // wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'danger');
            }
        });
    };
    SettingsComponent.prototype.changeStatusFor2FactorAuth = function (content) {
        var _this = this;
        this.twoFactorStatus = !this.twoFactorStatus;
        if (this.twoFactorStatus == true) {
            //   this.twoFactorStatus='1';
            this.out = false;
            this.googleTwoFactorAuth(content);
        }
        else {
            this.out = true;
            this.twoFactorStatus = '0';
            this.lockOutgoingTransactionStatus = '0';
            this.updateUserAppSettings();
            this.data.alert('Two factor verification turned off. Login Again', 'info');
            setTimeout(function () {
                _this.data.logout();
            }, 400);
        }
    };
    SettingsComponent.prototype.changeStatusForLockOutgoingTransaction = function () {
        var _this = this;
        this.lockOutgoingTransactionStatus = !this.lockOutgoingTransactionStatus;
        if (this.lockOutgoingTransactionStatus == true) {
            this.lockOutgoingTransactionStatus = '1';
        }
        else {
            this.lockOutgoingTransactionStatus = '0';
        }
        var updateSettingObj = {};
        updateSettingObj['userId'] = localStorage.getItem('user_id');
        updateSettingObj['lock_outgoing_transactions'] = this.lockOutgoingTransactionStatus;
        updateSettingObj['two_factor_auth'] = this.twoFactorStatus;
        updateSettingObj['rate_alert'] = this.rateAlert;
        updateSettingObj['vibrate_alert'] = this.vibrateAlert;
        updateSettingObj['incoming_transactions_alert'] = this.incomingTransactionAlert;
        updateSettingObj['pin_lock'] = this.pinLock;
        updateSettingObj['sound_alert'] = this.soundAlert;
        updateSettingObj['alert_rate'] = this.alertRate;
        var jsonString = JSON.stringify(updateSettingObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/SaveUserAppSettings', jsonString, { headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
            // wip(0);
            var result = response.data;
            if (result.error.error_data != '0') {
                //  ui_alert(result.error.error_msg,'');
            }
            else {
                _this.getUserAppSetting();
            }
        }, function (reason) {
            // wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server');
            }
        });
    };
    SettingsComponent.prototype.googleTwoFactorAuth = function (content) {
        var _this = this;
        var inputObj = {};
        inputObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(inputObj);
        // wip(1);
        this.http.post(this.data.WEBSERVICE + '/user/GetTwoFactorykey', jsonString, { headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                $('#google_auth_qr_code').html('');
                _this.google_auth_otp = '';
                //otpauth://totp/Example%20Company%3Atest%40example.com?secret=QUU6EA2GHORGMD22SN2YKU6VKISCKYAG&issuer=Example%20Company
                _this.twoFactorAuthKey = 'otpauth://totp/' + localStorage.getItem('email') + '?secret=' + result.userResult.twoFactorAuthKey + '&issuer=Tomya';
                _this.modalService.open(content, { centered: true });
                $('.google_auth_qr').show();
                // new QRCode(document.getElementById("google_auth_qr_code"), this.twoFactorAuthKey);
                //   $('#google_auth_otp').focus();
                //   $('#googleAuthModal').modal('show');
            }
        }, function (reason) {
            // wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'danger');
            }
        });
    };
    SettingsComponent.prototype.sendGoogleAuthOtp = function () {
        var _this = this;
        if (this.google_auth_otp != undefined) {
            var inputObj = {};
            inputObj['email'] = localStorage.getItem('email');
            inputObj['otp'] = this.google_auth_otp;
            var jsonString = JSON.stringify(inputObj);
            // wip(1);
            this.http.post(this.data.WEBSERVICE + '/user/CheckTwoFactor', jsonString, { headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                } })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.twoFactorStatus = '0';
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    // $('#googleAuthModal').modal('hide');
                    _this.twoFactorStatus = '1';
                    _this.updateUserAppSettings();
                    _this.data.alert('Two factor verification updated. Login Again', 'success');
                    _this.data.logout();
                }
            }, function (reason) {
                // wip(0);
                if (reason.data.error == 'invalid_token') {
                    this.data.logout();
                }
                else {
                    this.data.logout();
                    this.data.alert('Could Not Connect To Server', 'danger');
                }
            });
        }
        else {
            alert('Please Enter Otp');
        }
    };
    SettingsComponent.prototype.cancelSettings = function () {
        $('#googleAuthModal').modal('hide');
        if (this.twoFactorStatus == true) {
            this.twoFactorStatus = '0';
            this.lockOutgoingTransactionStatus = '0';
            this.updateUserAppSettings();
        }
        else {
            this.twoFactorStatus = '1';
            this.updateUserAppSettings();
        }
    };
    SettingsComponent.prototype.passModal = function (content) {
        this.modalService.open(content, { centered: true });
    };
    SettingsComponent = __decorate([
        Component({
            selector: 'app-settings',
            templateUrl: './settings.component.html',
            styleUrls: ['./settings.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient, BodyService, CoreDataService, NgbModal])
    ], SettingsComponent);
    return SettingsComponent;
}());
export { SettingsComponent };
//# sourceMappingURL=settings.component.js.map