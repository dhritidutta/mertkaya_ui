import { async, TestBed } from '@angular/core/testing';
import { TvChartContainerComponent } from './tv-chart-container.component';
describe('TvChartContainerComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [TvChartContainerComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(TvChartContainerComponent);
        component = fixture.componentInstance;
    });
    it('should be created', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=tv-chart-container.component.spec.js.map