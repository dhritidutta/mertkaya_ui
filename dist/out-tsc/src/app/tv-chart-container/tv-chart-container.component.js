var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { widget, } from '../../assets/charting_library/charting_library.min';
var TvChartContainerComponent = /** @class */ (function () {
    function TvChartContainerComponent() {
        // private _symbol: ChartingLibraryWidgetOptions['symbol'] = 'AAPL';
        this._symbol = localStorage.getItem("buying_crypto_asset") + '/' + localStorage.getItem("selling_crypto_asset");
        this._interval = 'D';
        // BEWARE: no trailing slash is expected in feed URL
        //private _datafeedUrl = 'https://demo_feed.tradingview.com';
        this._datafeedUrl = 'https://api.tomya.com/api/public';
        this._libraryPath = '/assets/charting_library/';
        // private _chartsStorageUrl: ChartingLibraryWidgetOptions['charts_storage_url'] = 'https://saveload.tradingview.com';
        this._chartsStorageUrl = '';
        this._chartsStorageApiVersion = '1.1';
        this._clientId = 'tradingview.com';
        this._userId = 'public_user_id';
        this._fullscreen = false;
        this._autosize = true;
        this._containerId = 'tv_chart_container';
        this._tvWidget = null;
        this._theme = 'Dark';
    }
    Object.defineProperty(TvChartContainerComponent.prototype, "symbol", {
        //  public _theme:any;
        set: function (symbol) {
            this._symbol = symbol || this._symbol;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "interval", {
        set: function (interval) {
            this._interval = interval || this._interval;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "datafeedUrl", {
        set: function (datafeedUrl) {
            this._datafeedUrl = datafeedUrl || this._datafeedUrl;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "libraryPath", {
        set: function (libraryPath) {
            this._libraryPath = libraryPath || this._libraryPath;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "chartsStorageUrl", {
        set: function (chartsStorageUrl) {
            this._chartsStorageUrl = chartsStorageUrl || this._chartsStorageUrl;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "chartsStorageApiVersion", {
        set: function (chartsStorageApiVersion) {
            this._chartsStorageApiVersion = chartsStorageApiVersion || this._chartsStorageApiVersion;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "clientId", {
        set: function (clientId) {
            this._clientId = clientId || this._clientId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "userId", {
        set: function (userId) {
            this._userId = userId || this._userId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "fullscreen", {
        set: function (fullscreen) {
            this._fullscreen = fullscreen || this._fullscreen;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "theme", {
        set: function (theme) {
            this._theme = theme || this._theme;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "autosize", {
        set: function (autosize) {
            this._autosize = autosize || this._autosize;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TvChartContainerComponent.prototype, "containerId", {
        set: function (containerId) {
            this._containerId = containerId || this._containerId;
        },
        enumerable: true,
        configurable: true
    });
    TvChartContainerComponent.prototype.ngOnInit = function () {
        var _this = this;
        // alert('++++++++++');
        this.buyingAsset = localStorage.getItem("buying_crypto_asset").toLocaleUpperCase();
        this.sellingAsset = localStorage.getItem("selling_crypto_asset").toLocaleUpperCase();
        this._symbol = (this.buyingAsset) + '/' + this.sellingAsset;
        //   this._theme = 'Light';
        var test = localStorage.getItem('themecolor');
        //this._theme='Light';
        // if(test=="Light"){
        //     this._theme='Light';
        //     console.log(this._theme);
        // }
        // else{
        //     this._theme='Dark';
        //     console.log(this._theme);
        // }
        function getLanguageFromURL() {
            var regex = new RegExp('[\\?&]lang=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }
        var widgetOptions = {
            symbol: this._symbol,
            datafeed: new window.Datafeeds.UDFCompatibleDatafeed(this._datafeedUrl),
            interval: this._interval,
            container_id: this._containerId,
            library_path: this._libraryPath,
            locale: getLanguageFromURL() || 'en',
            disabled_features: ['use_localstorage_for_settings', 'header_saveload', 'header_settings', 'header_compare'],
            enabled_features: ['study_templates', 'hide_left_toolbar_by_default', 'header_indicators'],
            charts_storage_url: this._chartsStorageUrl,
            charts_storage_api_version: this._chartsStorageApiVersion,
            client_id: this._clientId,
            user_id: this._userId,
            fullscreen: this._fullscreen,
            autosize: this._autosize,
            theme: this._theme
        };
        //,'header_indicators','header_undo_redo'
        // disabled_features: ['use_localstorage_for_settings','header_saveload','header_settings','header_compare'],
        //enabled_features: ['study_templates','hide_left_toolbar_by_default','header_indicators'],
        var tvWidget = new widget(widgetOptions);
        this._tvWidget = tvWidget;
        tvWidget.onChartReady(function () {
            // alert('1st');
            tvWidget.headerReady().then(function () {
                var button = tvWidget.createButton();
                // button.setAttribute('title', 'Click to show a notification popup');
                // button.classList.add('apply-common-tooltip');
                // button.addEventListener('click', () => tvWidget.showNoticeDialog({
                //     title: 'Notification',
                //     body: 'TradingView Charting Library API works correctly',
                //     callback: () => {
                //         console.log('Noticed!');
                //     },
                // }));
                // button.innerHTML = 'Check API';
            });
            tvWidget.setSymbol(_this._symbol, 'D', function () {
                //alert('2st');
            });
        });
    };
    TvChartContainerComponent.prototype.ngDoCheck = function () {
        this.buyingAsset = localStorage.getItem("buying_crypto_asset").toLocaleUpperCase();
        this.sellingAsset = localStorage.getItem("selling_crypto_asset").toLocaleUpperCase();
        this._symbol = (this.buyingAsset) + '/' + this.sellingAsset;
        var test = localStorage.getItem('themecolor').toString();
        if (test == "Light") {
            this._tvWidget.changeTheme('Light');
        }
        else {
            this._tvWidget.changeTheme('Dark');
        }
        if (localStorage.getItem("buying_crypto_asset") != 'BTC') {
            this._tvWidget.setSymbol(this._symbol, 'D', function () {
            });
        }
    };
    TvChartContainerComponent.prototype.ngOnDestroy = function () {
        if (this._tvWidget !== null) {
            this._tvWidget.remove();
            this._tvWidget = null;
        }
    };
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "symbol", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "interval", null);
    __decorate([
        Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], TvChartContainerComponent.prototype, "datafeedUrl", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "libraryPath", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "chartsStorageUrl", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "chartsStorageApiVersion", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "clientId", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "userId", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "fullscreen", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "theme", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "autosize", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TvChartContainerComponent.prototype, "containerId", null);
    TvChartContainerComponent = __decorate([
        Component({
            selector: 'app-tv-chart-container',
            templateUrl: './tv-chart-container.component.html',
            styleUrls: ['./tv-chart-container.component.css']
        })
    ], TvChartContainerComponent);
    return TvChartContainerComponent;
}());
export { TvChartContainerComponent };
//# sourceMappingURL=tv-chart-container.component.js.map