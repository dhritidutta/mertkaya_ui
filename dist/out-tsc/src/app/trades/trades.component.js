var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from '../navbar/navbar.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
//import { OrderBookComponent } from '../order-book/order-book.component';
import { StopLossComponent } from '../stop-loss/stop-loss.component';
var TradesComponent = /** @class */ (function () {
    //
    function TradesComponent(data, http, modalService, nav, dash, _StopLossComponent) {
        this.data = data;
        this.http = http;
        this.modalService = modalService;
        this.nav = nav;
        this.dash = dash;
        this._StopLossComponent = _StopLossComponent;
        this.selldata = 0;
        this.buydata = 0;
    }
    TradesComponent.prototype.ngOnInit = function () {
        this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
        this.selectedSellingAssetText = this.data.selectedSellingAssetText;
        this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
        this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
        this.myTradeTableDisplayStatus = 0;
        // this.getMarketTradeBuy();
        this.getDashBoardInfo();
        // this.getOfferLists();
        this.GetTradeDetail();
        this.getOfferLists();
        this.loader = false;
        // console.log('trade');
        this.reload();
        // this.tradeInterval = setInterval(() => {
        //   var us = localStorage.getItem('user_id');
        //   if (us != null) {
        //     this.getOfferLists();
        //     this.getStopLossOfferForBuy();
        //     //this.getStopLossOfferForSell();
        //   }
        // }, 5000);
    };
    TradesComponent.prototype.ngDoCheck = function () {
        this.changemode();
    };
    TradesComponent.prototype.changemode = function () {
        //console.log('++++++++++++++');
        if (this.data.changescreencolor == true) {
            $(".bg_new_class").removeClass("bg-dark").css("background-color", "#fefefe");
            $(".bg-black").css("background-color", "transparent");
            $(".btn").css("color", "#000");
            $(".text-blue").css("color", "black");
        }
        else {
            $(".bg_new_class").removeClass("bg-dark").css("background-color", "#16181a");
            $(".bg-black").css("background-color", "transparent");
            $(".btn").css("color", "#fff");
            $(".text-blue").css("color", "white");
        }
    };
    //malini
    //random function for flash class in server sent orderbook
    TradesComponent.prototype.randomNoForOrderBook = function (minVal, maxVal) {
        var minVal1 = parseInt(minVal);
        var maxVal1 = parseInt(maxVal);
        return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
    };
    TradesComponent.prototype.reload = function () {
        this.GetTradeDetail();
        this.getOfferLists();
        //  this.getStopLossOfferForBuy();
        //  this.getStopLossOfferForSell();
    };
    // getMarketTradeBuy() {
    //   $('#marketTradeBody').html('<tr><td colspan="3" class="text-center"><img src="./assets/svg-loaders/three-dots.svg" alt="" width="50"></td></tr>');
    //   var result: any;
    //   this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    //   this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    //   this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    //   this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;//'GBDWSH2SAZM3U5FR6LPO4E2OQZNVZXUAZVH5TWEJD64MGYJJ7NWF4PBX';
    //   var url ='https://api.bitrump.com/StreamApi/rest/TradeHistory?baseAssetCode=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase() + '&baseAssetIssuer=' + this.sellingAssetIssuer +'&counterAssetCode=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&counterAssetIssuer=' + this.buyingAssetIssuer + '&order=desc' + '&limit=' + 50;
    //   if (this.source != undefined ) {
    //     this.source.close();
    //   }
    //   this.source= new EventSource(url);
    //   this.source.addEventListener('message', event => {
    //     result = JSON.parse(event.data);
    //     var response = JSON.parse(result.response);
    //     this.marketTradeRecords=response._embedded.records;
    //     this.itemcount = this.marketTradeRecords.length;
    //     var arraylength = this.marketTradeRecords.length;
    //         if (arraylength != null) {
    //         this.marketTradeBodyHtml = '';
    //         var randomNoForFlashArr = [];
    //         var randomNo = this.randomNoForOrderBook(0, 10);
    //         randomNoForFlashArr.push(randomNo);
    //         for (var i = 0; i < this.marketTradeRecords.length; i++) {
    //           var className = "text-green";
    //           if (!$.inArray(i, randomNoForFlashArr)) {
    //             if (randomNo % 2 == 0) {
    //               var className = 'text-red';
    //             } else if (randomNo % 2 != 0) {
    //               var className = 'text-red';
    //             }
    //           }
    //           var timeStampString = this.marketTradeRecords[i].ledger_close_time;
    //           var timeStampStringArr = timeStampString.split('T');
    //           var date = timeStampStringArr[0];
    //           var time = (timeStampStringArr[1]).slice(0, -1);
    //           var amount: any = parseFloat(this.marketTradeRecords[i].base_amount);
    //           var price: any = parseFloat(this.marketTradeRecords[i].counter_amount) / parseFloat(this.marketTradeRecords[i].base_amount);
    //           //this.marketTradeBodyHtml+='<tr class="' + className + '">';
    //           this.marketTradeBodyHtml += '<tr class="text-ash">';
    //           this.marketTradeBodyHtml += '<td class="text-left">' + time + '</td>';
    //           // this.marketTradeBodyHtml+='<td>'+ this.marketTradeRecords[i].base_asset_code+'</td>';
    //           if (this.selectedSellingAssetText == 'eur') { //changed by sanu
    //             this.marketTradeBodyHtml += '<td class="text-right">' + parseFloat(amount) + '</td>';
    //             this.marketTradeBodyHtml += '<td class="' + className + ' right">' + (parseFloat(price)).toFixed(4) + '</td>';
    //           } else {
    //             this.marketTradeBodyHtml += '<td class="text-right">' + (parseFloat(amount)).toFixed(8) + '</td>';
    //             this.marketTradeBodyHtml += '<td class="' + className + ' right">' + (parseFloat(price)).toFixed(8) + '</td>';
    //           }
    //           this.marketTradeBodyHtml += '</tr>';
    //           //  console.log(this.marketTradeBodyHtml);
    //           //}
    //         }
    //         // this.getMarketTradeBuy();
    //         //
    //       }
    //       $('#marketTradeBody').html(this.marketTradeBodyHtml);
    //       if (this.marketTradeRecords.length == 0)
    //         $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
    //     }, error => {
    //       $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
    //     })
    // }
    TradesComponent.prototype.getDashBoardInfo = function () {
        var _this = this;
        var infoObj = {};
        infoObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(infoObj);
        this.http.post(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, { headers: { 'Content-Type': 'application/json', 'authorization': 'BEARER ' + localStorage.getItem('access_token') } })
            .subscribe(function (response) {
            var result = response;
            // console.log('result--------------dhriti',result);
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                var storeDashboardInfo = JSON.stringify(result);
                var environmentSettingsListObj = {};
                localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
                for (var i = 0; i < result.settingsList.length; i++) {
                    environmentSettingsListObj['' + result.settingsList[i].name + ''] = result.settingsList[i];
                }
                environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
                localStorage.setItem('environment_settings_list', environmentSettingsListObj);
                _this.data.environment_settings_list = environmentSettingsListObj;
                //showing disclaimer for sell
                var environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
                var buyDisclaimer = environmentSettingListObj['buy_max_value'][_this.selectedCurrency + '_description'];
                var buyTxnDisclaimer = environmentSettingListObj['buy_txn_charges'][_this.selectedCurrency + '_description'];
                var sellDisclaimer = environmentSettingListObj['sell_max_value'][_this.selectedCurrency + '_description'];
                var sellTxnDisclaimer = environmentSettingListObj['sell_txn_charges'][_this.selectedCurrency + '_description'];
                // var sendDisclaimer = environmentSettingListObj['send_other_max_value'][this.selectedCurrency + '_description'];
                // var sendMiningDisclaimer = environmentSettingListObj['send_other_m_charges'][this.selectedCurrency + '_description'];
                if (result.userAppSettingsResult.user_docs_status == '') {
                    _this.indentificationStatus = 'Identity verification documents not submitted';
                }
                if (result.userAppSettingsResult.user_docs_status == '1') {
                    _this.indentificationStatus = 'Identity verification documents verified';
                }
                if (result.userAppSettingsResult.user_docs_status == '0') {
                    _this.indentificationStatus = ' Identity verification documents submitted for Verification';
                }
                if (result.userAppSettingsResult.user_docs_status == '2') {
                    _this.indentificationStatus = ' Identity verification documents declined, please submit again';
                }
                if (result.userAppSettingsResult.bank_details_status == '') {
                    _this.bankDetailStatus = 'Bank details not submitted';
                }
                if (result.userAppSettingsResult.bank_details_status == '0') {
                    _this.bankDetailStatus = 'Bank details  submitted for Verification';
                }
                if (result.userAppSettingsResult.bank_details_status == '2') {
                    _this.bankDetailStatus = ' Bank details verified';
                }
                if (result.userAppSettingsResult.bank_details_status == '3') {
                    _this.bankDetailStatus = ' Bank documents declined, please submit again';
                }
                if (localStorage.getItem('check_id_verification_status') == 'true' &&
                    result.userAppSettingsResult.user_docs_status == '') {
                    _this.data.alert('Please submit Identity verification documents to access all Paybito features', 'warning');
                }
                localStorage.setItem('check_id_verification_status', 'false');
            }
        }, function (reason) {
            //   wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.alert('Could Not Connect To Server', 'danger');
            }
        });
    };
    TradesComponent.prototype.GetTradeDetail = function () {
        var _this = this;
        var tradeObj = {};
        tradeObj['userId'] = localStorage.getItem('user_id');
        tradeObj['page_no'] = 1;
        tradeObj['no_of_items_per_page'] = 10;
        tradeObj['buying_asset_code'] = this.data.selectedBuyingAssetText.toLocaleLowerCase();
        tradeObj['selling_asset_code'] = this.data.selectedSellingAssetText.toLocaleLowerCase();
        //P_TOTAL_ROW_PER_PAGE
        var jsonString = JSON.stringify(tradeObj);
        this.http.post(this.data.WEBSERVICE + '/userTrade/GetTradeHistoryById', jsonString, { headers: { 'Content-Type': 'application/json', 'authorization': 'BEARER ' + localStorage.getItem('access_token') } })
            .subscribe(function (response) {
            var result = response;
            // var totalCount = result.totalCount;
            //console.log('dhrititi----', result);
            if (result.error.error_data != 0) {
                // this.historyTableTr += '<tr><td colspan="5" class="text-center">No Data Available</td></tr>';
            }
            else {
                var tradeDetails = result.tradeListResult;
                _this.historyTableTr = '';
                if (tradeDetails)
                    if (tradeDetails.length > 0) {
                        for (var i = 0; i < tradeDetails.length; i++) {
                            var action = tradeDetails[i].action;
                            var trnid = tradeDetails[i].trade_id;
                            var timestamp = tradeDetails[i].transactionTimeStamp;
                            var timestampArr = timestamp.split('.');
                            timestamp = _this.data.readable_timestamp(timestampArr[0]);
                            _this.historyTableTr += '<tr class="filter ' + action.toLowerCase() + '">';
                            _this.historyTableTr += '<td>' + timestamp + '</td>';
                            _this.historyTableTr += '<td class="">' + tradeDetails[i].buying_asset_code.toUpperCase() + '</td>';
                            _this.historyTableTr += '<td class="">' + tradeDetails[i].selling_asset_code.toUpperCase() + '</td>';
                            _this.historyTableTr += '<td class="">' + (parseFloat(tradeDetails[i].amount)) + '&nbsp;' + tradeDetails[i].buying_asset_code.toUpperCase() + '</td>';
                            _this.historyTableTr += '<td class="">' + (parseFloat(tradeDetails[i].price)).toFixed(6) + '</td>';
                            var cls = action == 'BUY' ? 'skyblue' : 'red';
                            _this.historyTableTr += '<td class="text-' + cls + '">' + action + '</td>';
                            _this.historyTableTr += '</tr>';
                        }
                    }
                    else {
                        _this.historyTableTr += '<tr><td colspan="5" class="text-center">No Data Available</td></tr>';
                    }
                $('#myTradeBody').html(_this.historyTableTr);
                if (_this.myTradeTableDisplayStatus == 0) {
                    $('#myTradeBody').children('.filter').show();
                }
                if (_this.myTradeTableDisplayStatus == 1) {
                    $('#myTradeBody').children('.filter').show();
                    $('#myTradeBody').children('tr.sell').hide();
                }
                if (_this.myTradeTableDisplayStatus == 2) {
                    $('#myTradeBody').children('.filter').show();
                    $('#myTradeBody').children('tr.buy').hide();
                }
            }
        }, function (reason) {
            if (reason.error.error == 'invalid_token') {
                _this.data.logout();
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    TradesComponent.prototype.myTradeDisplay = function (value) {
        this.myTradeTableDisplayStatus = value;
        this.GetTradeDetail();
    };
    TradesComponent.prototype.getOfferLists = function () {
        var _this = this;
        this.selectedBuyingAssetText = this.data.selectedBuyingAssetText.toLowerCase();
        this.selectedSellingAssetText = this.data.selectedSellingAssetText.toLowerCase();
        var inputObj = {};
        var mnObj;
        //inputObj['account_pub_key']=localStorage.getItem('trade_public_key');
        inputObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(inputObj);
        if (localStorage.getItem('user_id') != null)
            this.http.post(this.data.WEBSERVICE + '/userTrade/GetOfferByAccountID', jsonString, { headers: { 'Content-Type': 'application/json' } })
                .subscribe(function (response) {
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    var result = response;
                    // var apiResponse = JSON.parse(response.apiResponse);
                    var myOfferRecords = result.tradeListResult;
                    mnObj = [];
                    if (myOfferRecords)
                        for (var i = 0; i < myOfferRecords.length; i++) {
                            if ((myOfferRecords[i].buying_asset_code == (_this.selectedBuyingAssetText).toUpperCase() && myOfferRecords[i].selling_asset_code == (_this.selectedSellingAssetText).toUpperCase()) ||
                                (myOfferRecords[i].buying_asset_code == (_this.selectedSellingAssetText).toUpperCase() && myOfferRecords[i].selling_asset_code == (_this.selectedBuyingAssetText).toUpperCase())) {
                                if (myOfferRecords[i].txn_type == 1) {
                                    var myOfferType = 'Buy';
                                }
                                if (myOfferRecords[i].txn_type == 2) {
                                    var myOfferType = 'Sell';
                                }
                                var rsObj = {};
                                var timestamp = myOfferRecords[i].transactionTimeStamp;
                                var timestampArr = timestamp.split('.');
                                rsObj['time'] = _this.data.readable_timestamp(timestampArr[0]);
                                rsObj['offerId'] = myOfferRecords[i].offer_id;
                                rsObj['amount'] = (parseFloat(myOfferRecords[i].amount));
                                rsObj['buy'] = myOfferRecords[i].buying_asset_code;
                                if (_this.selectedSellingAssetText == 'usd') {
                                    rsObj['price'] = (parseFloat(myOfferRecords[i].price));
                                    rsObj['volume'] = (parseFloat(myOfferRecords[i].price) * parseFloat(myOfferRecords[i].amount));
                                }
                                else {
                                    rsObj['price'] = (parseFloat(myOfferRecords[i].price));
                                    rsObj['volume'] = (parseFloat(myOfferRecords[i].price) * parseFloat(myOfferRecords[i].amount));
                                }
                                rsObj['type'] = myOfferType;
                                rsObj['txn_typ'] = myOfferRecords[i].txn_type;
                                mnObj.push(rsObj);
                            }
                        }
                    _this.myOfferRecordsHtml = mnObj;
                    //  console.log('myOfferRecordsHtml+++', this.myOfferRecordsHtml);
                }
                //  console.log('data:');
                //  console.log(this.myOfferRecordsHtml);
            }, function (reason) {
                if (reason.error.error == 'invalid_token') {
                    _this.data.logout();
                    _this.data.alert('Session Timeout. Login Again', 'warning');
                }
                else {
                    _this.data.logout();
                    _this.data.alert('Could Not Connect To Server', 'danger');
                }
            });
    };
    TradesComponent.prototype.getOfferDetails = function (content, offerId) {
        var _this = this;
        var inputObj = {};
        this.data.alert('Loading...', 'dark');
        inputObj['offer_id'] = offerId;
        inputObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(inputObj);
        this.http.post(this.data.WEBSERVICE + '/userTrade/GetOfferByID', jsonString, { headers: { 'Content-Type': 'application/json' } })
            .subscribe(function (response) {
            _this.data.loader = false;
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.manageOfferId = offerId;
                _this.modifyAmount = parseFloat(result.tradeResult.amount);
                _this.modifyPrice = parseFloat(result.tradeResult.price);
                _this.modifyVolume = parseFloat(_this.modifyAmount) * parseFloat(_this.modifyPrice);
                _this.modifyType = result.tradeResult.txn_type;
                _this.modalService.open(content, { centered: true });
            }
        }, function (reason) {
            if (reason.error.error == 'invalid_token') {
                _this.data.logout();
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    TradesComponent.prototype.manageOffer = function () {
        var _this = this;
        this.data.alert('Loading...', 'dark');
        var inputObj = {};
        inputObj['offer_id'] = this.manageOfferId;
        inputObj['userId'] = localStorage.getItem('user_id');
        if (this.modifyType == 1) {
            inputObj['selling_asset_code'] = this.data.selectedSellingAssetText.toUpperCase();
            inputObj['buying_asset_code'] = this.data.selectedBuyingAssetText.toUpperCase();
        }
        else {
            inputObj['buying_asset_code'] = this.data.selectedSellingAssetText.toUpperCase();
            inputObj['selling_asset_code'] = this.data.selectedBuyingAssetText.toUpperCase();
        }
        inputObj['amount'] = this.modifyAmount;
        inputObj['price'] = this.modifyPrice;
        inputObj['txn_type'] = this.modifyType;
        var jsonString = JSON.stringify(inputObj);
        this.http.post(this.data.WEBSERVICE + '/userTrade/TradeManageOffer', jsonString, { headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
            var result = response;
            _this.data.loader = false;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.data.alert(result.error.error_msg, 'success');
                _this.GetTradeDetail();
                _this._StopLossComponent.getUserTransaction();
                $('.reset').click();
            }
        }, function (reason) {
            if (reason.error.error == 'invalid_token') {
                _this.data.logout();
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    TradesComponent.prototype.deleteTrade = function (content, offerId, txnType, price) {
        this.delOfferId = offerId;
        this.delTxnType = txnType;
        this.delPrice = price;
        this.modalService.open(content, { centered: true });
    };
    TradesComponent.prototype.delOffer = function () {
        var _this = this;
        this.data.alert('Loading...', 'dark');
        $('.load').fadeIn();
        var inputObj = {};
        inputObj['offer_id'] = this.delOfferId;
        inputObj['userId'] = localStorage.getItem('user_id');
        if (this.delTxnType == 2) {
            inputObj['selling_asset_code'] = this.selectedBuyingAssetText.toUpperCase();
            inputObj['buying_asset_code'] = this.selectedSellingAssetText.toUpperCase();
        }
        if (this.delTxnType == 1) {
            inputObj['selling_asset_code'] = this.selectedSellingAssetText.toUpperCase();
            inputObj['buying_asset_code'] = this.selectedBuyingAssetText.toUpperCase();
        }
        inputObj['amount'] = '0';
        inputObj['txn_type'] = this.delTxnType;
        inputObj['price'] = this.delPrice;
        var jsonString = JSON.stringify(inputObj);
        this.http.post(this.data.WEBSERVICE + '/userTrade/TradeManageOffer', jsonString, { headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            } })
            .subscribe(function (response) {
            _this.data.loader = false;
            $('.load').fadeOut();
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'warning');
            }
            else {
                _this.GetTradeDetail();
                _this._StopLossComponent.getUserTransaction();
                $('.reset').click();
                _this.data.alert(result.error.error_msg, 'success');
                $('#trade').click();
            }
        }, function (reason) {
            if (reason.error.error == 'invalid_token') {
                _this.data.logout();
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    TradesComponent.prototype.getStopLossOfferForSell = function () {
        var _this = this;
        var inputObj = {};
        inputObj['userId'] = localStorage.getItem('user_id');
        inputObj['selling_asset_code'] = (localStorage.getItem('buying_crypto_asset') ? localStorage.getItem('buying_crypto_asset') : 'btc').toUpperCase();
        inputObj['buying_asset_code'] = (localStorage.getItem('selling_crypto_asset') ? localStorage.getItem('selling_crypto_asset') : 'usd').toUpperCase();
        inputObj['txn_type'] = '2';
        var jsonString = JSON.stringify(inputObj);
        this.http.post(this.data.WEBSERVICE + '/userTrade/GetStopLossBuySell', jsonString, { headers: { 'Content-Type': 'application/json' } })
            .subscribe(function (response) {
            var result = response;
            var apiResponse = JSON.parse(result.apiResponse);
            _this.selldata = (apiResponse) ? apiResponse.response : '';
        }, function (reason) {
            this.data.alert(reason, 'warning');
        });
    };
    TradesComponent.prototype.getStopLossOfferForBuy = function () {
        var _this = this;
        //alert(localStorage.getItem('selling_crypto_asset')+ '      '+localStorage.getItem('buying_crypto_asset'))
        var inputObj = {};
        inputObj['userId'] = localStorage.getItem('user_id');
        inputObj['buying_asset_code'] = (localStorage.getItem('buying_crypto_asset') ? localStorage.getItem('buying_crypto_asset') : 'btc').toUpperCase();
        inputObj['selling_asset_code'] = (localStorage.getItem('selling_crypto_asset') ? localStorage.getItem('selling_crypto_asset') : 'usd').toUpperCase();
        inputObj['txn_type'] = '1';
        var jsonString = JSON.stringify(inputObj);
        var us = localStorage.getItem('user_id');
        if (us != null)
            this.http.post(this.data.WEBSERVICE + '/userTrade/GetStopLossBuySell', jsonString, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
                .subscribe(function (response) {
                var result = response;
                var apiResponse = JSON.parse(result.apiResponse);
                _this.buydata = (apiResponse) ? apiResponse.response : '';
            }, function (reason) {
                this.data.alert(reason, 'warning');
            });
    };
    TradesComponent.prototype.modifyStoploss = function (content, id, q, p, t, flag) {
        this.modalService.open(content, { centered: true });
        this.modifySlAmount = q;
        this.modifySlPrice = p;
        this.modifySlTrigger = t;
        this.modifySlOffer = id;
        this.flag = flag;
    };
    TradesComponent.prototype.manageStoploss = function () {
        var _this = this;
        this.data.alert('Loading...', 'dark');
        if (this.flag == 'Buy') {
            var marketOrderUrl = this.data.TRADESERVICE + '/getAmountBuy/' + localStorage.getItem('selling_crypto_asset').toUpperCase() + localStorage.getItem('buying_crypto_asset').toUpperCase() + '/' + localStorage.getItem('buying_crypto_asset').toUpperCase() + localStorage.getItem('selling_crypto_asset').toUpperCase() + '/' + this.modifySlAmount;
            //  console.log(this.flag);
            // console.log(marketOrderUrl);
        }
        else {
            var marketOrderUrl = this.data.TRADESERVICE + '/getAmountSell/' + localStorage.getItem('buying_crypto_asset').toUpperCase() + localStorage.getItem('selling_crypto_asset').toUpperCase() + '/' + localStorage.getItem('selling_crypto_asset').toUpperCase() + localStorage.getItem('buying_crypto_asset').toUpperCase() + '/' + this.modifySlAmount;
        }
        this.http.get(marketOrderUrl)
            .subscribe(function (data) {
            // wip(0);
            var result = data;
            if (result.code == '0') {
                _this.marketOrderPrice = parseFloat(result.price);
                if (_this.flag == 'Buy') {
                    // console.log(this.marketOrderPrice,this.modifySlTrigger, this.modifySlPrice,this.flag);
                    if (_this.marketOrderPrice < _this.modifySlTrigger &&
                        _this.marketOrderPrice < _this.modifySlPrice &&
                        _this.modifySlTrigger < _this.modifySlPrice) {
                        var inputObj = {};
                        inputObj['selling_asset_code'] = localStorage.getItem('selling_crypto_asset').toUpperCase();
                        inputObj['buying_asset_code'] = localStorage.getItem('buying_crypto_asset').toUpperCase();
                        // inputObj['selling_asset_code']=localStorage.getItem('buying_crypto_asset').toUpperCase();
                        // inputObj['buying_asset_code']=localStorage.getItem('selling_crypto_asset').toUpperCase();
                        inputObj['userId'] = localStorage.getItem('user_id');
                        inputObj['modify_type'] = 'edit';
                        inputObj['stoploss_id'] = _this.modifySlOffer;
                        inputObj['stop_loss_price'] = _this.modifySlPrice;
                        inputObj['trigger_price'] = _this.modifySlTrigger;
                        inputObj['quantity'] = _this.modifySlAmount;
                        inputObj['txn_type'] = '1';
                        //"sellassetcode":"BTC", "buyassetcode":"ETH" ,"cust_Id":"9001", "modifyType":"edit", "stoploss_Id":"2100", "stopprice":"190.01", "triggerPrice":"191.01", "quantity":"28.29"}
                        var jsonString = JSON.stringify(inputObj);
                        // wip(1);
                        _this.http.post(_this.data.WEBSERVICE + '/userTrade/ModifyStopLossBuySell', jsonString, { headers: { "Content-Type": "application/json" } })
                            .subscribe(function (data) {
                            // wip(0);
                            _this.data.loader = false;
                            var result = data;
                            if (result.error.error_data != '0') {
                                _this.data.alert(result.error.error_msg, 'danger');
                            }
                            else {
                                _this.data.alert(result.error.error_msg, 'success');
                                _this.getStopLossOfferForBuy();
                                // $('#trade').click();
                                // wip(1);
                                // location.reload();
                            }
                        });
                    }
                    else {
                        _this.data.alert('*Market order price should be greater than trigger price & trigger price should be greater than stop loss price', 'warning');
                    }
                }
                else {
                    //   console.log(this.marketOrderPrice,this.modifySlTrigger, this.modifySlPrice);
                    if (_this.marketOrderPrice > _this.modifySlTrigger &&
                        _this.marketOrderPrice > _this.modifySlPrice &&
                        _this.modifySlTrigger > _this.modifySlPrice) {
                        var inputObj = {};
                        inputObj['selling_asset_code'] = localStorage.getItem('buying_crypto_asset').toUpperCase();
                        inputObj['buying_asset_code'] = localStorage.getItem('selling_crypto_asset').toUpperCase();
                        inputObj['userId'] = localStorage.getItem('user_id');
                        inputObj['modify_type'] = 'edit';
                        inputObj['stoploss_id'] = _this.modifySlOffer;
                        inputObj['stop_loss_price'] = _this.modifySlPrice;
                        inputObj['trigger_price'] = _this.modifySlTrigger;
                        inputObj['quantity'] = _this.modifySlAmount;
                        inputObj['txn_type'] = '2';
                        //"sellassetcode":"BTC", "buyassetcode":"ETH" ,"cust_Id":"9001", "modifyType":"edit", "stoploss_Id":"2100", "stopprice":"190.01", "triggerPrice":"191.01", "quantity":"28.29"}
                        var jsonString = JSON.stringify(inputObj);
                        // wip(1);
                        _this.http.post(_this.data.WEBSERVICE + '/userTrade/ModifyStopLossBuySell', jsonString, { headers: { "Content-Type": "application/json" } })
                            .subscribe(function (data) {
                            // wip(0);
                            _this.data.loader = false;
                            var result = data;
                            if (result.error.error_data != '0') {
                                _this.data.alert(result.error.error_msg, 'danger');
                            }
                            else {
                                _this.data.alert(result.error.error_msg, 'success');
                                $('#trade').click();
                                // wip(1);
                                // location.reload();
                            }
                        });
                    }
                    else {
                        _this.data.alert('*Market order price should be greater than trigger price & trigger price should be greater than stop loss price', 'warning');
                    }
                }
            }
            else {
                _this.data.alert('*Orderbook depth reached, price not found', 'warning');
            }
        });
    };
    TradesComponent.prototype.delStoploss = function (content, id, q, p, t, flag) {
        this.modalService.open(content, { centered: true });
        this.modifySlAmount = q;
        this.modifySlPrice = p;
        this.modifySlTrigger = t;
        this.modifySlOffer = id;
        this.flag = flag;
    };
    TradesComponent.prototype.removeStoploss = function () {
        var _this = this;
        this.data.alert('Loading...', 'dark');
        this.stopLossBuyingAsset = localStorage.getItem('buying_crypto_asset');
        this.stopLossSellingAsset = localStorage.getItem('selling_crypto_asset');
        // console.log( this.stopLossBuyingAsset+' '+ this.stopLossSellingAsset);
        var inputObj = {};
        inputObj['selling_asset_code'] = (this.stopLossSellingAsset).toUpperCase();
        inputObj['buying_asset_code'] = (this.stopLossBuyingAsset).toUpperCase();
        inputObj['userId'] = localStorage.getItem('user_id');
        inputObj['modify_type'] = 'delete';
        inputObj['stoploss_id'] = this.modifySlOffer;
        inputObj['stop_loss_price'] = this.modifySlPrice;
        inputObj['trigger_price'] = this.modifySlTrigger;
        inputObj['quantity'] = this.modifySlAmount;
        if (this.flag == 'buy') {
            inputObj['txn_type'] = '1';
        }
        else {
            inputObj['txn_type'] = '2';
        }
        var jsonString = JSON.stringify(inputObj);
        this.http.post(this.data.WEBSERVICE + '/userTrade/ModifyStopLossBuySell', jsonString, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .subscribe(function (data) {
            _this.data.loader = false;
            var result = data;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.data.alert(result.error.error_msg, 'success');
                $('#trade').click();
                // wip(1);
                // location.reload();
            }
        });
    };
    TradesComponent = __decorate([
        Component({
            selector: 'app-trades',
            templateUrl: './trades.component.html',
            styleUrls: ['./trades.component.css'],
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [CoreDataService,
            HttpClient,
            NgbModal,
            NavbarComponent,
            DashboardComponent,
            StopLossComponent])
    ], TradesComponent);
    return TradesComponent;
}());
export { TradesComponent };
//# sourceMappingURL=trades.component.js.map