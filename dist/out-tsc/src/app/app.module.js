var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { AngularDraggableModule } from 'angular2-draggable';
import { UserIdleModule } from 'angular-user-idle';
import { QRCodeModule } from 'angularx-qrcode';
import { RecaptchaModule } from 'ng-recaptcha';
import { Ng2OdometerModule } from 'ng2-odometer';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { AppComponent } from './app.component';
import { OrderBookComponent } from './order-book/order-book.component';
import { ChartComponent } from './chart/chart.component';
import { TradesComponent } from './trades/trades.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StopLossComponent } from './stop-loss/stop-loss.component';
import { SafePipe } from './chart/safe.pipe';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CoreDataService } from "./core-data.service";
import { BodyService } from "./body.service";
import { LoginComponent } from './login/login.component';
import { CookieService } from "ngx-cookie-service";
import { MyWalletComponent } from './my-wallet/my-wallet.component';
import { DepositFundsComponent } from './deposit-funds/deposit-funds.component';
import { WithdrawFundsComponent } from './withdraw-funds/withdraw-funds.component';
import { HistoryComponent } from './history/history.component';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';
import { IdentityVerificationComponent } from './identity-verification/identity-verification.component';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { SettingsComponent } from './settings/settings.component';
import { SupportComponent } from './support/support.component';
import { SignupComponent } from './signup/signup.component';
import { SecureTokenComponent } from './secure-token/secure-token.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { OtpComponent } from './otp/otp.component';
import { ModalsComponent } from './modals/modals.component';
import { LoaderComponent } from './loader/loader.component';
import { ReportComponent } from './report/report.component';
import { NumericDirective } from '../app/core-data.directive';
import { NgbdModalContent, } from './deposit-funds/deposit-funds.component';
import { TvChartContainerComponent } from './tv-chart-container/tv-chart-container.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export * from './chart/chart.component';
var appRoutes = [
    { path: '', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'otp', component: OtpComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'login', component: LoginComponent },
    { path: 'my-wallet', component: MyWalletComponent },
    { path: 'deposit-funds', component: DepositFundsComponent },
    { path: 'deposit-funds/:transaction_id/:msid', component: DepositFundsComponent },
    { path: 'withdraw-funds', component: WithdrawFundsComponent },
    { path: 'history', component: HistoryComponent },
    { path: 'profile-details', component: ProfileDetailsComponent },
    { path: 'identity-verification', component: IdentityVerificationComponent },
    { path: 'bank-details', component: BankDetailsComponent },
    { path: 'promotion', component: PromotionsComponent },
    { path: 'settings', component: SettingsComponent },
    { path: 'support', component: SupportComponent },
    { path: 'secure-token', component: SecureTokenComponent },
    { path: 'forget-password', component: ForgetPasswordComponent },
    { path: 'report', component: ReportComponent },
    { path: 'modaltest', component: ModalsComponent }
];
export function HttpLoaderFactory(httpClient) {
    return new TranslateHttpLoader(httpClient);
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                OrderBookComponent,
                ChartComponent,
                TradesComponent,
                StopLossComponent,
                SafePipe,
                NavbarComponent,
                DashboardComponent,
                LoginComponent,
                MyWalletComponent,
                DepositFundsComponent,
                NgbdModalContent,
                WithdrawFundsComponent,
                HistoryComponent,
                ProfileDetailsComponent,
                IdentityVerificationComponent,
                BankDetailsComponent,
                PromotionsComponent,
                SettingsComponent,
                SupportComponent,
                SignupComponent,
                SecureTokenComponent,
                ForgetPasswordComponent,
                OtpComponent,
                ModalsComponent,
                LoaderComponent,
                ReportComponent,
                NumericDirective,
                TvChartContainerComponent,
                AppHeaderComponent
            ],
            imports: [
                FormsModule,
                QRCodeModule,
                RecaptchaModule,
                AngularDraggableModule,
                LazyLoadImageModule,
                NgbModule.forRoot(),
                BrowserModule,
                Ng2OdometerModule.forRoot(),
                HttpClientModule,
                UserIdleModule.forRoot({ idle: 300, timeout: 10, ping: 15 }),
                RouterModule.forRoot(appRoutes
                // { enableTracing: true } // <-- debugging purposes only
                ),
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ],
            providers: [
                ChartComponent,
                OrderBookComponent,
                StopLossComponent,
                CoreDataService,
                CookieService,
                TradesComponent,
                NavbarComponent,
                BodyService,
                DashboardComponent,
                MyWalletComponent,
                NgbdModalContent,
                SignupComponent,
                TvChartContainerComponent,
                { provide: LocationStrategy, useClass: HashLocationStrategy }
            ],
            entryComponents: [NgbdModalContent],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map