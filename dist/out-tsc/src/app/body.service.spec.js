import { TestBed, inject } from '@angular/core/testing';
import { BodyService } from './body.service';
describe('BodyService', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [BodyService]
        });
    });
    it('should be created', inject([BodyService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=body.service.spec.js.map