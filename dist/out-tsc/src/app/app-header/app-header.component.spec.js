import { async, TestBed } from '@angular/core/testing';
import { AppHeaderComponent } from './app-header.component';
describe('AppHeaderComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [AppHeaderComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(AppHeaderComponent);
        component = fixture.componentInstance;
    });
    it('should be created', function () {
        expect(component).toBeTruthy();
    });
    it("should have as title 'TradingView Charting Library and Angular 5 Integration Example'", async(function () {
        var app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('TradingView Charting Library and Angular 5 Integration Example');
    }));
});
//# sourceMappingURL=app-header.component.spec.js.map