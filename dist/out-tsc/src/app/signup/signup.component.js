var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from "jquery";
import { TranslateService } from '@ngx-translate/core';
var SignupComponent = /** @class */ (function () {
    function SignupComponent(http, data, route, activeRoute, translate) {
        this.http = http;
        this.data = data;
        this.route = route;
        this.activeRoute = activeRoute;
        this.translate = translate;
        this.title = 'identity';
        this.reg = {};
        this.reg2 = {};
        this.signupObj = {
            country: ''
        };
        this.signupObj1 = {};
        this.IDMSERVICE = "https://sandbox.identitymind.com/im/account/consumer?graphScoreResponse=false";
        this.WEBSERVICE = "https://api.digitalterminal.net/webservice";
        this.merchantid = 'cornerstore';
        this.merchantpassword = '8215631cc6e8b0ef43a2aba1aa61a489ebe547f2';
        this.accessKey = btoa(("this.merchantid:this.merchantpassword"));
        // translate.addLangs(['Turkish', 'English']);
        // translate.setDefaultLang('Turkish');
        // const browserLang = translate.getBrowserLang();
        // translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    SignupComponent.prototype.ngOnInit = function () {
        /* $(function(){
          $('a#tab1').click(function(){
            location.href = 'https://www.paybito.com/dashboard/signup';
          });
        }); */
        this.getLoc();
        this.checkurlemail(this.loadmail);
    };
    SignupComponent.prototype.checkurlemail = function (loadmail) {
        var _this = this;
        this.activeRoute.queryParams.subscribe(function (params) {
            //console.log(params);
            _this.loadmail = params['email'];
        });
        if (this.loadmail != undefined && this.loadmail != '') {
            this.signupObj.email = this.loadmail;
            //alert(this.signupObj.email);
        }
        else {
            this.signupObj.email = '';
        }
        //alert(this.loadmail);
    };
    SignupComponent.prototype.getLoc = function () {
        var _this = this;
        this.http.get('./assets/data/country.json')
            .subscribe(function (data) {
            _this.contries = data;
        });
    };
    SignupComponent.prototype.resolved = function (captchaResponse) {
        this.captcha = captchaResponse;
    };
    // sendIdentity(isValid) {
    //   debugger;
    //   var fd = new FormData();
    //   if (isValid) {
    //       fd.append('g_recaptcha_response', this.captcha);
    //       // fd.append('g_recaptcha_response', "help");
    //       fd.append('bfn', this.signupObj.fast_name);
    //       fd.append('bmn', this.signupObj.middle_name);
    //       fd.append('bln', this.signupObj.last_name);
    //       fd.append('bgd', this.signupObj.gender);
    //       fd.append('bsn', this.signupObj.address);
    //       fd.append('bc', this.signupObj.city);
    //       fd.append('bco', this.signupObj.country);
    //       fd.append('bz', this.signupObj.zip);
    //       fd.append('tea', this.signupObj.email);
    //       fd.append('phn', this.signupObj.phone);
    //       fd.append('bs', this.signupObj.state);
    //       fd.append('docType',this.signupObj.signupInputdoctype);
    //       fd.append('soc', 'Google');
    //       var date = Date.now();
    //       fd.append('accountCreationTime', date.toString());
    //       fd.append('man', this.signupObj.fast_name + ' ' + this.signupObj.middle_name + ' ' + this.signupObj.last_name);
    //       fd.append('password', this.signupObj.password);
    //       var ds = this.signupObj.dateofbirth;
    //       var Dateofbirth =ds.year.toString()+ds.month.toString()+ds.day.toString();
    //       fd.append('dob', Dateofbirth);
    //      if ($('.document_front_side')[0].files[0] != undefined) {
    //       // this.signupObj1['scanData']=$('.document_front_side')[0].files[0];
    //       fd.append('scanData', $('.document_front_side')[0].files[0]);
    //       this.file1=($('.document_front_side')[0].files[0]);
    //         } else {
    //       //this.signupObj1['scanData'] = '';
    //       fd.append('scanData', '');
    //     }
    //     if ($('.document_back_side')[0].files[0] != undefined) {
    //       fd.append('backsideImageData', $('.document_back_side')[0].files[0]);
    //        } else {
    //      fd.append('backsideImageData', '')
    //     }
    //     // this.http.post < any > (this.IDMSERVICE, iamString, {jsonString,{headers: {'Content-Type': 'application/json'}})
    //     if (this.captcha != '') {
    //     this.http.post<any>(this.WEBSERVICE + '/user/AddIDMUserDetails', fd, { headers: { } })
    //         .subscribe(response => {
    //           // wip(0);
    //           var result = response;
    //           if (result.error.error_data != '0') {
    //             this.data.alert(result.error.error_msg, 'danger');
    //           } else {
    //             var userId = result.userResult.user_id;
    //             localStorage.setItem('signup_user_id', userId);
    //             this.data.alert('Registration Done,  Kindly check your email for verification token', 'success');
    //             this.route.navigateByUrl('/otp');
    //           }
    //         }, reason => {
    //           // wip(0);
    //           this.data.alert('Internal Server Error', 'danger')
    //         });
    //     }
    //     else {
    //       this.data.alert('Captcha Unverified', 'warning');
    //     }
    //   }
    //   else {
    //     this.data.alert('Please fill up all the fields properly', 'warning');
    //   }
    // }
    SignupComponent.prototype.signupData = function (isValid) {
        var _this = this;
        if (isValid) {
            // console.log(this.signupObj);
            //this.signupObj['gRecaptchaResponse']=this.captcha;
            var jsonString = JSON.stringify(this.signupObj);
            console.log(jsonString);
            //START
            if (this.captcha != '') {
                this.http.post(this.data.WEBSERVICE + '/user/AddUserDetails', jsonString, { headers: { 'Content-Type': 'application/json' } })
                    .subscribe(function (response) {
                    // wip(0);
                    var result = response;
                    if (result.error.error_data != '0') {
                        _this.data.alert(result.error.error_msg, 'danger');
                    }
                    else {
                        var userId = result.userResult.userId;
                        localStorage.setItem('signup_user_id', userId);
                        _this.data.alert('Registration Done,  Kindly check your email for verification token', 'success');
                        _this.route.navigateByUrl('/otp');
                    }
                }, function (reason) {
                    // wip(0);
                    _this.data.alert('Internal Server Error', 'danger');
                });
            }
            else {
                this.data.alert('Captcha Unverified', 'warning');
            }
        }
        else {
            this.data.alert('Please fill up all the fields properly', 'warning');
        }
    };
    SignupComponent.prototype.getSize = function (content) {
        var sz = $('#' + content)[0].files[0];
        // console.log(sz);
        if (sz.type == "image/jpeg") {
            if (sz.size > 5000000) {
                this.data.alert('File size should be less than 2MB', 'warning');
                $('#' + content).val('');
            }
        }
        else {
            this.data.alert('File should be in JPG or JPEG. ' + sz.type.split('/')[1].toUpperCase() + ' is not allowed', 'warning');
            $('#' + content).val('');
        }
    };
    // return this.http
    //       .get('https://ip-api.com/json/').subscribe(response =>{
    //         if(response!=""){
    //           console.log('ip',response);
    //           this.reg['ip']= response['query'];
    //           this.reg['clong'] = response['lon'];
    //           this.reg['clat'] = response['lat'];
    //           this.reg['man'] = this.reg['bfn'] + ' ' + this.reg['bln'];
    //           var iamString=JSON.stringify(this.reg);
    //           //console.log(this.accessKey);
    //           this.http.post < any > (this.IDMSERVICE, iamString, {
    //             headers: {
    //               'Content-Type': 'application/json',
    //               'Accept' : 'application/json',
    //               'Authorization': 'Basic' + '' + 'Y29ybmVyc3RvcmU6ODIxNTYzMWNjNmU4YjBlZjQzYTJhYmExYWE2MWE0ODllYmU1NDdmMg==',
    //             }
    //           })
    //           .subscribe(response =>{
    //             if(response){
    //               console.log('IDMSERVICE',response);
    //               var jsonString = JSON.stringify(this.reg);
    //               this.http.post<any>(this.WEBSERVICE+'/user/AddUserDetails',jsonString,{headers: {'Content-Type': 'application/json'}})
    //               .subscribe(response=>{
    //                   // wip(0);
    //                   var result=response;
    //                   if(result.error.error_data!='0'){
    //                   //this.data.alert(result.error.error_msg,'danger');
    //                   }else{
    //                   var userId=result.userResult.user_id;
    //                   localStorage.setItem('signup_user_id',userId);
    //                   /* this.data.alert('Registration Done,  Kindly check your email for verification token','success');
    //                   this.route.navigateByUrl('/otp'); */
    //                   }
    //               },reason=>{
    //                   // wip(0);
    //                   //this.data.alert('Internal Server Error','danger')
    //               });
    //             }
    //             else{
    //               alert("failed");
    //             }
    //           });
    //           //console.log(jsonString);
    //         }
    //         else{
    //           error =>  this.errormessage = <any>error;
    //         }
    //       });
    //}
    // signupData(isValid){
    //   if(isValid){
    //     this.signupObj['g_recaptcha_response']=this.captcha;
    //     var jsonString=JSON.stringify(this.signupObj);
    //     //START
    //     if(this.captcha!=''){
    //       this.http.post<any>(this.data.WEBSERVICE+'/user/AddUserDetails',jsonString,{headers: {'Content-Type': 'application/json'}})
    //         .subscribe(response=>{
    //             // wip(0);
    //             var result=response;
    //             if(result.error.error_data!='0'){
    //             this.data.alert(result.error.error_msg,'danger');
    //             }else{
    //             var userId=result.userResult.user_id;
    //             localStorage.setItem('signup_user_id',userId);
    //             this.data.alert('Registration Done,  Kindly check your email for verification token','success');
    //             this.route.navigateByUrl('/otp');
    //             }
    //         },reason=>{
    //             // wip(0);
    //             this.data.alert('Internal Server Error','danger')
    //         });
    //  }else{
    //     this.data.alert('Captcha Unverified','warning');
    //  }
    //   }else{
    //     this.data.alert('Please fill up all the fields properly','warning');
    //   }
    // }
    //confirm password
    SignupComponent.prototype.confirmPassword = function (e) {
        if ((this.signupObj.password != '' && this.signupObj.password != undefined) &&
            (this.signupObj.repassword1 != '' && this.signupObj.repassword1 != undefined)) {
            if (this.signupObj.password == this.signupObj.repassword1) {
                $('.confirm_password_text').html('Password Matched');
                $('.confirm_password_text').css('color', 'lightgreen');
                $('#submit_btn').removeAttr('disabled');
            }
            else {
                $('.confirm_password_text').html('Password  Mismatched');
                $('.confirm_password_text').css('color', 'red');
                $('#submit_btn').attr('disabled', 'disabled');
            }
        }
        else {
        }
    };
    //check email
    SignupComponent.prototype.checkEmail = function (loadmail) {
        //console.log('this.signupObj.email.value: '+this.signupObj.email);
        var _this = this;
        if (this.signupObj.email != undefined && this.signupObj.email != '') {
            //console.log(this.loadmail);
            // this.signupObj.email = this.loadmail;
            //  alert(this.signupObj.email);
            if (this.signupObj.email != '' && this.signupObj.email != undefined) {
                if (this.is_mail(this.signupObj.email) == true) {
                    // wip(1);
                    var emailValue = this.signupObj.email;
                    var emailObj = {};
                    emailObj['email'] = emailValue;
                    var jsonString = JSON.stringify(emailObj);
                    this.http.post(this.data.WEBSERVICE + '/user/CheckEmail', jsonString, {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                        .subscribe(function (response) {
                        // wip(0);
                        var result = response;
                        if (result.error.error_data != '0') {
                            _this.data.alert(result.error.error_msg, 'danger');
                        }
                        else {
                            if (result.userResult.check_email_phone_flag == 1) {
                                _this.data.alert('Email already registered , please try with another email address', 'warning');
                                _this.signupObj.email = '';
                                //$('#signupInputEmail').focus();
                            }
                            else {
                            }
                        }
                    }, function (reason) {
                        // wip(0);
                        _this.data.alert('Internal Server Error', 'danger');
                    });
                }
                else {
                    // ui_alert('Please Provide Valid Email','error')
                }
            }
        }
        else {
            this.data.alert('Please Provide Email Id', 'warning');
        }
    };
    //check phone
    SignupComponent.prototype.checkPhone = function () {
        var _this = this;
        if (this.signupObj.phone != '' && this.signupObj.phone != undefined) {
            // wip(1);
            var phoneValue = this.signupObj.phone;
            var phoneObj = {};
            phoneObj['phone'] = phoneValue;
            var jsonString = JSON.stringify(phoneObj);
            this.http.post(this.data.WEBSERVICE + '/user/CheckPhone', jsonString, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .subscribe(function (response) {
                // wip(0);
                var result = response;
                if (result.error.error_data != '0') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                else {
                    if (result.userResult.check_email_phone_flag == 1) {
                        _this.data.alert('Phone No. already registered , please try with another phone no.', 'warning');
                        _this.signupObj.phone.value = '';
                        //$('#signupInputPhone').focus();
                    }
                    else {
                    }
                }
            }, function (reason) {
                // wip(0);
                _this.data.alert('Internal Server Error', 'danger');
            });
        }
        else {
            this.data.alert('Please Provide Phone No.', 'warning');
        }
    };
    //check password
    SignupComponent.prototype.checkPassword = function () {
        var password = this.signupObj.password;
        if (password != '' && password != undefined && password.length >= 8) {
            var passwordStatus = this.checkAlphaNumeric(password);
            if (passwordStatus == false) {
                this.data.alert('Password should have atleast one upper case letter, one lowercase letter , one special case and one number', 'warning');
            }
            else {
            }
        }
        else {
            // this.data.alert('Password should be minimum 8 Charecter', 'warning');
        }
    };
    SignupComponent.prototype.checkAlphaNumeric = function (string) {
        if (string.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)) {
            //  pattern="(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,20}"
            return true;
        }
        else {
            return false;
        }
    };
    SignupComponent.prototype.is_mail = function (email) {
        var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return regex.test(email);
    };
    SignupComponent.prototype.showHide = function () {
        $(".showHide-password").each(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            }
            else {
                input.attr("type", "password");
            }
        });
    };
    SignupComponent = __decorate([
        Component({
            selector: 'app-signup',
            templateUrl: './signup.component.html',
            styleUrls: ['./signup.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient, CoreDataService, Router, ActivatedRoute, TranslateService])
    ], SignupComponent);
    return SignupComponent;
}());
export { SignupComponent };
//# sourceMappingURL=signup.component.js.map