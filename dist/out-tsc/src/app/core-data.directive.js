var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Directive, ElementRef, HostListener, Input } from '@angular/core';
var NumericDirective = /** @class */ (function () {
    function NumericDirective(el) {
        this.el = el;
        this.regex = {
            number: new RegExp(/^\d+$/),
            decimal: new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g)
        };
        this.specialKeys = {
            number: ['Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight'],
            decimal: ['Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight'],
        };
    }
    NumericDirective.prototype.onKeyDown = function (event) {
        if (this.specialKeys[this.numericType].indexOf(event.key) !== -1) {
            return;
        }
        // Do not use event.keycode this is deprecated.
        // See: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode
        var current = this.el.nativeElement.value;
        var next = current.concat(event.key);
        if (next && !String(next).match(this.regex[this.numericType])) {
            event.preventDefault();
        }
    };
    __decorate([
        Input('numericType'),
        __metadata("design:type", String)
    ], NumericDirective.prototype, "numericType", void 0);
    __decorate([
        HostListener('keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], NumericDirective.prototype, "onKeyDown", null);
    NumericDirective = __decorate([
        Directive({
            selector: '[numeric]'
        }),
        __metadata("design:paramtypes", [ElementRef])
    ], NumericDirective);
    return NumericDirective;
}());
export { NumericDirective };
//# sourceMappingURL=core-data.directive.js.map