var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';
var OtpComponent = /** @class */ (function () {
    function OtpComponent(http, data, route) {
        this.http = http;
        this.data = data;
        this.route = route;
    }
    OtpComponent.prototype.ngOnInit = function () {
    };
    OtpComponent.prototype.onKeyupSendOtp = function () {
        if ((this.otp).length == 5) {
            this.otpData();
        }
    };
    OtpComponent.prototype.otpData = function () {
        var _this = this;
        if (this.otp != undefined) {
            var userId = localStorage.getItem('signup_user_id');
            var userId = userId;
            var otp = this.otp;
            var otpObj = {};
            otpObj['userId'] = userId;
            otpObj['otp'] = otp;
            var jsonString = JSON.stringify(otpObj);
            this.http.post(this.data.WEBSERVICE + '/user/CheckOTP', jsonString, { headers: { 'Content-Type': 'application/json' } })
                .subscribe(function (response) {
                var result = response;
                if (result.error.error_data == '2') {
                    _this.route.navigateByUrl('/login');
                    _this.data.alert(result.error.error_msg, 'info');
                }
                if (result.error.error_data == '1') {
                    _this.data.alert(result.error.error_msg, 'danger');
                }
                if (result.error.error_data == '0') {
                    var userId = result.userResult.user_id;
                    localStorage.setItem('signup_user_id', userId);
                    _this.data.alert('OTP Verified', 'success');
                    _this.route.navigateByUrl('/login');
                }
            }, function (reason) {
                _this.data.alert(reason, 'danger');
            });
        }
        else {
            this.data.alert('Please fill up all the fields properly', 'warning');
        }
    };
    OtpComponent.prototype.resendOtp = function () {
        var _this = this;
        var otpObj = {};
        otpObj['userId'] = localStorage.getItem('signup_user_id');
        var jsonString = JSON.stringify(otpObj);
        this.http.post(this.data.WEBSERVICE + '/user/ResendOTP', jsonString, { headers: {
                'Content-Type': 'application/json'
            } })
            .subscribe(function (response) {
            var result = response;
            if (result.error.error_data != '0') {
                _this.data.alert(result.error.error_msg, 'danger');
            }
            else {
                _this.data.alert('OTP has been sent to your email', 'success');
            }
        }, function (reason) {
            _this.data.alert('Could Not Connect To Server', 'warning');
        });
    };
    OtpComponent = __decorate([
        Component({
            selector: 'app-otp',
            templateUrl: './otp.component.html',
            styleUrls: ['./otp.component.css']
        }),
        __metadata("design:paramtypes", [HttpClient,
            CoreDataService,
            Router])
    ], OtpComponent);
    return OtpComponent;
}());
export { OtpComponent };
//# sourceMappingURL=otp.component.js.map