var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { BodyService } from "../body.service";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import * as $ from "jquery";
var WithdrawFundsComponent = /** @class */ (function () {
    function WithdrawFundsComponent(main, http, data, _StopLossComponent) {
        this.main = main;
        this.http = http;
        this.data = data;
        this._StopLossComponent = _StopLossComponent;
        this.currencyID = 1;
        this.main.getUserTransaction();
        this.main.getDashBoardInfo();
    }
    WithdrawFundsComponent.prototype.ngOnInit = function () {
        this.getBankDetails();
        // this.main.getDashBoardInfo();
        this.appsettingscall();
        this.currencyBalance = this.main.balencelist;
        this.usdbalance = localStorage.getItem('usdbalance');
        if (this.currencyBalance != null) {
            for (var i = 0; i < this.currencyBalance.length; i++) {
                if (this.currencyBalance[i].currencyCode == "TRY") {
                    this.usdbalance = this.currencyBalance[i].closingBalance;
                    this.currencyId = this.currencyBalance[i].currencyId;
                }
            }
        }
        //  this.environmentSettingListObj=JSON.parse(localStorage.getItem('environment_settings_list'));
        // console.log(this.environmentSettingListObj);
        // alert(settingsList["withdrawal_max_value"]);
        //  this.withdrawMaxValue=this.environmentSettingListObj["withdrawal_max_value"+this.currencyId].value;
        //  this.withdrawMinValue=this.environmentSettingListObj["withdrawal_min_value"+this.currencyId].value;
        //   this.withdrawDisclaimer=this.environmentSettingListObj["withdrawal_min_value"+this.currencyId].description;
        //   this.withdrawTxnChargeDisclaimer=this.environmentSettingListObj["withdrawal_txn_charges"+this.currencyId].description;
    };
    WithdrawFundsComponent.prototype.appsettingscall = function () {
        var _this = this;
        var infoObj = {};
        infoObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(infoObj);
        this.http.post(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            var result = response;
            //  localStorage.setItem("currencyId",result.settingsList[i].currencyId);
            //   console.log('+++++++++++appsettings',response);
            if (result.error.error_data != '0') {
                //    alert(result.error.error_msg);
            }
            else {
                var storeDashboardInfo = JSON.stringify(result);
                var environmentSettingsListObj = {};
                localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
                for (var i = 0; i < result.settingsList.length; i++) {
                    environmentSettingsListObj['' + result.settingsList[i].name + result.settingsList[i].currencyId + ''] = result.settingsList[i];
                }
                environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
                localStorage.setItem('environment_settings_list', environmentSettingsListObj);
                _this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
                if (_this.environmentSettingListObj["withdrawal_min_value1"].currencyId == '1') {
                    _this.withdrawDisclaimer = _this.environmentSettingListObj["withdrawal_min_value1"].description;
                    _this.withdrawTxnChargeDisclaimer = _this.environmentSettingListObj["withdrawal_txn_charges1"].description;
                    _this.withdrawMaxValue = _this.environmentSettingListObj["withdrawal_max_value1"].value;
                    _this.withdrawMinValue = _this.environmentSettingListObj["withdrawal_min_value1"].value;
                }
            }
        }, function (reason) {
            //   wip(0);
            _this.data.logout();
            if (reason.error.error == 'invalid_token') {
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    WithdrawFundsComponent.prototype.getBankDetails = function () {
        var _this = this;
        var bankDetailsObj = {};
        bankDetailsObj["userId"] = localStorage.getItem("user_id");
        var jsonString = JSON.stringify(bankDetailsObj);
        // wip(1);
        this.http
            .post(this.data.WEBSERVICE + "/user/GetUserBankDetails", jsonString, {
            headers: {
                "Content-Type": "application/json",
                authorization: "BEARER " + localStorage.getItem("access_token")
            }
        })
            .subscribe(function (response) {
            // wip(0);
            var result = response;
            if (result.error.error_data != "0") {
                _this.data.alert(result.error.error_msg, "danger");
            }
            else {
                _this.accountNo = result.bankDetails.account_no;
                _this.beneficiaryName = result.bankDetails.benificiary_name;
                _this.bankName = result.bankDetails.bank_name;
                _this.routingNo = result.bankDetails.routing_no;
                _this.bankAddress = result.bankDetails.address;
            }
        }, function (reason) {
            // wip(0);
            if (reason.data.error == "invalid_token") {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert("Could Not Connect To Server", "danger");
            }
        });
    };
    WithdrawFundsComponent.prototype.getUserbalance = function () {
        this.usdbalance = localStorage.getItem('usdbalance');
    };
    WithdrawFundsComponent.prototype.withdrawFund = function () {
        var _this = this;
        //   console.log(this.withdraw_fund_amount);
        if (this.main.userDocVerificationStatus() &&
            this.main.userBankVerificationStatus()) {
            if (this.withdraw_fund_amount != undefined) {
                if (parseFloat(this.withdraw_fund_amount) >=
                    parseFloat(this.withdrawMinValue) &&
                    parseFloat(this.withdraw_fund_amount) <=
                        parseFloat(this.withdrawMaxValue)) {
                    var withdawObj = {};
                    withdawObj["userId"] = localStorage.getItem("user_id");
                    withdawObj["currencyId"] = this.currencyID.toString();
                    withdawObj["sendAmount"] = this.withdraw_fund_amount.toFixed(2);
                    withdawObj["bankName"] = this.bankName;
                    withdawObj["ifscCode"] = this.routingNo;
                    withdawObj["beneficiaryName"] = this.beneficiaryName;
                    if (this.lockOutgoingTransactionStatus == 1) {
                        withdawObj["otp"] = this.withdrawOtp;
                    }
                    var jsonString = JSON.stringify(withdawObj);
                    // wip(1);
                    this.http
                        .post(this.data.WEBSERVICE + "/transaction/createWithdrawalOrder", jsonString, {
                        headers: {
                            "Content-Type": "application/json",
                            authorization: "BEARER " + localStorage.getItem("access_token")
                        }
                    })
                        .subscribe(function (response) {
                        // wip(0);
                        var result = response;
                        if (result.error.error_data != "0") {
                            _this.data.alert(result.error.error_msg, "dark");
                        }
                        else {
                            _this.withdraw_fund_amount = "";
                            _this.data.alert("Withdrawal Done Successfully", "success");
                            _this.usdbalance = "";
                            _this._StopLossComponent.getUserTransaction();
                            _this.getUserbalance();
                        }
                    }, function (reason) {
                        // wip(0);
                        if (reason.data.error == "invalid_token") {
                            this.logout();
                        }
                        else {
                            alert("Could Not Connect To Server");
                        }
                    });
                }
                else {
                    this.data.alert("Please Provide Proper Amount", "danger");
                }
            }
            else {
                this.data.alert("Please Provide Proper Amount", "warning");
            }
        }
        else {
            $("#wfn").attr("disabled", true);
            this.data.alert("Your Bank Details are being verified", "warning");
        }
    };
    WithdrawFundsComponent.prototype.ngDoCheck = function () {
        this.usdbalance = localStorage.getItem('usdbalance');
    };
    WithdrawFundsComponent.prototype.determineLockStatusForWithdraw = function () {
        var userAppSettingsObj = JSON.parse(localStorage.getItem("user_app_settings_list"));
        this.lockOutgoingTransactionStatus =
            userAppSettingsObj.lock_outgoing_transactions;
        if (this.lockOutgoingTransactionStatus == 1) {
            //   $('#otpModal').modal('show');
            this.data.alert("You are not eligble to withdraw", "danger");
        }
        else {
            this.withdrawFund();
        }
    };
    WithdrawFundsComponent.prototype.resendOtpForOutgoing = function () {
        var _this = this;
        var otpObj = {};
        otpObj["email"] = localStorage.getItem("email");
        var jsonString = JSON.stringify(otpObj);
        this.http
            .post(this.data.WEBSERVICE + "/user/ResendOTP", jsonString, {
            headers: {
                "Content-Type": "application/json"
            }
        })
            .subscribe(function (response) {
            var result = response;
            if (result.error.error_data != "0") {
                _this.data.alert(result.error.error_msg, "danger");
            }
            else {
                $(".send_btn").show();
                $(".get_Otp_btn").hide();
            }
        }, function (reason) {
            _this.data.logout();
            //   wip(0);
            _this.data.alert("Could Not Connect To Server", "danger");
        });
    };
    WithdrawFundsComponent = __decorate([
        Component({
            selector: "app-withdraw-funds",
            templateUrl: "./withdraw-funds.component.html",
            styleUrls: ["./withdraw-funds.component.css"]
        }),
        __metadata("design:paramtypes", [BodyService,
            HttpClient,
            CoreDataService,
            StopLossComponent])
    ], WithdrawFundsComponent);
    return WithdrawFundsComponent;
}());
export { WithdrawFundsComponent };
//# sourceMappingURL=withdraw-funds.component.js.map