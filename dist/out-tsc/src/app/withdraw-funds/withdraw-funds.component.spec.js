import { async, TestBed } from '@angular/core/testing';
import { WithdrawFundsComponent } from './withdraw-funds.component';
describe('WithdrawFundsComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [WithdrawFundsComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(WithdrawFundsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=withdraw-funds.component.spec.js.map