var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { BodyService } from '../body.service';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { StopLossComponent } from "../stop-loss/stop-loss.component";
var NgbdModalContent = /** @class */ (function () {
    function NgbdModalContent(activeModal) {
        this.activeModal = activeModal;
    }
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NgbdModalContent.prototype, "name", void 0);
    NgbdModalContent = __decorate([
        Component({
            selector: 'ngbd-modal-content',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Hi there!</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body\">\n      <p>Hello, {{name}}!</p>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"activeModal.close('Close click')\">Close</button>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [NgbActiveModal])
    ], NgbdModalContent);
    return NgbdModalContent;
}());
export { NgbdModalContent };
var DepositFundsComponent = /** @class */ (function () {
    function DepositFundsComponent(modalService, router, route, main, data, http, _StopLossComponent) {
        this.modalService = modalService;
        this.router = router;
        this.route = route;
        this.main = main;
        this.data = data;
        this.http = http;
        this._StopLossComponent = _StopLossComponent;
        this.currencyID = 1;
        this.noOfItemPerPage = '20';
        this.main.getUserTransaction();
    }
    DepositFundsComponent.prototype.ngOnInit = function () {
        this.paymentOrderList(1);
        this.getAdminBankDetails();
        this.appsettingscall();
        this.currencyBalance = this.main.balencelist;
        this.usdbalance = localStorage.getItem('usdbalance');
        if (this.currencyBalance != null) {
            for (var i = 0; i < this.currencyBalance.length; i++) {
                if (this.currencyBalance[i].currencyCode == "USD") {
                    this.usdbalance = this.currencyBalance[i].closingBalance;
                    this.currencyId = this.currencyBalance[i].currencyId;
                }
            }
        }
        this.CURRENCYNAME = this.data.CURRENCYNAME;
        var environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
        //console.log('deposit',environmentSettingListObj);
        // if(environmentSettingListObj["load_max_value"].currencyId=='1'){
        //   alert('1');
        //   this.depositDisclaimer = environmentSettingListObj["load_max_value"].description;
        //   this.loadMaxValue = environmentSettingListObj["load_max_value"].value;
        //   this.loadMinValue = environmentSettingListObj["load_min_value"].value;
        // }
        // else if(environmentSettingListObj["load_max_value1"].currencyId=='1'){
        //   //var environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
        //   this.depositDisclaimer = environmentSettingListObj["load_max_value1"].description;
        //   this.loadMaxValue = environmentSettingListObj["load_max_value1"].value;
        //   this.loadMinValue = environmentSettingListObj["load_min_value1"].value;
        // }
        // this.depositDisclaimer=localStorage.getItem('loadmaxdescription');
        // alert(this.depositDisclaimer);
    };
    DepositFundsComponent.prototype.appsettingscall = function () {
        var _this = this;
        var infoObj = {};
        infoObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(infoObj);
        this.http.post(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            var result = response;
            //  localStorage.setItem("currencyId",result.settingsList[i].currencyId);
            //   console.log('+++++++++++appsettings',response);
            if (result.error.error_data != '0') {
                //    alert(result.error.error_msg);
            }
            else {
                var storeDashboardInfo = JSON.stringify(result);
                var environmentSettingsListObj = {};
                localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
                for (var i = 0; i < result.settingsList.length; i++) {
                    // alert(result.settingsList[i].currencyId);
                    environmentSettingsListObj['' + result.settingsList[i].name + result.settingsList[i].currencyId + ''] = result.settingsList[i];
                }
                environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
                localStorage.setItem('environment_settings_list', environmentSettingsListObj);
                ////showing disclaimer for sell
                _this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
                // console.log('this.environmentSettingListObj+++++',this.environmentSettingListObj);
                if (_this.environmentSettingListObj["load_max_value1"].currencyId == '1') {
                    //var environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
                    _this.depositDisclaimer = _this.environmentSettingListObj["load_max_value1"].description;
                    _this.loadMaxValue = _this.environmentSettingListObj["load_max_value1"].value;
                    _this.loadMinValue = _this.environmentSettingListObj["load_min_value1"].value;
                    //  localStorage.setItem('loadmaxdescription',a);
                    //  localStorage.setItem('loadminvalue',b);
                    //  localStorage.setItem('loadminvalue',c);
                }
            }
        }, function (reason) {
            //   wip(0);
            _this.data.logout();
            if (reason.error.error == 'invalid_token') {
                _this.data.alert('Session Timeout. Login Again', 'warning');
            }
            else
                _this.data.alert('Could Not Connect To Server', 'danger');
        });
    };
    DepositFundsComponent.prototype.depositAmount = function (content) {
        var _this = this;
        if (this.deposit_amount != undefined || this.deposit_amount == 0) {
            if (this.main.userDocVerificationStatus() && this.main.userBankVerificationStatus()) {
                if (parseFloat(this.deposit_amount) <= parseFloat(this.loadMaxValue) && parseFloat(this.deposit_amount) >= parseFloat(this.loadMinValue)) {
                    var depositObj = {};
                    depositObj['sendAmount'] = this.deposit_amount.toFixed(2);
                    depositObj['currencyId'] = this.currencyID.toString();
                    depositObj['userId'] = localStorage.getItem('user_id');
                    // depositObj['referenceNo'] = "kkk";
                    var jsonString = JSON.stringify(depositObj);
                    this.http.post(this.data.WEBSERVICE + '/transaction/createPaymentOrder', jsonString, {
                        headers: {
                            'Content-Type': 'application/json',
                            'authorization': 'BEARER ' + localStorage.getItem('access_token'),
                        }
                    })
                        .subscribe(function (response) {
                        var result = response;
                        if (result.error.error_data != '0') {
                            _this.deposit_amount = '';
                            _this.data.alert(result.error.error_msg, 'warning');
                        }
                        else {
                            _this.modalService.open(content, {
                                centered: true
                            });
                            _this.deposit_amount = '';
                            _this.paymentOrderList(1);
                            _this._StopLossComponent.getUserTransaction();
                            _this.getAdminBankDetails();
                            _this.orderNoToShow = result.paymentOrdersListResult[0].order_no;
                            // $('.order_no').html(this.orderNo);
                            // $('#order_id_modal').modal('show');
                        }
                    }, function (reason) {
                        if (reason.data.error == 'invalid_token') {
                            this.data.logout();
                        }
                        else {
                            this.data.logout();
                            this.data.alert('Could Not Connect To Server', 'danger');
                        }
                    });
                }
                else {
                    this.data.alert(this.depositDisclaimer, 'warning');
                }
            }
            else {
                $('#payn').attr('disabled', true);
                this.data.alert('Your Bank Details are not being verified', 'warning');
            }
        }
        else {
            this.data.alert('Please Provide Deposit Amount', 'info');
        }
    };
    DepositFundsComponent.prototype.paymentOrderList = function (pageNo) {
        var _this = this;
        var paymentOrderObj = {};
        paymentOrderObj['userId'] = localStorage.getItem('user_id');
        paymentOrderObj['pageNo'] = pageNo.toString();
        paymentOrderObj['noOfItemsPerPage'] = this.noOfItemPerPage;
        var jsonString = JSON.stringify(paymentOrderObj);
        this.http.post(this.data.WEBSERVICE + '/transaction/getInvoicesList', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            var result = response;
            if (result.error.error_data != '0') {
                alert(result.error.error_msg);
            }
            else {
                var paymentOrdersListResult = result.invoicesListResult;
                var paymentOrderListHtml = '';
                _this.totalCount = result.totalCount;
                $('.payment_order_list_body').html('');
                if (paymentOrdersListResult != null) {
                    _this.invoiceList = paymentOrdersListResult;
                    for (var i = 0; i < paymentOrdersListResult.length; i++) {
                        var timestamp = paymentOrdersListResult[i].created;
                        var timestampArr = timestamp.split('.');
                        timestamp = timestampArr[0];
                        if (paymentOrdersListResult[i].paymentOrders.status == '15') {
                            var status = 'Order Received';
                            var actionText = 'View Invoice';
                            var actionTextColor = 'text-blue';
                        }
                        if (paymentOrdersListResult[i].paymentOrders.status == '16') {
                            var status = 'Reference Updated';
                            var actionText = 'Update Reference Number';
                            var actionTextColor = 'text-yellow';
                        }
                        if (paymentOrdersListResult[i].paymentOrders.status == '17') {
                            var status = 'Order Confirm';
                            var actionText = 'Order Confirm';
                            var actionTextColor = 'text-green';
                        }
                        _this.orderNo = paymentOrdersListResult[i].paymentOrders.order_no;
                        paymentOrderListHtml += '<tr>';
                        paymentOrderListHtml += '<td>' + timestamp + '</td>';
                        paymentOrderListHtml += '<td>' + _this.orderNo + '</td>';
                        paymentOrderListHtml += '<td class="text-white">' + ' ' + (paymentOrdersListResult[i].fiat_amount).toFixed(4) + '</td>';
                        paymentOrderListHtml += '<td><span class="text-blue" data-order-id="' + _this.orderNo + '" data-invoice-id="' + paymentOrdersListResult[i].invoice_id + '" onclick="angular.element(this).scope().getInvoiceDetails(this)" style="cursor:pointer;">View Invoice</span></td>';
                        paymentOrderListHtml += '</tr>';
                    }
                    _this.main.pagination(_this.totalCount, _this.noOfItemPerPage, 'paymentOrderList');
                }
                else {
                    paymentOrderListHtml += '<tr colspan="5">No Data Exist</tr>';
                }
                //   $('.payment_order_list_body').html(paymentOrderListHtml);
            }
        }, function (reason) {
            //   wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'danger');
            }
        });
    };
    DepositFundsComponent.prototype.getAdminBankDetails = function () {
        var _this = this;
        var bankDetailsObj = {};
        bankDetailsObj['userId'] = localStorage.getItem('user_id');
        var jsonString = JSON.stringify(bankDetailsObj);
        this.http.post(this.data.WEBSERVICE + '/user/GetAdminBankDetails', jsonString, {
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
        })
            .subscribe(function (response) {
            var result = response;
            if (result.error.error_data != '0') {
                //    alert(result.error.error_msg);
            }
            else {
                _this.bankName = result.bankDetails.bank_name;
                _this.routingNo = result.bankDetails.routing_no;
                _this.accountNo = result.bankDetails.account_no;
                _this.routingNo = result.bankDetails.routing_no;
                _this.bankUserName = result.bankDetails.bank_user_name;
                _this.bankAddress = result.bankDetails.bank_address;
            }
        }, function (reason) {
            //   wip(0);
            if (reason.data.error == 'invalid_token') {
                this.data.logout();
            }
            else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'danger');
            }
        });
    };
    DepositFundsComponent.prototype.invoice = function (content, id, amount) {
        this.modalService.open(content, {
            centered: true
        });
        this.invoiceOrderNo = id;
        this.invoiceAmount = amount;
    };
    DepositFundsComponent.prototype.myFunction = function () {
        // Get the checkbox
        var checkBox = document.getElementById("disclaim");
        // Get the output text
        var text = document.getElementById("text");
        this.TransactionId = localStorage.getItem('user_id') + Date.now();
        //  console.log("+++++++++", this.TransactionId);
        //this.password =Md5.hashStr('VIP2019@!$') ;
        //  alert( this.password);
        // If the checkbox is checked, display the output text
        var checked = document.forms["uc-cart-checkout-form"]["disclaim"].checked;
        //alert(checked);
        if (checked == true) {
            this.disclaim = true;
        }
        else {
            this.disclaim = false;
        }
    };
    DepositFundsComponent.prototype.Opendisclaimer = function (depodisc, CURRENCYNAME, balence) {
        this.modalService.open(depodisc, {
            centered: true
        });
        this.currencyname = CURRENCYNAME;
        this.currentbalence = balence;
        this.dep_amount = (document.getElementById("deposit_amount").value);
        this.Name = localStorage.getItem('user_name');
        //this.Name = "Amit Bhunia";
        //console.log('++++++++++++email', this.Name);
        var temp = this.Name.split(" ");
        this.fname = temp[0];
        this.lname = temp[1];
        if (this.lname == "undefined" || this.lname == "")
            this.lname = '';
        //console.log('++++++++++++',this.fname,this.lname);
        this.email = localStorage.getItem('email');
        //console.log('++++++++++++email', this.email);
    };
    DepositFundsComponent = __decorate([
        Component({
            selector: 'app-deposit-funds',
            templateUrl: './deposit-funds.component.html',
            styleUrls: ['./deposit-funds.component.css']
        }),
        __metadata("design:paramtypes", [NgbModal, Router, ActivatedRoute, BodyService, CoreDataService, HttpClient, StopLossComponent])
    ], DepositFundsComponent);
    return DepositFundsComponent;
}());
export { DepositFundsComponent };
//# sourceMappingURL=deposit-funds.component.js.map