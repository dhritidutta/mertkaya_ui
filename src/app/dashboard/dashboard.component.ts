import { Component, OnInit } from "@angular/core";
import { CoreDataService } from "../core-data.service";
import { Router } from "@angular/router";
import * as $ from "jquery";
//import { OrderBookComponent } from "../order-book/order-book.component";
import { HttpClient } from "@angular/common/http";
import { TvChartContainerComponent } from "../tv-chart-container/tv-chart-container.component";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  alertType: any;
  alertMsg: any;
  chart: any = true;
  orderbook: any = true;
  trade: any = true;
  stoploss: any = true;
  drag: any = false;
  mode: any = false;
  chartlist: any;
  lowprice: any;
  highprice;
  any = 0;
  ctpdata: any = 0;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  screencolor: boolean;
  public Themecolor;
  getlang:any;
  constructor(
    public data: CoreDataService,
    private route: Router,
    private http: HttpClient,
    public tvChartContainerComponent: TvChartContainerComponent,
    public translate:TranslateService
  ) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  ngOnInit() {
    this.selectedSellingCryptoCurrencyName = "try";
    this.selectedBuyingCryptoCurrencyName = "btc";
    this.data.changescreencolor = false;
   this.Themecolor ='Dark';
  localStorage.setItem('themecolor',this.Themecolor);
    this.http
      .get<any>(
        this.data.CHARTSERVISE +
          "trendsTradeGraphFor24Hours/" +
          this.selectedBuyingCryptoCurrencyName +
          "/" +
          this.selectedSellingCryptoCurrencyName
      )
      .subscribe(value => {
        if (value != "") {
          this.chartlist = value[0];
          this.ctpdata = this.data.ctpdata = this.chartlist.CTP;
          this.lowprice = this.data.lowprice = this.chartlist.LOW_PRICE;
          this.highprice = this.data.highprice = this.chartlist.HIGH_PRICE;
        }
      });
    $(document).ready(function() {
      $(this).scrollTop(0);
      var i = 1;

      $(".drg").click(function() {
        i++;
        $(this).css("z-index", i);
      });

      $(':input[type="number"]').keyup(function() {
      });
    });
  }

  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  changebg(val) {
    this.screencolor = val;
    this.data.changescreencolor = val;
    if (this.data.changescreencolor == true) {
      this.Themecolor ='Light';
       localStorage.setItem('themecolor',this.Themecolor);
      $(".content-wrapper").css("background-color", "#ececec").addClass("intro");
      document.getElementById("night").style.display = "block";
      document.getElementById("light").style.display = "none";

    } else {
      $(".content-wrapper").css("background-color", "#060707").removeClass("intro");
      document.getElementById("light").style.display = "block";
      document.getElementById("night").style.display = "none";
      document.getElementById("drag-btn").style.backgroundColor = "#292931";
      this.Themecolor ='Dark';
      localStorage.setItem('themecolor',this.Themecolor);
    }
  }
  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);

    var maxVal1: number = parseInt(maxVal);

    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }
}