import {
  Component,
  OnInit
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  CoreDataService
} from '../core-data.service';
import {
  BodyService
} from '../body.service';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-my-wallet',
  templateUrl: './my-wallet.component.html',
  styleUrls: ['./my-wallet.component.css']
})
export class MyWalletComponent implements OnInit {
  flag: boolean;
  transactionType = 'send';
  historyDetails: any;
  totalCount: any;
  historyTableTr: string;
  selectedCurrency: string;
  selectedCurrencyText: string;
  rcvCode: any;
  cryptoCurrency: string;
  rcv: any;
  lockOutgoingTransactionStatus: any;
  paybito_phone: any;
  paybito_amount: any;
  paybito_otp: any;
  other_address: string;
  other_amount: any;
  other_otp: any;
  sendDisclaimer: any;
  sendDisclaimer2: any;
  rateList: any;
  trigx: any;
  accept: boolean;
  mining_fees: number = 0;
  balance: number = 0;
  limit: any = 0;
  xrptag:any;
  xrp_tag:any;
  btcLink: any = 'https://live.blockcypher.com/btc/tx/';
  ltcLink: any = 'https://live.blockcypher.com/ltc/tx/';
  bchLink: any = 'https://explorer.bitcoin.com/bch/search/';
  ethLink: any = 'https://etherscan.io/tx/';
  etclink: any =  'http://gastracker.io/tx/';
  bsvlink: any =  'https://blockchair.com/search?q=';
  xrplink:any = 'https://bithomp.com/explorer/';
  triggerslink: any = 'https://xchain.io/tx/';

  loading: boolean;
  disclaim:boolean=false;
  currencyID:any;
  currencyId:any;
  public currencyBalance:any;
  public usdbalance:any;
  getlang:any;
  constructor( private http: HttpClient, public translate:TranslateService, private data: CoreDataService, public main: BodyService, private modalService: NgbModal, private route: Router) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  ngOnInit() {
    this.main.getUserTransaction();
    this.walletHistoryList('1');
    this.main.getDashBoardInfo();
    this.sendMax();
    this.currencyBalance =this.main.balencelist;
    if( this.currencyBalance !=null){
    for(var i=0;i<this.currencyBalance.length;i++){
    if(this.currencyBalance[i].currencyCode=="TRY"){
      this.usdbalance=this.currencyBalance[i].closingBalance;
      this.currencyId=this.currencyBalance[i].currencyId;
    }
  }
}
}

Selectedlangulage(lang) {
  localStorage.getItem('selectedlang');
}

  walletHistoryList(pageNo) {
    $('.walletHistoryTableBody').html(`<tr>
    <td colspan="5" class="text-center py-3">
    <img src="./assets/svg-loaders/puff.svg" width="50" alt="">
    </td>
  </tr>`);
    var historyObj = {};
    historyObj['userId'] = localStorage.getItem('user_id');
    historyObj['pageNo'] = 1;
    historyObj['noOfItemsPerPage'] = 20;
    historyObj['timeSpan'] = this.main.timeSpan;
    historyObj['transactionType'] = this.transactionType;

    var jsonString = JSON.stringify(historyObj);
    // wip(1);
    this.http.post < any > (this.data.WEBSERVICE + '/transaction/getUserAllTransaction', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(response => {
        // wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          if (result.error.error_msg == 'no result') {
            if(this.translate.currentLang == 'English'){
            $('.walletHistoryTableBody').html(`<tr>
            <td colspan="5" class="text-center py-3">
              No Data Available
            </td>
          </tr>`);
            }
            else{
              $('.walletHistoryTableBody').html(`<tr>
              <td colspan="5" class="text-center py-3">
              Veri Bulunmuyor
              </td>
            </tr>`);
              }
          } else {
            this.historyDetails = result.userTransactionsResult;
            this.totalCount = result.totalCount;
            this.historyTableTr = '';
            if (this.historyDetails != null) {
              for (var i = 0; i < this.historyDetails.length; i++) {
                var timestamp = this.historyDetails[i].transactionTimestamp;
                var timestampArr = timestamp.split('.');
                timestamp = this.data.readable_timestamp(timestampArr[0]);
                var action = this.historyDetails[i].action;
                var hrefForTxn = '';
                var toolTipDesc = '';
                var amount = '';
                if (action == 'Send' || this.transactionType == 'send') {
                  if (this.historyDetails[i]['debitAmount'] != '0' && this.historyDetails[i]['currency']=='BTC') {
                    var amount = '<span class="text-white">' + this.historyDetails[i]['currency'] + this.historyDetails[i]['debitAmount'] + ' Dr.';
                    if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                      if ((this.historyDetails[i].currencyTxnid).length >0) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.btcLink + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                  if (this.historyDetails[i]['debitAmount'] != '0' && this.historyDetails[i]['currency']=='ETH') {
                    var amount = '<span class="text-white">' + this.historyDetails[i]['currency'] + this.historyDetails[i]['debitAmount'] + ' Dr.';
                    if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                      if ((this.historyDetails[i].currencyTxnid).length >0) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.ethLink + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                  if (this.historyDetails[i]['debitAmount'] != '0' && this.historyDetails[i]['currency']=='XRP') {
                    var amount = '<span class="text-white">' + this.historyDetails[i]['currency'] + ' ' + this.historyDetails[i]['debitAmount'] + ' Dr.';
                    if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                      if ((this.historyDetails[i].currencyTxnid).length >0) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.xrplink + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                  if (this.historyDetails[i]['debitAmount'] != '0' && this.historyDetails[i]['currency']=='USDT') {
                    var amount = '<span class="text-white">' + this.historyDetails[i]['currency'] + ' ' + this.historyDetails[i]['debitAmount'] + ' Dr.';
                    if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                      if ((this.historyDetails[i].currencyTxnid).length >0) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.ethLink + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                }

                if (action == 'Received' || this.transactionType == 'received') {
                  if (this.historyDetails[i]['creditAmount'] != '0' && this.historyDetails[i]['currency']=='BTC') {
                    var amount = '<span class="text-white">' + this.historyDetails[i]['currency'] + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr.';
                    if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                      if ((this.historyDetails[i].currencyTxnid).length >0) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.btcLink + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                  if (this.historyDetails[i]['creditAmount'] != '0' && this.historyDetails[i]['currency']=='ETH') {
                    var amount = '<span class="text-white">' + this.historyDetails[i]['currency'] + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr.';
                    if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                      if ((this.historyDetails[i].currencyTxnid).length >0) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.ethLink + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                  if (this.historyDetails[i]['creditAmount'] != '0' && this.historyDetails[i]['currency']=='XRP') {
                    var amount = '<span class="text-white">' + this.historyDetails[i]['currency'] + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr.';
                    if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                      if ((this.historyDetails[i].currencyTxnid).length >0) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.xrplink + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                  if (this.historyDetails[i]['creditAmount'] != '0' && this.historyDetails[i]['currency']=='USDT') {
                    var amount = '<span class="text-white">' + this.historyDetails[i]['currency'] + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr.';
                    if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                      if ((this.historyDetails[i].currencyTxnid).length >0) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.ethLink + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                 }

                if (this.historyDetails[i].status == '1' || this.historyDetails[i].status == '1') {
                  var status = 'Confirmed';
                  var statusClass = 'text-green';
                }
                if (this.historyDetails[i].status == '0' || this.historyDetails[i].status == '0') {
                  var status = 'Pending';
                  var statusClass = 'text-orange';
                }
                if (this.historyDetails[i].status == 'cancel' || this.historyDetails[i].status == 'Cancel') {
                  var status = 'Cancelled';
                  var statusClass = 'text-red';
                }

                toolTipDesc = '<div class="position-absolute tool_tip_div tool_tip_' + this.historyDetails[i].transactionId + ' mt-4 bg-pbgreen text-white p-1 rounded " style="display:none;">' + this.historyDetails[i].description + '  <span data-txn-id="' + this.historyDetails[i].transactionId + '" onClick="angular.element(this).scope().hideToolTipDesc()"><i class="fa fa-times"></i></span></div>';
                var description = (this.historyDetails[i].description != null) ? this.historyDetails[i].description : 'Received Amount';
                if (description.length < 25) {
                  var descriptionTd = '<td width="30%" data-txn-id="' + this.historyDetails[i].transactionId + '" > ' + description + ' ' + hrefForTxn + '</td>';;
                } else {
                  var descriptionTd = '<td width="30%" data-txn-id="' + this.historyDetails[i].transactionId + '" title="' + description + '">' + toolTipDesc + ' ' + (description).substr(0, 25) + '...  ' + hrefForTxn + '</td>';
                }
                this.historyTableTr += '<tr>';
                this.historyTableTr += '<td width="30%">' + timestamp + '</td>';
                this.historyTableTr += descriptionTd;
                this.historyTableTr += '<td class="text-white" width="10%">' + action + '</td>';
                this.historyTableTr += '<td width="20%">' + amount + '</td>';
                this.historyTableTr += '<td class="' + statusClass + '" width="10%">' + status + ' </td>';
                this.historyTableTr += '</tr>';
                //}
              }
              this.main.pagination(this.totalCount, this.main.noOfItemPerPage, 'walletHistoryList');
            } else {
              this.historyTableTr += '<tr colspan="5" class="text-center">No Data Exist</tr>';
            }
            $('.walletHistoryTableBody').html(this.historyTableTr);
            this.main.getUserTransaction();

          }
        }
      }, function (reason) {
        // wip(0);
        if (reason.data.error == 'invalid_token') {
          this.data.logout();
        } else {
          this.data.logout();
          this.data.alert('Could Not Connect To Server', 'warning');
        }
      });
  }

  filterType(type) {
    if (type == 'send') {
      this.transactionType = 'send';
      this.walletHistoryList('1');
      $('.mywallet_filter_btn').removeClass('btn_active');
      $('.send_filter_btn_wallet').addClass('btn_active');

    } else {
      this.transactionType = 'received';
      this.walletHistoryList('1');
      $('.mywallet_filter_btn').removeClass('btn_active');
      $('.recieved_filter_btn_wallet').addClass('btn_active');
    }
  }

  showToolTipDesc(elem) {
    var txnId = elem.getAttribute('data-txn-id');
    $('.tool_tip_div').hide();
    $('.tool_tip_' + txnId).show();
  }

  hideToolTipDesc() {
    setTimeout(function () {
      $('.tool_tip_div').hide();
    }, 100);
  }

  copy(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

  getCurrencyForRecieve(elem,currency,CID) {
    this.cryptoCurrency = currency;
    this.currencyID=CID;

    $('.receive_address_label, .receive_address, .recieve_qr_code').hide();
    $('.generate_address_btn').hide();
    $('#qr_code').html('');
    if (this.main.userDocVerificationStatus() == true) {
      this.generateAddress(this.currencyID);
      this.modalService.open(elem, {
        centered: true
      });
    }

  }

  generateAddress(currencyId) {
    this.rcv = null;
    this.xrptag = null;
    var rcvObj = {};
    rcvObj['userId'] = localStorage.getItem('user_id');
    rcvObj['currencyId'] =currencyId;

    var jsonString = JSON.stringify(rcvObj);
    // wip(1);
    if (this.cryptoCurrency != 'triggers') {
      this.http.post < any > (this.data.WEBSERVICE + '/transaction/getCryptoAddress', jsonString, {
          headers: {
            'Content-Type': 'application/json',
            'authorization': 'BEARER ' + localStorage.getItem('access_token'),
          }
        })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            this.rcv = result.customerLedgerResult.publicKey;//change by sanu
            this.xrptag=result.customerLedgerResult.xrpTag;
          }

        });
    } else {
      this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/GetCounterPartyNewAddress', jsonString, {
          headers: {
            'Content-Type': 'application/json',
            'authorization': 'BEARER ' + localStorage.getItem('access_token'),
          }
        })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            this.rcv = result.customerLedgerResult.publicKey;//change by sanu
          }

        });

    }

  }

  getCurrencyForSend(md, elem, bal,cId) {
    this.cryptoCurrency = elem;
    this.selectedCurrency = elem;
    this.balance = bal;
    this.mining_fees = 0;
    this.currencyId=cId;
    localStorage.setItem("currencyId",this.currencyId);
    if (this.main.userDocVerificationStatus() == true) {
      this.modalService.open(md, {
        centered: true
      });
      this.paybito_phone = this.paybito_amount = this.other_address = this.other_amount = null;
      var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
      this.lockOutgoingTransactionStatus = userAppSettingsObj.lock_outgoing_transactions;
      if (this.lockOutgoingTransactionStatus == 1) {
        $('.sendOtpSection').show();
        $('.send_btn').show();

      } else {
        $('.sendOtpSection').hide();
        $('.send_btn').show();
      }
      var settingsList = JSON.parse(localStorage.getItem('environment_settings_list'));
      this.sendDisclaimer = settingsList["send_other_min_value"+this.currencyId].description;
      this.sendDisclaimer2 = settingsList["send_other_m_charges"+this.currencyId].description;
    }
    if(this.cryptoCurrency =='triggers'){
     this.flag = true;
   }
   else{
    this.flag = false;
   }
  }

  rate(content) {
    this.modalService.open(content, {
      centered: true
    });
    this.rateList = null;
    var feeObj = {};
    feeObj['currency'] = this.cryptoCurrency;
    var jsonString = JSON.stringify(feeObj);
    this.http.post < any > (this.data.WEBSERVICE + '/transaction/getFees', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token')
        }
      })
      .subscribe(data => {
        this.rateList = data.feesListResult;
      })
  }

  Disclaimerdtl(trigger){
    this.modalService.open(trigger, {
      centered: true
    });
  }

  transactionSendWithinPaybito() {
    var sendToPaybitoObj = {};
    sendToPaybitoObj['userId'] = localStorage.getItem('user_id');
    sendToPaybitoObj['toAdd'] = this.paybito_phone;
    sendToPaybitoObj['sendAmount'] = this.paybito_amount;
    sendToPaybitoObj['currencyId'] = localStorage.getItem("currencyId");
    if (this.lockOutgoingTransactionStatus == 1) {
      sendToPaybitoObj['otp'] = this.paybito_otp;
    }
    var jsonString = JSON.stringify(sendToPaybitoObj);
    // wip(1);
    this.http.post < any > (this.data.WEBSERVICE + '/transaction/sendToWallet', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(response => {
        // wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.data.alert('Sent succesfully', 'success');
          this.paybito_phone = '';
          this.paybito_amount = '';
          this.other_address = '';
          this.other_amount = '';
          this.main.getUserTransaction();
          this.route.navigateByUrl('/my-wallet');
        }

      }, reason => {
        // wip(0);
        this.data.logout();
        this.data.alert('Could Not Connect To Server', 'danger');
      });
  }

  transactionSendForOthers(openmodal) {
    var sendToPaybitoObj = {};
    sendToPaybitoObj['userId'] = localStorage.getItem('user_id');
    sendToPaybitoObj['currencyId'] =localStorage.getItem("currencyId");
    sendToPaybitoObj['toAdd'] = this.other_address;
     sendToPaybitoObj['sendAmount'] = this.other_amount;
     sendToPaybitoObj['xrpTag'] = this.xrp_tag;
    if (this.lockOutgoingTransactionStatus == 1) {
      sendToPaybitoObj['otp'] = this.other_otp;
    }
    var jsonString = JSON.stringify(sendToPaybitoObj);
    if (this.cryptoCurrency != 'triggers'){
    this.http.post < any > (this.data.WEBSERVICE + '/transaction/sendToOther', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(response => {
        //  wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.modalService.open(openmodal, {
            centered: true
          })
          this.data.alert('Balance transfered successfully', 'success');
          this.main.getUserTransaction();
          this.paybito_phone = '';
          this.paybito_amount = '';
          this.other_address = '';
          this.other_amount = '';
        }

      }, reason => {
        //  wip(0);
        this.data.alert(reason, 'danger');
      });
    }else{
      this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/SendTriggers', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(response => {
        //  wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.modalService.open(openmodal, {
            centered: true
          })
          this.data.alert('Balance transfered successfully', 'success');
          this.paybito_phone = '';
          this.paybito_amount = '';
          this.other_address = '';
          this.other_amount = '';
        }

      }, reason => {
        //  wip(0);
        this.data.alert(reason, 'danger');
      });
    }
  }
  testmodal(test){
  }

  myFunction(){
    var checked = document.forms["uc-disclaimer-form"]["disclaim"].checked;
    if (checked == true) {
     document.getElementById('sclaimer').style.display='block';
    } else {
      document.getElementById('sclaimer').style.display='none';
    }
  }

  swapModal(content, bal) {
    this.modalService.open(content, {
      centered: true,
      size: 'lg'
    });
    this.trigx = bal;
  }

   getRate(event, cur) {
    this.mining_fees = 0;
    if (typeof event == 'object') {
      var am = event.target.value;
    } else {
      var am = event;
    }
    this.http.get('./assets/appdata/rateRange.json')
      .subscribe(data => {
        var datax: any = data;
        function curx(arr) {
          return arr.currency == cur;
        }
        var solid = datax.filter(curx);
        function ratex(arr) {
          return arr.fromFee <= parseInt(am) && arr.toFee >= parseInt(am);
        }
        var solid2 = solid.find(ratex);
        if (solid2) {
          this.mining_fees = solid2.feeRate;
          this.limit = this.balance - this.mining_fees;
        }
      })
  }

  sendMax() {
    this.other_amount = 0;
    this.getRate(this.balance, this.cryptoCurrency);
    this.loading = true;
    setTimeout(() => {
      if (this.balance > 0 && this.balance > this.limit || this.balance < this.mining_fees) {
        this.other_amount = this.balance - this.mining_fees;
      } else {
        this.other_amount = 0;
      }
      this.loading = false;
    }, 500);
  }
}