import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BodyService } from "../body.service";
import { CoreDataService } from "../core-data.service";
import * as $ from "jquery";
import { NavbarComponent } from "../navbar/navbar.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { not } from "@angular/compiler/src/output/output_ast";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.css"]
})
export class SettingsComponent implements OnInit {
  match: boolean = false;
  out: boolean = false;

  getlang: any;
  constructor(
    private http: HttpClient,
    public translate: TranslateService,
    private main: BodyService,
    private data: CoreDataService,
    private modalService: NgbModal
  ) {
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }

  ngOnInit() {
    this.main.getDashBoardInfo();
    this.getUserAppSetting();
  }

  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }

  oldPassword;
  newPassword;

  changePassword(isValid) {
    if (isValid) {
      var changePasswordObj = {};
      changePasswordObj["userId"] = localStorage.getItem("user_id");
      changePasswordObj["password"] = this.oldPassword;
      changePasswordObj["newPassword"] = this.newPassword;
      var jsonString = JSON.stringify(changePasswordObj);
      // wip(1);
      this.http
        .post<any>(this.data.WEBSERVICE + "/user/ChangePassword", jsonString, {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        })
        .subscribe(
          response => {
            // wip(0);
            var result = response;
            if (result.error.error_data != "0") {
              this.data.alert(result.error.error_msg, "danger");
            } else {
              this.data.alert(
                "Password Changed Successfully,You will be logged out",
                "success"
              );
              this.data.logout();
            }
          },
          function(reason) {
            // wip(0);
            if (reason.data.error == "invalid_token") {
              this.data.logout();
            } else {
              this.data.alert("Could Not Connect To Server", "danger");
            }
          }
        );
    } else {
    }
  }
  retypePassword;

  matchPassword() {
    if (this.retypePassword != undefined) {
      if (this.newPassword == this.retypePassword) {
        $(".match_error").css("color", "green");
        $(".match_error").html('<i class="fa fa-check"></i> Password Matched');
        $(".generate_password_otp_btn").removeAttr("disabled");
        this.match = true;
      } else {
        $(".match_error").css("color", "red");
        $(".match_error").html(
          '<i class="fa fa-times"></i> Password Mismatched'
        );
        $(".generate_password_otp_btn").attr("disabled", "disabled");
        this.match = false;
      }
    } else {
      $(".match_error").html("");
      $(".generate_password_otp_btn").attr("disabled", "disabled");
    }
  }
  lockOutgoingTransactionStatus;
  twoFactorStatus;
  incomingTransactionAlert;
  pinLock;
  soundAlert;
  alertRate;
  rateAlert;
  vibrateAlert;
  indentificationStatus;
  bankDetailStatus;
  google_auth_otp;
  twoFactorAuthKey;

  getUserAppSetting() {
    this.data.alert("Loading...", "dark");
    var settingObj = {};
    settingObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(settingObj);
    // wip(1);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/user/GetUserAppSettings",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          this.data.loader = false;
          // wip(0);
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            this.lockOutgoingTransactionStatus =
              result.userAppSettingsResult.lock_outgoing_transactions;
            this.twoFactorStatus = result.userAppSettingsResult.two_factor_auth;
            this.incomingTransactionAlert =
              result.userAppSettingsResult.incoming_transactions_alert;
            this.pinLock = result.userAppSettingsResult.pin_lock;
            this.soundAlert = result.userAppSettingsResult.sound_alert;
            this.alertRate = result.userAppSettingsResult.alert_rate;
            this.rateAlert = result.userAppSettingsResult.rate_alert;
            this.vibrateAlert = result.userAppSettingsResult.vibrate_alert;
            if (result.userAppSettingsResult.user_docs_status == "") {
              this.indentificationStatus =
                "Identity verification documents not submitted";
            }
            if (result.userAppSettingsResult.user_docs_status == "1") {
              this.indentificationStatus =
                "Identity verification documents verified";
            }
            if (result.userAppSettingsResult.user_docs_status == "0") {
              this.indentificationStatus =
                " Identity verification documents submitted for Verification";
            }
            if (result.userAppSettingsResult.user_docs_status == "2") {
              this.indentificationStatus =
                " Identity verification documents declined, please submit again";
            }
            if (result.userAppSettingsResult.bank_details_status == "") {
              this.bankDetailStatus = "Bank details not submitted";
            }
            if (result.userAppSettingsResult.bank_details_status == "0") {
              this.bankDetailStatus =
                "Bank details  submitted for Verification";
            }
            if (result.userAppSettingsResult.bank_details_status == "2") {
              this.indentificationStatus = " Bank details verified";
            }
            if (result.userAppSettingsResult.bank_details_status == "3") {
              this.bankDetailStatus =
                " Identity verification documents declined, please submit again";
            }
            if (this.twoFactorStatus == 0) {
              $(".twoFactorTrueEffect").hide();
              $(".twoFactorFalseEffect").show();
            }
            if (this.twoFactorStatus == 1) {
              $(".twoFactorFalseEffect").hide();
              $(".twoFactorTrueEffect").show();
            }
          }
        },
        function(reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.logout();
            this.data.alert("Could Not Connect To Server", "danger");
          }
        }
      );
  }

  updateUserAppSettings() {
    var updateSettingObj = {};
    updateSettingObj["userId"] = localStorage.getItem("user_id");
    updateSettingObj[
      "lock_outgoing_transactions"
    ] = this.lockOutgoingTransactionStatus;
    updateSettingObj["two_factor_auth"] = this.twoFactorStatus;
    updateSettingObj["rate_alert"] = this.rateAlert;
    updateSettingObj["vibrate_alert"] = this.vibrateAlert;
    updateSettingObj[
      "incoming_transactions_alert"
    ] = this.incomingTransactionAlert;
    updateSettingObj["pin_lock"] = this.pinLock;
    updateSettingObj["sound_alert"] = this.soundAlert;
    updateSettingObj["alert_rate"] = this.alertRate;
    var jsonString = JSON.stringify(updateSettingObj);
    // wip(1);

    this.http
      .post<any>(
        this.data.WEBSERVICE + "/user/SaveUserAppSettings",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            this.getUserAppSetting();
          }
        },
        function(reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.logout();
            this.data.alert("Could Not Connect To Server", "danger");
          }
        }
      );
  }

  changeStatusFor2FactorAuth(content) {
    this.twoFactorStatus = !this.twoFactorStatus;
    if (this.twoFactorStatus == true) {
      this.out = false;
      this.googleTwoFactorAuth(content);
    } else {
      this.out = true;
      this.twoFactorStatus = "0";
      this.lockOutgoingTransactionStatus = "0";
      this.updateUserAppSettings();
      this.data.alert(
        "Two factor verification turned off. Login Again",
        "info"
      );
      setTimeout(() => {
        this.data.logout();
      }, 400);
    }
  }

  changeStatusForLockOutgoingTransaction() {
    this.lockOutgoingTransactionStatus = !this.lockOutgoingTransactionStatus;
    if (this.lockOutgoingTransactionStatus == true) {
      this.lockOutgoingTransactionStatus = "1";
    } else {
      this.lockOutgoingTransactionStatus = "0";
    }
    var updateSettingObj = {};
    updateSettingObj["userId"] = localStorage.getItem("user_id");
    updateSettingObj[
      "lock_outgoing_transactions"
    ] = this.lockOutgoingTransactionStatus;
    updateSettingObj["two_factor_auth"] = this.twoFactorStatus;
    updateSettingObj["rate_alert"] = this.rateAlert;
    updateSettingObj["vibrate_alert"] = this.vibrateAlert;
    updateSettingObj[
      "incoming_transactions_alert"
    ] = this.incomingTransactionAlert;
    updateSettingObj["pin_lock"] = this.pinLock;
    updateSettingObj["sound_alert"] = this.soundAlert;
    updateSettingObj["alert_rate"] = this.alertRate;
    var jsonString = JSON.stringify(updateSettingObj);
    // wip(1);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/user/SaveUserAppSettings",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          // wip(0);
          var result = response.data;
          if (result.error.error_data != "0") {
          } else {
            this.getUserAppSetting();
          }
        },
        function(reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.logout();
            this.data.alert("Could Not Connect To Server");
          }
        }
      );
  }

  googleTwoFactorAuth(content) {
    var inputObj = {};
    inputObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(inputObj);
    // wip(1);
    this.http
      .post<any>(this.data.WEBSERVICE + "/user/GetTwoFactorykey", jsonString, {
        headers: {
          "Content-Type": "application/json",
          authorization: "BEARER " + localStorage.getItem("access_token")
        }
      })
      .subscribe(
        response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            $("#google_auth_qr_code").html("");
            this.google_auth_otp = "";
            this.twoFactorAuthKey =
              "otpauth://totp/" +
              localStorage.getItem("email") +
              "?secret=" +
              result.userResult.twoFactorAuthKey +
              "&issuer=Tomya";
            this.modalService.open(content, { centered: true });
            $(".google_auth_qr").show();
          }
        },
        function(reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.logout();
            this.data.alert("Could Not Connect To Server", "danger");
          }
        }
      );
  }

  sendGoogleAuthOtp() {
    if (this.google_auth_otp != undefined) {
      var inputObj = {};
      inputObj["email"] = localStorage.getItem("email");
      inputObj["otp"] = this.google_auth_otp;
      var jsonString = JSON.stringify(inputObj);
      // wip(1);
      this.http
        .post<any>(this.data.WEBSERVICE + "/user/CheckTwoFactor", jsonString, {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        })
        .subscribe(
          response => {
            // wip(0);
            var result = response;
            if (result.error.error_data != "0") {
              this.twoFactorStatus = "0";
              this.data.alert(result.error.error_msg, "danger");
            } else {
              // $('#googleAuthModal').modal('hide');
              this.twoFactorStatus = "1";
              this.updateUserAppSettings();
              this.data.alert(
                "Two factor verification updated. Login Again",
                "success"
              );
              this.data.logout();
            }
          },
          function(reason) {
            // wip(0);
            if (reason.data.error == "invalid_token") {
              this.data.logout();
            } else {
              this.data.logout();
              this.data.alert("Could Not Connect To Server", "danger");
            }
          }
        );
    } else {
      alert("Please Enter Otp");
    }
  }

  cancelSettings() {
    $("#googleAuthModal").modal("hide");
    if (this.twoFactorStatus == true) {
      this.twoFactorStatus = "0";
      this.lockOutgoingTransactionStatus = "0";
      this.updateUserAppSettings();
    } else {
      this.twoFactorStatus = "1";
      this.updateUserAppSettings();
    }
  }

  passModal(content) {
    this.modalService.open(content, { centered: true });
  }
}
