import {
  Injectable
} from '@angular/core';
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ViewChild, ElementRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoreDataService {

  /* Change here */
  CURRENCYICON: any = '₺';
  CURRENCYNAME: any = 'TRY';
  exchange: any = 'Tomya Pro';
  logo: any = './assets/img/bitrump-trimmed-logo_new.png';

  /* environment: any = 'dev';
   ADDR = "http://52.53.237.92:8080/main.html#!/";*/

  environment: any = 'live';
  ADDR = "http://52.53.237.92:8080/main.html#!/";
  /*Change here*/
  loader: boolean = false;
  reason: any;
  // count: any = 0;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  selectedCryptoCurrencySymbol: string;
  triggersBalance: any = 0;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  environment_settings_list: string;
  //new
  selectedBuyingCryptoCurrencyissuer: string;
  selectedSellingCryptoCurrencyissuer: string;
  ctpdata: string;
  lowprice: string;
  highprice: string;
  changescreencolor: boolean = false;
  rocdata;
  act;
  react;
  cur;
  ACTION;
  rocreact;
  negetive;
  volumndata;
  indicatorGroup1: any = ['ATR', 'BBAND', 'MACD', 'EMA', 'ROC', 'KDJ', 'MFI', 'CMF'];
  indicatorGroup2: any = ['ARN', 'CHO', 'HA', 'KCH', 'SSMA', 'SOSC', 'Willams %r', 'TRIX'];

  interval: any;
  time: any;
  url: boolean = false;
  resize: boolean = false;
  icon: any;
  WEBSERVICE: string;
  REPORTSERVISE: string;
  TRADESERVICE: string;
  CHARTSERVISE: string;
  //malini CURRENCYSERVISE
  CURRENCYSERVISE: string;
  //end
  settings: any;
  count: any;
  source2:any;

  constructor(private route: Router, private http: HttpClient) {

    var alertPl = `<div class="alertPlace"></div>`;
    $('html').append(alertPl).fadeIn();

    switch (this.environment) {
      case 'live':
        // this.WEBSERVICE = "https://api.bitrump.com/api";
        // this.TRADESERVICE = "https://api.bitrump.com/TradeAmount/rest/MyAmount";
        // this.CHARTSERVISE = "https://api.bitrump.com/TrendSpriceVolume/paybito/";//live
        // this.REPORTSERVISE = "https://api.bitrump.com/PaybitoReportModule/report/";
        this.WEBSERVICE = "https://api.tomya.com/api";
        this.TRADESERVICE = "https://api.tomya.com/TradeAmount/rest/MyAmount";
        this.CHARTSERVISE = "https://api.tomya.com/TrendSpriceVolume/paybito/";
        this.REPORTSERVISE = "https://api.tomya.com/PaybitoReportModule/report/";
        break;

    }

    this.getSettings();
  }

  getSettings() {
    this.http.get<any>('./assets/settings.json')
      .subscribe(data => {
        this.settings = data;
      })
  }

  // Login Session Management
  // idleLogout() {
  //   var us = localStorage.getItem('access_token');
  //   if (us != null) {
  //     //this.userIdle.onTimerStart();
  //     this.userIdle.startWatching();
  //     this.userIdle.resetTimer();
  //     // this.time = 10;
  //     // this.userIdle.onTimerStart().subscribe(count =>
  //     //   console.log(count));
  //     this.userIdle.onTimerStart().subscribe(count => {
  //       //  console.log(this.time);
  //       console.log('count', count);
  //       if (count == 1) {
  //         $('#logoutWarn').click();
  //         // this.time=10;
  //       }
  //       else if (count == 20) {
  //         $('#logoutWarncl').click();
  //         this.logout();
  //       }
  //        //this.time --;
  //       this.count = count;
  //     });
  //     this.userIdle.ping$.subscribe(ping => {

  //     }
  //     );
  //     this.userIdle.onTimeout().subscribe(() => {
  //     //  console.log('Time is up!');
  //       $('#logoutWarncl').click();
  //       this.logout();
  //     }
  //     );

  //   }

  // }
  // startWatching() {
  //   this.userIdle.startWatching();
  // }
  // stopWatching() {
  //   this.userIdle.stopWatching();
  // }
  userId;
  accessToken;
  refreshToken;
  expiresIn;
  x;
  regenarateToken(){
    this.userId = localStorage.getItem('user_id');
      let body = new URLSearchParams();
      body.set('username', this.userId);
      var password=localStorage.getItem('password');
      body.set('password',password);
      body.set('grant_type', 'password');
      let options = {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('authorization', 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg==')
      };
      this.http.post<any>(this.WEBSERVICE + '/oauth/token', body.toString(), options)
        .subscribe(dataAuth => {
          this.accessToken = dataAuth.access_token;
          this.refreshToken = dataAuth.refresh_token;
          this.expiresIn = dataAuth.expires_in;
          localStorage.setItem('access_token', this.accessToken);
          localStorage.setItem('refresh_token', this.refreshToken);
        },
      reason => {
        // wip(0);
        this.alert(reason, 'danger');
      });
  }
  Stop() {
    $('#logoutWarncl').click();
    $('.d-block').removeClass('show');
    $('.d-block').siblings().removeClass("modal-backdrop");
    this.userId = localStorage.getItem('user_id');
      let body = new URLSearchParams();
      body.set('username', this.userId);
      var password=localStorage.getItem('password');
      body.set('password',password);
      body.set('grant_type', 'password');
      let options = {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('authorization', 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg==')
      };
      this.http.post<any>(this.WEBSERVICE + '/oauth/token', body.toString(), options)
        .subscribe(dataAuth => {
          this.accessToken = dataAuth.access_token;
          this.refreshToken = dataAuth.refresh_token;
          this.expiresIn = dataAuth.expires_in;
          localStorage.setItem('access_token', this.accessToken);
          localStorage.setItem('refresh_token', this.refreshToken);
          clearInterval(this.x);
          this.Countdown();
        },
      reason => {
        // wip(0);
        this.alert(reason, 'danger');
      });
    // this.userIdle.stopTimer();
    // this.userIdle.stopWatching();
    // this.userIdle.resetTimer();
    // // this.restart();
    // var presentRefreshToken = localStorage.getItem('refresh_token');
    // //console.log('localstorage',presentRefreshToken)

    // var fd = new FormData();
    // fd.append('refresh_token', presentRefreshToken);
    // fd.append('grant_type', 'refresh_token');
    // //this.data.Stop();
    // this.http.post<any>(this.WEBSERVICE + '/oauth/token?grant_type=refresh_token&refresh_token=' + presentRefreshToken, '', {
    //   headers: {
    //     // 'Content-Type': 'application/x-www-form-urlencoded',
    //     'authorization': 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg=='
    //   }
    // })
    //   .subscribe(response => {
    //     var result = response;
    //     localStorage.setItem('access_token', result.access_token);
    //     localStorage.setItem('refresh_token', result.refresh_token);
    //    var userObj = {};
    //     userObj['user_id'] = localStorage.getItem('user_id');
    //     var userJsonString = JSON.stringify(userObj);
    //     // wip(1);
    //     var accessToken = localStorage.getItem('access_token');
    //     // this.http.post<any>(this.WEBSERVICE + '/user/GetUserDetails', userJsonString, {
    //     //   headers: {
    //     //     'Content-Type': 'application/json',
    //     //     'authorization': 'BEARER ' + accessToken,
    //     //   }
    //     // })
    //     //   .subscribe(response => {
    //     //     // wip(0);
    //     //     var result = response;
    //     //     if (result.error.error_data != '0') {
    //     //       this.alert(result.error.error_msg, 'danger');
    //     //     } else {
    //     //       localStorage.setItem('user_name', result.userResult.full_name);
    //     //       localStorage.setItem('user_id', result.userResult.user_id);
    //     //       localStorage.setItem('phone', result.userResult.phone);
    //     //       localStorage.setItem('email', result.userResult.email);
    //     //       localStorage.setItem('address', result.userResult.address);
    //     //       localStorage.setItem('profile_pic', result.userResult.profile_pic);
    //     //       //  clearInterval(this.data.interval);
    //     //       //console.log('this.data.interval++++++++++++',this.interval);
    //     //       //  this.data.time = 10;

    //     //       this.idleLogout();
    //     //       //this.data.restart();
    //     //       //  location.reload();
    //     //       // window.location='main.html';
    //     //       //    $('#sessionExpireModal').modal('hide');
    //     //       //    clearInterval($scope.expireTimerInterval);
    //     //     }
    //     //   });
    //     this.idleLogout();
    //   }, reason => {
    //     //   wip(0);
    //     //this.alert(reason.error.message,'danger');
    //     //this.data.idleLogout();
    //     location.reload();
    //     this.logout();
    //   });
  }
  // restart() {
  //   this.userIdle.resetTimer();
  // }
  // onTimeout() {
  //   this.userIdle.onTimeout();
  // }
  Countdown(){
    var now = new Date().getTime();
    var starttime = Math.round(now/1000);
   var  countDowntime=starttime + 3600;
    var x = setInterval(function() {
     var now = new Date().getTime();
     var starttime = Math.round(now/1000);
    this.distance=countDowntime - starttime;

    if(this.distance == 20){
     $('#logoutWarn').click();
    }
    else if(this.distance ==0){
      clearInterval(this.x);
     localStorage.clear();
      this.route.navigateByUrl('/login');
    }
    },1000)
   }

  readable_timestamp(t) {
    // Split timestamp into [ Y, M, D, h, m, s ]
    var t: any = t.split(/[- :]/);

    // Apply each element to the Date function
    var d = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));

    //handle parsing GMT as UTC
    d.setMinutes(d.getMinutes() - 330);

    t = d.toTimeString();
    t = t.split(" ");
    return d.toDateString() + " " + t[0];
  }

  logout() {
    localStorage.clear();
    //caches.delete;
    //location.reload();
    this.route.navigateByUrl('/login');
  }

  alert(msg, type, time = 3000) {
    this.reason = msg;
    this.icon = 'puff';

    if (msg == 'Loading...') {
      this.loader = true;
      setTimeout(() => {
        this.loader = false;
      }, 10000);
    } else {
      $('.alert:first').fadeOut();
      var htx = `<div class="alert alert-` + type + ` my-2" role="alert">` + msg + `</div>`;
      $('.alertPlace').append(htx).fadeIn();
      setTimeout(() => {
        $('.alert:last').remove().fadeOut();
      }, time);
    }
  }

}
