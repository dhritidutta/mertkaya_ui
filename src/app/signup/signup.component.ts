import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { Router, ActivatedRoute } from "@angular/router";
import * as $ from "jquery";
import { Observable } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  contries: any;
  captcha: string;
  loadmail: any;
  dataemail: any;
  dateofbirth;
  model;
  file1: any;
  file2: any;
  search: (text$: Observable<string>) => Observable<any>;

  title = "identity";
  countries;
  man;
  reg: any = {};
  reg2: any = {};
  errormessage;
  checkphone: any;
  constructor(
    private http: HttpClient,
    public data: CoreDataService,
    private route: Router,
    private activeRoute: ActivatedRoute,
    public translate: TranslateService
  ) {
    translate.addLangs(["Turkish", "English"]);
    var getlang = localStorage.getItem("selectedlang");
    if (getlang == undefined || getlang == null) {
      translate.setDefaultLang("Turkish");
    } else {
      translate.setDefaultLang(getlang);
    }
    const browserLang = translate.getBrowserLang();
  }

  signupObj: any = {
    country: ""
  };
  signupObj1: any = {};
  ngOnInit() {
    this.signupObj.country = "Turkey";
    this.getLoc();
    this.checkurlemail(this.loadmail);
  }

  checkurlemail(loadmail) {
    this.activeRoute.queryParams.subscribe(params => {
      this.loadmail = params["email"];
    });
    if (this.loadmail != undefined && this.loadmail != "") {
      this.signupObj.email = this.loadmail;
    } else {
      this.signupObj.email = "";
    }
  }

  getLoc() {
    this.http.get<any>("./assets/data/country.json").subscribe(data => {
      this.contries = data;
    });
  }

  resolved(captchaResponse: string) {
    this.captcha = captchaResponse;
  }
  IDMSERVICE: any =
    "https://sandbox.identitymind.com/im/account/consumer?graphScoreResponse=false";
  WEBSERVICE: any = "https://api.digitalterminal.net/webservice";
  merchantid: any = "cornerstore";
  merchantpassword: any = "8215631cc6e8b0ef43a2aba1aa61a489ebe547f2";
  accessKey = btoa("this.merchantid:this.merchantpassword");

  signupData(isValid) {
    if (isValid) {
      var jsonString = JSON.stringify(this.signupObj);
      if (this.captcha != "") {
        this.http
          .post<any>(
            this.data.WEBSERVICE + "/user/AddUserDetails",
            jsonString,
            { headers: { "Content-Type": "application/json" } }
          )
          .subscribe(
            response => {
              // wip(0);
              var result = response;
              if (result.error.error_data != "0") {
                this.data.alert(result.error.error_msg, "danger");
              } else {
                var userId = result.userResult.userId;
                localStorage.setItem("signup_user_id", userId);
                this.data.alert(
                  "Registration Done,  Kindly check your email for verification token",
                  "success"
                );
                this.route.navigateByUrl("/otp");
              }
            },
            reason => {
              // wip(0);
              this.data.alert("Internal Server Error", "danger");
            }
          );
      } else {
        this.data.alert("Captcha Unverified", "warning");
      }
    } else {
      this.data.alert("Please fill up all the fields properly", "warning");
    }
  }

  getSize(content) {
    var sz = $("#" + content)[0].files[0];
    if (sz.type == "image/jpeg") {
      if (sz.size > 5000000) {
        this.data.alert("File size should be less than 2MB", "warning");
        $("#" + content).val("");
      }
    } else {
      this.data.alert(
        "File should be in JPG or JPEG. " +
          sz.type.split("/")[1].toUpperCase() +
          " is not allowed",
        "warning"
      );
      $("#" + content).val("");
    }
  }

  confirmPassword(e) {
    if (
      this.signupObj.password != "" &&
      this.signupObj.password != undefined &&
      this.signupObj.repassword1 != "" &&
        this.signupObj.repassword1 != undefined
    ) {
      if (this.signupObj.password == this.signupObj.repassword1) {
        $(".confirm_password_text").html("Password Matched");
        $(".confirm_password_text").css("color", "lightgreen");
        $("#submit_btn").removeAttr("disabled");
      } else {
        $(".confirm_password_text").html("Password  Mismatched");
        $(".confirm_password_text").css("color", "red");
        $("#submit_btn").attr("disabled", "disabled");
      }
    } else {
    }
  }

  checkEmail(loadmail) {
    if (this.signupObj.email != undefined && this.signupObj.email != "") {
      if (this.signupObj.email != "" && this.signupObj.email != undefined) {
        if (this.is_mail(this.signupObj.email) == true) {
          // wip(1);
          var emailValue = this.signupObj.email;
          var emailObj = {};
          emailObj["email"] = emailValue;
          var jsonString = JSON.stringify(emailObj);
          this.http
            .post<any>(this.data.WEBSERVICE + "/user/CheckEmail", jsonString, {
              headers: {
                "Content-Type": "application/json"
              }
            })
            .subscribe(
              response => {
                // wip(0);
                var result = response;
                if (result.error.error_data != "0") {
                  this.data.alert(result.error.error_msg, "danger");
                } else {
                  if (result.userResult.check_email_phone_flag == 1) {
                    this.data.alert(
                      "Email already registered , please try with another email address",
                      "warning"
                    );
                    this.signupObj.email = "";
                  } else {
                  }
                }
              },
              reason => {
                // wip(0);
                this.data.alert("Internal Server Error", "danger");
              }
            );
        } else {
        }
      }
    } else {
      this.data.alert("Please Provide Email Id", "warning");
    }
  }

  checkPhone() {
    if (this.signupObj.phone != undefined && this.signupObj.phone != "") {
      var phoneValue = this.signupObj.phone;
      var phoneObj = {};
      phoneObj["phone"] = phoneValue;
      var jsonString = JSON.stringify(phoneObj);
      this.http
        .post<any>(this.data.WEBSERVICE + "/user/CheckPhone", jsonString, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .subscribe(
          response => {
            // wip(0);
            var result = response;
            if (result.error.error_data != "0") {
              this.data.alert(result.error.error_msg, "danger");
            } else {
              if (result.userResult.check_email_phone_flag == 1) {
                this.data.alert(
                  "Phone No. already registered , please try with another phone no.",
                  "warning"
                );
                this.signupObj.phone.value = "";
              } else {
              }
            }
          },
          reason => {
            // wip(0);
            this.data.alert("Internal Server Error", "danger");
          }
        );
    } else {
      this.data.alert("Please Provide Phone No.", "warning");
    }
  }

  checkPassword() {
    var password = this.signupObj.password;
    if (password != "" && password != undefined && password.length >= 8) {
      var passwordStatus = this.checkAlphaNumeric(password);
      if (passwordStatus == false) {
        this.data.alert(
          "Password should have atleast one upper case letter, one lowercase letter , one special case and one number",
          "warning"
        );
      } else {
      }
    } else {
    }
  }

  checkAlphaNumeric(string) {
    if (string.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)) {
      return true;
    } else {
      return false;
    }
  }

  is_mail(email) {
    var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return regex.test(email);
  }

  showHide() {
    $(".showHide-password").each(function() {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
  }

  keyTab(event, nextInput) {
    var input = event.target;
    var length = input.value.length;
    var maxlength = input.attributes.maxlength.value;
    if (length >= maxlength) {
      nextInput.focus();
    }
  }

  valkeyUp() {
    var part1 = $("#signupInputPhone1").val();
    var part2 = $("#signupInputPhone2").val();
    var part3 = $("#signupInputPhone3").val();
    var part4 = $("#signupInputPhone4").val();
    var phone_num = part1 + part2 + part3 + part4;
    this.checkphone = phone_num;
    $("#phone_num").val(phone_num);
  }
}
