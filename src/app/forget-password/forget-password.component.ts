import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  email: any;
  password: any;
  secure_token: any;
  repassword: any;
  forgetpwdObj:any={};
  getlang:any;

  constructor(private http:HttpClient, public data:CoreDataService, private route:Router, public translate:TranslateService) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
    translate.setDefaultLang('Turkish');
    const browserLang = translate.getBrowserLang();

    translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
  }
  else {

    translate.setDefaultLang(this.getlang);
    const browserLang = translate.getBrowserLang();

    translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
  }
  }

  ngOnInit() {

  }

  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  confirmPassword(e) {
    if (
      (this.forgetpwdObj.password != '' && this.forgetpwdObj.password != undefined) &&
      (this.forgetpwdObj.repassword1 != '' && this.forgetpwdObj.repassword1 != undefined)
    ) {
      if (this.forgetpwdObj.password == this.forgetpwdObj.repassword1) {
        $('.confirm_password_text').html('Password Matched');
        $('.confirm_password_text').css('color', 'lightgreen');
        $('#submit_btn').removeAttr('disabled');
      } else {
        $('.confirm_password_text').html('Password  Mismatched');
        $('.confirm_password_text').css('color', 'red');
        $('#submit_btn').attr('disabled', 'disabled');
      }
    } else {
    }
  }

  checkPassword() {
    var password = this.forgetpwdObj.password;
    if (password != '' && password != undefined && password.length >= 8) {
      var passwordStatus = this.checkAlphaNumeric(password);
      if (passwordStatus == false) {
        this.data.alert('Password should have atleast one upper case letter, one lowercase letter , one special case and one number', 'warning');
      } else {
      }
    }
    else {
      this.data.alert('Password should be minimum 8 Charecter', 'warning');
    }
  }

  checkAlphaNumeric(string) {
    if (string.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)) {
      return true;
    } else {
      return false;
    }
  }

  forgotPassword(isValid){
     if(isValid){
       var jsonString=JSON.stringify(this.forgetpwdObj);
       this.http.post<any>(this.data.WEBSERVICE+'/user/ResetPassword',jsonString,{headers: {
           'Content-Type': 'application/json'
         }})
      .subscribe(response=>{
        //  wip(0);
         var result=response;
         if(result.error.error_data!='0'){
           this.data.alert(result.error.error_msg,'danger');
         }else{
         this.data.alert('Password successfully reset','success');
         this.route.navigateByUrl('/login');
         }

       },function(reason){
        //  wip(0);
         this.data.alert('Internal Server Error','danger')
       });
     }else{
       this.data.alert('Please provide valid email','warning');
     }
   }

   showHide(){
    $(".showHide-password").each(function() {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
  }
}