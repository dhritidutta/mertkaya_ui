import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { BodyService } from "../body.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-support",
  templateUrl: "./support.component.html",
  styleUrls: ["./support.component.css"]
})
export class SupportComponent implements OnInit {
  getlang: any;
  constructor(
    private http: HttpClient,
    public translate: TranslateService,
    private data: CoreDataService,
    private main: BodyService
  ) {
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }

  ngOnInit() {
    this.main.getDashBoardInfo();
  }
  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }

  message;
  subject;

  supportMail() {
    if (this.message != undefined && this.subject != undefined) {
      var supportObj = {};
      supportObj["user_id"] = localStorage.getItem("user_id");
      supportObj["email"] = localStorage.getItem("email");
      supportObj["subject"] = this.subject;
      supportObj["message"] = this.message;
      supportObj["name"] = localStorage.getItem("user_name");
      var jsonString = JSON.stringify(supportObj);
      // wip(1);
      this.http
        .post<any>(this.data.WEBSERVICE + "/user/SendMailToUser", jsonString, {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        })
        .subscribe(
          response => {
            // wip(0);
            var result = response;
            if (result.error.error_data != "0") {
              this.data.alert(result.error.error_msg, "danger");
            } else {
              this.data.alert(
                "Mail Sent , We Will Contact You Shortly",
                "success"
              );
              this.subject = "";
              this.message = "";
            }
          },
          function(reason) {
            // wip(0);
            if (reason.data.error == "invalid_token") {
              this.data.logout();
            } else {
              this.data.alert("Could Not Connect To Server", "danger");
            }
          }
        );
    } else {
      this.data.alert("Please provide proper details", "warning");
    }
  }
}
