import { Component, OnInit, DoCheck } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { OrderBookComponent } from "../order-book/order-book.component";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { TradesComponent } from "../trades/trades.component";
import * as $ from "jquery";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-chart",
  templateUrl: "./chart.component.html",
  styleUrls: ["./chart.component.css"]
})
export class ChartComponent implements OnInit {
  filePath: string;
  fileDir: string;
  url: any = null;
  chartDataInit: any;
  chartData: any;
  selected: any;
  indicatorStatus: any = 0;
  indicatorName: any = "";
  apiUrl: string;
  apiData: any;
  loader: boolean;
  errorText: string;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  selectedCryptoCurrencySymbol: string;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  currencyListEur: any = 0;
  currencyListBtc: any = 0;
  currencyListEth: any = 0;
  currencyListUsd: any = 0;
  buyPriceText: any;
  chartD: any = "d";
  dateD: any = "1m";
  indicatorGroup1: any;
  indicatorGroup2: any;
  highValue;
  lowValue;
  cur: any;
  tools: boolean;
  chartlist: any;
  ctpdata: any = 0;
  lowprice: any = 0;
  highprice: any = 0;
  rocdata: number = 0;
  action: any;
  volumndata: any = 0;
  act: any;
  react: boolean;
  buyingAssetIssuer: string;
  sellingAssetIssuer: string;
  //rocact:any;
  rocreact: boolean;
  droc: any;
  negetive: boolean;
  Tonight: boolean;
  Tomorning: boolean;
  filterCurrency: any;
  logic: boolean;
  flag: boolean;
  Cname: any;
  testval = [];
  currency_code: any;
  base_currency: any;
  assetpairbuy: string;
  assetpairsell: string;
  responseBuySell: any;
  header: any;
  assets: any;
  currencyId: any;
  getlang: any;
  constructor(
    private http: HttpClient,
    public data: CoreDataService,
    private orderBook: OrderBookComponent,
    private stoploss: StopLossComponent,
    private trade: TradesComponent,
    public dash: DashboardComponent,
    private translate:TranslateService
  ) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  ngOnInit() {

    this.currency_code = 'BTC';
    this.base_currency = 'TRY';
    localStorage.setItem("buying_crypto_asset", 'BTC');
    localStorage.setItem("selling_crypto_asset", 'TRY');
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
    this.data.cur =this.data.selectedBuyingAssetText === "TRY"
    ? "$"
    : this.data.selectedSellingAssetText + " ";
    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName = this.base_currency + this.currency_code;
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText = this.currency_code;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText = this.base_currency;
    this.selectedCryptoCurrencySymbol = this.data.selectedCryptoCurrencySymbol =
      "./assets/img/currency/" +
      this.data.selectedSellingAssetText.toUpperCase() +
      ".svg";
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer = "GCJK5PBMMAGO3SIH3BVDKNSDNTJHETXFQEOSYUILXFWSDNCA3LTCSI4U";

    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer = "GBXI2ZBD44SZQ7OVCJXEDOTPEMIBWNJ5VQFCFROW5G3BSMWPLLWSSXNS";
    this.http.get<any>(this.data.WEBSERVICE + '/userTrade/GetAssetIssuer')
      .subscribe(responseBuySell => {
        $('#' + this.dateD).addClass('active');

        $('.reset').click();
        this.tools = true;
        $('#dropHolder').css('overflow', 'scroll');
        $(window).resize(function () {
          var wd = $('#chartHolder').width();
          $('#dropHolder').width(wd);
          $('#dropHolder').css('overflow', 'scroll');
        });
        this.http.get<any>(this.data.CHARTSERVISE + '/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency)
          .subscribe(value => {
            if (value != '') {
              if(this.currency_code == 'BTC' || this.base_currency == 'TRY' || this.base_currency == 'USDT'){
                this.chartlist = value[0];
                this.ctpdata = this.data.ctpdata = this.chartlist.CTP.toFixed(2);
                this.lowprice = this.data.lowprice = this.chartlist.LOW_PRICE.toFixed(2);
                this.highprice = this.data.highprice = this.chartlist.HIGH_PRICE.toFixed(2);
                this.act = this.chartlist.ACTION;
                // this.rocdata = this.chartlist.ROC.toFixed(2);
                this.rocdata =this.data.rocdata = this.chartlist.ROC.toFixed(2);
                if (this.rocdata > 0) {
                  this.rocreact = true;

                }
                else {
                  this.rocreact = false;
                }
                if (this.rocdata < 0) {
                  this.negetive = true;
                }
                else {
                  this.negetive = false;
                }

                // this.volumndata = this.chartlist.VOLUME.toFixed(2);
                this.volumndata = this.data.volumndata = this.chartlist.VOLUME.toFixed(2);
                if (this.rocdata >= 0) { this.rocreact = true }

                if (this.act == 'sell') {

                  this.react = true;
                }
                else {
                  this.react = false;
                }
              }
              else{
                this.chartlist = value[0];
                this.ctpdata = this.data.ctpdata = this.chartlist.CTP;
                this.lowprice = this.data.lowprice = this.chartlist.LOW_PRICE;
                this.highprice = this.data.highprice = this.chartlist.HIGH_PRICE;
                this.act = this.chartlist.ACTION;
                // this.rocdata = this.chartlist.ROC.toFixed(2);
                this.rocdata =this.data.rocdata = this.chartlist.ROC;
                if (this.rocdata > 0) {
                  this.rocreact = true;

                }
                else {
                  this.rocreact = false;
                }
                if (this.rocdata < 0) {
                  this.negetive = true;
                }
                else {
                  this.negetive = false;
                }

                // this.volumndata = this.chartlist.VOLUME.toFixed(2);
                this.volumndata = this.data.volumndata = this.chartlist.VOLUME;
                if (this.rocdata >= 0) { this.rocreact = true }

                if (this.act == 'sell') {

                  this.react = true;
                }
                else {
                  this.react = false;
                }
              }
              }
               else {
              this.ctpdata = 0;
              this.lowprice = 0;
              this.highprice = 0;
              this.rocdata = 0;
              this.volumndata = 0;

            }
          })

      })
  }

  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  changemode() {
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".btn")
        .removeClass("bg-black")
        .css("background-color", "#dedede");
      $(".btn").css("border-color", "#b3b4b4");
      $(".btn").css("color", "#000");
      $(".charts-tab.active").css("background-color", "#fefefe");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".btn")
        .removeClass("bg-black")
        .css("background-color", "rgb(44, 65, 66)");
      $(".btn").css("border-color", "transparent");
      $(".btn").css("color", "#fff");
      $(".charts-tab.active").css("background-color", "#242e3e");
    }
  }

  ngDoCheck() {
    this.changemode();
  }

  getNewCurrency() {

    this.http.get<any>(this.data.WEBSERVICE + '/userTrade/GetCurrencyDetails')
      .subscribe(responseCurrency => {
        this.filterCurrency = responseCurrency;
        var mainArr = [];
        this.header = this.filterCurrency.Header;
        this.assets = this.filterCurrency.Values;

      }
      )
  }
  //clear indicators
  destroy(): void {
    this.selected = null;
  }

}