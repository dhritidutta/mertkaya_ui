import { Component, OnInit, DoCheck } from "@angular/core";
import { TradesComponent } from "../trades/trades.component";
import * as $ from "jquery";
import { CoreDataService } from "../core-data.service";
import { Router } from "@angular/router";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { TranslateService } from "@ngx-translate/core";
export interface Todo {
  id: number | string;
  createdAt: number;
  value: string;
}

@Component({
  selector: "app-order-book",
  templateUrl: "./order-book.component.html",
  styleUrls: ["./order-book.component.css"]
})

export class OrderBookComponent implements OnInit {
  buyingAssetIssuer: string;
  sellingAssetIssuer: string;
  bidBody: string;
  askBody: string;
  orderBookUrl: string;
  count: any;
  lowprice: any;
  highprice;
  any = 0;
  act:any;
  rocdata:any;
  chartlist: any = 0;
  getlang:any;
  ctpdata: any = 0;
  marketTradeBodyHtml: any;
  private _todos = new BehaviorSubject<Todo[]>([]);
  private dataStore: { todos: Todo[] } = { todos: [] };
  readonly todos = this._todos.asObservable();
  public biddata:any;
  public askdata:any;
  constructor(
    public data: CoreDataService,
    private route: Router,
    public dash: DashboardComponent,
    private http: HttpClient,
    private trade:TradesComponent,
    private stoploss:StopLossComponent,
    public translate:TranslateService

  ) {
    switch (this.data.environment) {
      case "live":
        this.orderBookUrl = "https://mainnet.tomya.com/"; //live
        break;

      case "dev":
        this.orderBookUrl =
          ""; //dev
        break;

      case "uat":
        this.orderBookUrl = ""
        break;

      case "demo":
        this.orderBookUrl = "https://mainnet.tomya.com/"; //demo
        break;
    }
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  urlBid: any;

  urlAsk: any;
  rocreact:any;

  selectedBuyingCryptoCurrencyName: string;

  selectedSellingCryptoCurrencyName: string;

  sellingAssetType: string;

  buyingAssetType: string;

  source11: any;

  source12: any;
  volumndata:any;
  react:any;
  negetive:any;
 // source2: any;
  source:any;
  token: any;
  marketTradeRecords: any;
  itemcount: any;

  ngOnInit() {
    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName;
    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName;
    this.currency_code = 'BTC';
    this.base_currency = 'TRY';
    // this.http
    //   .get<any>(
    //     this.data.CHARTSERVISE +
    //     "trendsTradeGraphFor24Hours/" +
    //     this.currency_code +
    //     "/" +
    //     this.base_currency
    //   )

    //   .subscribe(value => {
    //     if (value != "") {
    //       if(this.currency_code == 'BTC'){

    //       }
    //       this.chartlist = value[0];
    //       var randomNoForFlashArr = [];
    //       var arraylength = 2;
    //       this.ctpdata = this.chartlist.CTP;
    //       this.lowprice = this.chartlist.LOW_PRICE;
    //       this.highprice = this.chartlist.HIGH_PRICE;
    //       var randomNo = this.randomNoForOrderBook(0, arraylength);
    //       randomNoForFlashArr.push(randomNo);
    //       for (var i = 0; i < arraylength; i++) {
    //         if (!$.inArray(i, randomNoForFlashArr)) {
    //           if (i == 0) {
    //             document.getElementById("pricered").style.display =
    //               "inline-block";
    //             document.getElementById("pricegreen").style.display = "none";
    //           } else {
    //             document.getElementById("pricered").style.display = "none";
    //             document.getElementById("pricegreen").style.display =
    //               "inline-block";
    //           }
    //         }
    //       }
    //     }
    //   });
      this.serverSentEventForOrderbookAsk();
      this.getMarketTradeBuy();
  }

  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  changemode() {
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".sp-highlow").css("background-color", "#d3dddd");
      $(".sp-highlow").css("color", "Black");
      $(".border-col").css("border-color", "#d3dddd");
      $("th").css({ "background-color": "#d3dddd", color: "#273338" });
      $(".text-left").css("color", "black");
      $(".text-right").css("color", "black");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".sp-highlow").css("background-color", "#273338");
      $(".sp-highlow").css("color", "yellow");
      $(".border-col").css("border-color", "#273338");
      $("th").css({ "background-color": "#273338", color: "#d3dddd" });
      $(".text-left")
        .css("color", "")
        .css("color", "rgb(153, 152, 152)");
      $(".text-right").css("color", "rgb(153, 152, 152)");
    }
  }

  ngDoCheck() {
    this.changemode();
    var randomNoForFlashArr = [];
    var arraylength = 2;
    var valcurrency = this.data.ctpdata;
    if (valcurrency == undefined) {
      this.ctpdata = this.chartlist.CTP;
      this.lowprice = this.chartlist.LOW_PRICE;
      this.highprice = this.chartlist.HIGH_PRICE;
    } else {
      this.ctpdata = this.data.ctpdata;
      this.lowprice = this.data.lowprice;
      this.highprice = this.data.highprice
    }
    var randomNo = this.randomNoForOrderBook(0, arraylength);
    randomNoForFlashArr.push(randomNo);
    for (var i = 0; i < arraylength; i++) {
      if (!$.inArray(i, randomNoForFlashArr)) {
        if (i == 0) {
          document.getElementById("pricered").style.display = "inline-block";

          document.getElementById("pricegreen").style.display = "none";
        } else {
          document.getElementById("pricered").style.display = "none";

          document.getElementById("pricegreen").style.display = "inline-block";
        }
      }
    }

  }

  filterCurrency: any;
  header: any;
  assets: any;

  getNewCurrency() {
    this.http.get<any>('https://api.tomya.com/CacheService/api/getData?Name=Assets')
      .subscribe(responseCurrency => {
        this.filterCurrency = JSON.parse(responseCurrency.value);
        var mainArr = [];
        this.header = this.filterCurrency.Header;
        this.assets = this.filterCurrency.Values;

      }
      )
  }

  currency_code: any;
  base_currency: any;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  assetpairbuy: string;
  assetpairsell: string;
  responseBuySell: any;
  cur:any

  buySellCode(item) {
    this.currency_code = item.currencyCode;
    this.base_currency = item.baseCurrency;
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
    this.data.selectedBuyingCryptoCurrencyName =this.base_currency+ this.currency_code;
    this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingAssetText = item.currencyCode;
    this.selectedSellingAssetText = item.baseCurrency;
    localStorage.setItem("buying_crypto_asset", this.currency_code.toLocaleLowerCase());
    localStorage.setItem("selling_crypto_asset", this.base_currency.toLocaleLowerCase());
    this.data.cur = this.selectedSellingAssetText;
    this.assetpairsell = item.currencyCode + item.baseCurrency;
    this.assetpairbuy = item.baseCurrency + item.currencyCode;
    this.data.selectedSellingCryptoCurrencyissuer = "";
    this.data.selectedBuyingCryptoCurrencyissuer = "";
    this.http.get<any>('https://api.tomya.com/CacheService/api/getData?Name=AssetIssuer')
      .subscribe(responseBuySell => {
       this.responseBuySell = JSON.parse(responseBuySell.value);
        var x = this.responseBuySell.length;
        for (var i = 0; i < x ; i++) {
          if (this.assetpairsell == this.responseBuySell[i].assetPair) {
            this.data.selectedSellingCryptoCurrencyissuer = this.responseBuySell[i].issuer;

          }
          else if (this.assetpairbuy == this.responseBuySell[i].assetPair) {
            this.data.selectedBuyingCryptoCurrencyissuer = this.responseBuySell[i].issuer;
          }
          else if (this.data.selectedSellingCryptoCurrencyissuer != "" && this.data.selectedBuyingCryptoCurrencyissuer != "") {
            break;
          }
        }

        this.stoploss.update();
        this.stoploss.market = false;
        this.serverSentEventForOrderbookAsk();
        this.getMarketTradeBuy();
        this.trade.reload();
        this.stoploss.onlyBuyAmount = this.stoploss.onlyBuyPrice = this.stoploss.onlyBuyTotalPrice = 0;
        this.stoploss.onlySellAmount = this.stoploss.onlySellPrice = this.stoploss.onlySellTotalPrice = 0;
        $('.reset').click();
        this.trade.myTradeDisplay(0);
        $('#trade').click();
        $('#dropHolder').css('overflow', 'scroll');
        $(window).resize(function () {
          var wd = $('#chartHolder').width();
          $('#dropHolder').width(wd);
          $('#dropHolder').css('overflow', 'scroll');
        });

        this.http.get<any>(this.data.CHARTSERVISE + '/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency)
          .subscribe(value => {
            if (value != '') {
              if(this.currency_code == 'BTC' || this.base_currency == 'TRY' || this.base_currency == 'USDT'){
                this.chartlist = value[0];
                this.ctpdata = this.data.ctpdata = this.chartlist.CTP.toFixed(2);
                this.lowprice = this.data.lowprice = this.chartlist.LOW_PRICE.toFixed(2);
                this.highprice = this.data.highprice = this.chartlist.HIGH_PRICE.toFixed(2);
                this.act = this.data.ACTION = this.chartlist.ACTION;
                this.data.rocdata = this.chartlist.ROC.toFixed(2);
                if (this.data.rocdata > 0) {
                  this.rocreact = true;
                }
                else {
                  this.data.rocreact = false;
                }
                if (this.data.rocdata < 0) {
                  this.data.negetive = true;
                }
                else {
                  this.data.negetive = false;
                }
                this.data.volumndata = this.chartlist.VOLUME.toFixed(2);
                if (this.data.rocdata >= 0) { this.rocreact = true }
                if (this.data.act == 'sell') {
                  this.data.react = true;
                }
                else {
                  this.data.react = false;
                }
              }
              else{
                this.chartlist = value[0];
                this.ctpdata = this.data.ctpdata = this.chartlist.CTP;
                this.lowprice = this.data.lowprice = this.chartlist.LOW_PRICE;
                this.highprice = this.data.highprice = this.chartlist.HIGH_PRICE;
                this.act = this.data.ACTION = this.chartlist.ACTION;
                this.data.rocdata = this.chartlist.ROC.toFixed(2);
                if (this.data.rocdata > 0) {
                  this.rocreact = true;
                }
                else {
                  this.data.rocreact = false;
                }
                if (this.data.rocdata < 0) {
                  this.data.negetive = true;
                }
                else {
                  this.data.negetive = false;
                }
                this.data.volumndata = this.chartlist.VOLUME.toFixed(2);
                if (this.data.rocdata >= 0) { this.rocreact = true }
                if (this.data.act == 'sell') {
                  this.data.react = true;
                }
                else {
                  this.data.react = false;
                }
              }
              }
               else {
              this.ctpdata = 0;
              this.lowprice = 0;
              this.highprice = 0;
              this.rocdata = 0;
              this.volumndata = 0;
            }
          })

      })
  }

  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);
    var maxVal1: number = parseInt(maxVal);
    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }

  getMarketTradeBuy() {
    $('#marketTradeBody').html('<tr><td colspan="3" class="text-center"><img src="./assets/svg-loaders/three-dots.svg" alt="" width="50"></td></tr>');
    var result: any;
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;//'GBDWSH2SAZM3U5FR6LPO4E2OQZNVZXUAZVH5TWEJD64MGYJJ7NWF4PBX';
    var url ='https://api.tomya.com/StreamApi/rest/TradeHistory?baseAssetCode=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase() + '&baseAssetIssuer=' + this.sellingAssetIssuer +'&counterAssetCode=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&counterAssetIssuer=' + this.buyingAssetIssuer + '&order=desc' + '&limit=' + 50;
    if (this.source != undefined ) {
      this.source.close();
    }
    this.source= new EventSource(url);
    this.source.addEventListener('message', event => {
      result = JSON.parse(event.data);
      this.marketTradeBodyHtml = '';
      if(result.response !=null){
      var response = JSON.parse(result.response);
      this.marketTradeRecords=response._embedded.records;
      this.itemcount = this.marketTradeRecords.length;
      var arraylength = this.marketTradeRecords.length;
          if (arraylength != null) {
          this.marketTradeBodyHtml = '';
                  var randomNoForFlashArrM = [];
          var randomNo = this.randomNoForOrderBook(0, 10);
          randomNoForFlashArrM.push(randomNo);

          randomNoForFlashArrM.push(randomNo);

          for (var i = 0; i < this.marketTradeRecords.length; i++) {
           // var className = "text-green";
            if (!$.inArray(i, randomNoForFlashArrM)) {
              //alert(randomNo);
              if (randomNo % 2 == 0) {
             var pickclass='.'+i+'newclassmarket';
              // alert('////'+randomNo+pickclass);
               $(pickclass).addClass('bg-flash-red');

              } else if (randomNo % 2 != 0) {
                var pickclass='.'+i+'newclassmarket';
             //   alert('////////'+randomNo+pickclass);
               // var className = 'text-red';
               $(pickclass).addClass("bg-flash-red");
              }
            }
            // var timeStampString = this.marketTradeRecords[i].ledger_close_time;
            // var timeStampStringArr = timeStampString.split('T');
            // var date = timeStampStringArr[0];
            // var time = (timeStampStringArr[1]).slice(0, -1);

            // var amount: any = parseFloat(this.marketTradeRecords[i].base_amount);
            // var price: any = parseFloat(this.marketTradeRecords[i].counter_amount) / parseFloat(this.marketTradeRecords[i].base_amount);
            // this.marketTradeBodyHtml += '<tr class="text-ash">';
            // this.marketTradeBodyHtml += '<td class="text-left">' + time + '</td>';
            // if (this.selectedSellingAssetText == 'eur') { //changed by sanu
            //   this.marketTradeBodyHtml += '<td class="text-right">' + parseFloat(amount) + '</td>';
            //   this.marketTradeBodyHtml += '<td class="' + className + ' right">' + (parseFloat(price)).toFixed(4) + '</td>';
            // } else {
            //   this.marketTradeBodyHtml += '<td class="text-right">' + (parseFloat(amount)).toFixed(8) + '</td>';
            //   this.marketTradeBodyHtml += '<td class="' + className + ' right">' + (parseFloat(price)).toFixed(8) + '</td>';
            // }
            // this.marketTradeBodyHtml += '</tr>';
          }
        }
        else{
          // if(this.translate.currentLang =='English'){
          //   this.marketTradeBodyHtml += '<tr><td colspan="3" class="text-center text-danger">No Data</td></tr>';
          // }
          // else{
          //   this.marketTradeBodyHtml += '<tr><td colspan="3" class="text-center text-danger">No Data</td></tr>';
          // }

        }
      }
      else{
      //  this.marketTradeBodyHtml += '<tr><td colspan="3" class="text-center text-danger">No Data</td></tr>';
      }
      }, error => {
      //  $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
      })

  }

  serverSentEventForOrderbookAsk() {
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    this.token = localStorage.getItem('access_token');
    var url = "https://api.tomya.com/StreamApi/rest/OrderBook?sellingAssetCode=" + this.data.selectedSellingCryptoCurrencyName + "&sellingAssetIssuer=" + this.sellingAssetIssuer + "&buyingAssetCode=" + this.data.selectedBuyingCryptoCurrencyName+ "&buyingAssetIssuer=" + this.buyingAssetIssuer + "&limit=50";

    if (this.data.source2 != undefined ) {
      this.data.source2.close();
    }
    if (this.token.length >= "null" || this.token.length >= 0) {
      this.data.source2 = new EventSource(url);
      this.urlAsk = url;
      var result: any = new Object();
      this.data.source2.onmessage = (event: MessageEvent) => {
        result = event.data;
        result = JSON.parse(event.data);
        var response = JSON.parse(result.response);
        var askdata = [];
        this.askdata = response.asks;
        var biddata = [];
        this.biddata = response.bids;
        var bidHtml = "";
        var askHtml = "";
        if (this.askdata.length != 0 ) {
          var randomNoForFlashArr1 = [];
          var randomNo = this.randomNoForOrderBook(0,this.askdata.length);
          randomNoForFlashArr1.push(randomNo);
          for (var i=0; i<=10; i++) {
          if (!$.inArray(i,randomNoForFlashArr1)) {
            var pickclass1='.'+i+'newclass';

            $(pickclass1).addClass('bg-flash-red');

            }

           }
        } else {

        }
       // this.askBody = askHtml;
      //  $("#orderbookAskBody").html(this.askBody);
        if (this.biddata.length != 0 ) {
          if (biddata.length > 5) { //changed by sanu
            var bidLength = biddata.length;
          } else {
            var bidLength = biddata.length;
          }
          var randomNoForFlashArr = [];
          var randomNo = this.randomNoForOrderBook(0,this.biddata.length);
          randomNoForFlashArr.push(randomNo);
         // alert('kk'+randomNo);
          if (!$.inArray(0, randomNoForFlashArr)) {

            var pickclass='.'+randomNo+'newclassask';
         // alert('pp'+pickclass);
           $(pickclass).addClass('bg-flash-red');

            }
        for (var i=0; i<=10; i++) { //changed by sanu
              if (!$.inArray(i, randomNoForFlashArr)) {

             var pickclass='.'+i+'newclass';
            //alert('pp'+pickclass);
            $(pickclass).addClass('bg-flash-red');

             }
            // if (parseFloat(biddata[i].amount).toFixed(4) != "0.0000") {
            //   bidHtml += '<tr class="' + className + '">';

            //   bidHtml +=
            //     '<td class="text-left">' +
            //     parseFloat((biddata[i].amount/biddata[i].price).toFixed(4)) +
            //     "</td>";

            //   if (this.data.selectedSellingCryptoCurrencyName == "usd") {
            //     bidHtml +=
            //       '<td class="text-green text-right">' +
            //       parseFloat(biddata[i].price).toFixed(4) +
            //       "</td>";
            //   } else {
            //     bidHtml +=
            //       '<td class="text-green text-right">' +
            //       parseFloat(biddata[i].price).toFixed(8) +
            //       "</td>";
            //   }
            //   bidHtml +=
            //     '<td class="text-right">' +
            //     ((biddata[i].amount / biddata[i].price)*(biddata[i].price)).toFixed(4) +
            //     "</td>";

            //     bidHtml += "</tr>";
            // } else {
            // }
           }
        } else {
          // bidHtml += "<tr>";
          // if(this.translate.currentLang =='English'){
          //   bidHtml += '<td class="text-white" colspan="2">No Data</td>';
          // }
          // else{
          //   bidHtml += '<td class="text-white" colspan="2">veri yok</td>';
          // }

          // bidHtml += "</tr>";
        }
        // this.bidBody = bidHtml;
        // $("#orderbookBidBody").html(this.bidBody);

      };
    } else {
      this.data.source2.close();
    }
  }

  ngOnDestroy(){
    if (this.data.source2 != undefined && this.source != undefined) {
      this.data.source2.close();
      this.source.close();
    }
  }
}