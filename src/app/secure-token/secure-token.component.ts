import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-secure-token",
  templateUrl: "./secure-token.component.html",
  styleUrls: ["./secure-token.component.css"]
})
export class SecureTokenComponent implements OnInit {
  signupObj: any;
  email: any;
  getlang: any;

  constructor(
    private http: HttpClient,
    private data: CoreDataService,
    private route: Router,
    public translate: TranslateService
  ) {
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }

  ngOnInit() {}

  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }

  secureToken(isValid) {
    if (isValid) {
      var tokenObj = {};
      tokenObj["email"] = this.email;
      var jsonString = JSON.stringify(tokenObj);
      // wip(1);
      //login webservice
      this.http
        .post<any>(this.data.WEBSERVICE + "/user/ForgotPassword", jsonString, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .subscribe(
          response => {
            // wip(0);
            console.log(response);

            var result = response;
            if (result.error.error_data != "0") {
              this.data.alert(result.error.error_msg, "danger");
            } else {
              this.data.alert(
                "Secure Token sent to registered email",
                "success"
              );
              this.route.navigateByUrl("/forget-password");
            }
          },
          function(reason) {
            // wip(0);
            this.data.alert("Internal Server Error", "danger");
          }
        );
    } else {
      this.data.alert("Please provide valid email", "warning");
    }
  }
}
