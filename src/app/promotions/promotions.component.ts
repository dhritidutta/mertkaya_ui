import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-promotions",
  templateUrl: "./promotions.component.html",
  styleUrls: ["./promotions.component.css"]
})
export class PromotionsComponent implements OnInit {
  getlang: any;

  constructor(public translate: TranslateService) {
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }

  ngOnInit() {}
  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }
}
