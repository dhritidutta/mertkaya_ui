import { Component } from '@angular/core';
import { CoreDataService } from './core-data.service';
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { TradesComponent } from './trades/trades.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  getlang:any;
  constructor(private modalService: NgbModal, private translate:TranslateService, private data:CoreDataService, private http:HttpClient, private route:Router, private trade:TradesComponent){
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }

    this.route.events.subscribe(()=>{
      if(this.route.url!='/dashboard'){
          clearInterval(this.trade.tradeInterval);
      }
  });

  // this.data.idleLogout();

  }

  Selectedlangulage(lang) {
    localStorage.setItem('selectedlang', lang);
  }

  warn(warn) {
    this.modalService.open(warn);
  }

}
