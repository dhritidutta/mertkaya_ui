import { Component, OnInit } from "@angular/core";
import { BodyService } from "../body.service";
import { HttpClient } from "@angular/common/http";
import * as $ from "jquery";
import { CoreDataService } from "../core-data.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-profile-details",
  templateUrl: "./profile-details.component.html",
  styleUrls: ["./profile-details.component.css"]
})
export class ProfileDetailsComponent implements OnInit {
  editName: any;
  editEmail: any;
  editOtp: any;
  getlang: any;

  constructor(
    public main: BodyService,
    public translate: TranslateService,
    private http: HttpClient,
    private data: CoreDataService,
    private modalService: NgbModal
  ) {
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }

  ngOnInit() {
    this.main.getDashBoardInfo();
    this.main.getUserDetails();
    this.main.getUserTransaction();
  }

  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }

  changeProfilePic() {
    var fd = new FormData();
    fd.append("userId", localStorage.getItem("user_id"));
    fd.append("profile_pic", $(".change_profile_pic")[0].files[0]);
    // wip(1);
    this.http
      .post<any>(this.data.WEBSERVICE + "/user/UpdateUserDocs", fd, {
        headers: {
          "Content-Type": undefined,
          authorization: "BEARER " + localStorage.getItem("access_token")
        }
      })
      .subscribe(
        response => {},
        function(reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.alert("Could Not Connect To Server", "danger");
          }
        }
      );
  }

  resendOtpForOutgoing(content) {
    this.modalService.open(content, { centered: true });
    var otpObj = {};
    otpObj["email"] = localStorage.getItem("email");
    var jsonString = JSON.stringify(otpObj);
    this.http
      .post<any>(this.data.WEBSERVICE + "/user/ResendOTP", jsonString, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .subscribe(
        response => {
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            $(".send_btn").show();
            $(".get_Otp_btn").hide();
            this.editName = this.main.fullName;
            this.editEmail = this.main.email;
          }
        },
        function(reason) {
          // wip(0);
          this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  updateUserProfileInfo() {
    var userInfoObj = {};
    if (this.editName != undefined) {
      userInfoObj["full_name"] = this.editName;
    } else {
      userInfoObj["full_name"] = "";
    }
    if (this.editEmail != undefined) {
      userInfoObj["email"] = this.editEmail;
    } else {
      userInfoObj["email"] = "";
    }
    userInfoObj["otp"] = this.editOtp;
    userInfoObj["userId"] = localStorage.getItem("user_id");

    var jsonString = JSON.stringify(userInfoObj);

    //  wip(1);
    this.http
      .post<any>(this.data.WEBSERVICE + "/user/UpdateUserDetails", jsonString, {
        headers: {
          "Content-Type": "application/json",
          authorization: "BEARER " + localStorage.getItem("access_token")
        }
      })
      .subscribe(
        response => {
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            $("#editProfileModal").modal("hide");

            this.data.alert("Profile Update Successfully", "success");
            localStorage.setItem("user_name", this.editName);
            localStorage.setItem("email", this.editEmail);
            this.main.getUserDetails();
          }
        },
        function(reason) {
          //  wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.alert("Could Not Connect To Server", "danger");
          }
        }
      );
  }
}
