import { Component, OnInit } from "@angular/core";
import { BodyService } from "../body.service";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import * as $ from "jquery";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { TradesComponent } from "../trades/trades.component";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-identity-verification",
  templateUrl: "./identity-verification.component.html",
  styleUrls: ["./identity-verification.component.css"]
})

export class IdentityVerificationComponent implements OnInit {
  getlang:any;

  constructor(
    public main: BodyService,
    private http: HttpClient,
    public data: CoreDataService,
    private modalService: NgbModal,
    private route: Router,
    private trade: TradesComponent, public translate: TranslateService
  ) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  ngOnInit() {
    this.main.getDashBoardInfo();
    this.getIdentityDetails();
  }

  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  panCardPic: any;
  aadharCardFront: any;
  aadharCardBack: any;

  getIdentityDetails() {
    this.data.alert("Loading...", "dark");
    var identityObj = {};
    identityObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(identityObj);
    // wip(1);
    this.http
      .post<any>(this.data.WEBSERVICE + "/user/GetUserDetails", jsonString, {
        headers: {
          "Content-Type": "application/json",
          authorization: "BEARER " + localStorage.getItem("access_token")
        }
      })
      .subscribe(
        response => {
          // wip(0);
          var result = response;
          if (result.error.error_data) {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            if (result.userListResult[0].profilePic != "") {
              this.main.profilePic =
                this.data.WEBSERVICE +
                "/user/" +
                localStorage.getItem("user_id") +
                "/file/" +
                result.userListResult[0].profilePic +
                "?access_token=" +
                localStorage.getItem("access_token");
            } else {
              this.main.profilePic = "./assets/img/default.png";
            }
            if (result.userListResult[0].idProofDoc != "") {
              this.panCardPic =
                this.data.WEBSERVICE +
                "/user/" +
                localStorage.getItem("user_id") +
                "/file/" +
                result.userListResult[0].idProofDoc +
                "?access_token=" +
                localStorage.getItem("access_token");
            } else {
              this.panCardPic = "./assets/img/file-empty-icon.png";
            }
            if (result.userListResult[0].addressProofDoc != "") {
              this.aadharCardFront =
                this.data.WEBSERVICE +
                "/user/" +
                localStorage.getItem("user_id") +
                "/file/" +
                result.userListResult[0].addressProofDoc +
                "?access_token=" +
                localStorage.getItem("access_token");
            } else {
              this.aadharCardFront = "./assets/img/file-empty-icon.png";
            }
            if (result.userListResult[0].addressProofDoc2 != "") {
              this.aadharCardBack =
                this.data.WEBSERVICE +
                "/user/" +
                localStorage.getItem("user_id") +
                "/file/" +
                result.userListResult[0].addressProofDoc2 +
                "?access_token=" +
                localStorage.getItem("access_token");
            } else {
              this.aadharCardBack = "./assets/img/file-empty-icon.png";
            }
          }
        },
        function(reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.logout();
            this.data.alert("Could Not Connect To Server", "danger");
          }
        },
        () => {
          this.data.loader = false;
        }
      );
  }

  upload(content) {
    if (
      $(".profile_pic")[0].files[0] != undefined ||
      $(".pan_card_pic")[0].files[0] != undefined ||
      $(".aadhar_card_front_side")[0].files[0] != undefined ||
      $(".aadhar_card_back_side")[0].files[0] != undefined
    ) {
      this.modalService.open(content);
    } else {
      this.data.alert("Please submit a detail to update", "warning");
    }
  }

  uploadDocs() {
    this.data.alert("Loading...", "dark");
    var fd = new FormData();
    fd.append("userId", localStorage.getItem("user_id"));
    fd.append("ssn", $(".pan_card").val());
    if ($(".profile_pic")[0].files[0] != undefined) {
      fd.append("profile_pic", $(".profile_pic")[0].files[0]);
    } else {
      fd.append("profile_pic", "");
    }
    if ($(".pan_card_pic")[0].files[0] != undefined) {
      fd.append("id_proof_doc", $(".pan_card_pic")[0].files[0]);
    } else {
      fd.append("id_proof_doc", "");
    }
    if ($(".aadhar_card_front_side")[0].files[0] != undefined) {
      fd.append("address_proof_doc", $(".aadhar_card_front_side")[0].files[0]);
    } else {
      fd.append("address_proof_doc", "");
    }
    if ($(".aadhar_card_back_side")[0].files[0] != undefined) {
      fd.append("address_proof_doc_2", $(".aadhar_card_back_side")[0].files[0]);
    } else {
      fd.append("address_proof_doc_2", "");
    }

    this.http
      .post<any>(this.data.WEBSERVICE + "/user/UpdateUserDocs", fd, {
        headers: {
          Authorization: "BEARER " + localStorage.getItem("access_token")
        }
      })
      .subscribe(
        result => {
          this.data.loader = false;

          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "dark");
          } else {
            this.data.alert(
              "Identity Verification Documents Submitted",
              "success"
            );
            this.getIdentityDetails();
          }
        },
        error => {
          this.data.alert(error.error.error_description, "danger");
        }
      );
  }

  getSize(content) {
    var sz = $('#' + content)[0].files[0];
    if (sz.type == "image/jpeg") {
      if (sz.size > 2000000) {
        this.data.alert('File size should be less than 2MB', 'warning');
        $('#' + content).val('');
      }
    }
    else{
        this.data.alert('File should be in JPG or JPEG. '+sz.type.split('/')[1].toUpperCase()+' is not allowed', 'warning');
        $('#' + content).val('');
    }
  }
}