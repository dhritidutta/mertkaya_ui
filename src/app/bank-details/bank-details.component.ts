import { Component, OnInit } from '@angular/core';
import { CoreDataService } from '../core-data.service';
import { BodyService } from '../body.service';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.css']
})
export class BankDetailsComponent implements OnInit {
    routing_no: any;
    getlang:any;

  constructor(private data:CoreDataService, public translate: TranslateService, public main:BodyService, private http:HttpClient, private modalService:NgbModal) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
   }

  ngOnInit() {
    this.main.getDashBoardInfo();
    this.getBankDetails();
  }
  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }
  accountNo;
  beneficiaryName;
  bankName;
  routingNo;
  beneficiary_name;
  bank_name;
  account_no;
  IFSC_code;
  bankDetailsForm;

    //get bank details
  getBankDetails(){
      this.data.alert('Loading...','dark');
        var bankDetailsObj={};
        bankDetailsObj['userId']=localStorage.getItem('user_id');
        var jsonString=JSON.stringify(bankDetailsObj);
        // wip(1);
        this.http.post<any>(this.data.WEBSERVICE+'/user/GetUserBankDetails',jsonString,{headers: {
              'Content-Type': 'application/json',
              'authorization': 'BEARER '+localStorage.getItem('access_token'),
            }})
        .subscribe(response=>{
            this.data.loader = false;
            // wip(0);
            var result=response;
            if(result.error.error_data!='0'){
                 alert(result.error.error_msg);
            }else{

                this.account_no=result.bankDetails.account_no;
                this.beneficiary_name=result.bankDetails.benificiary_name;
                this.bank_name=result.bankDetails.bank_name;
                this.routing_no=result.bankDetails.routing_no;
                this.accountNo=result.bankDetails.account_no;
                this.beneficiaryName=result.bankDetails.benificiary_name;
                this.bankName=result.bankDetails.bank_name;
                this.routingNo=result.bankDetails.routing_no;
                $('.update_bank_details_btn').hide();
            }
        },error=>{
            this.data.logout();
            this.data.alert(error.error.error_description, 'danger');
        });
    }

  checkBankInputfield(nameField){
      if(nameField=='beneficiary_name'){
          if(this.beneficiaryName!=this.beneficiary_name){
              $('.update_bank_details_btn').show();
          }else{
              $('.update_bank_details_btn').hide();
          }
      }
      if(nameField=='bank_name'){
          if(this.bankName!=this.bank_name){
              $('.update_bank_details_btn').show();
          }else{
              $('.update_bank_details_btn').hide();
          }
      }
      if(nameField=='account_no'){
          if(this.accountNo!=this.account_no){
              $('.update_bank_details_btn').show();
          }else{
              $('.update_bank_details_btn').hide();
          }
      }
      if(nameField=='routing_no'){
          if(this.routingNo!=this.IFSC_code){
              $('.update_bank_details_btn').show();
          }else{
              $('.update_bank_details_btn').hide();
          }
      }
  }

  updateBankDetails(content){
    if(this.beneficiary_name !='' && this.bank_name !='' && this.account_no  !='' && this.routing_no !=''){
      this.modalService.open(content,{centered:true});
    }
    else{
        this.data.alert('All fields are mandatory. Please fill up and submit again','warning');
    }
 }

 yesConfirm(){
    if(this.beneficiary_name !='' && this.bank_name !='' && this.account_no  !='' && this.routing_no !=''){
        var userId=parseInt(localStorage.getItem('user_id'));

        var updateBankObj={};
        updateBankObj['userId']=userId;
        updateBankObj['benificiary_name']=this.beneficiary_name;
        updateBankObj['bank_name']=this.bank_name;
        updateBankObj['account_no']=this.account_no;
        updateBankObj['routing_no']=this.routing_no;
        var jsonString=JSON.stringify(updateBankObj);
        // wip(1);
        this.http.post<any>(this.data.WEBSERVICE+'/user/UpdateUserBankDetails',jsonString,{headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER '+localStorage.getItem('access_token'),
            }})
       .subscribe(response=>{
        // wip(0);
        var result=response;
        if(result.error.error_data!='0'){
             this.data.alert(result.error.error_msg,'dark');
        }else{
            this.data.alert('Bank Details Updated','success');
            this.getBankDetails();
        }
        },error=>{
            this.data.logout();
            this.data.alert(error.error.error_description, 'danger');
        });
        }else{
        this.data.alert('You have not made any changes','warning');
        }
 }

}