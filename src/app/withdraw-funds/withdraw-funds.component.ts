import { Component, OnInit, DoCheck } from "@angular/core";
import { BodyService } from "../body.service";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import * as $ from "jquery";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-withdraw-funds",
  templateUrl: "./withdraw-funds.component.html",
  styleUrls: ["./withdraw-funds.component.css"]
})
export class WithdrawFundsComponent implements OnInit {
  public currencyBalance: any;
  public usdbalance: any;
  getlang: any;
  constructor(
    public main: BodyService,
    private http: HttpClient,
    public data: CoreDataService,
    public _StopLossComponent: StopLossComponent,
    public translate: TranslateService
  ) {
    this.main.getUserTransaction();
    this.main.getDashBoardInfo();
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }
  environmentSettingListObj;
  withdrawMaxValue;
  withdrawMinValue;
  withdrawDisclaimer;
  withdrawTxnChargeDisclaimer;
  currencyId: any;

  ngOnInit() {
    this.getBankDetails();
    this.appsettingscall();
    this.currencyBalance = this.main.balencelist;
    this.usdbalance = localStorage.getItem("usdbalance");
    if (this.currencyBalance != null) {
      for (var i = 0; i < this.currencyBalance.length; i++) {
        if (this.currencyBalance[i].currencyCode == "TRY") {
          this.usdbalance = this.currencyBalance[i].closingBalance;
          this.currencyId = this.currencyBalance[i].currencyId;
        }
      }
    }
  }

  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }

  appsettingscall() {
    var infoObj = {};
    infoObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(infoObj);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/user/GetUserAppSettings",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          var result = response;
          if (result.error.error_data != "0") {
          } else {
            var storeDashboardInfo = JSON.stringify(result);
            var environmentSettingsListObj: any = {};
            localStorage.setItem(
              "user_app_settings_list",
              JSON.stringify(result.userAppSettingsResult)
            );
            for (var i = 0; i < result.settingsList.length; i++) {
              environmentSettingsListObj[
                "" +
                  result.settingsList[i].name +
                  result.settingsList[i].currencyId +
                  ""
              ] = result.settingsList[i];
            }
            environmentSettingsListObj = JSON.stringify(
              environmentSettingsListObj
            );
            localStorage.setItem(
              "environment_settings_list",
              environmentSettingsListObj
            );
            this.environmentSettingListObj = JSON.parse(
              localStorage.getItem("environment_settings_list")
            );
            if (
              this.environmentSettingListObj["withdrawal_min_value1"]
                .currencyId == "1"
            ) {
              this.withdrawDisclaimer = this.environmentSettingListObj[
                "withdrawal_min_value1"
              ].description;
              this.withdrawTxnChargeDisclaimer = this.environmentSettingListObj[
                "withdrawal_txn_charges1"
              ].description;
              this.withdrawMaxValue = this.environmentSettingListObj[
                "withdrawal_max_value1"
              ].value;
              this.withdrawMinValue = this.environmentSettingListObj[
                "withdrawal_min_value1"
              ].value;
            }
          }
        },
        reason => {
          //   wip(0);
          this.data.logout();
          if (reason.error.error == "invalid_token") {
            this.data.alert("Session Timeout. Login Again", "warning");
          } else this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  accountNo;
  beneficiaryName;
  bankName;
  routingNo;
  bankAddress;
  lockOutgoingTransactionStatus;
  withdrawOtp;

  getBankDetails() {
    var bankDetailsObj = {};
    bankDetailsObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(bankDetailsObj);
    // wip(1);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/user/GetUserBankDetails",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            this.accountNo = result.bankDetails.account_no;
            this.beneficiaryName = result.bankDetails.benificiary_name;
            this.bankName = result.bankDetails.bank_name;
            this.routingNo = result.bankDetails.routing_no;
            this.bankAddress = result.bankDetails.address;
          }
        },
        function(reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.logout();
            this.data.alert("Could Not Connect To Server", "danger");
          }
        }
      );
  }

  getUserbalance() {
    this.usdbalance = localStorage.getItem("usdbalance");
  }

  withdraw_fund_amount;
  currencyID = 1;
  withdrawFund() {
    if (
      this.main.userDocVerificationStatus() &&
      this.main.userBankVerificationStatus()
    ) {
      if (this.withdraw_fund_amount != undefined) {
        if (
          parseFloat(this.withdraw_fund_amount) >=
            parseFloat(this.withdrawMinValue) &&
          parseFloat(this.withdraw_fund_amount) <=
            parseFloat(this.withdrawMaxValue)
        ) {
          var withdawObj = {};
          withdawObj["userId"] = localStorage.getItem("user_id");
          withdawObj["currencyId"] = this.currencyID.toString();
          withdawObj["sendAmount"] = this.withdraw_fund_amount.toFixed(2);
          withdawObj["bankName"] = this.bankName;
          withdawObj["ifscCode"] = this.routingNo;
          withdawObj["beneficiaryName"] = this.beneficiaryName;
          if (this.lockOutgoingTransactionStatus == 1) {
            withdawObj["otp"] = this.withdrawOtp;
          }
          var jsonString = JSON.stringify(withdawObj);
          // wip(1);
          this.http
            .post<any>(
              this.data.WEBSERVICE + "/transaction/createWithdrawalOrder",
              jsonString,
              {
                headers: {
                  "Content-Type": "application/json",
                  authorization:
                    "BEARER " + localStorage.getItem("access_token")
                }
              }
            )
            .subscribe(
              response => {
                // wip(0);
                var result = response;
                if (result.error.error_data != "0") {
                  this.data.alert(result.error.error_msg, "dark");
                } else {
                  this.withdraw_fund_amount = "";
                  this.data.alert("Withdrawal Done Successfully", "success");
                  this.usdbalance = "";
                  this.main.getUserTransaction();
                  this.getUserbalance();
                }
              },
              function(reason) {
                // wip(0);
                if (reason.data.error == "invalid_token") {
                  this.logout();
                } else {
                  alert("Could Not Connect To Server");
                }
              }
            );
        } else {
          this.data.alert("Please Provide Proper Amount", "danger");
        }
      } else {
        this.data.alert("Please Provide Proper Amount", "warning");
      }
    } else {
      $("#wfn").attr("disabled", true);
      this.data.alert("Your Bank Details are being verified", "warning");
    }
  }

  ngDoCheck() {
    this.usdbalance = localStorage.getItem("usdbalance");
  }

  determineLockStatusForWithdraw() {
    var userAppSettingsObj = JSON.parse(
      localStorage.getItem("user_app_settings_list")
    );
    this.lockOutgoingTransactionStatus =
      userAppSettingsObj.lock_outgoing_transactions;
    if (this.lockOutgoingTransactionStatus == 1) {
      this.data.alert("You are not eligble to withdraw", "danger");
    } else {
      this.withdrawFund();
    }
  }

  resendOtpForOutgoing() {
    var otpObj = {};
    otpObj["email"] = localStorage.getItem("email");
    var jsonString = JSON.stringify(otpObj);
    this.http
      .post<any>(this.data.WEBSERVICE + "/user/ResendOTP", jsonString, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .subscribe(
        response => {
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            $(".send_btn").show();
            $(".get_Otp_btn").hide();
          }
        },
        reason => {
          this.data.logout();
          //   wip(0);
          this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }
}
