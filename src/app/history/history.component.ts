import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { BodyService } from '../body.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  getlang:any;

  constructor(public main:BodyService , public translate:TranslateService,) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
   }

  noOfItemPerPage;
  timeSpan;
  collection;
  page=1;

  ngOnInit() {
    this.main.getDashBoardInfo();
    this.main.transactionHistory(1);
    this.collection = this.main.noOfItemPerPage;
    $('.historyTableBody').html(this.main.historyTableTr);

  }
  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  pager(pg){

    this.page = pg;
    this.main.transactionHistory(pg);
  }

  pagerNext(pg){

    pg++;
    this.page = pg;
    this.main.transactionHistory(pg);
  }

  pagerPre(pg){
    pg--;
    this.page = pg;
    this.main.transactionHistory(pg);
  }

  getDuration(duration){
      this.main.timeSpan=duration;
      this.main.transactionHistory('1');
      if(duration=='all'){
          $('.filter-button').removeClass('btn_active');
          $('.all_btn').addClass('btn_active');
      }
       if(duration=='last week'){
           $('.filter-button').removeClass('btn_active');
          $('.last_week_btn').addClass('btn_active');
      }
      if(duration=='last month'){
          $('.filter-button').removeClass('btn_active');
          $('.last_month_btn').addClass('btn_active');
      }
  }
}