import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { CoreDataService } from "../core-data.service";
import { HttpClient } from "@angular/common/http";
import * as $ from "jquery";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, interval } from "rxjs";
import { NavbarComponent } from "../navbar/navbar.component";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: "app-trades",
  templateUrl: "./trades.component.html",
  styleUrls: ["./trades.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TradesComponent implements OnInit {
  url: any;
  currency: any;
  selectedSellingAssetText: any;
  selectedBuyingAssetText: any;
  marketTradeBodyHtml: any;
  transactionType: any;
  myTradeTableDisplayStatus: any;
  CreditCurrencyAmount: any;
  CreditBaseAmount: any;
  selectedCurrency: any;
  indentificationStatus: any;
  bankDetailStatus: any;
  historyTableTr: any;
  myOfferRecordsHtml: Observable<any>;
  closeResult: string;
  modifyAmount: any;
  modifyPrice: any;
  modifyVolume: any;
  manageOfferId: any;
  modifyType: any;
  delOfferId: any;
  delTxnType: any;
  delPrice: any;
  loader: boolean;
  dateString: string;
  dateStringArr: any;
  stopLossTableDisplayStatus: number;
  selldata: any = 0;
  flag: string;
  modifySlAmount: number;
  modifySlPrice: number;
  modifySlTrigger: number;
  marketOrderPrice: number;
  stopLossSellingAsset: any;
  stopLossBuyingAsset: any;
  stopLossOfferId: any;
  stopLossQty: any;
  modifySlOffer: any;
  buydata: any = 0;

  tradeInterval: any;
  buyingAssetIssuer: any;
  sellingAssetIssuer: any;
  //malini
  marketTradeRecords: any;
  itemcount: any;
  source: any;
  getlang: any;
  //
  constructor(
    public data: CoreDataService,
    private http: HttpClient,
    private modalService: NgbModal,
    private nav: NavbarComponent,
    public dash: DashboardComponent,
    public translate: TranslateService,
    private _StopLossComponent: StopLossComponent
  ) {
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }

  ngOnInit() {
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    this.myTradeTableDisplayStatus = 0;
    this.getDashBoardInfo();
    this.GetTradeDetail();
    this.getOfferLists();
    this.loader = false;
    this.reload();
  }

  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }

  ngDoCheck() {
    this.changemode();
  }

  changemode() {
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".bg-black").css("background-color", "transparent");
      $(".btn").css("color", "#000");
      $(".text-blue").css("color", "black");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".bg-black").css("background-color", "transparent");
      $(".btn").css("color", "#fff");
      $(".text-blue").css("color", "white");
    }
  }

  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);
    var maxVal1: number = parseInt(maxVal);
    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }

  reload() {
    this.GetTradeDetail();
    this.getOfferLists();
  }

  getDashBoardInfo() {
    var infoObj = {};
    infoObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(infoObj);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/user/GetUserAppSettings",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            var storeDashboardInfo = JSON.stringify(result);
            var environmentSettingsListObj: any = {};
            localStorage.setItem(
              "user_app_settings_list",
              JSON.stringify(result.userAppSettingsResult)
            );
            for (var i = 0; i < result.settingsList.length; i++) {
              environmentSettingsListObj[
                "" + result.settingsList[i].name + ""
              ] = result.settingsList[i];
            }
            environmentSettingsListObj = JSON.stringify(
              environmentSettingsListObj
            );
            localStorage.setItem(
              "environment_settings_list",
              environmentSettingsListObj
            );
            this.data.environment_settings_list = environmentSettingsListObj;
            var environmentSettingListObj: any = JSON.parse(
              localStorage.getItem("environment_settings_list")
            );
            var buyDisclaimer =
              environmentSettingListObj["buy_max_value"][
                this.selectedCurrency + "_description"
              ];
            var buyTxnDisclaimer =
              environmentSettingListObj["buy_txn_charges"][
                this.selectedCurrency + "_description"
              ];
            var sellDisclaimer =
              environmentSettingListObj["sell_max_value"][
                this.selectedCurrency + "_description"
              ];
            var sellTxnDisclaimer =
              environmentSettingListObj["sell_txn_charges"][
                this.selectedCurrency + "_description"
              ];
            if (result.userAppSettingsResult.user_docs_status == "") {
              this.indentificationStatus =
                "Identity verification documents not submitted";
            }
            if (result.userAppSettingsResult.user_docs_status == "1") {
              this.indentificationStatus =
                "Identity verification documents verified";
            }
            if (result.userAppSettingsResult.user_docs_status == "0") {
              this.indentificationStatus =
                " Identity verification documents submitted for Verification";
            }
            if (result.userAppSettingsResult.user_docs_status == "2") {
              this.indentificationStatus =
                " Identity verification documents declined, please submit again";
            }
            if (result.userAppSettingsResult.bank_details_status == "") {
              this.bankDetailStatus = "Bank details not submitted";
            }
            if (result.userAppSettingsResult.bank_details_status == "0") {
              this.bankDetailStatus =
                "Bank details  submitted for Verification";
            }
            if (result.userAppSettingsResult.bank_details_status == "2") {
              this.bankDetailStatus = " Bank details verified";
            }
            if (result.userAppSettingsResult.bank_details_status == "3") {
              this.bankDetailStatus =
                " Bank documents declined, please submit again";
            }
            if (
              localStorage.getItem("check_id_verification_status") == "true" &&
              result.userAppSettingsResult.user_docs_status == ""
            ) {
              this.data.alert(
                "Please submit Identity verification documents to access all Paybito features",
                "warning"
              );
            }
            localStorage.setItem("check_id_verification_status", "false");
          }
        },
        function(reason) {
          //   wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.alert("Could Not Connect To Server", "danger");
          }
        }
      );
  }

  GetTradeDetail() {
    var tradeObj = {};
    tradeObj["userId"] = localStorage.getItem("user_id");
    tradeObj["page_no"] = 1;
    tradeObj["no_of_items_per_page"] = 10;
    tradeObj[
      "buying_asset_code"
    ] = this.data.selectedBuyingAssetText.toLocaleLowerCase();
    tradeObj[
      "selling_asset_code"
    ] = this.data.selectedSellingAssetText.toLocaleLowerCase();
    var jsonString = JSON.stringify(tradeObj);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/userTrade/GetTradeHistoryById",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          var result = response;
          if (result.error.error_data != 0) {
          } else {
            var tradeDetails = result.tradeListResult;
            this.historyTableTr = "";
            if (tradeDetails)
              if (tradeDetails.length > 0) {
                for (var i = 0; i < tradeDetails.length; i++) {
                  var action = tradeDetails[i].action;
                  var trnid = tradeDetails[i].trade_id;
                  var timestamp = tradeDetails[i].transactionTimeStamp;
                  var timestampArr = timestamp.split(".");
                  timestamp = this.data.readable_timestamp(timestampArr[0]);
                  this.historyTableTr +=
                    '<tr class="filter ' + action.toLowerCase() + '">';
                  this.historyTableTr += "<td>" + timestamp + "</td>";
                  this.historyTableTr +=
                    '<td class="">' +
                    tradeDetails[i].buying_asset_code.toUpperCase() +
                    "</td>";
                  this.historyTableTr +=
                    '<td class="">' +
                    tradeDetails[i].selling_asset_code.toUpperCase() +
                    "</td>";
                  this.historyTableTr +=
                    '<td class="">' +
                    parseFloat(tradeDetails[i].amount) +
                    "&nbsp;" +
                    tradeDetails[i].buying_asset_code.toUpperCase() +
                    "</td>";
                  this.historyTableTr +=
                    '<td class="">' +
                    parseFloat(tradeDetails[i].price).toFixed(6) +
                    "</td>";
                  var cls = action == "BUY" ? "skyblue" : "red";
                  this.historyTableTr +=
                    '<td class="text-' + cls + '">' + action + "</td>";
                  this.historyTableTr += "</tr>";
                }
              } else {
                this.historyTableTr +=
                  '<tr><td colspan="5" class="text-center">No Data Available</td></tr>';
              }
            $("#myTradeBody").html(this.historyTableTr);
            if (this.myTradeTableDisplayStatus == 0) {
              $("#myTradeBody")
                .children(".filter")
                .show();
            }
            if (this.myTradeTableDisplayStatus == 1) {
              $("#myTradeBody")
                .children(".filter")
                .show();
              $("#myTradeBody")
                .children("tr.sell")
                .hide();
            }
            if (this.myTradeTableDisplayStatus == 2) {
              $("#myTradeBody")
                .children(".filter")
                .show();
              $("#myTradeBody")
                .children("tr.buy")
                .hide();
            }
          }
        },
        reason => {
          if (reason.error.error == "invalid_token") {
            this.data.logout();
            this.data.alert("Session Timeout. Login Again", "warning");
          } else this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  myTradeDisplay(value) {
    this.myTradeTableDisplayStatus = value;
    this.GetTradeDetail();
  }

  getOfferLists() {
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText.toLowerCase();
    this.selectedSellingAssetText = this.data.selectedSellingAssetText.toLowerCase();
    var inputObj = {};
    var mnObj;
    inputObj["userId"] = localStorage.getItem("user_id");
    inputObj[
      "selling_asset_code"
    ] = this.data.selectedBuyingAssetText.toUpperCase();
    inputObj[
      "buying_asset_code"
    ] = this.data.selectedSellingAssetText.toUpperCase();
    var jsonString = JSON.stringify(inputObj);
    if (localStorage.getItem("user_id") != null)
      this.http
        .post<any>(
          this.data.WEBSERVICE + "/userTrade/GetOfferByAccountID",
          jsonString,
          { headers: { "Content-Type": "application/json" } }
        )
        .subscribe(
          response => {
            var result = response;
            if (result.error.error_data != "0") {
              this.data.alert(result.error.error_msg, "danger");
            } else {
              var result = response;
              var myOfferRecords = result.tradeListResult;
              mnObj = [];
              if (myOfferRecords)
                for (var i = 0; i < myOfferRecords.length; i++) {
                  if (
                    (myOfferRecords[i].buying_asset_code ==
                      this.selectedBuyingAssetText.toUpperCase() &&
                      myOfferRecords[i].selling_asset_code ==
                        this.selectedSellingAssetText.toUpperCase()) ||
                    (myOfferRecords[i].buying_asset_code ==
                      this.selectedSellingAssetText.toUpperCase() &&
                      myOfferRecords[i].selling_asset_code ==
                        this.selectedBuyingAssetText.toUpperCase())
                  ) {
                    if (myOfferRecords[i].txn_type == 1) {
                      var myOfferType = "Buy";
                    }
                    if (myOfferRecords[i].txn_type == 2) {
                      var myOfferType = "Sell";
                    }
                    var rsObj = {};
                    var timestamp = myOfferRecords[i].transactionTimeStamp;
                    var timestampArr = timestamp.split(".");
                    rsObj["time"] = this.data.readable_timestamp(
                      timestampArr[0]
                    );
                    rsObj["offerId"] = myOfferRecords[i].offer_id;
                    rsObj["amount"] = parseFloat(myOfferRecords[i].amount);
                    rsObj["buy"] = myOfferRecords[i].buying_asset_code;
                    if (this.selectedSellingAssetText == "usd") {
                      rsObj["price"] = parseFloat(myOfferRecords[i].price);
                      rsObj["volume"] = (
                        parseFloat(myOfferRecords[i].price) *
                        parseFloat(myOfferRecords[i].amount)
                      ).toFixed(8);
                    } else {
                      rsObj["price"] = parseFloat(myOfferRecords[i].price);
                      rsObj["volume"] = (
                        parseFloat(myOfferRecords[i].price) *
                        parseFloat(myOfferRecords[i].amount)
                      ).toFixed(8);
                    }
                    rsObj["type"] = myOfferType;
                    rsObj["txn_typ"] = myOfferRecords[i].txn_type;
                    mnObj.push(rsObj);
                  }
                }
              this.myOfferRecordsHtml = mnObj;
            }
          },
          reason => {
            if (reason.error.error == "invalid_token") {
              this.data.logout();
              this.data.alert("Session Timeout. Login Again", "warning");
            } else {
              this.data.logout();
              this.data.alert("Could Not Connect To Server", "danger");
            }
          }
        );
  }

  getOfferDetails(content, offerId) {
    var inputObj = {};
    this.data.alert("Loading...", "dark");
    inputObj["offer_id"] = offerId;
    inputObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(inputObj);
    this.http
      .post<any>(this.data.WEBSERVICE + "/userTrade/GetOfferByID", jsonString, {
        headers: { "Content-Type": "application/json" }
      })
      .subscribe(
        response => {
          this.data.loader = false;
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            this.manageOfferId = offerId;
            this.modifyAmount = parseFloat(result.tradeResult.amount);
            this.modifyPrice = parseFloat(result.tradeResult.price);
            this.modifyVolume =
              parseFloat(this.modifyAmount) * parseFloat(this.modifyPrice);
            this.modifyType = result.tradeResult.txn_type;
            this.modalService.open(content, { centered: true });
          }
        },
        reason => {
          if (reason.error.error == "invalid_token") {
            this.data.logout();
            this.data.alert("Session Timeout. Login Again", "warning");
          } else this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  manageOffer() {
    this.data.alert("Loading...", "dark");
    var inputObj = {};
    inputObj["offer_id"] = this.manageOfferId;
    inputObj["userId"] = localStorage.getItem("user_id");
    if (this.modifyType == 1) {
      inputObj["selling_asset_code"] = this.data.selectedSellingAssetText.toUpperCase();
      inputObj["buying_asset_code"] = this.data.selectedBuyingAssetText.toUpperCase();
    } else {
      inputObj["buying_asset_code"] = this.data.selectedSellingAssetText.toUpperCase();
      inputObj["selling_asset_code"] = this.data.selectedBuyingAssetText.toUpperCase();
    }
    inputObj["amount"] = this.modifyAmount;
    inputObj["price"] = this.modifyPrice;
    inputObj["txn_type"] = this.modifyType;

    var jsonString = JSON.stringify(inputObj);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/userTrade/TradeManageOffer",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          var result = response;
          this.data.loader = false;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            $(".reset").click();
            this.data.alert(result.error.error_msg, "success");
            this.GetTradeDetail();
            this.getOfferLists();

            $("#trade").click();
          }
        },
        reason => {
          if (reason.error.error == "invalid_token") {
            this.data.logout();
            this.data.alert("Session Timeout. Login Again", "warning");
          } else this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  deleteTrade(content, offerId, txnType, price) {
    this.delOfferId = offerId;
    this.delTxnType = txnType;
    this.delPrice = price;
    this.modalService.open(content, { centered: true });
  }

  delOffer() {
    this.data.alert("Loading...", "dark");
    $(".load").fadeIn();
    var inputObj = {};
    inputObj["offer_id"] = this.delOfferId;
    inputObj["userId"] = localStorage.getItem("user_id");
    if (this.delTxnType == 2) {
      inputObj[
        "selling_asset_code"
      ] = this.selectedBuyingAssetText.toUpperCase();
      inputObj[
        "buying_asset_code"
      ] = this.selectedSellingAssetText.toUpperCase();
    }
    if (this.delTxnType == 1) {
      inputObj[
        "selling_asset_code"
      ] = this.selectedSellingAssetText.toUpperCase();
      inputObj[
        "buying_asset_code"
      ] = this.selectedBuyingAssetText.toUpperCase();
    }
    inputObj["amount"] = "0";
    inputObj["txn_type"] = this.delTxnType;
    inputObj["price"] = this.delPrice;
    var jsonString = JSON.stringify(inputObj);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/userTrade/TradeManageOffer",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          this.data.loader = false;
          $(".load").fadeOut();
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "warning");
          } else {
            this.GetTradeDetail();
            this._StopLossComponent.getUserTransaction();
            $(".reset").click();
            this.data.alert(result.error.error_msg, "success");
            $("#trade").click();
          }
        },
        reason => {
          if (reason.error.error == "invalid_token") {
            this.data.logout();
            this.data.alert("Session Timeout. Login Again", "warning");
          } else this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  getStopLossOfferForSell() {
    var inputObj = {};
    inputObj["userId"] = localStorage.getItem("user_id");
    inputObj["selling_asset_code"] = (localStorage.getItem(
      "buying_crypto_asset"
    )
      ? localStorage.getItem("buying_crypto_asset")
      : "btc"
    ).toUpperCase();
    inputObj["buying_asset_code"] = (localStorage.getItem(
      "selling_crypto_asset"
    )
      ? localStorage.getItem("selling_crypto_asset")
      : "usd"
    ).toUpperCase();
    inputObj["txn_type"] = "2";
    var jsonString = JSON.stringify(inputObj);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/userTrade/GetStopLossBuySell",
        jsonString,
        { headers: { "Content-Type": "application/json" } }
      )
      .subscribe(
        response => {
          var result = response;
          var apiResponse: any = JSON.parse(result.apiResponse);
          this.selldata = apiResponse ? apiResponse.response : "";
        },
        function(reason) {
          this.data.alert(reason, "warning");
        }
      );
  }

  getStopLossOfferForBuy() {
    var inputObj = {};
    inputObj["userId"] = localStorage.getItem("user_id");
    inputObj["buying_asset_code"] = (localStorage.getItem("buying_crypto_asset")
      ? localStorage.getItem("buying_crypto_asset")
      : "btc"
    ).toUpperCase();
    inputObj["selling_asset_code"] = (localStorage.getItem(
      "selling_crypto_asset"
    )
      ? localStorage.getItem("selling_crypto_asset")
      : "usd"
    ).toUpperCase();
    inputObj["txn_type"] = "1";
    var jsonString = JSON.stringify(inputObj);
    var us = localStorage.getItem("user_id");
    if (us != null)
      this.http
        .post<any>(
          this.data.WEBSERVICE + "/userTrade/GetStopLossBuySell",
          jsonString,
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .subscribe(
          response => {
            var result = response;
            var apiResponse = JSON.parse(result.apiResponse);
            this.buydata = apiResponse ? apiResponse.response : "";
          },
          function(reason) {
            this.data.alert(reason, "warning");
          }
        );
  }

  modifyStoploss(content, id, q, p, t, flag) {
    this.modalService.open(content, { centered: true });
    this.modifySlAmount = q;
    this.modifySlPrice = p;
    this.modifySlTrigger = t;
    this.modifySlOffer = id;
    this.flag = flag;
  }

  manageStoploss() {
    this.data.alert("Loading...", "dark");
    if (this.flag == "Buy") {
      var marketOrderUrl =
        this.data.TRADESERVICE +
        "/getAmountBuy/" +
        localStorage.getItem("selling_crypto_asset").toUpperCase() +
        localStorage.getItem("buying_crypto_asset").toUpperCase() +
        "/" +
        localStorage.getItem("buying_crypto_asset").toUpperCase() +
        localStorage.getItem("selling_crypto_asset").toUpperCase() +
        "/" +
        this.modifySlAmount;
    } else {
      var marketOrderUrl =
        this.data.TRADESERVICE +
        "/getAmountSell/" +
        localStorage.getItem("buying_crypto_asset").toUpperCase() +
        localStorage.getItem("selling_crypto_asset").toUpperCase() +
        "/" +
        localStorage.getItem("selling_crypto_asset").toUpperCase() +
        localStorage.getItem("buying_crypto_asset").toUpperCase() +
        "/" +
        this.modifySlAmount;
    }

    this.http.get<any>(marketOrderUrl).subscribe(data => {
      var result = data;
      if (result.code == "0") {
        this.marketOrderPrice = parseFloat(result.price);
        if (this.flag == "Buy") {
          if (
            this.marketOrderPrice < this.modifySlTrigger &&
            this.marketOrderPrice < this.modifySlPrice &&
            this.modifySlTrigger < this.modifySlPrice
          ) {
            var inputObj = {};
            inputObj["selling_asset_code"] = localStorage
              .getItem("selling_crypto_asset")
              .toUpperCase();
            inputObj["buying_asset_code"] = localStorage
              .getItem("buying_crypto_asset")
              .toUpperCase();
            inputObj["userId"] = localStorage.getItem("user_id");
            inputObj["modify_type"] = "edit";
            inputObj["stoploss_id"] = this.modifySlOffer;
            inputObj["stop_loss_price"] = this.modifySlPrice;
            inputObj["trigger_price"] = this.modifySlTrigger;
            inputObj["quantity"] = this.modifySlAmount;
            inputObj["txn_type"] = "1";
            var jsonString = JSON.stringify(inputObj);
            // wip(1);
            this.http
              .post<any>(
                this.data.WEBSERVICE + "/userTrade/ModifyStopLossBuySell",
                jsonString,
                { headers: { "Content-Type": "application/json" } }
              )
              .subscribe(data => {
                // wip(0);
                this.data.loader = false;
                var result = data;
                if (result.error.error_data != "0") {
                  this.data.alert(result.error.error_msg, "danger");
                } else {
                  this.data.alert(result.error.error_msg, "success");
                  this.getStopLossOfferForBuy();
                }
              });
          } else {
            this.data.alert(
              "*Market order price should be greater than trigger price & trigger price should be greater than stop loss price",
              "warning"
            );
          }
        } else {
          if (
            this.marketOrderPrice > this.modifySlTrigger &&
            this.marketOrderPrice > this.modifySlPrice &&
            this.modifySlTrigger > this.modifySlPrice
          ) {
            var inputObj = {};
            inputObj["selling_asset_code"] = localStorage
              .getItem("buying_crypto_asset")
              .toUpperCase();
            inputObj["buying_asset_code"] = localStorage
              .getItem("selling_crypto_asset")
              .toUpperCase();
            inputObj["userId"] = localStorage.getItem("user_id");
            inputObj["modify_type"] = "edit";
            inputObj["stoploss_id"] = this.modifySlOffer;
            inputObj["stop_loss_price"] = this.modifySlPrice;
            inputObj["trigger_price"] = this.modifySlTrigger;
            inputObj["quantity"] = this.modifySlAmount;
            inputObj["txn_type"] = "2";
            var jsonString = JSON.stringify(inputObj);
            // wip(1);
            this.http
              .post<any>(
                this.data.WEBSERVICE + "/userTrade/ModifyStopLossBuySell",
                jsonString,
                { headers: { "Content-Type": "application/json" } }
              )
              .subscribe(data => {
                // wip(0);
                this.data.loader = false;
                var result = data;
                if (result.error.error_data != "0") {
                  this.data.alert(result.error.error_msg, "danger");
                } else {
                  this.data.alert(result.error.error_msg, "success");
                  $("#trade").click();
                }
              });
          } else {
            this.data.alert(
              "*Market order price should be greater than trigger price & trigger price should be greater than stop loss price",
              "warning"
            );
          }
        }
      } else {
        this.data.alert("*Orderbook depth reached, price not found", "warning");
      }
    });
  }

  delStoploss(content, id, q, p, t, flag) {
    this.modalService.open(content, { centered: true });
    this.modifySlAmount = q;
    this.modifySlPrice = p;
    this.modifySlTrigger = t;
    this.modifySlOffer = id;
    this.flag = flag;
  }

  removeStoploss() {
    this.data.alert("Loading...", "dark");
    this.stopLossBuyingAsset = localStorage.getItem("buying_crypto_asset");
    this.stopLossSellingAsset = localStorage.getItem("selling_crypto_asset");
    var inputObj = {};
    inputObj["selling_asset_code"] = this.stopLossSellingAsset.toUpperCase();
    inputObj["buying_asset_code"] = this.stopLossBuyingAsset.toUpperCase();
    inputObj["userId"] = localStorage.getItem("user_id");
    inputObj["modify_type"] = "delete";
    inputObj["stoploss_id"] = this.modifySlOffer;
    inputObj["stop_loss_price"] = this.modifySlPrice;
    inputObj["trigger_price"] = this.modifySlTrigger;
    inputObj["quantity"] = this.modifySlAmount;
    if (this.flag == "buy") {
      inputObj["txn_type"] = "1";
    } else {
      inputObj["txn_type"] = "2";
    }

    var jsonString = JSON.stringify(inputObj);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/userTrade/ModifyStopLossBuySell",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .subscribe(data => {
        this.data.loader = false;
        var result = data;
        if (result.error.error_data != "0") {
          this.data.alert(result.error.error_msg, "danger");
        } else {
          this.data.alert(result.error.error_msg, "success");
          $("#trade").click();
        }
      });
  }
}
