import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userName: any;
  profilePic: any;
  closeResult: string;
  token: any = localStorage.getItem('access_token');
  mode: any
  modeMessage: any;
  alertType: any;
  alertMsg: any;
  currentRoute: any;
  reason: any;
  time: number = 10;
  isNavbarCollapsed = true;
  getlang: any;
  sideMenu: any;
  upperMenu: any;
  interval;
  userid: string;
  act: string;
  imageLink: string;

  constructor(public translate: TranslateService, private modalService: NgbModal, private route: Router, public data: CoreDataService, private http: HttpClient) {
    this.route.events.subscribe(val => {
      this.currentRoute = val;
    });
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  ngOnInit() {
    if (this.route.url != '/dashboard') {
    }
    if (this.getlang == 'English') {
      this.sideMenu = [
        { name: 'Dashboard', alias: 'dashboard' },
        { name: 'Profile Details', alias: 'profile-details' },
        { name: 'My Wallet', alias: 'my-wallet' },
        { name: 'History', alias: 'history' },
        { name: 'Report', alias: 'report' },
        { name: 'Identity Verification', alias: 'identity-verification' },
        { name: 'Bank Details', alias: 'bank-details' },
        { name: 'Promotions', alias: 'promotion' },
        { name: 'Settings', alias: 'settings' },
        { name: 'Support', alias: 'support' },
      ];
      this.upperMenu = [
        { name: 'Deposit Funds', alias: 'deposit-funds' },
        { name: 'Withdraw Funds', alias: 'withdraw-funds' },
      ]
    }
    else {
      this.sideMenu = [
        { name: 'Gösterge Paneli', alias: 'dashboard' },
        { name: 'Profil Detayları', alias: 'profile-details' },
        { name: 'Cüzdanım', alias: 'my-wallet' },
        { name: 'Geçmiş', alias: 'history' },
        { name: 'Raporlar', alias: 'report' },
        { name: 'Kimlik Doğrulama', alias: 'identity-verification' },
        { name: 'Banka Detayları', alias: 'bank-details' },
        { name: 'Promosyonlar', alias: 'promotion' },
        { name: 'Ayarlar', alias: 'settings' },
        { name: 'Destek', alias: 'support' },
      ];
      this.upperMenu = [
        { name: 'Para Yatırma', alias: 'deposit-funds' },
        { name: 'Para Çekme', alias: 'withdraw-funds' },
      ]
    }

    if (this.token == null) {
      this.route.navigateByUrl('/login');
    }
    this.userName = localStorage.getItem('user_name');
    this.userid = localStorage.getItem('user_id');
    this.act = localStorage.getItem('access_token');
    this.imageLink = (this.profilePic) ? this.data.WEBSERVICE + '/user/' + this.userid + '/file/' + this.profilePic + '?access_token=' + this.act : './assets/img/default.png'
    // this.data.idleLogout();

    // var str = document.getElementById("ngb-tooltip-2").innerHTML;
    // var res = str.replace("logout", "jkkk");
    // document.getElementById("ngb-tooltip-2").innerHTML = res;

    // var text = "Logout";
    //  var new_text = text.replace("Logout", "dont want");
    // document.write(new_text);
    $(".nav-item").click(function(){
        $(".tooltip-inner").replace("Logout","Çıkış");
    });

  }
  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  open(content) {
    this.modalService.open(content);
  }
}